import * as React from "react";
import { StyleSheet, TouchableOpacity, Text } from 'react-native';
import { fSize, sHeight } from "../utils/Responsive";
import { MEDIUM, SEMI_BOLD } from "../utils/typography";

const OutlineButton = ({
    title,
    borderColor,
    textColor,
    onPress
}) => {
    return(
        <TouchableOpacity 
            style={[
                styles.outlineButton,
                {borderColor:borderColor}
            ]}
            onPress={onPress}
        >
            <Text style={[styles.outlineButtonText,{color:textColor}]}>
                {title}
            </Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    outlineButton: {
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        height: 54,
        borderRadius: 15,
        borderWidth: 2,
    },
    outlineButtonText : {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17
    }
})

export default OutlineButton;