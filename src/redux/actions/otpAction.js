import AsyncStorage from '@react-native-async-storage/async-storage';

import authService from "../../service/authService";
import { ScreenNames } from '../../utils/ScreenNames'
import { navigate } from "../../root/navigation/rootNavigation";
import appService from '../../service/appService';
import { setUserResponse } from './accountAction';
import { setToAsyncStorage } from './authAction';

export function setOtp(otp) {
    return {
        type: 'SET_OTP',
        otp
    };
}

export function setResponse(response) {
    return {
        type: 'SET_OTP_RESPONSE',
        response
    };
}

export function setError(isError) {
    return {
        type: 'SET_OTP_ERROR',
        isError
    };
}
export function setEditOtpError(isEditOtpError) {
    return {
        type: 'SET_EDIT_OTP_ERROR',
        isEditOtpError
    };
}

export function setEditOtpResponse(editOtpResponse) {
    return {
        type: 'SET_EDIT_OTP_RESPONSE',
        editOtpResponse
    };
}

export function setEditOtp(editOtp) {
    return {
        type: 'SET_EDIT_OTP',
        editOtp
    };
}

export function resetState() {
    return {
        type: 'RESET_OTP_STATE'
    };
}

export function submitOtp(data) {
    return async (dispatch) => {
        return authService.post('fb/v1/auth/verify', data)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then(async (response) => {
                dispatch(setResponse(response))
                await setToAsyncStorage(response)
                if (response?.profile_completed) {
                    navigate(ScreenNames.START_RIDE)
                } else {
                    navigate(ScreenNames.ACCOUNT)
                }
            })
            .catch((error) => {
                console.log(error)
                dispatch(setError(true))
            });
    };
}

export function editPhoneOtp(data) {

    return async (dispatch) => {
        return appService.post('fb/v1/accounts/verify', data)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then(async (response) => {
                dispatch(setUserResponse(response))
                await setToAsyncStorage(response)
                navigate(ScreenNames.ACCOUNT_PROFILE)
            })
            .catch((error) => {
                console.log(error)
                dispatch(setEditOtpError(true))
            });
    };
}
