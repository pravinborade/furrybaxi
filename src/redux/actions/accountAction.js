import appService from '../../service/appService'
import { ScreenNames } from '../../utils/ScreenNames'
import { backNavigation, navigate } from "../../root/navigation/rootNavigation";
import { SubmissionError } from 'redux-form'
import { Alert } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setIsRegisteredToAsyncStorage } from './authAction';


export function setOtp(otp) {
    return {
        type: 'SET_OTP',
        otp
    };
}

export function setResponse(response) {
    return {
        type: 'SET_ACCOUNT_RESPONSE',
        response
    };
}

export function setError(isError) {
    return {
        type: 'SET_ACCOUNT_ERROR',
        isError
    };
}

export function setVerifyEmailModal(isVerifyEmailModal) {
    return {
        type: 'SET_VERIFY_MAIL_MODAL',
        isVerifyEmailModal
    };
}

export function setUserResponse(response) {
    return {
        type: 'SET_USER_RESPONSE',
        response
    };
}

export function setAccountPicture(pic) {
    return {
        type: 'SET_ACCOUNT_PICTURE',
        pic
    }
}
export function setAccountPictureError(isPicError) {
    return {
        type: 'SET_ACCOUNT_PICTURE_ERROR',
        isPicError
    };
}


export function createAccount(data) {
    return async (dispatch) => {
        return appService.post('fb/v1/accounts/user', data)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setResponse(data))
                setIsRegisteredToAsyncStorage(data)
                navigate(ScreenNames.PAYMENT)
            })
            .catch((error) => {
                if (data?.isFormLogin) {
                    throw new SubmissionError({ email: error.response.data.detail })
                } else {
                    error.response.data.detail &&
                        Alert.alert("Email has already been used - please chose a different account")
                }
            });
    };
}
export function updateAccount(data) {
    return async (dispatch) => {
        return appService.put('fb/v1/accounts/user', data)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setUserResponse(response))
                setIsRegisteredToAsyncStorage(response)

                if (!data.email) {
                    backNavigation()
                } else {
                    dispatch(setVerifyEmailModal(true))
                    setTimeout(() => {
                        dispatch(setVerifyEmailModal(false))
                        backNavigation()
                    }, 2000)
                }
                return response
            })
            .catch((error) => {
                if (data?.isFormLogin) {
                    throw new SubmissionError({ email: error.response.data.detail })
                } else {
                    error.response.data.detail &&
                        Alert.alert("Email has already been used - please chose a different account")
                }
            });
    };
}

export function uploadAccountPicture(data, token) {
    return async (dispatch) => {
        return appService.post(`fb/v1/accounts/upload`, data)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setUserResponse(response))
                navigate(ScreenNames.ACCOUNT_PROFILE)
            })
            .catch((error) => {
                console.log(JSON.stringify(error))
                dispatch(setAccountPictureError(true))
            });
    };
}

