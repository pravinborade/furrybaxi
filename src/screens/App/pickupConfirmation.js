import React, { useRef, useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Text, Dimensions, TouchableOpacity } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Geolocation from 'react-native-geolocation-service';

import Images from '../../assets/Images/Images';
import { useDispatch, useSelector } from 'react-redux';
import { MEDIUM, REGULAR } from '../../utils/typography';
import PrimaryButton from '../../component/PrimaryButton';
import PaymentButton from '../Auth/component/PaymentButton';
import { ScreenNames } from '../../utils/ScreenNames';
import { getLocationDetails } from '../../redux/actions/mapAction'
import { getSocketData, saveScheduleTrips, setScheduleTrips } from '../../redux/actions/ridingAction';
import { navigate } from '../../root/navigation/rootNavigation';
import googleMapStyle from '../../utils/googleMapStyle';
import { find_dimesions } from '../../utils/helper';
import { getCurrentLocation } from '../../redux/actions/homeAction';
import MyLocationButton from './component/MyLocationButton';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const PickupConfirmation = (props) => {
    let mapRef = useRef(null);
    const dispatch = useDispatch()
    const [dynamicHeight, setDynamicHeight] = useState(height)
    const [origin, setOrigin] = useState()
    const [region, setRegion] = useState({
        latitude: 51.509865,
        longitude: -0.11809,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    })
    const { 
        currentPlace,
        destinationPlace
    } = useSelector(state=> state.map)

    const { 
        selectedCab,
        scheduleDate
    } = useSelector(state=> state.riding)

    const {
        currentLocation,
    } = useSelector(state => state.home)

    useEffect(()=>{
        dispatch(getCurrentLocation())
    },[])

    useEffect(()=>{
        if(currentPlace && currentPlace !== null){
            setOrigin({
                latitude: currentPlace?.lat, 
                longitude: currentPlace?.long
            })
           
        }
    },[currentPlace])

    const formatPlaceName = (place) => {
        let str = '';
        if(place){
            str = place?.split(',',3)
            return(str?.join(','))
        }else{
            return str
        }
    }

    const onConfirmLocation = () => {
        if(currentPlace && selectedCab && !scheduleDate) {
            navigate(ScreenNames.CONFIRMING_RIDE)
        }else if(currentPlace && selectedCab && scheduleDate && destinationPlace) {
            let data = {
                scheduled_date_time: scheduleDate,
                from_location: [currentPlace?.lat, currentPlace?.long],
                to_location: [destinationPlace?.lat, destinationPlace?.long],
                id: selectedCab?.id
            }
            dispatch(saveScheduleTrips(data))
        }                      
    }

    useEffect(() => {
        if (currentLocation && currentLocation !== null) {
            mapRef.current.animateToRegion({
                ...currentLocation,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }, 1000);
        }
    }, [currentLocation])
    
    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE}
                style={{ ...StyleSheet.absoluteFillObject, height:dynamicHeight}}
                ref={mapRef}
                initialRegion={region}    
                onRegionChange={(e)=>{
                    setRegion(e)
                }}
                showsCompass={false}
                customMapStyle={googleMapStyle}
            >
                {
                    currentLocation
                        &&
                    <Marker
                        coordinate={currentLocation}
                    >
                        <Image source={Images.driverLocation} style={styles.driverLocation}/>
                    </Marker>
                }
                {
                    origin  &&
                    <MapView.Marker 
                        coordinate={origin} 
                        onDragEnd={(e) => {
                            let latLng = e?.nativeEvent?.coordinate?.latitude +','+ e?.nativeEvent?.coordinate?.longitude;
                            dispatch(getLocationDetails(latLng))
                        }}               
                        draggable
                        image={Images.marker_blak}
                    />
                }
            </MapView>
            <View style={styles.menuOverLay}>
                <TouchableOpacity onPress={()=>{props.navigation.navigate(ScreenNames.START_RIDE)}}>
                    <View style={styles.backImageContainer}>
                        <Image
                            source={Images.arrow_back_black}
                            style={styles.iconImages}
                        />
                    </View>
                </TouchableOpacity>
            </View>
            
            <View
            onLayout={(event) => setDynamicHeight(find_dimesions(height, event.nativeEvent.layout))}
                style={styles.actionContainer1}
            >   
                <MyLocationButton marginTop={-60} />
                <View style={styles.actionContainer1Section1}>
                    <View style={styles.actionContainer1Section1Row1}>
                        <Text style={styles.actionContainer1Section1Row1Text1}>
                            {"Pick up Location"}
                        </Text>
                        <Text style={styles.actionContainer1Section1Row1Text2}>
                            {"Move pin to adjust your pickup point"}
                        </Text>
                    </View>
                    <View style={styles.actionContainer1Section1Row2}>
                        <PaymentButton 
                            title={formatPlaceName(currentPlace?.name)}
                            onPress={()=>{}}
                            buttonStyle={styles.buttonStyle}
                            buttonTextStyle={styles.buttonTextStyle}
                        />
                    </View>
                    <View style={styles.actionContainer1Section1Row3}>
                        <PrimaryButton 
                            disable={false}
                            onPress={()=>{onConfirmLocation()}}
                            title={"Confirm Pickup"}
                        />
                    </View>
                </View>
            </View>               
        </View>

    )
}


const styles = StyleSheet.create({
    container: { 
        ...StyleSheet.absoluteFillObject,
        flex: 1,
    },
    driverLocation:{
        height:50,
        width:50
    },
    menuOverLay: {
        position: 'absolute',
        top: 20,
        minHeight: hp(10),
        right: 0,
        left: 20,
    },
    iconImages: {
        height: 30,
        width: 15,
    },
    backImageContainer: {
        height: 44,
        width: 44,
        borderRadius: 12,
        justifyContent:'flex-start',
        alignItems: 'flex-start'
    },
    actionContainer1: {
        backgroundColor: "#FDFFFF",
        position: 'absolute',
        //top: hp(70),
        bottom: 0,
        left: 0,
        right: 0,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
    },
    actionContainer1Section1: {
        // height: 'auto',
        backgroundColor: "#17202D",
        paddingHorizontal: 20,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        paddingBottom: 20
    },
    actionContainer1Section1Row1: {
        height: hp(10),
        justifyContent: 'center',
        alignItems:'center'
    },
    actionContainer1Section1Row1Text1: {
        fontFamily: MEDIUM,
        fontSize: 18,
        lineHeight: 22,
        paddingVertical:5,
        color: "#E1CDA9"
    },
    actionContainer1Section1Row1Text2: {
        fontFamily: REGULAR,
        fontSize: 14,
        lineHeight: 17,
        color: "#B59B7C"
    },
    buttonStyle: {
        backgroundColor: "#E2EAED",
        borderRadius: 15,
        height: 54,
        justifyContent:'center',
        alignItems: 'center',
        padding:8
    },
    buttonTextStyle: {
        fontFamily: MEDIUM,
        fontSize: 12,
        lineHeight: 15,
        color: "#4E535A",
    },
    actionContainer1Section1Row2: {
        //height: hp(10),
        justifyContent: 'center'
    },
    actionContainer1Section1Row3: {
        //height: hp(10),
        paddingVertical:10,
        justifyContent: 'center'
    },
    markerEdge: {
        top: 100,
        bottom: 100,
        left: 30,
        right: 30
    }
});

export default  memo(PickupConfirmation);
