import React from 'react';
import { StyleSheet, View, Modal, Dimensions, Text, TouchableOpacity } from 'react-native';
import { fSize } from '../../../../utils/Responsive';
import { REGULAR } from '../../../../utils/typography';

const screenHeight = Dimensions.get('screen').height
const screenWidth = Dimensions.get('screen').width

const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}
const getWidth = (wd) => {
    return screenWidth * (wd / 100)
}

const AmountModal = ({ charge, onOk, disabled, onClose, paymentSecretResponse }) => {
    return (
        <Modal
            transparent={true}
            visible={true}
        >
            <View style={styles.modalContainer}>
                <View style={styles.modalContent}>
                    <Text style={styles.contentTitle}>{`A charge of${paymentSecretResponse?.currency}  ${paymentSecretResponse?.charge}  will be deducted now to confirm your card. This charge will be refunded back after the transction is confirmed`}</Text>
                    <TouchableOpacity disabled={disabled} style={styles.submitButton} onPress={() => { onOk() }}>
                        <Text style={styles.submitButtonText}>
                            {"Okay"}
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.6)',
    },
    modalContent: {
        height: getHeight(30),
        width: getWidth(90),
        paddingVertical: 40,
        backgroundColor: '#253040',
        borderRadius: 20,
        alignItems: 'center',
        paddingHorizontal: 20
    },
    contentTitle: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 22,
        color: "#F5EDDC",
        textAlign: 'center'
    },
    submitButton: {
        marginTop: 40,
        width: getWidth(30),
        padding: 10,
        borderRadius: 20,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: "#9D7E62"
    },
    submitButtonText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 22,
        color: "#F5EDDC"
    }
})

export default AmountModal;