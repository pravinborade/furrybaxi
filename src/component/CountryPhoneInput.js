import React from 'react';
import{StyleSheet,Text,Image} from 'react-native'
import PhoneInput from 'react-native-phone-number-input';
import { MEDIUM,SEMI_BOLD } from '../utils/typography';
import Images from '../assets/Images/Images';
import { fSize, sHeight } from '../utils/Responsive';
const CountryPhoneInput = ({
    phoneInputRef,
    phoneNumber,
    country,
    onChangeNumber,
    onChangeCountry
  }) => {
    return (
        <PhoneInput
            ref={phoneInputRef}
            defaultValue={phoneNumber}
            placeholder={' '}
            defaultCode={country?.code}
            withDarkTheme={true}
            containerStyle={styles.phoneContainer}
            renderDropdownImage={
                <Image source={Images.arrow_down} />
            }
            textContainerStyle={
                phoneNumber?
                styles.phoneTextInputContainerDark:
                styles.phoneTextInputContainerLight
            }
            codeTextStyle={styles.phoneCountryCodeText}
            textInputStyle={styles.phoneTextInput}
            onChangeText={number => { onChangeNumber(number) }}
            onChangeCountry={(country) =>{ onChangeCountry(country)}}
        />
    );
  };

  const styles = StyleSheet.create({
    phoneContainer: {
        width: '100%',
        //height: sHeight(8),
        height:54,
        backgroundColor:'#253040',
        borderBottomLeftRadius: 15,
        borderTopLeftRadius: 15,
        borderBottomRightRadius: 15,
        borderTopRightRadius: 15,
    },
    phoneTextInputContainerLight: {
        paddingVertical: 0,
        borderBottomRightRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor:'#E2EAED'
    },
    phoneTextInputContainerDark: {
        paddingVertical: 0,
        borderBottomRightRadius: 15,
        borderTopRightRadius: 15,
        backgroundColor:'#253040'
    },
    phoneCountryCodeText:{
        color:'#A58B6C',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(16)
    },
    phoneTextInput:{
        color:'#E1CDA9',
        fontFamily:MEDIUM,
        fontSize: fSize(16)
    },
  })

  export default CountryPhoneInput;