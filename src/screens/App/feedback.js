import React, { memo, useEffect, useRef, useState } from "react";
import { Image, StyleSheet, View, TouchableOpacity, Text, Dimensions } from 'react-native';
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Rating, AirbnbRating } from 'react-native-ratings';

import Images from "../../assets/Images/Images";
import { fSize } from "../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../utils/typography";
import { useDispatch, useSelector } from "react-redux";
import MapView, { Marker, PROVIDER_GOOGLE } from "react-native-maps";
import moment from "moment";
import { giveFeedback } from "../../redux/actions/ridingAction";
import MapViewDirections from 'react-native-maps-directions';
import googleMapStyle from "../../utils/googleMapStyle"
import { GoogleCloudKey } from "../../config";

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


const Feedback = (props) => {
    const { navigation } = props;
    const mapRef = useRef(null);
    const dispatch = useDispatch();
    const [origin, setOrigin] = useState(
        {
            latitude: 18.5011567,
            longitude: 73.9242136,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        }

    )
    const [destination, setDestination] = useState(
        {
            latitude: 18.5011567,
            longitude: 73.9242136,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,

        })
    const [wayPoints, setWayPoints] = useState([])


    const [region, setRegion] = useState({
        latitude: 18.5011567,
        longitude: 73.9242136,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    })

    const {
        summaryResponse
    } = useSelector(state => state.riding)

    const {
        userResponse
    } = useSelector(state => state.auth)

    const getFormattedDate = (date) => {
        return moment(date).format("Do MMM");
    }

    const getFormattedTime = (date) => {
        return moment(date).format("LT");
    }

    useEffect(() => {
        // setRegion({
        //     ...region,
        //     latitude: summaryResponse?.data?.ride_summary?.to[0],
        //     longitude: summaryResponse?.data?.ride_summary?.to[1]
        // })
    }, [summaryResponse])

    useEffect(() => {
        if (summaryResponse && summaryResponse !== null) {
            let tmp = [];
            setOrigin({
                latitude: summaryResponse?.data?.ride_summary?.from[0],
                longitude: summaryResponse?.data?.ride_summary?.from[1]
            })
            setDestination({
                latitude: summaryResponse?.data?.ride_summary?.to[0],
                longitude: summaryResponse?.data?.ride_summary?.to[1]
            })
            summaryResponse?.data?.ride_summary?.waypoints?.map((way, ind) => {
                tmp.push({
                    latitude: way[0],
                    longitude: way[1]
                })
            })
            setWayPoints(tmp)
            mapRef.current.fitToCoordinates(tmp, {
                edgePadding: styles.markerEdge,
                animated: true
            })
        }
    }, [summaryResponse])

    const onClickFeedback = async (rating) => {
        console.log("@@@@rating", rating)
        let data = {
            id: summaryResponse?.data?.id,
            rating: rating
        }
        dispatch(giveFeedback(data))
    }
    
    return (
        <View style={styles.container}>
            <View style={styles.header}>
            </View>
            <View style={styles.content}>
                <View style={styles.contentSection1}>
                    <Text style={styles.contentSection1Text1}>
                        {userResponse?.data?.first_name},
                    </Text>
                    <Text style={styles.contentSection1Text2}>
                        {"Your ride has been successfully completed,\n Hope you had a good journey"}
                    </Text>
                </View>
                <View style={styles.contentSection2}>
                    <View style={styles.contentSection2Row1}>
                        <Text style={styles.contentSection2Row1Text1}>
                            {"Trip Details"}
                        </Text>
                        <Text style={styles.contentSection2Row1Text2}>
                            {getFormattedDate(summaryResponse?.data?.ride_summary?.start_date_time)}
                        </Text>
                    </View>
                    <View style={styles.contentSection2Row2}>
                        <View style={styles.contentSection2Row2Section1}>
                            <MapView
                                provider={PROVIDER_GOOGLE}
                                style={{ ...StyleSheet.absoluteFillObject }}
                                ref={mapRef}
                                initialRegion={region}                            
                                showsUserLocation={false}
                                showsCompass={false}
                                customMapStyle={googleMapStyle}
                            >
                                {
                                    origin &&
                                    <Marker coordinate={origin}>
                                        <Image
                                            source={Images.marker}
                                            style={{ width: 26, height: 36 }}
                                        />
                                    </Marker>
                                }
                                {
                                    destination &&
                                    <Marker coordinate={destination}>
                                        <Image
                                            source={Images.marker}
                                            style={{ width: 26, height: 36 }}
                                        />
                                    </Marker>
                                }
                                {
                                    origin && destination &&
                                    <MapViewDirections
                                        origin={origin}
                                        destination={destination}
                                        apikey={GoogleCloudKey}
                                        strokeWidth={4}
                                        strokeColor="#6E7887"
                                        onReady={(result) => {
                                            mapRef.current.fitToCoordinates(result.coordinates, {
                                                edgePadding: styles.markerEdge,
                                                animated: true
                                            })
                                        }}
                                    />
                                }

                            </MapView>
                        </View>
                        <View style={styles.contentSection2Row2Section2}>
                            <View style={styles.contentSection2Row2Section2Left}>
                                <Text style={styles.contentSection2Row2Section2LeftText}>
                                    {
                                        getFormattedTime(summaryResponse?.data?.ride_summary?.start_date_time)
                                        + " - " +
                                        getFormattedTime(summaryResponse?.data?.ride_summary?.end_date_time)
                                    }
                                </Text>
                            </View>
                            <View style={styles.contentSection2Row2Section2Right}>
                                <Text style={styles.contentSection2Row2Section2RightText}>
                                    {summaryResponse?.data?.ride_summary?.distance?.text}
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.contentSection3}>
                    <View style={styles.contentSection3Row1}>
                        <View style={styles.contentSection3Row1Left}>
                            <View style={styles.contentSection3Row1LeftDot} />
                        </View>
                        <View style={styles.contentSection3Row1Right}>
                            <Text style={styles.contentSection3Row1RightText1}>
                                {"From Address"}
                            </Text>
                            <Text style={styles.contentSection3Row1RightText2} numberOfLines={2} ellipsizeMode={'tail'}>
                                {summaryResponse?.data?.ride_summary?.from_address}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.contentSection3Row2}>
                        <View style={styles.contentSection3Row2Left}>
                            <View style={styles.contentSection3Row2LeftDot} />
                        </View>
                        <View style={styles.contentSection3Row2Right}>
                            <Text style={styles.contentSection3Row1RightText1}>
                                {"Destination Address"}
                            </Text>
                            <Text style={styles.contentSection3Row1RightText2} numberOfLines={2} ellipsizeMode={'tail'}>
                                {summaryResponse?.data?.ride_summary?.to_address}
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.footer}>
                <View style={styles.footerSection1}>
                    <Text style={styles.footerSection1Text1}>
                        {"How was your ride with"}
                    </Text>
                    <Text style={styles.footerSection1Text2}>
                        {summaryResponse?.data?.driver?.first_name + " " +
                            summaryResponse?.data?.driver?.last_name}?
                    </Text>
                </View>
                <View style={styles.footerSection2}>
                    <AirbnbRating
                        count={5}
                        defaultRating={0}
                        starImage={Images.star}
                        size={30}
                        showRating={false}
                        onFinishRating={(rating) => { onClickFeedback(rating) }}
                    />
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#253040",
        height: hp(100),
    },
    header: {
        height: hp(5),
        justifyContent: 'flex-start',
        padding: 20
    },
    headerButton: {
        width: 44,
        height: 44,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        height: hp(75),
        padding: 20
    },
    contentSection1: {
        height: hp(15),
        borderBottomWidth: 1,
        borderBottomColor: "#564F47",
        justifyContent: 'center'
    },
    contentSection1Text1: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 27,
        color: '#F9D591'
    },
    contentSection1Text2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 24,
        color: '#E1CDA9'
    },
    contentSection2: {
        height: hp(32)
    },
    contentSection2Row1: {
        height: hp(7),
        flexDirection: 'row',
        alignItems: 'center',
    },
    contentSection2Row1Text1: {
        flex: 7,
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: '#F5EDDC'
    },
    contentSection2Row1Text2: {
        flex: 3,
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#F5EDDC'
    },
    contentSection2Row2: {
        height: hp(25),
        backgroundColor: "#323E51",
        borderRadius: 15
    },
    contentSection2Row2Section1: {
        height: hp(18),
        borderRadius: 15,
        backgroundColor: "#fff"
    },
    contentSection2Row2Section2: {
        height: hp(7),
        flexDirection: 'row',
        padding: 8
    },
    contentSection2Row2Section2Left: {
        flex: 8,
        justifyContent: 'center'
    },
    contentSection2Row2Section2LeftText: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        lineHeight: 20,
        color: '#E1CDA9'
    },
    contentSection2Row2Section2Right: {
        flex: 2,
        justifyContent: 'center'
    },
    contentSection2Row2Section2RightText: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        lineHeight: 20,
        color: '#E1CDA9'
    },
    contentSection3: {
        height: hp(25),
        justifyContent: "center",
    },
    contentSection3Row1: {
        height: hp(10),
        flexDirection: 'row'
    },
    contentSection3Row1Left: {
        flex: .5,
        justifyContent: 'center'
    },
    contentSection3Row1LeftDot: {
        width: 6,
        height: 6,
        borderRadius: 3,
        backgroundColor: "#E1CDA9"
    },
    contentSection3Row1Right: {
        flex: 9.5,
        justifyContent: 'center',
    },
    contentSection3Row1RightText1: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#E1CDA9'
    },
    contentSection3Row1RightText2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 18,
        color: '#B59B7C'
    },
    contentSection3Row2: {
        height: hp(10),
        flexDirection: 'row'
    },
    contentSection3Row2Left: {
        flex: .5,
        justifyContent: 'center'
    },
    contentSection3Row2LeftDot: {
        width: 6,
        height: 6,
        borderRadius: 3,
        backgroundColor: "#E1CDA9"
    },
    contentSection3Row2Right: {
        flex: 9.5,
        justifyContent: 'center'
    },
    contentSection3Row2RightText1: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#E1CDA9'
    },
    contentSection3Row2RightText2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 18,
        color: '#B59B7C'
    },
    footer: {
        height: hp(20),
        padding: 20,
        backgroundColor: "#17202D",
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
    },
    footerSection1: {
        height: hp(10),
        alignItems: 'center'
    },
    footerSection1Text1: {
        fontFamily: REGULAR,
        fontSize: fSize(16),
        lineHeight: 28,
        color: '#FEFEFE'
    },
    footerSection1Text2: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 28,
        color: '#F9D591'
    },
    footerSection2: {
        fontFamily: REGULAR,
        fontSize: fSize(16),
        lineHeight: 28,
        color: '#FEFEFE',
        alignItems: 'center',

    },
    markerEdge: {
        top: 50,
        bottom: 50,
        left: 40,
        right: 40
    },
})

export default memo(Feedback);