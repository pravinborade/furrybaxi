import { createNavigationContainerRef } from '@react-navigation/native';

export const navigationRef = createNavigationContainerRef()

export function navigate(name, params) {
  if (navigationRef.isReady()) {
        navigationRef.navigate(name, params);
    }
}

export function backNavigation() {
  if (navigationRef.isReady()) {
        navigationRef.goBack();
    }
}

export function resetNavigation(screen) {
  if (navigationRef.isReady()) {
      navigationRef.reset(
          {
              index: 0,
              routes: [{ name: screen }]
          }

      )
  }
}