const initialState = {
    phoneNumber: undefined,
    country: {
        code: "IN",
        value: '91'
    },
    response: {},
    isError: false,
    isLoading: false,
    editPhoneNumber: undefined,
    editCountry: {
        code: "IN",
        value: '91'
    },
    editResponse:{},
    isEditPhoneError:false
};

const phoneNumberReducer = (state = initialState, action) => {
    switch (action.type) {

        case 'SET_PHONE_NUMBER':
            return {
                ...state,
                phoneNumber: action.phoneNumber,
                isError: false
            }

        case 'SET_COUNTRY':
            return {
                ...state,
                country: { ...action.country },
                isError: false
            }

        case 'SET_PHONE_NUMBER_RESPONSE':
            return {
                ...state,
                response: { ...action.response },
                isError: false
            };

        case 'SET_PHONE_NUMBER_ERROR':
            return {
                ...state,
                isError: action.isError
            }

       

        case 'SET_EDIT_PHONE_NUMBER':
            return {
                ...state,
                editPhoneNumber: action.editPhoneNumber,
                isEditPhoneError: false
            }

        case 'SET_EDIT_COUNTRY':
            return {
                ...state,
                editCountry: { ...action.editCountry },
                isEditPhoneError: false
            }

        case 'SET_EDIT_PHONE_NUMBER_RESPONSE':
            return {
                ...state,
                editResponse: { ...action.editResponse },
                isEditPhoneError: false
            };

        case 'SET_EDIT_PHONE_NUMBER_ERROR':
            return {
                ...state,
                isEditPhoneError: action.isEditPhoneError
            }

        case 'RESET_PHONE_NUMBER_STATE':
            return {
                ...initialState
            }

        case 'PHONE_NUMBER_LOADING':
            return {
                ...state,
                isLoading: action.isLoading
            }

        default:
            return state;
    }
};

export default phoneNumberReducer;
