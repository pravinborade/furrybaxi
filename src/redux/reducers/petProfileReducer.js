const initialState = {
    size: [],
    category: [],
    petProfile: {},
    isError: false,
    isPicError: false,
    isSizeError: false,
    isCategoryError: false,
    isDatePickerOpen: false,
    isSizeDropdownOpen: false,
    isCategoryDropdownOpen: false,
 
};

const petProfileReducer = (state = initialState, action) => {
    switch (action.type) {

        case 'SET_PET_PROFILE_RESPONSE':
            return {
                ...state,
                petProfile: { ...action.response },
                isError: false
            }


        case 'SET_PET_PROFILE_ERROR':
            return {
                ...state,
                isError: action.isError
            }

        case 'SET_CATEGORY_RESPONSE':
            return {
                ...state,
                category: [...action.category],
                isCategoryError: false
            }

        case 'SET_CATEGORY_ERROR':
            return {
                ...state,
                isCategoryError: action.isSizeError
            }

        case 'SET_SIZE_RESPONSE':
            return {
                ...state,
                size: [...action.size],
                isSizeError: false
            };

        case 'SET_SIZE_ERROR':
            return {
                ...state,
                isSizeError: action.isSizeError
            }

        case 'OPEN_DATE_PICKER':
            return {
                ...state,
                isDatePickerOpen: action.isDatePickerOpen

            };

        case 'OPEN_CATEGORY_DROPDOWN':
            return {
                ...state,
                isCategoryDropdownOpen: action.isCategoryDropdownOpen
            }
        case 'OPEN_SIZE_DROPDOWN':
            return {
                ...state,
                isSizeDropdownOpen: action.isSizeDropdownOpen
            }

        case 'SET_PET_PICTURE':
            return {
                ...state,
                petProfile: {
                    ...state.petProfile,
                    pic: action.pic
                },
                isError: false
            }

        case 'SET_PET_PICTURE_ERROR':
            return {
                ...state,
                isPicError: action.isPicError
            }

        case 'ON_CHANGE_CATEGORY':
            return {
                ...state,
                petProfile: {
                    ...state.petProfile,
                    category: action.category
                }
            }

        case 'ON_CHANGE_SIZE':
            return {
                ...state,
                petProfile: {
                    ...state.petProfile,
                    size: action.size
                }
            }

     


        default:
            return state;
    }
};

export default petProfileReducer;
