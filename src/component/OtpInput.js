import React from 'react';
import{StyleSheet} from 'react-native';
import OtpInputs from 'react-native-otp-inputs';
import { fSize } from '../utils/Responsive';
import { MEDIUM } from '../utils/typography';

const OtpInput = ({
    otpRef,
    otp,
    onChangeOtp,
    isError 
}) => {
    return (
        <OtpInputs
            ref={otpRef}
            defaultValue={otp}
            autofillFromClipboard={false}
            numberOfInputs={4}
            style={styles.inputContainer}
            inputStyles={[styles.codeInputFieldStyle, isError && styles.errorFieldStyle]}
            handleChange={(e)=>{onChangeOtp(e)}}
            selectionColor={"#253040"}
        />
    );
  };

  const styles = StyleSheet.create({
    inputContainer:{
        width: '70%',
        height: 120,
        flexDirection: 'row',
        paddingTop:20,
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    }, 
    codeInputFieldStyle: {
        width: 54,
        height: 78,
        borderWidth: 1,
        borderRadius: 10,
        borderColor :'#E1CDA9',
        textAlign: 'center',
        fontFamily: MEDIUM,
        fontSize: fSize(24),
        lineHeight: 29,
        color: '#F9D591',
    },
    errorFieldStyle:{
        borderColor :'#FF7373',
        color: '#FF7373',
    }
})

export default OtpInput;