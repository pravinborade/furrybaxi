import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import{StyleSheet,TouchableOpacity,Text} from 'react-native';

import { fSize, sHeight } from '../utils/Responsive';
import { MEDIUM } from '../utils/typography';

const CustomButton = ({
    title,
    onPress,
    disable,
    styleContainer,
    textStyle
  }) => {
    return (
        <TouchableOpacity onPress={onPress} style={styleContainer}>
            <Text style={textStyle}>
                {title}
            </Text>
        </TouchableOpacity>
    );
  };



  export default CustomButton;