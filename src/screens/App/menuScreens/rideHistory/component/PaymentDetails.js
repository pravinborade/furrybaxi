import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Rating } from "react-native-ratings";
import Images from "../../../../../assets/Images/Images";
import { fSize } from "../../../../../utils/Responsive";
import { MEDIUM } from "../../../../../utils/typography";

const PaymentDetails = ({data}) => {
    return (
        <View style={styles.container}>
            <View style={styles.containerRow1}>
                <View style={styles.containerRow1Left}>
                    <Text style={styles.containerRow1LeftText}>
                        {"Credit Card"}
                    </Text>
                </View>
                <View style={styles.containerRow1Right}>
                    <Text style={styles.containerRow1RightText}>
                        {data?.currency}{data?.price}
                    </Text>
                </View>
            </View>
            <View style={styles.containerRow2}>
                <Text style={styles.containerRow2Text}>
                    {`xxxx xxxx xxxx ${data?.card_no}`}
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#222831',
        padding: 20,
        borderRadius: 16
    },
    containerRow1: {
        paddingTop: 10,
        flexDirection: 'row',
    },
    containerRow1Left: {
        flex: .8,
        justifyContent: 'flex-start'
    },
    containerRow1LeftText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9"
    },
    containerRow1Right: {
        flex: .2,
        justifyContent: 'flex-end'
    },
    containerRow1RightText: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#E1CDA9"
    },
    containerRow2: {
        paddingTop: 10
    },
    containerRow2Text:{
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#A58B6C"
    }
})

export default PaymentDetails;