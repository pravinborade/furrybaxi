import { BackHandler  } from 'react-native'
import { backNavigation} from '../root/navigation/rootNavigation'

function handleBackButtonClick() {
    backNavigation();
    return true;
}

export  function  backListener () {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
}

export  function  removeBackListener () {
    BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
}