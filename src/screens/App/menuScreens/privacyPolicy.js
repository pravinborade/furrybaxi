import React from 'react';
import { Image, StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import Images from '../../../assets/Images/Images';
import { backNavigation } from '../../../root/navigation/rootNavigation';
import { fSize } from '../../../utils/Responsive';
import { REGULAR, SEMI_BOLD } from '../../../utils/typography';

const PrivacyPolicy = (props) => {
    
    return(
        <View style={styles.container}>
            <View style={styles.topView}>
                <TouchableOpacity style={styles.topViewButton} onPress={()=>{backNavigation()}}>
                    <Image source={Images.arrow_back} style={styles.topViewButtonIcon}/>
                </TouchableOpacity>
            </View>
            <View style={styles.middleView}>
                <Text style={styles.middleViewText}>
                    {"Privacy Policy"}
                </Text>
            </View>
            <View style={styles.bottomView}>
                <Text style={styles.bottomViewText}>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Neque, pulvinar
                    etiam proin ac. Pharetra quisque senectus egestas consectetur elementum volutpat
                    vitae ac ut. Odio nam neque, pretium, dictum ac congue amet. Dui elit quam vitae est.
                    Risus lacus lectus ultrices faucibus faucibus vestibulum nibh. Maecenas et, odio cursus
                    sed pretium, eu faucibus maecenas et. Tincidunt et, vestibulum imperdiet aliquet dui ornare.
                    Amet ullamcorper cras amet vehicula dictum. Porta velit eget et donec volutpat sit rhoncus
                    ornare sed. Ut laoreet viverra sagittis cursus imperdiet vitae. Tristique feugiat a ultricies
                    vitae interdum. Tellus volutpat enim turpis vivamus. Augue in sed euismod in vitae. Sed nunc
                    faucibus ante faucibus at amet.Sit blandit nunc mauris ligula purus. Enim a sed pretium platea
                    dolor, ornare. Cursus nisi, facilisis id eu at varius. Euismod amet porttitor enim massa sagittis\
                    eget dictum."
                </Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#17202D',
    },
    topView: {
        flex:1,
    },
    topViewButton: {
        padding: 10,
        width: 44,
        height: 44,
        backgroundColor: '#253040',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewButtonIcon: {
        width: 8,
        height: 16,
    },
    middleView: {
        flex:1,
        justifyContent: 'center'
    },
    middleViewText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#F9D591"
    },
    bottomView: {
        flex:8,
        backgroundColor: "#253040",
        borderRadius: 20,
        paddingHorizontal: 15,
        paddingVertical: 20
    },
    bottomViewText: {
        color: "#F5EDDC",
        fontFamily: REGULAR,
        fontSize: fSize(14), 
        lineHeight: 22,
        textAlign:'left',
        flexWrap: 'wrap-reverse'
    }

})

export default PrivacyPolicy;

