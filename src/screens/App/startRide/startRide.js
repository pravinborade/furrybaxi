import React, { useRef, useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Dimensions, Alert, TouchableOpacity } from 'react-native'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { useIsFocused } from '@react-navigation/core';
// import {useNavigation} from '@react-navigation/native';
import Images from '../../../assets/Images/Images';
import ActionContainer1 from './component/ActionContainer1';
import ActionContainer2 from './component/ActionContainer2';
import { useDispatch, useSelector } from 'react-redux';
import { getUserLocations, setActiveKey, setAvailableCabsResponse, setConfirmed, setGoogleInputFoucused, setHumanPets, setScheduleCabResponse, setScheduleDate, setWebSocketConnection } from '../../../redux/actions/ridingAction';
import {
    setSearchResult, getDestinationPlace,
    getDestinationSearchedPlace, getSearchPlaces,
} from "../../../redux/actions/mapAction";
import googleMapStyle from '../../../utils/googleMapStyle';
import { find_dimesions } from '../../../utils/helper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { checkLocationPermission, getCurrentLocation } from '../../../redux/actions/homeAction';
import Sidebar from '../component/Sidebar';
import { getPets, setEditResponse } from '../../../redux/actions/petEditAction';
import { ScreenNames } from '../../../utils/ScreenNames';
import ActionContainer3 from './component/ActionContainer3';
import { store } from '../../../redux/store/store';



const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


const StartRide = () => {
    let inputRef1 = useRef();
    let inputRef2 = useRef();
    let mapRef = useRef(null);

    const dispatch = useDispatch()
    const isVisible = useIsFocused();
    // const  navigation = useNavigation()

    const [dynamicHeight, setDynamicHeight] = useState(height)
    const [searchKeyword, setSearchKeyword] = useState('')
    const [isUpcomingRideView, setUpcominRideView] = useState(true)
    const [isMenuVisible, setIsMenuVisible] = useState(false)


    const { ws } = useSelector(state => state.riding)


    const { pets } = useSelector(state => state.petEdit)


    const [region, setRegion] = useState({
        latitude: 51.509865,
        longitude: -0.118092,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    })
    const {
        currentPlace,
        destinationPlace,
        searchResult,

    } = useSelector(state => state.map)

    const {
        isGoogleInputFoucused,
        userLocations,
        serviceLocationsResponse,
        isConfirmed,
        activeKey
    } = useSelector(state => state.riding)

    useEffect(() => {    
        if (activeKey == 0 && !isConfirmed && !isGoogleInputFoucused) {          
            setTimeout(()=>{             
                dispatch(getCurrentLocation())
            },200)            
        }
    }, [isConfirmed, isGoogleInputFoucused, activeKey])

    const {
        userResponse
    } = useSelector(state => state.auth)

    const {
        isLocationAccess,
        currentLocation
    } = useSelector(state => state.home)



    useEffect(() => {
        if (isVisible) {
            setSearchKeyword('')
            dispatch(setSearchResult([]))
            dispatch(setAvailableCabsResponse(null))
            dispatch(setScheduleDate(null))
            dispatch(setScheduleCabResponse(null))
            dispatch(getPets())
            dispatch(setGoogleInputFoucused(false))
        }
        dispatch(checkLocationPermission());

    }, [isVisible])

    useEffect(() => {
        if (isLocationAccess) {
            dispatch(getCurrentLocation())
        }

    }, [isLocationAccess])

    useEffect(() => {
        if (serviceLocationsResponse && serviceLocationsResponse !== null) {
            dispatch(getUserLocations())
        }
    }, [serviceLocationsResponse])


    useEffect(() => {
        if (ws == null) {
            setupWebSocketConnection()
        }
    }, [])

    useEffect(() => {
        if (store.getState().riding.pets.length > 0) {

        } else {
            dispatch(setHumanPets(pets))
        }
    }, [pets])

    const setHumanPetsList = () => {
        dispatch(setHumanPets(pets))
    }

    useEffect(() => {
        setSearchResult([])
        setSearchKeyword('')
    }, [isGoogleInputFoucused])

    useEffect(() => {
        if (currentLocation && currentLocation !== null) {
            setRegion({
                ...currentLocation,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            })
            mapRef.current.animateToRegion({
                ...currentLocation,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }, 1000);
        }
    }, [currentLocation])

    const onSearchLocation = async (text) => {
        if (!checkIfServicable()) {
            console.log("Not Servicable")
        } else {
            var str = text;
            var prefix = '';
            if (serviceLocationsResponse?.postal_town) {
                prefix = `${serviceLocationsResponse?.postal_town}, `
            }

            if (str.indexOf(prefix) === 0) {
                setSearchKeyword()
            } else {
                if (prefix.indexOf(str) >= 0) {
                    setSearchKeyword(str);
                } else {
                    setSearchKeyword(str);
                }
            }

            dispatch(getSearchPlaces((prefix + str), serviceLocationsResponse?.country_short))
        }
    };


    const onSelectLocation = async (item) => {
        if (!checkIfServicable()) {
            console.log("Not Servicable")
        } else {
            setSearchKeyword(item?.name)
            dispatch(getDestinationPlace(currentPlace, item))
        }
    }

    const onSelectSearchedLocation = async (item) => {
        if (!checkIfServicable()) {
            console.log("Not Servicable")
        } else {
            setSearchKeyword(item?.name)
            dispatch(getDestinationSearchedPlace(currentPlace, item))
        }
    }
    const checkIfServicable = () => {
        if (!serviceLocationsResponse || serviceLocationsResponse === null) {
            Alert.alert("Not Servicable")
            return false
        } else if (Object.keys(serviceLocationsResponse)?.length > 0) {
            return true
        } else {
            Alert.alert("Not Servicable")
            return false
        }
        // if (!serviceLocationsResponse || serviceLocationsResponse === null) {
        //     Alert.alert("Not Servicable")
        //     return false
        // } else if (!serviceLocationsResponse?.data || !serviceLocationsResponse?.data?.length === 0) {
        //     Alert.alert("Not Servicable")
        //     return false
        // } else if (serviceLocationsResponse?.message === 'servicable') {
        //     return true
        // } else {
        //     Alert.alert("Not Servicable")
        //     return false
        // }
    }

    const setupWebSocketConnection = async () => {
        const token = await AsyncStorage.getItem('token');
        //const token= "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI2ODhlMWVkOS05YjM4LTQ2MWItYTM4MS1iOTA5NDAzYWFjMmUiLCJpYXQiOjE2NDE3NDY2OTUsIm5iZiI6MTY0MTc0NjY5NSwianRpIjoiZjYyY2M1ODQtNzViZi00MWJkLTk0ZWYtZjM0YzNmYTQ0OWMwIiwiZXhwIjoxNjQxNzY0Njk1LCJ0eXBlIjoiYWNjZXNzIiwiZnJlc2giOmZhbHNlfQ.q6O00fi5JQeMoJezvhLC0baE8Y4YFz8fSKMmb6Wp0m4"
        var ws = new WebSocket(`ws://socket.furrybaxi.ml/fb/ws/user?token=${token}`);
        // ws.open()
        dispatch(setWebSocketConnection(ws))
    }

    const toggleMenu = () => {
        setIsMenuVisible(!isMenuVisible)
    }

    return (
        <View style={styles.container}>
            {
                isMenuVisible &&
                <Sidebar toggleMenu={toggleMenu} />
            }
            <MapView
                provider={PROVIDER_GOOGLE}
                style={{ ...StyleSheet.absoluteFillObject, height: dynamicHeight }}
                ref={mapRef}
                initialRegion={region}               
                customMapStyle={googleMapStyle}
                showsUserLocation={false}
                showsCompass={false}
            >
                {
                    currentLocation && currentLocation !== null &&
                    <Marker
                        coordinate={currentLocation}
                    >
                        <Image source={Images.driverLocation} style={styles.driverLocation} />
                    </Marker>
                }
            </MapView>
            {
                !isMenuVisible &&
                <TouchableOpacity style={styles.menuOverLay} onPress={() => { toggleMenu() }}>
                    <Image
                        source={Images.menu}
                        style={styles.iconImages}
                    />
                </TouchableOpacity>
            }
            <View
                onLayout={(event) => setDynamicHeight(find_dimesions(height, event.nativeEvent.layout))}
                style={
                    isGoogleInputFoucused
                        ?
                        styles.actionContainer2
                        :
                        styles.actionContainer1
                }
            >
                {
                    isGoogleInputFoucused
                        ?
                        <ActionContainer2
                            inputRef={inputRef2}
                            setGoogleInputFoucused={(data) => {
                                if (!checkIfServicable()) {
                                    Alert.alert("Not Servicable")
                                } else {
                                    dispatch(setGoogleInputFoucused(data))
                                }
                            }}
                            getSearchPlaces={getSearchPlaces}
                            searchResult={searchResult}
                            destinationPlace={destinationPlace}
                            searchKeyword={searchKeyword}
                            setSearchKeyword={setSearchKeyword}
                            userLocations={userLocations}
                            onSearchLocation={onSearchLocation}
                            onSelectLocation={onSelectLocation}
                            onSelectSearchedLocation={onSelectSearchedLocation}
                        />
                        : isConfirmed ?
                            <ActionContainer1
                                inputRef={inputRef1}
                                setGoogleInputFoucused={(data) => {
                                    if (!checkIfServicable()) {
                                        Alert.alert("Not Servicable")
                                        inputRef1.current.blur()
                                        return
                                    } else {

                                        dispatch(setGoogleInputFoucused(data))
                                    }
                                }}
                                userLocations={userLocations}
                                onSelectLocation={onSelectLocation}
                                userResponse={userResponse}
                                activeKey={activeKey}
                                setActiveKey={setActiveKey}
                                setHumanPetsList={setHumanPetsList}
                                isConfirmed={isConfirmed}
                                setConfirmed={setConfirmed}
                                isUpcomingRideView={userResponse?.scheduled_trips?.length > 0}
                            />
                            :
                            <ActionContainer3
                                inputRef={inputRef1}
                                setGoogleInputFoucused={(data) => {
                                    if (!checkIfServicable()) {
                                        Alert.alert("Not Servicable")
                                        inputRef1.current.blur()
                                        return
                                    } else {
                                        dispatch(setGoogleInputFoucused(data))
                                    }
                                }}
                                userLocations={userLocations}
                                onSelectLocation={onSelectLocation}
                                userResponse={userResponse}
                                activeKey={activeKey}
                                setActiveKey={setActiveKey}
                                setHumanPetsList={setHumanPetsList}
                                isConfirmed={isConfirmed}
                                setConfirmed={setConfirmed}
                                isUpcomingRideView={userResponse?.scheduled_trips?.length > 0}
                            />
                }
            </View>
        </View>

    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#17202D'

    },
    driverLocation: {
        height: 50,
        width: 50
    },
    menuOverLay: {
        position: 'absolute',
        top: 0,
        minHeight: hp(10),
        left: 0,
        padding: 30
    },
    actionContainer1: {
        // zIndex: 1,
        position: 'absolute',
        // top: hp(35),
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#F5EDDC",
        //padding: 20,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
    },
    actionContainer2: {
        position: 'absolute',
        top: hp(0),
        bottom: 0,
        left: 0,
        right: 0,
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
    },
    markerEdge: {
        top: 0,
        bottom: 0,
        left: 0,
        right: 0
    },
});

export default memo(StartRide);
