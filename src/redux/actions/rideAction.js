import appService from "../../service/appService";
import RNFetchBlob from 'rn-fetch-blob';

export const setRideList = (response) => {
    return {
        type: "SET_RIDE_LIST_RESPONSE",
        response
    }
}

export const setRideDetailsResponse = (response) => {
    return {
        type: "SET_RIDE_DETAILS_RESPONSE",
        response
    }
}

export const setRidePaymentResponse = (response) => {
    return {
        type: "SET_RIDE_PAYMENT_RESPONSE",
        response
    }
}

export function setSafetyModal(isSafetyModalVisible) {
    return {
        type: 'SET_SAFETY_MODAL',
        isSafetyModalVisible
    };
}


export function getRideList(requestData) {
    return async (dispatch) => {
        return appService.get('fb/v1/trips', requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                // console.log(response)
                dispatch(setRideList(response))
            })
            .catch((error) => {
                dispatch(setRideList(null))
                console.log(error)
            });
    };
}

export function getRideDetails(id) {
    return async (dispatch) => {
        return appService.get(`fb/v1/trips/${id}`)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setRideDetailsResponse(response))
            })
            .catch((error) => {
                dispatch(setRideDetailsResponse(null))
                console.log(error.response.data)
            });
    };
}

export function getRidePaymentDetails(id) {
    return async (dispatch) => {
        return appService.get(`fb/v1/trips/payment/${id}`)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setRidePaymentResponse(response))
            })
            .catch((error) => {
                dispatch(setRidePaymentResponse(null))
                console.log(error)
            });
    };
}

export function submitComment(requestData) {
    return new Promise((resolve, reject) => {
        return appService.post(`fb/v1/zsupport/text_based`, requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                return resolve(response)
            })
            .catch((error) => {
                return reject(error)
            });
    });
}

export function driveDangerous(requestData) {
    return new Promise((resolve, reject) => {
        return appService.post(`fb/v1/zsupport/dd`, requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                return resolve(response)
            })
            .catch((error) => {
                return reject(error)
            });
    });
}

export function vehicleDifferent(requestData) {
    return new Promise((resolve, reject) => {
        return appService.post(`fb/v1/zsupport/vdd`, requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                return resolve(response)
            })
            .catch((error) => {
                return reject(error)
            });
    });
}

export function accidentDetails(requestData) {
    return new Promise((resolve, reject) => {
        return appService.post(`fb/v1/zsupport/accident`, requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                return resolve(response)
            })
            .catch((error) => {
                return reject(error)
            });
    });
}

export function resendReceiptEmail(id) {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await appService.post(`fb/v1/trips/payment/${id}/send`);
            if (response.status !== 200) {
                throw response;
            }
            const response_1 = response.data;
            console.log(response_1)
            return resolve(response_1);
        } catch (error) {
            return reject(error);
        }
    });
}

export function cancelRide(requestData) {
    return new Promise(async (resolve, reject) => {
        return appService.post('fb/v1/trips/cancel', requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                return resolve(response)
            })
            .catch((error) => {
                return reject(response)
            });
    });
}

const getFileExtention = (fileUrl) => {
    return (/[.]/.exec(fileUrl) ?
        /[^.]+$/.exec(fileUrl) : undefined);
};

const downloadFile = (FILE_URL) => {
    let date = new Date();
    let file_ext = getFileExtention(FILE_URL);
    file_ext = '.' + file_ext[0];
    const { config, fs } = RNFetchBlob;
    let RootDir = fs.dirs.DownloadDir;
    let options = {
        fileCache: true,
        addAndroidDownloads: {
            path:
                RootDir +
                '/file_' +
                Math.floor(date.getTime() + date.getSeconds() / 2) +
                file_ext,
            description: 'downloading file...',
            notification: true,
            // useDownloadManager works with Android only
            useDownloadManager: true,
        },
    };
    config(options)
        .fetch('GET', FILE_URL)
        .then(res => {
            // Alert after successful downloading
            console.log('res -> ', JSON.stringify(res));
        });
};

export const checkPermission = async (fileUrl) => {
    if (Platform.OS === 'ios') {
        downloadFile(fileUrl);
    } else {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    title: 'Storage Permission Required',
                    message:
                        'Application needs access to your storage to download File',
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // Start downloading
                downloadFile(fileUrl);
                console.log('Storage Permission Granted.');
            } else {
                // If permission denied then show alert
                console.log('Error', 'Storage Permission Not Granted');
            }
        } catch (err) {
            // To handle permission related exception
            console.log("++++" + err);
        }
    }
};

export function shareTrip(trip) {
    return async (dispatch) => {
        return appService.get('fb/v1/trips/share/passenger?trip_id=' + trip)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then(async (response) => {
                try {
                    const result = await Share.share({
                        message: response?.link
                    });
                    if (result.action === Share.sharedAction) {
                        setTimeout(() => {
                            dispatch(setSafetyModal(false))
                        }, 100)

                        if (result.activityType) {
                            // shared with activity type of result.activityType
                        } else {
                            // shared
                        }
                    } else if (result.action === Share.dismissedAction) {
                        // dismissed
                        setTimeout(() => {
                            dispatch(setSafetyModal(false))
                        }, 100)

                    }
                } catch (error) {
                    alert(error.message);
                }

            })
            .catch((error) => {

            });
    };
}
