import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView, TextInput, TouchableOpacity } from 'react-native'
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { fSize } from '../../../../utils/Responsive';
import Images from '../../../../assets/Images/Images';
import BackButton from '../../../../component/BackButton';
import { MEDIUM, REGULAR } from '../../../../utils/typography';
import CurrentLocation from '../../component/CurrentLocationButtion';
import { useDispatch } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScreenNames } from '../../../../utils/ScreenNames';
import { navigate } from '../../../../root/navigation/rootNavigation';
import { store } from '../../../../redux/store/store';
import { shouldStartRideFromMap } from '../../../../redux/actions/ridingAction';
import SavedLocationList from '../savedLocationList';

const ActionContainer2 = (props) => {
    const { userLocations, setGoogleInputFoucused, searchResult,
        searchKeyword, onSearchLocation, onSelectLocation,
        onSelectSearchedLocation } = props;
    const listItems = searchResult.length > 0 ? searchResult : userLocations ? [...userLocations?.user_recent] : []
    const setLocationOnMap = () => {
        store.dispatch(shouldStartRideFromMap(true))
        navigate(ScreenNames.MAP_SEARCH)

    }
    const savedLocation = () => {
        store.dispatch(shouldStartRideFromMap(false))
        navigate(ScreenNames.SAVED_LOCATION_LIST)
    }
    return (
        <>
            <View style={[styles.actionContainer2Section1, { backgroundColor: '#ffffff' }]}>
                <View style={{ backgroundColor: '#253040' }}>
                    <View style={styles.actionContainer2Section1Top}>
                        <BackButton onPress={() => { setGoogleInputFoucused(false) }} />
                        <View style={{marginTop: 10}}>
                            <TextInput
                                placeholder="Enter your Destination"
                                placeholderTextColor="#000"
                                style={styles.searchInput}
                                onChangeText={(text) => onSearchLocation(text)}
                                value={searchKeyword}
                                caretHidden={false}
                            />
                            <Image source={Images.search} style={styles.searchIcon} />
                        </View>
                    </View>
                </View>

                <View style={styles.actionContainer2Section1Bottom}>
                    <TouchableOpacity onPress={() => savedLocation()} style={styles.actionContainer2Section1BottomContainer}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={Images.star_dark} style={styles.actionContainer2Section1BottomContainerImage1} />
                            <Text style={styles.actionContainer2Section1BottomContainerTitle} >{'Saved Locations'}</Text>
                        </View>
                        <Image source={Images.arrow_right} style={styles.actionContainer2Section1BottomContainerRightImage} />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => setLocationOnMap()} style={styles.actionContainer2Section1BottomContainer}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={Images.set_location} style={styles.actionContainer2Section1BottomContainerImage2} />
                            <Text style={styles.actionContainer2Section1BottomContainerTitle}>{'Set location on map'}</Text>
                        </View>
                        <Image source={Images.arrow_right} style={styles.actionContainer2Section1BottomContainerRightImage} />
                    </TouchableOpacity>
                </View>
                <View style={styles.actionContainer2Section2}>
                    <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                        {
                            searchKeyword != '' && listItems?.map((item, index) => (
                                <TouchableOpacity onPress={() => {
                                    item?.lang || item?.long
                                        ?
                                        onSelectLocation(item)
                                        :
                                        onSelectSearchedLocation(item)
                                }} key={index}>
                                    <View style={styles.listItem} key={index}>
                                        {
                                            !item.searched
                                            &&
                                            <View style={styles.listItemLeft}>
                                                <Image source={Images.clock} style={{ height: 20, width: 20 }} />
                                            </View>
                                        }
                                        <View style={styles.listItemRight}>
                                            <Text style={styles.listItemRightText1}>{item.name}</Text>
                                            <View style={{ height: 2 }}></View>
                                            <Text style={styles.listItemRightText2}>{item?.address}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            ))
                        }
                    </ScrollView>
                </View>
            </View>
        </>
    )
}


const styles = StyleSheet.create({
    iconImages: {
        height: 25,
        width: 25
    },

    actionContainer2Section1: {
        flex: 1,
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        zIndex: 2,
    },

    actionContainer2Section1Top: {
        backgroundColor: "#17202D",
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        padding: 20,
        zIndex: 2,
        // minHeight: 100
    },

    actionContainer2Section1Bottom: {
        backgroundColor: "#253040",
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        paddingHorizontal: 20,
    },

    actionContainer2Section2: {
        backgroundColor: '#FDFFFF',
        padding: 20,
        flex: 1
    },

    listItem: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        paddingVertical: 10
    },

    listItemLeft: {
        flex: 1
    },

    listItemRight: {
        flex: 9,
        borderBottomColor: '#F2F2F2',
        borderBottomWidth: 1,
        paddingBottom: 10
    },
    listItemRightText1: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#6D7580"
    },
    listItemRightText2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#A3A8AF"
    },
    searchInput: {
        backgroundColor: '#FFFFFF',
        height: 50,
        borderRadius: 15,
        paddingVertical: 5,
        paddingHorizontal: 15,
        fontSize: fSize(14),
        fontFamily: MEDIUM,
        color: "#364458"
    },
    searchIcon: {
        position: 'absolute',
        right: 15,
        top: 15
    },
    actionContainer2Section1BottomContainer: {
        paddingVertical: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    actionContainer2Section1BottomContainerTitle: {
        color: '#E1CDA9',
        fontSize: fSize(14),
        fontFamily: MEDIUM
    },
    actionContainer2Section1BottomContainerImage1: {
        height: 20,
        width: 20,
        marginHorizontal: 10
    },
    actionContainer2Section1BottomContainerImage2: {
        height: 20,
        width: 15,
        marginHorizontal: 10
    },
    actionContainer2Section1BottomContainerRightImage: {
        height: 15,
        width: 7,
        marginVertical: 5,
    },


});

export default ActionContainer2;
