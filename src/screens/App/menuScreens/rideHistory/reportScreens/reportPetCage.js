import React, { useState } from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text, Dimensions, TextInput, ScrollView, SafeAreaView } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../../../assets/Images/Images";
import { backNavigation, navigate } from "../../../../../root/navigation/rootNavigation";
import { fSize } from "../../../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../../utils/typography";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { submitComment } from "../../../../../redux/actions/rideAction";
import { ScreenNames } from "../../../../../utils/ScreenNames";
import ReportHeader from "./components/reportHeader";
import ReportFooter from "./components/reportFooter";


const screenHeight = Dimensions.get('screen').height
const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}

const ReportPetCage = (props) => {
    const dispatch = useDispatch()
    const [tripID, setTripID] = useState(props?.route?.params?.id)
    const [comment, setComment] = useState('')
    const [isError, setIsError] = useState(false)
    
    const onSubmit = async () => {
        if (!comment) {
            setIsError(true)
            return
        }
        try {
            let formData = {
                trip_id: tripID,
                query_content: comment,
                question_title: "FB_PET_CAGE"
            }
            let resp = await submitComment(formData)
            navigate(ScreenNames.TRIP_DETAILS,{id:tripID})
        } catch (error) {
            console.log(error)
        }
    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <ReportHeader/>
            </View>
            <KeyboardAwareScrollView 
                contentContainerStyle={styles.content} 
                enableOnAndroid={true} 
                scrollEnabled={true}
            >
            <View style={styles.content}>
                <ScrollView contentInsetAdjustmentBehavior="always">
                    <View style={styles.contentBody}>
                        <View style={styles.contentBodySection2}>
                            <Text style={styles.contentBodySection2Text}>
                                {"Please describe your issue"}
                            </Text>
                        </View>
                        <View style={styles.contentBodySection4}>
                            <TextInput 
                                placeholder="Type here...|" 
                                placeholderTextColor={"#000000"} 
                                style={
                                    [
                                        styles.contentBodySection4Input,
                                        isError && styles.error
                                    ]
                                }
                                onChangeText={(text)=>{
                                    setComment(text);
                                    if(isError){
                                        setIsError(false)
                                    }
                                }}
                                value={comment}
                                multiline
                                numberOfLines={20}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
            </KeyboardAwareScrollView>
            <View style={styles.footer}>
                <ReportFooter comment={comment} onSubmit={onSubmit} />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: getHeight(100),
        backgroundColor: '#17202D',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        height: getHeight(Platform.OS === 'ios' ? 5 : 15 ),
        paddingHorizontal: 20,
        justifyContent: 'center'
    },
    content: {
        height: getHeight(Platform.OS === 'ios' ? 80 : 70 ),
    },
    contentBody: {
        paddingTop: 10,
        paddingHorizontal: 20,
        justifyContent: 'flex-end'
    },
    contentBodySection1Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#F9D591"
    },
    contentBodySection2: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    contentBodySection2Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#E1CDA9",
    },
    contentBodySection3: {
        borderRadius: 15,
        backgroundColor: "#FFFFFF",
    },
    contentBodySection3TopView: {
        height: getHeight(18),
        borderRadius: 15,
        overflow: 'hidden',
    },
    mapImage: {
        width: "100%",
        height: undefined,
        aspectRatio: 2/1
    },
    contentBodySection3BottomView:{
        paddingHorizontal: 15,
    },
    contentBodySection3BottomViewSection1: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 10
    },
    contentBodySection3BottomViewSection1Left: {
        flex: 0.8,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    contentBodySection3BottomViewSection1LeftText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#000000'
    },
    contentBodySection3BottomViewSection1Right: {
        flex: 0.2,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    contentBodySection3BottomViewSection1RightText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#000000'
    },
    contentBodySection3BottomViewSection2: {
        paddingVertical: 10,
        flexDirection: 'row',
    },
    contentBodySection3BottomViewSection2Left: {
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    contentBodySection3BottomViewSection2LeftText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#000000'
    },
    contentBodySection4:{
        paddingVertical: 20
    },
    contentBodySection4Input:{
        color: "#000",
        backgroundColor: "#E2EAED",
        borderRadius: 15,
        padding: 10,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        height: getHeight(25),
    },
    error: {
        borderWidth: 2,
        borderColor: 'red'
    },
    footer: {
        height: getHeight(Platform.OS === 'ios' ? 5 : 5 ),
        paddingHorizontal: 20,
        justifyContent: 'center'
    }
})
export default ReportPetCage;
