import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import{StyleSheet,TouchableOpacity,Text} from 'react-native';

import { fSize, sHeight } from '../utils/Responsive';
import { MEDIUM } from '../utils/typography';

const  PrimaryButton = ({
    title,
    onPress,
    disabled
  }) => {
    return (
        <TouchableOpacity onPress={onPress} disabled={disabled}>
            <LinearGradient
                useAngle={true}
                angle={180} 
                colors={['#DAC5A2', '#A98C6F']} 
                start={{x: 0.0 , y: 0.0}}
                end={{x: 0.0, y: 1.0}}
                // locations={[-0.3, 1.5]}
                style={styles.bottomContainer}
            >
                <Text style={styles.buttonText}>
                    {title}
                </Text>
            </LinearGradient>  
        </TouchableOpacity>
    );
  };

  const styles = StyleSheet.create({
    bottomContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        height: 54,
        borderRadius: 15
    },
    buttonText:{
        color:'#FFFCF5',
        fontFamily:MEDIUM,
        fontSize: fSize(14)
    }
  })

  export default PrimaryButton;