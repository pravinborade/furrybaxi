// import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
// import { Dimensions, Platform, PixelRatio } from 'react-native'
// const height = Dimensions.get('window').height;

// export function fSize(fontSize){
//     return RFValue(fontSize,height)
// }   


import { Dimensions, Platform, PixelRatio } from 'react-native';

const {
  width: SCREEN_WIDTH,
  height: SCREEN_HEIGHT,
} = Dimensions.get('window');

// based on iphone 5s's scale
const scale = SCREEN_WIDTH / 320;

export function fSize(size) {
  const newSize = size * scale 
  if (Platform.OS === 'ios') {
    return Math.round(PixelRatio.roundToNearestPixel(newSize))
  } else {
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2
  }
}

export function sHeight(size) {
    return (SCREEN_HEIGHT*size/100)
}

export function sWidth(size) {
  return (SCREEN_WIDTH*size/100)
}