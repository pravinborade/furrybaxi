import React, { useRef, useEffect, useState, memo } from 'react';
import { View, StyleSheet, SafeAreaView, Text, FlatList, RefreshControl, TouchableOpacity, Image, TextInput } from "react-native"
import { useDispatch, useSelector } from 'react-redux';
import Images from '../../../assets/Images/Images';
import BackButton from '../../../component/BackButton';
import CustomButton from '../../../component/CustomButton';
import Header from '../../../component/Header';
import { getSocketData, saveSavedLocations, setSavedLocationName } from '../../../redux/actions/ridingAction';
import { navigate } from '../../../root/navigation/rootNavigation';
import { fSize } from '../../../utils/Responsive';
import { ScreenNames } from '../../../utils/ScreenNames';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';
import CurrentLocation from '../component/CurrentLocationButtion';

const SaveLocation = (props) => {
     const dispatch = useDispatch()
     const { saveLocationDetail, saveLocationName } = useSelector(state => state.riding)
       const [isSaveNameError, setSaveNameError] = useState(false);


     const headerLeft = () => {
          return <BackButton onPress={() => { props.navigation.goBack() }} />
     }

     const headerMiddle = () => {
          return <Text style={styles.headerTitle}>{'Save Location'}</Text>
     }


     const save = () => {
          if (saveLocationName != '') {
               setSaveNameError(false)            
               if (saveLocationDetail) {
                    var requestData = {                       
                         "lat": saveLocationDetail.lat,
                         "long": saveLocationDetail.long,
                         "name": saveLocationName,
                         "location_type": "user_home"
                    }                 
                    dispatch(saveSavedLocations(requestData))
               }
          } else {
               setSaveNameError(true)
          }
     }

     const onChangeName = (name) => {
          if (name != '') {
               setSaveNameError(false)
          }
          dispatch(setSavedLocationName(name))
     }

     return (
          <SafeAreaView style={styles.container}>
               <Header cmHeight={'10%'} leftView={headerLeft} middleView={headerMiddle} />
               <View style={{ flex: 1 }}>
                    <View style={{ paddingVertical: 10 }}>
                         <TextInput
                              placeholder="Name"                           
                              style={isSaveNameError ? styles.searchInputError : styles.searchInput}
                              placeholderTextColor={"#ADADAE"}
                              value={saveLocationName}
                              caretHidden={false}
                              onChangeText={(name) => { onChangeName(name) }}
                         />

                    </View>
                    <TouchableOpacity onPress={() => { }} style={styles.saveCard}>
                         <View style={styles.saveInfo}>
                              <Text style={styles.placeInfoText1}>{'Address'}</Text>
                              <Text style={styles.placeInfoText1}>{`${saveLocationDetail?.address}`}</Text>
                         </View>
                    </TouchableOpacity>
               </View>
               <CustomButton
                    title={"Save"}
                    onPress={(e) => save()}
                    disable={false}
                    styleContainer={styles.bottomButton}
                    textStyle={styles.bottomButtonText}
               />
          </SafeAreaView>
     )
}
const styles = StyleSheet.create({
     container: {
          flex: 1,
          backgroundColor: '#17202D',
          padding: 20
     },
     headerTitle: {
          fontSize: fSize(18),
          color: '#FDFDFD',
          fontFamily: MEDIUM,
          marginLeft: -30
     },
     saveCard: {
          flexDirection: 'row',
          backgroundColor: '#253040',
          borderRadius: 15,
          marginVertical: 10

     },
     saveCardContainer: {
          flexDirection: 'row'

     },

     saveInfo: {
          padding: 10,

          width: '100%',


     },
     placeInfoText1: {
          color: '#A58B6C',
          fontFamily: REGULAR,
          fontSize: fSize(12),
          paddingVertical: 5

     },



     searchInput: {
          backgroundColor: '#FFFFFF',
          height: 50,
          borderRadius: 15,
          paddingVertical: 5,
          paddingHorizontal: 15,
          fontSize: fSize(14),
          fontFamily: MEDIUM,
          color: "#364458"
     },
     searchInputError: {
          backgroundColor: '#FFFFFF',
          height: 50,
          borderRadius: 15,
          borderColor: 'red',
          borderWidth: 1,
          paddingVertical: 5,
          paddingHorizontal: 15,
          fontSize: fSize(14),
          fontFamily: MEDIUM,
          color: "#364458"
     },
     actionContainer2Section1BottomContainer: {
          paddingVertical: 15,
          flexDirection: 'row',
          justifyContent: 'space-between'
     },
     actionContainer2Section1BottomContainerImage2: {
          height: 20,
          width: 15,
          marginHorizontal: 10
     },
     actionContainer2Section1BottomContainerTitle: {
          color: '#E1CDA9',
          fontSize: fSize(14),
          fontFamily: MEDIUM
     },
     actionContainer2Section1BottomContainerRightImage: {
          height: 15,
          width: 7,
          marginVertical: 5,
          //alignSelf:'flex-end'
     },
     bottomButton: {
          backgroundColor: '#F9D591',
          height: 54,
          borderRadius: 15,
          justifyContent: 'center',
          marginVertical: 10,
          alignItems: 'center',

     },
     bottomButtonText: {
          textAlign: 'center',
          color: '#253040',
          fontSize: fSize(14),
          fontFamily: SEMI_BOLD
     },
})
export default  memo(SaveLocation);