import moment from "moment";
import React, { useEffect, useRef, useState} from "react";
import { Dimensions, Image, StyleSheet, Text, View } from "react-native";
import { AirbnbRating, Rating } from "react-native-ratings";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import Images from "../../../../../assets/Images/Images";
import { DATE_FORMATS, RIDE_STATUS } from "../../../../../utils/constants";

import { fSize } from "../../../../../utils/Responsive";
import { MEDIUM, REGULAR } from "../../../../../utils/typography";


const CompletedList = (props) => {
    const {data} = props;
    return(
        <View style={styles.container}>
            <View style={styles.mapContainer}>
               <Image source={{uri: data?.ride?.map_url}} style={styles.mapImage} />
            </View>
            <View  style={styles.bottomView}>
                <View style={styles.bottomViewSection1}>
                    <View style={styles.bottomViewSection1Left}>
                        <Text style={styles.bottomViewSection1LeftText}>
                            {moment(data?.timestamp).format(DATE_FORMATS.DO_MMM_HH_MM_A)}
                        </Text>
                    </View>
                    <View style={styles.bottomViewSection1Right}>
                        <Text style={styles.bottomViewSection1RightText}>
                            {data?.currency}{data?.price}
                        </Text>
                    </View>
                </View>
                <View style={styles.bottomViewSection2}>
                    <View style={styles.bottomViewSection2Left}>
                        <Text style={styles.bottomViewSection2LeftText}>
                            {data?.ride?.cab_name}
                        </Text>
                    </View>
                    <View style={styles.bottomViewSection2Right}>
                        {
                            data?.status === RIDE_STATUS.COMPLETED 
                            ?   
                            <AirbnbRating
                                count={5}
                                defaultRating={data?.ride?.ride_rating}
                                starImage={Images.star}
                                size={15}
                                showRating={false}
                                isDisabled={true}
                                selectedColor={"#000000"}
                            />
                            :
                            <Text style={styles.bottomViewSection2RightText}>
                                {data?.status}
                            </Text>
                        }
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#FFFFFF',
        borderRadius: 15,
        marginVertical: 10
    },
    mapContainer:{
        height: hp(20),
        borderRadius: 15,
        overflow: 'hidden' 
    },
    mapImage: {
        width: "100%",
        height: undefined,
        aspectRatio: 2/1
    },
    bottomView:{
        borderRightBottomRadius: 15,
        borderLeftBottomRadius: 15,
        padding: 10
    },
    bottomViewSection1: {
        flex: 1,
        flexDirection: 'row',
        padding: 10
    },
    bottomViewSection1Left: {
        flex: .8,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    bottomViewSection1LeftText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#000000'
    },
    bottomViewSection1Right: {
        flex: .2,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    bottomViewSection1RightText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#000000'
    },
    bottomViewSection2: {
        flex: 1,
        flexDirection: 'row',
        padding: 10
    },
    bottomViewSection2Left: {
        flex: .6,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    bottomViewSection2LeftText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#000000'
    },
    bottomViewSection2Right: {
        flex: .4,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    bottomViewSection2RightText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#000000'
    },
}) 

export default React.memo(CompletedList);