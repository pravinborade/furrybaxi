import moment from "moment";
import React, { useState } from "react";
import { Image, StyleSheet, Text, View, TouchableOpacity } from "react-native";
import Images from "../../../../../assets/Images/Images";
import { DATE_FORMATS } from "../../../../../utils/constants";
import { fSize } from "../../../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../../utils/typography";
import { cancelRide, getRideList } from "../../../../../redux/actions/rideAction";
import { useDispatch } from "react-redux";
import ReceiptModal from "./ReceiptModal";

const UpcomingList = (props) => {
    const {data} = props;
    const [modalVisible, setModalVisible] = useState(false);
    const dispatch = useDispatch()

    const onCancelRide = async() => {
        try {
            let resp = await cancelRide({id:data?.id})
            if(resp){
                setModalVisible(true)
                dispatch(getRideList())
            }
        } catch (error) {
            console.log(e)
        }
    }

    return(
        <View style={styles.container}>
            <View style={styles.topView}>
                <View style={styles.topViewLeft}>
                    <Text style={styles.topViewLeftText}>
                        {moment(data?.timestamp).format(DATE_FORMATS.DO_MMM_HH_MM_A)}
                        {/* {"14th Aug  10:00 AM"} */}
                    </Text>
                </View>
                <View style={styles.topViewRight}>
                    <Text style={styles.topViewRightText}>
                    {data?.currency}{data?.price}
                    </Text>
                </View>    
            </View>
            <View style={styles.middleView}>
                <View style={styles.middleViewSection1}>
                    <View style={styles.middleViewSection1Left}>
                        <Image source={Images.locationBlackIcon} style={styles.middleViewSection1LeftImage}/>
                    </View>
                    <View style={styles.middleViewSection1Right}>
                        <Text style={styles.middleViewSection1RightText}>
                            {data?.source_name}
                        </Text>
                    </View>
                </View>
                <View style={styles.middleViewSection2}/>
                <View style={styles.middleViewSection3}>
                    <View style={styles.middleViewSection3Left}>
                        <View style={styles.middleViewSection3LeftDotConatiner}>
                            <View style={styles.middleViewSection3LeftDot}/>    
                        </View>
                    </View>
                    <View style={styles.middleViewSection3Right}>
                        <Text style={styles.middleViewSection3RightText}>
                            {data?.destination_name}
                        </Text>
                    </View>
                </View>
            </View>
            <View style={styles.bottomView}>
                <Text style={styles.bottomViewText}>
                    {"Actual estimate to be provided before pick-up"}
                </Text>
                <View style={styles.bottomViewButtonContainer}>
                    <TouchableOpacity style={styles.bottomViewButton} onPress={()=>{onCancelRide()}}>
                        <Text style={styles.bottomViewButtonText}>{"Cancel Ride"}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            {
                modalVisible &&
                <ReceiptModal 
                    title={"Cancelled"}
                    message1={"Your ride has been cancelled."}
                    message2={""}
                    onOk={()=>{
                        setModalVisible(false)
                    }}
                />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#FFFFFF',
        borderRadius: 15,
        padding: 20,
        marginVertical: 10
    },
    topView:{
        flex: 1,
        flexDirection: 'row'
    },
    topViewLeft:{
        flex: 0.8,
        alignItems: 'flex-start'
    },
    topViewLeftText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#000000'
    },
    topViewRight:{
        flex: 0.2,
        alignItems: 'flex-end'
    },
    topViewRightText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: '#000000'
    },
    middleView:{
        flex: 4,
        paddingVertical: 20,
        borderBottomWidth:1,
        borderColor: '#D5D5D5' 
    },
    middleViewSection1:{
        flex: 3,
        flexDirection: 'row',
        paddingVertical: 10
    },
    middleViewSection1Left:{
        flex: 0.2,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    middleViewSection1LeftImage:{
        width: 20,
        height: 20
    },
    middleViewSection1Right:{
        flex: 0.8   ,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    middleViewSection1RightText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#000000'
    },
    middleViewSection2:{
        flex: 4,
        padding: 15,
        borderLeftWidth: 2,
        borderColor: '#000000',
        marginLeft: 8
    },
    middleViewSection3:{
        flex: 3,
        flexDirection: 'row',
        paddingVertical: 10
    },
    middleViewSection3Left:{
        flex: 0.2,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    middleViewSection3LeftDotConatiner: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: "#E2EAED",
        justifyContent: 'center',
        alignItems: 'center'
    },
    middleViewSection3LeftDot:{
        height: 12,
        width: 12,
        borderRadius: 6,
        backgroundColor: '#000000'
    },
    middleViewSection3Right:{
        flex: 0.8,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    middleViewSection3RightText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#000000'
    },
    bottomView:{
        flex: 5,
        paddingTop: 20,
    },
    bottomViewText:{
       fontFamily: REGULAR,
       fontSize: fSize(12),
       lineHeight: 20,
       color: "#000000"
    },
    bottomViewButtonContainer: {
        alignItems: 'flex-end',
        marginTop:10
    },
    bottomViewButton: {
        backgroundColor: '#9A9A9A',
        padding: 15,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bottomViewButtonText: {
        fontFamily: MEDIUM,
       fontSize: fSize(12),
       lineHeight: 15,
       color: "#EEEEEE"
    }
    
})

export default UpcomingList;