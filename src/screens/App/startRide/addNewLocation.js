import React, { useRef, useEffect, useState } from 'react';
import { View, StyleSheet, SafeAreaView, Text, FlatList, RefreshControl, TouchableOpacity, Image, TextInput } from "react-native"
import { useDispatch, useSelector } from 'react-redux';
import Images from '../../../assets/Images/Images';
import BackButton from '../../../component/BackButton';
import Header from '../../../component/Header';
import { getSavedSearchedLocationDetail, getSearchPlaces, setSearchResult } from '../../../redux/actions/mapAction';
import { setSavedLocationName, shouldStartRideFromMap } from '../../../redux/actions/ridingAction';
import { navigate } from '../../../root/navigation/rootNavigation';
import { fSize } from '../../../utils/Responsive';
import { ScreenNames } from '../../../utils/ScreenNames';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';
import CurrentLocation from '../component/CurrentLocationButtion';

const AddNewLocation = (props) => {
     const dispatch = useDispatch()
     const { savedPlaces, serviceLocationsResponse } = useSelector(state => state.riding)
     const {

          searchResult,
     } = useSelector(state => state.map)
     const [isRefreshing, setIsRefreshing] = useState(false);
     const [searchKeyword, setSearchKeyword] = useState('')

     const _onRefresh = () => {
          //setIsRefreshing(true);

          // dispatch(getPets()).then(() => {
          //      setIsRefreshing(false)
          // });
     };
     const headerLeft = () => {
          return <BackButton onPress={() => { props.navigation.goBack() }} />
     }

     const headerMiddle = () => {
          return <Text style={styles.headerTitle}>{'Add New Location'}</Text>
     }

     const saveLocationDetail = (item) => {
          dispatch(getSavedSearchedLocationDetail(item))
          navigate(ScreenNames.SAVE_LOCATION)
     }

     const renderItem = ({ item }) => {
          return (
               <TouchableOpacity onPress={() => saveLocationDetail(item)} style={styles.placeCard}>
                    <View style={styles.placeCardContainer}>
                         <View>
                              <Image source={Images.marker} style={styles.placeIcon} />
                         </View>
                         <View style={styles.placeInfo}>
                              <Text style={styles.placeInfoText1}>{item?.name}</Text>
                              <Text style={styles.placeInfoText2}>{`(${item?.address})`}</Text>

                         </View>
                    </View>
               </TouchableOpacity>
          );
     };

     useEffect(() => {
          dispatch(setSearchResult([]))
          dispatch(setSavedLocationName(''))
          setSearchKeyword('')
     }, [])


     useEffect(() => {
          if (searchKeyword && searchKeyword != '') {
               onSearchLocation(searchKeyword)
          } else {
               dispatch(setSearchResult([]))
          }
     }, [searchKeyword])

     const onSearchLocation = async (text) => {
          var str = text;
          var prefix = '';
          if (serviceLocationsResponse?.postal_town) {
               prefix = `${serviceLocationsResponse?.postal_town}, `
          }

          if (str.indexOf(prefix) === 0) {
               setSearchKeyword('')
          } else {
               if (prefix.indexOf(str) >= 0) {
                    setSearchKeyword(str);
               } else {
                    setSearchKeyword(str);
               }
          }

          if (str) {
               await dispatch(getSearchPlaces((prefix + str), serviceLocationsResponse?.country_short))
          } else {
               await dispatch(setSearchResult([]))

          }
     };

     const navigateToMap = () => {
          dispatch(shouldStartRideFromMap(false))
          navigate(ScreenNames.MAP_SEARCH)
     }

     const ListFooterComponent = () => {
          return (
               <TouchableOpacity onPress={() => navigateToMap()} style={styles.actionContainer2Section1BottomContainer}>
                    <View style={{ flexDirection: 'row' }}>
                         <Image source={Images.set_location} style={styles.actionContainer2Section1BottomContainerImage2} />
                         <Text style={styles.actionContainer2Section1BottomContainerTitle}>{'Set location on map'}</Text>
                    </View>
                    <Image source={Images.arrow_right} style={styles.actionContainer2Section1BottomContainerRightImage} />
               </TouchableOpacity>
          )
     }


     return (
          <SafeAreaView style={styles.container}>
               <Header cmHeight={'10%'} leftView={headerLeft} middleView={headerMiddle} />

               {/* <CurrentLocation
                    onPress={() => { }}
                    disable={false}
               /> */}
               <View style={{ paddingVertical: 10 }}>
                    <TextInput
                         placeholder="Search place"
                         style={styles.searchInput}
                         placeholderTextColor={"#ADADAE"}
                         value={searchKeyword}
                         caretHidden={false}
                         onChangeText={(text) => setSearchKeyword(text)}
                    />
                    <Image source={Images.search} style={styles.searchIcon} />
               </View>

               <FlatList
                    scrollEnabled
                    data={searchKeyword != '' ? searchResult : []}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    refreshControl={
                         <RefreshControl
                              refreshing={isRefreshing}
                              onRefresh={() => _onRefresh()}
                         />
                    }
                    ListFooterComponent={ListFooterComponent}
               />

          </SafeAreaView>
     )
}
const styles = StyleSheet.create({
     container: {
          flex: 1,
          backgroundColor: '#17202D',
          padding: 20
     },
     headerTitle: {
          fontSize: fSize(18),
          color: '#FDFDFD',
          fontFamily: MEDIUM,
          marginLeft: -30
     },
     placeCard: {

     },
     placeCardContainer: {
          flexDirection: 'row'

     },
     placeIcon: {
          height: 20,
          width: 14,
          margin: 15

     },
     placeInfo: {
          padding: 10,
          borderBottomWidth: 1,
          width: '100%',
          borderBottomColor: '#919498'

     },
     placeInfoText1: {
          color: '#E1CDA9',
          fontFamily: MEDIUM,
          fontSize: fSize(14)

     },
     placeInfoText2: {
          color: '#A58B6C',
          fontFamily: REGULAR,
          fontSize: fSize(12)
     },


     searchIcon: {
          position: 'absolute',
          right: 15,
          top: 30
     },
     searchInput: {
          backgroundColor: '#FFFFFF',
          height: 50,
          borderRadius: 15,
          paddingVertical: 5,
          paddingHorizontal: 15,
          fontSize: fSize(14),
          fontFamily: MEDIUM,
          color: "#364458"
     },
     actionContainer2Section1BottomContainer: {
          paddingVertical: 15,
          flexDirection: 'row',
          justifyContent: 'space-between'
     },
     actionContainer2Section1BottomContainerImage2: {
          height: 20,
          width: 15,
          marginHorizontal: 10
     },
     actionContainer2Section1BottomContainerTitle: {
          color: '#E1CDA9',
          fontSize: fSize(14),
          fontFamily: MEDIUM
     },
     actionContainer2Section1BottomContainerRightImage: {
          height: 15,
          width: 7,
          marginVertical: 5,
          //alignSelf:'flex-end'
     },
})
export default AddNewLocation;