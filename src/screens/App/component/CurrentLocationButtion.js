import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import{StyleSheet,TouchableOpacity,Text, View,Image} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { fSize } from '../../../utils/Responsive';
import { MEDIUM, REGULAR } from '../../../utils/typography';
import Images from '../../../assets/Images/Images';



const CurrentLocation = ({
    onPress,
    disable
  }) => {
    return (
        <TouchableOpacity style={styles.bottomContainer}  onPress={onPress} disabled={disable}>
            <View style={{flexDirection:'row'}}>
                <Image source={Images.currentLocation} style={{height:20,width:20}}/>
                <Text style={styles.buttonText}>
                    {'Current Location'}
                </Text>
            </View>
        </TouchableOpacity>
    );
  };

  const styles = StyleSheet.create({
    bottomContainer:{
        justifyContent: 'center',
        borderRadius: 15,
        backgroundColor: '#17202D',
        height: 50,
        paddingLeft:8,
        borderColor:'#253040',
        borderWidth:1,
        justifyContent: 'center'
    },
    buttonText:{
        color:'#F9D591',
        fontFamily:REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        marginLeft: 15,
        paddingVertical:5
    }
  })

  export default CurrentLocation;