const initialState = {
    stripeResponse: null,
    paymentSecretResponse: null
};

const paymentReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_STRIPE_RESPONSE':
            return {
                ...state,
                stripeResponse: action.response,
            };

        case 'SET_PAYMENT_SECRET_RESPONSE':
            return {
                ...state,
                paymentSecretResponse: action.response,
            };
            
        default:
            return state;
    }
};


export default paymentReducer;
