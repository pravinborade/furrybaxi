import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { AirbnbRating, Rating } from "react-native-ratings";
import Images from "../../../../../assets/Images/Images";
import { RIDE_STATUS } from "../../../../../utils/constants";
import { fSize } from "../../../../../utils/Responsive";
import { MEDIUM } from "../../../../../utils/typography";

const   DriverDetails = ({data}) => {
    const driver = data?.driver;
    const status = data?.status
    const rating = data?.ride?.ride_rating
    
    return (
        <View style={styles.container}>
            <View style={styles.containerLeft}>
                <Image source={Images.profile_image} style={styles.containerLeftImage} />
            </View>
            <View style={styles.containerRight}>
                <View style={styles.containerRightRow1}>
                    <Text style={styles.containerRightRow1Text}>
                        {driver?.first_name} {driver?.last_name}
                    </Text>
                </View>
                <View style={styles.containerRightRow2}>
                    <Text style={styles.containerRightRow2Text}>
                        {driver?.vehicle_no}
                        <Text style={styles.containerRightRow2Text2}>
                            {` / ${driver?.cab_name}`}
                        </Text>
                    </Text>
                </View>
                <View style={styles.containerRightRow3}>
                    <Text style={styles.containerRightRow3Text}>
                        { status === RIDE_STATUS.COMPLETED ?  "You Rated" : "Your ride was cancelled"}
                    </Text>
                    {
                        status === RIDE_STATUS.COMPLETED 
                        &&    
                        <AirbnbRating
                            type='custom'
                            count={5}
                            defaultRating={rating}
                            starImage={Images.star}
                            size={15}
                            showRating={false}
                            isDisabled={true}
                            selectedColor={"#A58B6C"}
                        />
                    }
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        padding: 10,
        borderRadius: 20,
        backgroundColor: "#E2EAED",
    },
    containerLeft: {
        flex: 0.30,
    },
    containerLeftImage: {
        width: '100%',
        height: '100%',
        resizeMode: 'cover',
        borderRadius: 10,
    },
    containerRight: {
        flex: 0.80,
        paddingHorizontal: 10
    },
    containerRightRow1: {
        flex: 0.25,
        paddingBottom: 10
    },
    containerRightRow1Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#364458"
    },
    containerRightRow2:{
        flex: 0.25,
    },
    containerRightRow2Text:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#364458"
    },
    containerRightRow2Text2:{
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#364458"
    },
    containerRightRow3:{
        flex: 0.45,
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingTop: 25
    },
    containerRightRow3Text:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#364458"
    },

})

export default DriverDetails;