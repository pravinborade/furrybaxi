import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { SafeAreaView } from "react-native-safe-area-context";
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';


import Header from "../../../component/Header";
import { fSize } from "../../../utils/Responsive";
import OtpInput from "../../../component/OtpInput";
import BackButton from "../../../component/BackButton";
import { useIsFocused } from "@react-navigation/native";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../utils/typography";
import { submitPhoneNumber } from "../../../redux/actions/phoneNumberAction";
import { setError, setOtp, submitOtp, resetState } from "../../../redux/actions/otpAction";
import { backListener, removeBackListener } from "../../../utils/backHandler";
import CustomGradientButton from "../../../component/CustomGradientButton";
import commanStyle from "../../../utils/commanStyle";

const Otp = (props) => {
    const dispatch = useDispatch();
    const { otp, isError } = useSelector(state => state.otp)
    const phoneNumberState = useSelector(state => state.phoneNumber)
    const otpRef = useRef(null)
    const isVisible = useIsFocused();

    useEffect(() => {
        if (isVisible) {
            resetInput()
            dispatch(resetState())
        }
        backListener()
        return () => {
            removeBackListener()
        };
    }, [isVisible])

    const onChangeOtp = (e) => {
        dispatch(setError(false))
        dispatch(setOtp(e))
    }

    const checkIfOtpValid = async () => {
        let inputVariable = {
            "otp_id": phoneNumberState?.response?.otp_id,
            "otp": otp
        }
        dispatch(submitOtp(inputVariable));
    }

    const resendOtp = () => {
        resetInput()
        dispatch(resetState())
        let inputVar = {
            phone: phoneNumberState?.phoneNumber,
            country_code: phoneNumberState?.country?.value
        }
        dispatch(submitPhoneNumber(inputVar))
    }

    const resetInput = () => {
        otpRef.current.reset();
    }

    const headerLeft = () => {
        return <BackButton onPress={() => {
            dispatch(setOtp(''))
            props.navigation.goBack()
        }} />
    }

    return (
        <SafeAreaView style={styles.container}>
            <Header
                leftView={headerLeft}
            />
            <View style={styles.content}>
                <View style={styles.contentSection1}>
                    {
                        !isError
                            ?
                            <Text style={styles.contentSection1Text1}>{"Please Enter the code"}</Text>
                            :
                            <Text style={styles.contentSection1ErrorText}>{"You entered a wrong code, \nPlease enter the correct one"}</Text>
                    }
                    <Text style={styles.contentSection1Text2}>{
                        `A 4 digit code is sent to +${phoneNumberState?.country?.value} ${phoneNumberState?.phoneNumber}`}
                    </Text>
                </View>
                <View style={styles.contentSection2}>
                    <OtpInput
                        otpRef={otpRef}
                        otp={otp}
                        onChangeOtp={(e) => { onChangeOtp(e) }}
                        isError={isError}
                    />
                </View>
                <View style={styles.contentSection3}>
                    <TouchableOpacity onPress={() => { resendOtp() }}>
                        <Text style={styles.contentSection3Text1}>
                            {"I haven’t received the code, Resend it Again >"}
                        </Text>
                        <View style={styles.contentSection3Border} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.footer}>
                <CustomGradientButton
                    title={'Next'}
                    onPress={() => otp?.length < 4 || isError ? {} : checkIfOtpValid()}
                    disable={false}
                    gradientColor={otp?.length < 4 || isError ? ['#253040', '#17202D'] : ['#DAC5A2', '#A98C6F']}
                    styleContainer={commanStyle.buttonContainer}
                    textStyle={otp?.length < 4 || isError ? commanStyle.darkButtonText : commanStyle.primaryButtonText}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#17202D',
        flex: 1,
        padding: 20
    },
    content: {
        height: "60%",
    },
    footer: {
        height: "20%",
        justifyContent: 'flex-end'
    },
    contentSection1: {
        minHeight: "5%",
    },
    contentSection2: {
        minHeight: "15%",
    },
    contentSection3: {
        minHeight: "10%",
    },
    contentSection1Text1: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: '#E1CDA9',
        paddingBottom: 10
    },
    contentSection1ErrorText: {
        fontFamily: MEDIUM,
        fontSize: fSize(20),
        lineHeight: 24,
        color: '#FF7373',
        marginBottom: 5
    },
    contentSection1Text2: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#A58B6C',
    },
    contentSection3Text1: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 15,
        color: '#D1DBEA',
    },
    contentSection3Border: {
        borderBottomColor: '#596475',
        borderBottomWidth: 0.8,
        marginVertical: 5,
        width: '80%',
        alignSelf: 'flex-start'
    },

})

export default Otp;