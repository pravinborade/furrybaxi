import { Alert, BackHandler } from 'react-native';
import {check,request, openSettings, PERMISSIONS, RESULTS} from 'react-native-permissions';
import { checkCurrentLocationValid } from './ridingAction';
import Geolocation from 'react-native-geolocation-service';


export function setLocationAccess(isLocationAccess){
    return{
        type:"SET_LOCATION_ACCESS",
        payload:isLocationAccess
    }
}

export function setLocationCurrent(location){
    return{
        type:"SET_CURRENT_LOCATION",
        payload: location
    }
}

export function setConfigurations(data){
    return{
        type:"SET_CONFIGURATIONS",
        payload: data
    }
}

export function checkLocationPermission () {
    return async (dispatch) => {
    let permission = (Platform.OS === 'ios' ? PERMISSIONS.IOS.LOCATION_ALWAYS : PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)
    check(permission).then((result) => {
        if(result === 'granted'){
            dispatch(setLocationAccess(true))
        }else{
             if(Platform.OS === 'android'){
                 if(result[PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION] !== 'granted'){
                     request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION).then((result) => {
                         if(result !== 'granted'){
                             Alert.alert(
                                 "Location Permission",
                                 "Please give the location access",
                                 [
                                     {
                                         text: "Exit",
                                         onPress: () => BackHandler.exitApp(),
                                         style: "cancel"
                                     },
                                     { text: "OK", onPress: () => openSettings()}
                                 ]
                             );
                         }else{
                            dispatch(setLocationAccess(true))
                         }
                     });
                 }
             }else{
                 if(result[PERMISSIONS.IOS.LOCATION_ALWAYS] !== 'granted'){
                     request(PERMISSIONS.IOS.LOCATION_ALWAYS).then((result) => {
                         if(result !== 'granted'){
                             Alert.alert(
                                 "Location Permission",
                                 "Please give the location access",
                                 [
                                     {
                                         text: "Exit",
                                         onPress: () => BackHandler.exitApp(),
                                         style: "cancel"
                                     },
                                     { text: "OK", onPress: () => openSettings()}
                                 ]
                             );
                         }else{
                            dispatch(setLocationAccess(true))
                         }
                     });
                 }
             }            
         }
        
     })
     .catch((error) => {
         console.log(error)
     });
    }
}

export function getCurrentLocation() {   
    return async (dispatch) => {
        Geolocation.getCurrentPosition(
            (position) => {
                let data = {
                    latitude: position?.coords?.latitude,
                    longitude: position?.coords?.longitude
                }              
                dispatch(setLocationCurrent(data))
                dispatch(checkCurrentLocationValid(data))
            },
            (error) => {
              // See error code charts below.
              console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    }
    
}
