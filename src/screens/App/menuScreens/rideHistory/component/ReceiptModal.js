import React, { useState } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View, Touchable, TouchableOpacity } from "react-native";
import { fSize } from "../../../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../../utils/typography";

const ReceiptModal = (props) => {
    const {title, message1, message2, onOk} = props;
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={true}
            onRequestClose={onOk}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <View style={styles.section1}>
                        <Text style={styles.section1Text}>
                            {title}
                        </Text>
                    </View>
                    <View style={styles.section2}>
                        <Text style={styles.section2Text}>
                            {message1}
                        </Text>
                        <Text style={styles.section2Text}>
                            {message2}
                        </Text>
                    </View>
                    <View style={styles.section3}>
                        <TouchableOpacity style={styles.section3Button} onPress={onOk}>
                            <Text style={styles.section3ButtonText}>
                                {"Okay"}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal> 
    );
};

const styles = StyleSheet.create({
    
    centeredView: {
        flex:1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor:'rgba(0,0,0,0.6)'
    },
    modalView: {
        backgroundColor: "#253040",
        borderRadius: 20,
        padding: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: '90%'
    },
    section1: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    section1Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#E1CDA9",
        textAlign: 'center'
    },
    section2: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    section2Text: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 22,
        color: "#F5EDDC",
        paddingVertical: 15,
        textAlign: 'center'
    },
    section3: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 10
    },
    section3Button:{
        paddingVertical: 10,
        paddingHorizontal: 40,
        borderWidth: 2,
        borderColor: '#9D7E62',
        borderRadius: 15
    },
    section3ButtonText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 22,
        color: "#E1CDA9",
    }
});

export default ReceiptModal;
