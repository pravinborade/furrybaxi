import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ScreenNames } from '../../utils/ScreenNames';
import { screenOption } from './constants/options';
import PhoneNumber from '../../screens/Auth/phoneNumber/phoneNumber';

import Home from '../../screens/home';
import Otp from '../../screens/Auth/otp/otp';
import Account from '../../screens/Auth/account/account';
import CreatePetProfile from '../../screens/Auth/pet/createPetProfile';
import PetProfile from '../../screens/Auth/pet/petProfile';
import PetPicture from '../../screens/Auth/pet/petPicture';
import Payment from '../../screens/Auth/payment/payment';
import CardPayment from '../../screens/Auth/payment/cardPayment';
import StartRide from '../../screens/App/startRide/startRide';
import TypesOfRide from '../../screens/App/typesOfRide/typesOfRide';
import PickupConfirmation from '../../screens/App/pickupConfirmation';
import ConfirmingRide from '../../screens/App/confirmingRide';
import RideConfirming from '../../screens/App/rideConfirming';
import RideArriving from '../../screens/App/rideArriving';
import OnRide from '../../screens/App/onRide';
import Feedback from '../../screens/App/feedback';
import RideBooked from '../../screens/App/rideBooked';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { navigate, resetNavigation } from './rootNavigation';
import PrivacyPolicy from '../../screens/App/menuScreens/privacyPolicy';
import TermsAndConditions from '../../screens/App/menuScreens/termsAndConditions';
import EditPhone from '../../screens/App/menuScreens/phone/editPhone';
import EditOtp from '../../screens/App/menuScreens/phone/editOtp';

import AccountProfile from '../../screens/App/menuScreens/account/accountProfile';
import AccountProfileDetails from '../../screens/App/menuScreens/account/accountProfileDetails';
import TripList from '../../screens/App/menuScreens/rideHistory/tripList';
import TripDetails from '../../screens/App/menuScreens/rideHistory/tripDetails';
import TripReceipt from '../../screens/App/menuScreens/rideHistory/tripReceipt';
import TripHelp from '../../screens/App/menuScreens/rideHistory/tripHelp';

import PetDetails from '../../screens/App/menuScreens/pets/petDetails';
import EditPets from '../../screens/App/menuScreens/pets/editPets';
import AccountPicture from '../../screens/App/menuScreens/account/accountPicture';
import PetList from '../../screens/App/menuScreens/pets/petList';
import EditPetPicture from '../../screens/App/menuScreens/pets/editPetPicture';


// import Sidebar from '../../screens/App/component/Sidebar';
import MapSearch from '../../screens/App/startRide/mapSearch';
import AddNewLocation from '../../screens/App/startRide/addNewLocation';
import SaveLocation from '../../screens/App/startRide/saveLocation';

import SavedLocationList from '../../screens/App/startRide/savedLocationList';

import FindLostItem from '../../screens/App/menuScreens/rideHistory/findLostItem';
import TripReport from '../../screens/App/menuScreens/rideHistory/tripReport';
import ReportAccident from '../../screens/App/menuScreens/rideHistory/reportScreens/reportAccident';
import ReportDriverBehavior from '../../screens/App/menuScreens/rideHistory/reportScreens/reportDriverBehavior';
import ReportVehicleDifferent from '../../screens/App/menuScreens/rideHistory/reportScreens/reportVehicleDifferent';
import ReportFaceMask from '../../screens/App/menuScreens/rideHistory/reportScreens/reportFaceMask';
import ReportDriveDangerous from '../../screens/App/menuScreens/rideHistory/reportScreens/reportDriveDangerous';
import ReportExtraCash from '../../screens/App/menuScreens/rideHistory/reportScreens/reportExtraCash';
import ReportPetCage from '../../screens/App/menuScreens/rideHistory/reportScreens/reportPetCage';
import ReportPaymentRelated from '../../screens/App/menuScreens/rideHistory/reportScreens/reportPaymentRelated';


const Stack = createNativeStackNavigator();

export const Routes = () => {
    const { token, isRegistered } = useSelector(state => state.auth)
    useEffect(() => {
        if (token !== null && isRegistered !== null) {
            resetNavigation(ScreenNames.START_RIDE)
        }
    }, [token, isRegistered])


    return (
        <Stack.Navigator
            headerMode="none"
            initialRouteName={ScreenNames.HOME}
            screenOptions={screenOption}
        >
            <Stack.Screen name={ScreenNames.HOME} component={Home} />
            <Stack.Screen name={ScreenNames.PHONE_NUMBER} component={PhoneNumber} />
            <Stack.Screen name={ScreenNames.OTP} component={Otp} />
            <Stack.Screen name={ScreenNames.ACCOUNT} component={Account} />
            <Stack.Screen name={ScreenNames.CREATE_PET_PROFILE} component={CreatePetProfile} />
            <Stack.Screen name={ScreenNames.PET_PROFILE} component={PetProfile} />
            <Stack.Screen name={ScreenNames.PET_PICTURE} component={PetPicture} />
            <Stack.Screen name={ScreenNames.PAYMENT} component={Payment} />
            <Stack.Screen name={ScreenNames.CARD_PAYMENT} component={CardPayment} />
            <Stack.Screen name={ScreenNames.START_RIDE} component={StartRide} />
            <Stack.Screen name={ScreenNames.SAVED_LOCATION_LIST} component={SavedLocationList} />
            <Stack.Screen name={ScreenNames.MAP_SEARCH} component={MapSearch} />
            <Stack.Screen name={ScreenNames.ADD_NEW_LOCATION} component={AddNewLocation} />
            <Stack.Screen name={ScreenNames.SAVE_LOCATION} component={SaveLocation} />

            <Stack.Screen name={ScreenNames.TYPE_OF_RIDES} component={TypesOfRide} />
            <Stack.Screen name={ScreenNames.PICKUP_CONFIRMATION} component={PickupConfirmation} />
            <Stack.Screen name={ScreenNames.CONFIRMING_RIDE} component={ConfirmingRide} />
            <Stack.Screen name={ScreenNames.RIDE_ARRIVING} component={RideArriving} />
            <Stack.Screen name={ScreenNames.RIDE_CONFIRMING} component={RideConfirming} />
            <Stack.Screen name={ScreenNames.ON_RIDE} component={OnRide} />
            <Stack.Screen name={ScreenNames.FEEDBACK} component={Feedback} />
            <Stack.Screen name={ScreenNames.RIDE_BOOKED} component={RideBooked} />

            <Stack.Screen name={ScreenNames.PRIVACY_POLICY} component={PrivacyPolicy} />
            <Stack.Screen name={ScreenNames.TERMS_AND_CONDITIONS} component={TermsAndConditions} />
            <Stack.Screen name={ScreenNames.EDIT_PHONE} component={EditPhone} />
            <Stack.Screen name={ScreenNames.EDIT_OTP} component={EditOtp} />
            <Stack.Screen name={ScreenNames.TRIP_LIST} component={TripList} />
            <Stack.Screen name={ScreenNames.TRIP_DETAILS} component={TripDetails} />
            <Stack.Screen name={ScreenNames.TRIP_RECEIPT} component={TripReceipt} />
            <Stack.Screen name={ScreenNames.TRIP_HELP} component={TripHelp} />
            <Stack.Screen name={ScreenNames.ACCOUNT_PROFILE_DETAIL} component={AccountProfileDetails} />
            <Stack.Screen name={ScreenNames.ACCOUNT_PROFILE} component={AccountProfile} />
            <Stack.Screen name={ScreenNames.PET_LIST} component={PetList} />
            <Stack.Screen name={ScreenNames.PET_DETAILS} component={PetDetails} />
            <Stack.Screen name={ScreenNames.EDIT_PETS} component={EditPets} />
            <Stack.Screen name={ScreenNames.ACCOUNT_PICTURE} component={AccountPicture} />
            <Stack.Screen name={ScreenNames.EDIT_PET_PICTURE} component={EditPetPicture} />
            <Stack.Screen name={ScreenNames.FIND_LOST_ITEM} component={FindLostItem} />
            <Stack.Screen name={ScreenNames.TRIP_REPORT} component={TripReport} />
            
            <Stack.Screen name={ScreenNames.REPORT_ACCIDENT} component={ReportAccident} />
            <Stack.Screen name={ScreenNames.REPORT_DRIVER_BEHAVIOR} component={ReportDriverBehavior} />
            <Stack.Screen name={ScreenNames.REPORT_DRIVE_DANGEROUS} component={ReportDriveDangerous} />
            <Stack.Screen name={ScreenNames.REPORT_VEHICLE_DIFFERENT} component={ReportVehicleDifferent} />
            <Stack.Screen name={ScreenNames.REPORT_FACE_MASK} component={ReportFaceMask} />
            <Stack.Screen name={ScreenNames.REPORT_EXTRA_CASH} component={ReportExtraCash} />
            <Stack.Screen name={ScreenNames.REPORT_PET_CAGE} component={ReportPetCage} />
            <Stack.Screen name={ScreenNames.REPORT_PAYMENT_RELATED} component={ReportPaymentRelated} />
            
    
        </Stack.Navigator>
    )
}