export const ScreenNames = {
    HOME: 'HOME',
    PHONE_NUMBER:'PHONE_NUMBER',
    OTP: 'OTP',
    ACCOUNT:'ACCOUNT',
    PET_PROFILE:'PET_PROFILE',
    PET_PICTURE:'PET_PICTURE',
    CREATE_PET_PROFILE:'CREATE_PET_PROFILE',
    PAYMENT:'PAYMENT',
    CARD_PAYMENT:'CARD_PAYMENT',
    START_RIDE:'START_RIDE',
    SAVED_LOCATION_LIST:'SAVED_LOCATION_LIST',
    MAP_SEARCH:'MAP_SEARCH',
    ADD_NEW_LOCATION:'ADD_NEW_LOCATION',
    SAVE_LOCATION:'SAVE_LOCATION',
    TYPE_OF_RIDES: 'TYPE_OF_RIDES',
    PICKUP_CONFIRMATION: 'PICKUP_CONFIRMATION',
    CONFIRMING_RIDE: 'CONFIRMING_RIDE',
    RIDE_ARRIVING: 'RIDE_ARRIVING',
    RIDE_CONFIRMING: 'RIDE_CONFIRMING',
    ON_RIDE: 'ON_RIDE',
    FEEDBACK: 'FEEDBACK',
    RIDE_BOOKED: "RIDE_BOOKED",
    PRIVACY_POLICY: "PRIVACY_POLICY",
    TERMS_AND_CONDITIONS: "TERMS_AND_CONDITIONS",
    EDIT_PROFILE_DETAIL: "EDIT_PROFILE_DETAIL",
    EDIT_PHONE: "EDIT_PHONE",
    EDIT_OTP: "EDIT_OTP",
    TRIP_LIST: "TRIP_LIST",
    TRIP_DETAILS: "TRIP_DETAILS",
    TRIP_RECEIPT: "TRIP_RECEIPT",
    TRIP_HELP: "TRIP_HELP",
    ACCOUNT_PROFILE_DETAIL: "ACCOUNT_PROFILE_DETAIL",
    ACCOUNT_PROFILE: "ACCOUNT_PROFILE",
    PET_LIST:'PET_LIST',
    PET_DETAILS: "PET_DETAILS",
    EDIT_PETS: "EDIT_PETS",
    ACCOUNT_PICTURE: "ACCOUNT_PICTURE",
    EDIT_PET_PICTURE: "EDIT_PET_PICTURE",
    
    FIND_LOST_ITEM: "FIND_LOST_ITEM",
    TRIP_REPORT: "TRIP_REPORT",
    REPORT_ACCIDENT: "REPORT_ACCIDENT",
    REPORT_DRIVER_BEHAVIOR: "REPORT_DRIVER_BEHAVIOR",
    REPORT_DRIVE_DANGEROUS: "REPORT_DRIVE_DANGEROUS",
    REPORT_VEHICLE_DIFFERENT: "REPORT_VEHICLE_DIFFERENT",
    REPORT_FACE_MASK: "REPORT_FACE_MASK",
    REPORT_EXTRA_CASH: "REPORT_EXTRA_CASH",
    REPORT_PET_CAGE: "REPORT_PET_CAGE",
    REPORT_PAYMENT_RELATED: "REPORT_PAYMENT_RELATED",
};
    