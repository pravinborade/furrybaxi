
const initialState = {
    rideListResponse: null,
    rideDetailsResponse: null,
    ridePaymentResponse: null,
    isSafetyModalVisible: false
};

const rideReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_RIDE_LIST_RESPONSE':
            return {
                ...state,
                rideListResponse: { ...action.response },
            };

        case 'SET_RIDE_DETAILS_RESPONSE':
            return {
                ...state,
                rideDetailsResponse: { ...action.response },
            };

        case 'SET_RIDE_PAYMENT_RESPONSE':
            return {
                ...state,
                ridePaymentResponse: { ...action.response },
            };

        case 'SET_SAFETY_MODAL':      
            return {
                ...state,
                isSafetyModalVisible: action.isSafetyModalVisible,
            }


        default:
            return state;
    }
};

export default rideReducer;

