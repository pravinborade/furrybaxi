
import React, { useEffect, useState } from 'react';


import Header from '../../../../component/Header';
import { useDispatch, useSelector } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, Dimensions, Image, BackHandler, SafeAreaView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


import { fSize } from '../../../../utils/Responsive';
import Images from '../../../../assets/Images/Images';
import BackButton from '../../../../component/BackButton';
import SkipButton from '../../../../component/SkipButton';
import { ScreenNames } from '../../../../utils/ScreenNames';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../../utils/typography';
import { backListener, removeBackListener } from '../../../../utils/backHandler';
import CustomButton from '../../../../component/CustomButton';
import { deletePet } from '../../../../redux/actions/petEditAction';

const PetDetails = (props) => {

    React.useEffect(() => {
        backListener()
        return () => {
            removeBackListener()
        };
    }, []);
     const dispatch = useDispatch()

    const { petProfile } = useSelector((state) => state.petEdit)
   

    const headerLeft = () => {
        return <BackButton onPress={() => { props.navigation.goBack() }} />
    }

    const headerRight = () => {
        return <SkipButton title={'Edit Profile'} onPress={() => { props.navigation.navigate(ScreenNames.EDIT_PETS) }} />
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={{ padding: 20, backgroundColor: '#253040', }}>
                <Header
                    bgColor={'#253040'}
                    leftView={headerLeft}
                    rightView={headerRight}
                />
            </View>
            <View style={styles.container1} >
                <View style={styles.pofileContainer}>
                    {
                        petProfile && petProfile?.pic ?
                            <Image source={{ uri: petProfile?.pic }} style={styles.profileImage} />
                            :
                            <Image source={Images.pet_profile_placeholder} style={styles.profileImage} />
                    }
                    <Text style={styles.profileTitle1}>{`${petProfile?.name}`}</Text>
                    <Text style={styles.profileTitle2}>{`(${petProfile?.category?.name})`}</Text>
                </View>
                <View style={styles.infoContainer}>
                    <View style={styles.intoTitleValueContainer}>
                        <Text style={styles.infoTitle}>{'Breed'}</Text>
                        <Text style={styles.infoTitleValue}>{`${petProfile?.breed?.name}`}</Text>
                    </View>
                    <View style={styles.intoTitleValueContainer}>
                        <Text style={styles.infoTitle}>{'Size'}</Text>
                        <Text style={styles.infoTitleValue}>{`${petProfile?.size?.name}`}</Text>
                    </View>
                    <View style={styles.intoTitleValueContainer}>
                        <Text style={styles.infoTitle}>{'Date of Birth'}</Text>
                        <Text style={styles.infoTitleValue}>{`${petProfile?.dob}`}</Text>
                    </View>
                    <View style={styles.intoTitleValueContainer}>
                        <Text style={styles.infoTitle}>{'About'}</Text>
                        <Text style={styles.infoTitleValueAbout}>{`${petProfile?.about}`}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.footer}>
                <CustomButton
                    onPress={() =>{dispatch(deletePet(petProfile.pid)) }}
                    styleContainer={styles.buttonContainer}
                    textStyle={styles.buttonText}
                    title={'Delete'}
                />
            </View>

        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#17202D',
        flex: 1,
    },
    container1: {
        flex: 1,
        padding: 20,
    },
    pofileContainer: {
        alignItems: 'center',
        position: 'absolute',
        top: -hp(13),
        right: wp(30),
        zIndex: 1
    },
    profileImage: {
        borderRadius: 25,
        width: wp(40),
        aspectRatio: 1,
    },
    profileTitle1: {
        color: '#F9D591',
        fontSize: fSize(22),
        fontFamily: SEMI_BOLD
    },
    profileTitle2: {
        color: '#E1CDA9',
        fontSize: fSize(16),
        fontFamily: MEDIUM
    },
    infoContainer: {
        backgroundColor: '#253040',
        borderRadius: 20,
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginTop: hp(18)
    },
    intoTitleValueContainer: {
        paddingVertical: 5
    },
    infoTitle: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        color: '#B59B7C'
    },
    infoTitleValue: {
        fontFamily: MEDIUM,
        fontSize: fSize(15),
        color: '#F5EDDC'
    },
    infoTitleValueAbout: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        color: '#F5EDDC'
    },
    footer: {
        paddingHorizontal: 20,
        justifyContent: 'flex-end'
    },
    buttonContainer: {
        height: 54,
        borderColor: '#253040',
        borderWidth: 2,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        marginVertical: 20
    },
    buttonText: {
        color: '#E1CDA9',
        fontSize: fSize(14),
        fontFamily: MEDIUM
    },

});

export default PetDetails;
