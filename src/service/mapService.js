import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { GoogleCloudKey } from '../config';


export const searchPlaces = async (text, searchableLocation) => {
    let countiryString = searchableLocation ? `country:${searchableLocation}` : '';
    return axios
    .request({
        method: 'post',
        url: `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${GoogleCloudKey}&input=${text}&components=${countiryString}`,
    })
    .then((response) => {
        return Promise.resolve(response)
    })
    .catch((err) => {
        console.log(JSON.stringify(err))
        return Promise.reject(err);
    });
};


export const selectPlace = async (placeID) => {
    return axios
    .request({
        method: 'post',
        url: `https://maps.googleapis.com/maps/api/place/details/json?place_id=${placeID}&fields=formatted_address%2Cgeometry&key=${GoogleCloudKey}`,
    })
    .then((response) => {
        return Promise.resolve(response)
    })
    .catch((err) => {
        console.log(JSON.stringify(err))
        return Promise.reject(err);
    });
}

export const currentLocation = async () => {
    return axios
    .request({
        method: 'post',
        url: `https://www.googleapis.com/geolocation/v1/geolocate?key=${GoogleCloudKey}`,
    })
    .then((response) => {
        return Promise.resolve(response)
    })
    .catch((err) => {
        console.log(JSON.stringify(err))
        return Promise.reject(err)
    });
}

export const locationDetails = async (latlng) => {
    return axios
    .request({
        method: 'post',
        url: `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${GoogleCloudKey}`,
    })
    .then((response) => {
        return Promise.resolve(response)
    })
    .catch((err) => {
        console.log(JSON.stringify(err))
        return Promise.reject(err)
    });
}



// export const socketConnection  = async (requestData) => {
//     const token =  await AsyncStorage.getItem('token');             
//     return new Promise(function(resolve, reject) {
//         var ws = new WebSocket(`ws://socket.furrybaxi.ml/fb/ws/user?token=${token}`);
//         ws.onopen = () => {
//             // connection opened
//             // console.log("onOpen");
//             // console.log(requestData,token)
//             ws.send(JSON.stringify(requestData)) // send a message
//         };
        
//         ws.onmessage = (e) => {
//             // a message was received
//             // console.log("onMessage",e.data);
//             // console.log((e.data))
//             // console.log(e.data)
//             return resolve(e.data)
//         };
        
//         ws.onerror = (e) => {
//             // an error occurred
//             console.log("onError",e.message);
//             return reject(e.message)
//         };

//         ws.onclose = (e) => {
//             // connection closed
//             console.log("onClose",e.code, e.reason);
//             resolve(requestData)
//             return reject(e)
//         };

//     });

    
    
// }