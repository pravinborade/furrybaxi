import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from "react-native";
import { fSize } from '../../../../../../utils/Responsive';
import { MEDIUM } from '../../../../../../utils/typography';
const ReportFooter = ({comment, onSubmit}) => {
    return(
        <>
            {
                comment ?
                <TouchableOpacity style={styles.footerButtonActive} onPress={() => { onSubmit() }}>
                    <Text style={styles.footerButtonTextActive}>
                        {"Submit"}
                    </Text>
                </TouchableOpacity> :
                <TouchableOpacity style={styles.footerButton} disabled={true}>
                    <Text style={styles.footerButtonText}>
                        {"Submit"}
                    </Text>
                </TouchableOpacity>
            }
        </>
    )
}

const styles = StyleSheet.create({
    footerButton: {
        borderWidth: 2,
        borderColor: '#253040',
        borderRadius: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        width: '100%',
        backgroundColor: '#253040'
    },
    footerButtonText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#5E718A'
    },
    footerButtonActive: {
        borderWidth: 2,
        borderColor: '#9D7E62',
        borderRadius: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        width: '100%'
    },
    footerButtonTextActive: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#E1CDA9'
    }
})

export default ReportFooter;