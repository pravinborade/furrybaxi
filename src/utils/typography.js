 export const BOLD = 'Montserrat-Bold'//font-weight-700
 export const SEMI_BOLD = 'Montserrat-SemiBold'//font-weight-600
 export const MEDIUM = 'Montserrat-Medium'//font-weight-500
 export const REGULAR = 'Montserrat-Regular'//font-weight-400
