import {Alert} from 'react-native';
import { socketConnection } from "../../service/mapService";
import appService from '../../service/appService'
import { getLocationDetails } from "./mapAction";
import { navigate, navigationRef } from "../../root/navigation/rootNavigation";
import { ScreenNames } from "../../utils/ScreenNames";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { store } from '../store/store'
import { clearToAsyncStorage, setUserResponse } from "./authAction";
import { getCurrentLocation, setConfigurations } from "./homeAction";
export function setGoogleInputFoucused(isFocused) {
    return {
        type: 'SET_GOOGLE_INPUT_FOCUSED',
        isFocused
    };
}

export function rideInProgress(isRideInProgress) {
    return {
        type: 'RIDE_IN_PROGRESS',
        isRideInProgress
    };
}

export function setUserLocations(locations) {
    return {
        type: 'SET_USER_LOCATIONS',
        locations
    };
}

export function setAvailableCabsResponse(response) {
    return {
        type: 'SET_AVAILABLE_CABS_RESPONSE',
        response
    };
}

export function setAvailableCabsLoading(loading) {
    return {
        type: 'SET_AVAILABLE_CABS_LOADING',
        loading
    };
}

export function setServiceLocationResponse(response) {
    return {
        type: 'SET_SERVICE_LOCATION_RESPONSE',
        response
    };
}

export function setSelectedCab(cab) {
    return {
        type: 'SET_SELECTED_CAB',
        cab
    };
}

export function setSelectedCabResponse(response) {
    return {
        type: 'SET_SELECTED_CAB_RESPONSE',
        response
    };
}

export function setDurationResponse(response) {
    return {
        type: 'SET_DURATION_RESPONSE',
        response
    };
}

export function setPickupOtpResponse(response) {
    return {
        type: 'SET_PICKUP_OTP_RESPONSE',
        response
    };
}

export function setScheduleDate(date) {
    return {
        type: 'SET_SCHEDULE_DATE',
        date
    };
}

export function setIsCalendar(flag) {
    return {
        type: 'SET_IS_CALENDAR',
        flag
    };
}

export function setScheduleCabResponse(response) {
    return {
        type: 'SET_SCHEDULED_CAB_RESPONSE',
        response
    };
}

export function setSelectedCabLoading(loading) {
    return {
        type: 'SET_SELECTED_CAB_LOADING',
        loading
    };
}

export function setUserTripReponse(response) {
    return {
        type: 'SET_USER_TRIP_RESPONSE',
        response
    };
}

export function setUserOnRideReponse(response) {
    return {
        type: 'SET_USER_ON_RIDE_RESPONSE',
        response
    };
}

export function setSummaryResponse(response) {
    return {
        type: 'SET_SUMMARY_RESPONSE',
        response
    };
}

export function resetState() {
    return {
        type: 'RESET_STATE',

    };
}

export function setHumanPets(pets) {
    return {
        type: 'SET_HUMAN_PETS',
        pets
    };
}

export function selectPet(pet) {
    return {
        type: 'SELECT_PET',
        pet
    };
}

export function incrementHumanCounter() {
    return {
        type: 'INCREMENT_HUMAN_COUNT',
    };
}

export function decrementHumanCounter() {
    return {
        type: 'DECREMENT_HUMAN_COUNT',
    };
}

export function setHumanData(humanData) {
    return {
        type: 'SET_HUMAN_DATA',
        humanData
    };
}

export function setCapacityError(isCapacityError) {
    return {
        type: 'SET_CAPACITY_ERROR',
        isCapacityError
    };
}

export function setSaveLocationDetail(saveLocationDetail) {
    return {
        type: 'SET_SAVE_LOCATION_DETAIL',
        saveLocationDetail
    };
}


export function setSavedLocationName(saveLocationName) {
    return {
        type: 'SET_SAVED_LOCATION_NAME',
        saveLocationName
    };
}

export function shouldStartRideFromMap(shouldStartRideFromMap) {
    return {
        type: 'SHOULD_START_RIDE_FROM_MAP',
        shouldStartRideFromMap
    };
}

export function setConfirmed(isConfirmed) {
    return {
        type: 'SET_IS_CONFIRM',
        isConfirmed
    };
}

export function setActiveKey(activeKey) {
    return {
        type: 'SET_ACTIVE_KEY',
        activeKey
    };
}

export function setWebSocketConnection(ws) {
    return {
        type: 'SET_WEB_SOCKET_CONNECT',
        ws
    };
}


export function checkCurrentLocationValid(data) {
    return async (dispatch) => {
        return appService.post('fb/v1/locations/check', data)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                let latLng = data?.latitude + ',' + data.longitude
                dispatch(setServiceLocationResponse(response.data))
                dispatch(setUserResponse({ data: response.user }))
                dispatch(setConfigurations(response.configs))
                return dispatch(getLocationDetails(latLng))
            })
            .catch((error) => {
                console.log(error.response.data)
                dispatch(setServiceLocationResponse(null))
            });
    };
}


export function getUserLocations() {
    return async (dispatch) => {
        return appService.get('fb/v1/accounts/user/locations')
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setUserLocations(response))
            })
            .catch((error) => {
                dispatch(setUserLocations(null))
                console.log(error)
            });
    };
}

export function saveSavedLocations(data) {
    return async (dispatch) => {
        return appService.post('fb/v1/accounts/user/locations', data)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setUserLocations(response))
                navigate(ScreenNames.SAVED_LOCATION_LIST)
            })
            .catch((error) => {
                console.log(error)
            });
    };
}

export function saveScheduleTrips(requestData) {
    return async (dispatch) => {
        return appService.post('fb/v1/trips/schedule', requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setScheduleCabResponse(response))
                navigate(ScreenNames.RIDE_BOOKED)
            })
            .catch((error) => {
                console.log(error)
                dispatch(setScheduleCabResponse(null))
            });
    };
}

export function getScheduledAvailableCab(data) {
    let requestData = {
        scheduled_date_time: data,
    }
    return async (dispatch) => {
        dispatch(setAvailableCabsLoading(true))
        return appService.post('fb/v1/trips/schedule/cabs', requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setSelectedCab())
                dispatch(setAvailableCabsResponse(response))
                dispatch(setScheduleDate(data))
                dispatch(setIsCalendar(false))
                dispatch(setAvailableCabsLoading(false))
            })
            .catch((error) => {
                console.log(error)
                dispatch(setSelectedCab())
                dispatch(setAvailableCabsResponse(null))
                dispatch(setScheduleDate())
                dispatch(setAvailableCabsLoading(false))
            });
    };
}

export function giveFeedback(requestData) {
    return async (dispatch) => {
        return appService.post('fb/v1/trips/feedback/user', requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                navigate(ScreenNames.START_RIDE)
                dispatch(resetState())
            })
            .catch((error) => {
                console.log(error)
            });
    };
}

export function cancelRide(requestData) {
    return async (dispatch) => {
        return appService.post('fb/v1/trips/cancel', requestData)
            .then((response) => {
                if (response.status !== 200) {
                    throw response;
                }
                return response.data;
            })
            .then((response) => {
                dispatch(resetState())
            })
            .catch((error) => {
                console.log(error)
            });
    };
}

export async function onDriverContact() {
    // return async (dispatch) => {
    return appService.get('fb/v1/ex/vnumber?role=passenger')
    .then((response) => {
        if (response.status !== 200) {
            throw response;
        }
        return response.data;
    })
    .then((response) => {
        return response
    })
    .catch((error) => {
        return error
    });
    // };
}



export function getSocketData(inputData) {
    console.log(inputData)
    return async (dispatch) => {
        // const token =  await AsyncStorage.getItem('token');    

        return new Promise(function (resolve, reject) {
            if (inputData.message === 601) {
                dispatch(setAvailableCabsLoading(true))
            }
            if (inputData.message === 603) {
                dispatch(setSelectedCabLoading(true))
            }
            // var ws = new WebSocket(`ws://socket.furrybaxi.ml/fb/ws/user?token=${token}`);
            //  ws.onopen = () => {
            const ws = store.getState().riding.ws;
            console.log("checking", ws)
            if (ws != null) {
                console.log("sending")
                ws.send(JSON.stringify(inputData)) // send a message
                ws.onmessage = (response) => {
                    console.log("receving")
                    if (response?.data) {
                        response = response?.data?.replace(/\\/g, "")
                        
                        response = JSON.parse(response)
                        console.log(response?.data)
                        debugger
                        if(response?.status === 1000){
                            Alert.alert(
                                "Something went Wrong",
                                "Please try after sometime",
                                [
                                    {
                                        text: "Cancel",
                                        onPress: () => {}, //clearToAsyncStorage(),
                                        style: "cancel"
                                    },
                                    { text: "OK", onPress: () => navigate(ScreenNames.START_RIDE)}
                                ]
                            );                            
                        }
                        if (response?.status === 600) {
                            dispatch(setSelectedCab())
                            dispatch(setAvailableCabsLoading(false))
                            dispatch(setAvailableCabsResponse(response))
                            // navigate(ScreenNames.TYPE_OF_RIDES)
                        } else if (response?.status === 602) {
                            dispatch(setSelectedCabLoading(false))
                            dispatch(setSelectedCabResponse(response))
                            navigate(ScreenNames.RIDE_ARRIVING)
                        } else if (response?.status === 608) {
                            dispatch(setDurationResponse(response))
                        } else if (response?.status === 610) {
                            dispatch(setPickupOtpResponse(response))
                            navigate(ScreenNames.RIDE_CONFIRMING)
                        } else if (response?.status === 612) {
                            dispatch(setUserTripReponse(response))
                        } else if (response?.status === 614) {
                            dispatch(setUserOnRideReponse(response))
                            navigate(ScreenNames.ON_RIDE)
                        } else if (response?.status === 616) {
                            dispatch(setSummaryResponse(response))
                            navigate(ScreenNames.FEEDBACK)
                        } else if (response?.status == 618 || response?.status == 620) {
                            navigate(ScreenNames.START_RIDE)
                            dispatch(resetState())
                        } else if (response?.status == 622) {
                            dispatch(setSummaryResponse(response))
                            dispatch(rideInProgress(false))
                            navigate(ScreenNames.FEEDBACK)
                        } else if (response?.status == 604) {
                            dispatch(setSelectedCab())
                            dispatch(setAvailableCabsLoading(false))
                            dispatch(setAvailableCabsResponse(null))
                            navigate(ScreenNames.TYPE_OF_RIDES)
                        } else if (response?.status === 606) {
                            dispatch(setSelectedCabResponse(response))
                            dispatch(setSelectedCabLoading(false))
                        } else if (response?.status == 1002) {
                            ws.close()
                            console.log("token expired")
                            wsAuthentication(response?.last_response)
                        }
                    }
                };

                ws.onerror = (e) => {
                    // an error occurred
                    console.log("onError", e.message);
                    navigate(ScreenNames.START_RIDE)
                    return reject(e.message)
                };

                ws.onclose = (e) => {
                    // connection closed
                    wsconnect(dispatch)
                    console.log("onClose", e.code, e.reason);
                    resolve(inputData)
                    return reject(e)
                };
            }

        });
    }
}
const wsconnect = async (dispatch) => {
    const token = await AsyncStorage.getItem('token');
    var ws = new WebSocket(`ws://socket.furrybaxi.ml/fb/ws/user?token=${token}`);
    setTimeout(()=>{             
        dispatch(getCurrentLocation())
    },200)
    dispatch(setWebSocketConnection(ws))
}

export const wsAuthentication = async (data) => {
    const token = await appService.refreshAccessToken()
    console.log("token expired", token.data.access_token)
    await AsyncStorage.setItem('token', token.data.access_token);
    const ws = new WebSocket(`ws://socket.furrybaxi.ml/fb/ws/user?token=${token.data.access_token}`);
    store.dispatch(setWebSocketConnection(ws))
    console.log(data)
    store.dispatch(getSocketData(data))
}

