
import React, { useEffect } from 'react';


import Header from '../../../component/Header';
import { useDispatch, useSelector } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, Dimensions, Image, BackHandler } from 'react-native'


import { fSize } from '../../../utils/Responsive';
import Images from '../../../assets/Images/Images';
import BackButton from '../../../component/BackButton';
import SkipButton from '../../../component/SkipButton';
import { ScreenNames } from '../../../utils/ScreenNames';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';
import { backListener, removeBackListener } from '../../../utils/backHandler';
import { SafeAreaView } from 'react-native-safe-area-context';
import CustomButton from '../../../component/CustomButton';
import CustomGradientButton from '../../../component/CustomGradientButton';
import commanStyle from '../../../utils/commanStyle';

const height = Dimensions.get('window').height

const PetProfile = (props) => {

    React.useEffect(() => {
        backListener()
        return () => {
            removeBackListener()
        };
    }, []);

    const { petProfile } = useSelector(state => state.petProfile)

    const headerLeft = () => {
        return <BackButton onPress={() => { props.navigation.goBack() }} />
    }

    const headerRight = () => {
        return <SkipButton title={'Edit Profile'} onPress={() => { props.navigation.navigate(ScreenNames.CREATE_PET_PROFILE) }} />
    }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                <View style={{ padding: 20, backgroundColor: '#253040', height: '20%' }}>
                    <Header
                        bgColor={'#253040'}
                        leftView={headerLeft}
                        rightView={headerRight}
                    />
                </View>
                <View style={styles.container1} >
                    <View style={styles.pofileContainer}>
                        {
                            petProfile && petProfile?.pic ?
                                <Image source={{ uri: petProfile?.pic }} style={styles.profileImage} />
                                :
                                <Image source={Images.pet_profile_placeholder} style={styles.profileImage} />
                        }
                        <Text style={styles.profileTitle1}>{`${petProfile?.name}`}</Text>
                        <Text style={styles.profileTitle2}>{`(${petProfile?.category?.name})`}</Text>
                    </View>
                    <View style={{ minHeight: height * 64 / 100 }}>
                        <View style={styles.infoContainer}>
                            <View style={styles.intoTitleValueContainer}>
                                <Text style={styles.infoTitle}>{'Breed'}</Text>
                                <Text style={styles.infoTitleValue}>{`${petProfile?.breed?.name}`}</Text>
                            </View>
                            <View style={styles.intoTitleValueContainer}>
                                <Text style={styles.infoTitle}>{'Size'}</Text>
                                <Text style={styles.infoTitleValue}>{`${petProfile?.size?.name}`}</Text>
                            </View>
                            <View style={styles.intoTitleValueContainer}>
                                <Text style={styles.infoTitle}>{'Date of Birth'}</Text>
                                <Text style={styles.infoTitleValue}>{`${petProfile?.dob}`}</Text>
                            </View>
                            <View style={styles.intoTitleValueContainer}>
                                <Text style={styles.infoTitle}>{'About'}</Text>
                                <Text style={styles.infoTitleValueAbout}>{`${petProfile?.about}`}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.footer}>
                        <View style={styles.bottomButtonContainer}>
                            <View style={{ width: '45%' }}>
                                <CustomButton
                                    title={"Add New"}
                                    styleContainer={styles.button1Container}
                                    textStyle={styles.button1Text}
                                    onPress={(e) => { }}
                                />
                            </View>
                            <View style={{ width: '45%' }}>
                                <CustomGradientButton
                                    title={'Start Ride'}
                                    onPress={() => {
                                        props.navigation.navigate(ScreenNames.START_RIDE)
                                    }}
                                    disable={false}
                                    gradientColor={['#DAC5A2', '#A98C6F']}
                                    styleContainer={commanStyle.buttonContainer}
                                    textStyle={commanStyle.primaryButtonText}
                                />
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#17202D',
        flex: 1,
    },

    container1: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        padding: 20,
    },

    pofileContainer: {
        height: '45%',
        zIndex: 1,
        position: 'absolute',
        top: '-20%',
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center'
    },

    profileImage: {
        // borderRadius: 25,
        // height: 100,
        // width: 100,
        // aspectRatio: 1,
        width: '50%',
        height: '50%',
        borderRadius: 20,
        aspectRatio: 1,
        justifyContent: 'center'
    },
    profileTitle1: {
        color: '#F9D591',
        fontSize: fSize(22),
        fontFamily: SEMI_BOLD
    },
    profileTitle2: {
        color: '#E1CDA9',
        fontSize: fSize(16),
        fontFamily: MEDIUM
    },
    infoContainer: {
        backgroundColor: '#253040',
        borderRadius: 20,
        paddingHorizontal: 20,
        paddingVertical: 10,
        marginTop: '30%'
    },
    intoTitleValueContainer: {
        paddingVertical: 5
    },
    infoTitle: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        color: '#B59B7C'
    },
    infoTitleValue: {
        fontFamily: MEDIUM,
        fontSize: fSize(15),
        color: '#F5EDDC'
    },
    infoTitleValueAbout: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        color: '#F5EDDC'
    },
    footer: {
        paddingVertical: 20,
    },

    bottomButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button1Container: {
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        height: 54,
        borderRadius: 15,
        borderWidth: 2,
        borderColor: '#253040'
    },
    button1Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#E1CDA9'
    },

    button2Container: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 54,
        borderRadius: 15
    },
    button2Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#E1CDA9'
    }
});

export default PetProfile;
