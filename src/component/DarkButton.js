import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import{StyleSheet,TouchableOpacity,Text} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { fSize } from '../utils/Responsive';
import { MEDIUM } from '../utils/typography';

const DarkButton = ({
    title,
    onPress,
    disable
  }) => {
    return (
        <TouchableOpacity  onPress={onPress} disabled={disable}>
            <LinearGradient
                useAngle={true}
                angle={180} 
                colors={['#253040', '#17202D']} 
                start={{x: 0.0 , y: 0.0}}
                end={{x: 0.0, y: 1.0}}
                // locations={[-0.3, 1.5]}
                style={styles.bottomContainer}
            >
                <Text style={styles.buttonText}>
                    {title}
                </Text>
            </LinearGradient>  
        </TouchableOpacity>
    );
  };

  const styles = StyleSheet.create({
    bottomContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        height:54,
        borderRadius: 15,
        
    },
    buttonText:{
        color:'#5E718A',
        fontFamily:MEDIUM,
        fontSize: fSize(14)
    }
  })

  export default DarkButton;