import React,{useState, useEffect} from 'react';
import {StyleSheet, View, Modal, Dimensions, Text, Image, ScrollView, Platform, TouchableOpacity, Alert} from 'react-native';
import {  useApplePay, useConfirmSetupIntent, useGooglePay } from '@stripe/stripe-react-native';
import SvgUri from 'react-native-svg-uri';
import Images from '../../../../assets/Images/Images';
import { fSize } from '../../../../utils/Responsive';
import { MEDIUM } from '../../../../utils/typography';
import Payment from './payment';
import { savePaymentDetails, savePaymentMethod } from '../../../../redux/actions/paymentAction';
import { getUserDetails } from '../../../../redux/actions/authAction';

const screenHeight = Dimensions.get('screen').height
const screenWidth = Dimensions.get('screen').width



const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}
const getWidth = (wd) => {
    return screenWidth * (wd / 100)
}


const SelectPaymentModal = ({
    setPaymentType, paymentType, onClose, dispatch,
    stripeResponse, paymentSecretResponse, userResponse
}) => {
    const { isApplePaySupported, presentApplePay, confirmApplePayPayment,  } = useApplePay();
    const { isGooglePaySupported, initGooglePay, createGooglePayPaymentMethod } = useGooglePay();

    const { confirmSetupIntent } = useConfirmSetupIntent();

    const [cardModal, setCardModal] = useState(false)
    const [amountModal, setAmountModal] = useState(false)
    const [clientSecretKey, setClientSecretKey] = useState()

    const [loader, setLoader] = useState(false)
    const [savedCards, setSavedCards] = useState([])
    const [initialized, setInitialized] = useState(false);

    const onSelectPaymentType = (item) => {
        let tmpData = {
            method_id:item?.method_id,
            method: item?.method
        }
        if(paymentType?.method_id === tmpData?.method_id){
            setPaymentType()
        }else{
            setPaymentType(tmpData)
        }
    }

    const onSubmitPaymentType = async() =>{
        if(!paymentType) {
            Alert.alert('Please select card');
            return
        }
        setLoader(true)
        try {
            let resp = await savePaymentMethod({method_id: paymentType.method_id})
            if(resp){
                onClose()
            }
        } catch (error) {
            Alert.alert(
                "Error",
                "Please try after sometime",
                [
                    { text: "OK", onPress: () => {}}
                ]
            );
        } finally{
            setLoader(false)
        }
    }

    const initialize = async () => {
        if (!(await isGooglePaySupported({ testEnv: true }))) {
            Alert.alert('Google Pay is not supported.');
            return;
        }
        
        const { error } = await initGooglePay({
          testEnv: true,
          merchantName: 'Furrybaxi Ltd.',
          countryCode: paymentSecretResponse?.country_code,
          billingAddressConfig: {
            format: 'MIN',
            isPhoneNumberRequired: false,
            isRequired: false,
          },
          isEmailRequired: false,
        });
        if (error) {
          Alert.alert(error.code, error.message);
          return;
        }
        setInitialized(true);
    }

    const onSelectGooglePay = async() => {
        if(paymentSecretResponse === null){
            Alert.alert(
                "Error",
                "Please try after sometime",
                [
                    { text: "OK", onPress: () => {}}
                ]
            );
            return
        }
        setLoader(true)
        const { error, paymentMethod } = await createGooglePayPaymentMethod({
            amount: paymentSecretResponse?.charge,
            currencyCode: paymentSecretResponse?.currency,
        });
        if (error) {
            Alert.alert(error.code, error.message);
            return;
        } else if (paymentMethod) {
            let data = {
                method_id: paymentMethod.id,
                method: "gpay"
            }
            saveCard(data)
        }
        setLoader(false)
        setInitialized(false);
    }

    const onSelectApplePay = async () => {
        if(!isApplePaySupported){
            Alert.alert(
                "Error",
                "Apple pay not supported on this device.",
                [
                    { text: "OK", onPress: () => {}}
                ]
            );
            return
        }
        if(paymentSecretResponse === null || !clientSecretKey){
            Alert.alert(
                "Error",
                "Payment server down",
                [
                    { text: "OK", onPress: () => {}}
                ]
            );
            return
        }
        const { error, paymentMethod } = await presentApplePay({
            cartItems: [{ label: 'Furrybaxy Ltd.', amount: paymentSecretResponse?.charge?.toString(), type: 'final' }],
            country: paymentSecretResponse?.country_code,
            currency: paymentSecretResponse?.currency,
            jcbEnabled: true,
        });
            if (error) {
                console.log(error)
            } else {
            const { error: confirmError } = await confirmApplePayPayment(
                clientSecretKey
            );
            if (confirmError) {
                console.log(confirmError)
            }else{
                let data = {
                    method_id: paymentMethod.id,
                    method: "applepay"
                }
                saveCard(data)
            }
        }
    }

    const createPayment = async () => {
        if(!clientSecretKey) return;
        setLoader(true)
        const billingDetails = {
            email: userResponse?.data?.email,
        };
        try {
            const { setupIntent, error } = await confirmSetupIntent(clientSecretKey, {
                type: 'Card',
                billingDetails,
            });

            if (error) {
                Alert.alert(
                    "Declined",
                    error.message,
                    [
                        { text: "OK", onPress: () => {
                            setAmountModal(falsedispatch)
                        }}
                    ]
                );
            } else {
                let data = {
                    method_id: setupIntent.paymentMethodId,
                    method: "card"
                }
                saveCard(data,'card')
            }
        } catch (error) {
            console.log(error)
        } finally {
            setLoader(false)
        }
    }

    const saveCard = async(data, key) => {
        try {
            let resp = await savePaymentDetails(data)
            if(resp){
                setPaymentType(data)
                if(key == 'card'){
                    dispatch(getUserDetails())
                    setAmountModal(false)
                    setCardModal(false)
                }
            }
            setLoader(false)
        } catch (error) {
            setAmountModal(false)
            setLoader(false)
        }
    }

    const onSubmitCard = () => {
        setAmountModal(true)
    }

    useEffect(()=>{
        if(paymentSecretResponse !== null){
            setClientSecretKey(paymentSecretResponse?.clientSecretKey)
        }
    },[paymentSecretResponse])

    useEffect(()=>{
        if(userResponse !== null){
            setSavedCards(userResponse?.data?.payments)
        }
    },[userResponse])
    
    useEffect(()=>{
        if(paymentSecretResponse !== null && Platform.OS === 'android'){
            initialize()
        }
    },[paymentSecretResponse])

    return(
        <Modal
            transparent={true}
            visible={true}
        >
        <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
                <View style={styles.modalHeader}>
                    <View style={{flex: .20}}>
                        <TouchableOpacity style={styles.backButton} 
                            onPress={()=>{
                                setPaymentType()
                                onClose()
                            }}
                        >
                            <Image source={Images.arrow_back}/>
                        </TouchableOpacity>
                    </View>
                    <View style={{flex: .80}}>
                        <Text style={styles.headerTitle}>
                            {"Choose Payment Type"}
                        </Text>
                    </View>
                </View>
                <View style={styles.content}>
                    <ScrollView>
                        {
                            savedCards?.map((item, index)=>(
                                <TouchableOpacity style={styles.paymentButton} key={index} onPress={()=>onSelectPaymentType(item)}>
                                    <View style={styles.paymentButtonLeft}>
                                        <SvgUri 
                                            source={Images.cardIconSvg}
                                        />
                                    </View>
                                    <View style={styles.paymentButtonRight}>
                                        <Text style={styles.paymentButtonRightText}>
                                            {`xxxx xxxx xxxx ${item.last_four}`}
                                        </Text>
                                    </View>
                                    {
                                        paymentType?.method_id === item?.method_id &&
                                        <View style={styles.paymentButtonTick}>
                                            <SvgUri 
                                                source={Images.paymentTickSvg}
                                            />
                                        </View>
                                    }
                                </TouchableOpacity>
                            ))
                        }
                        {
                            (Platform.OS === 'ios') ?
                            <TouchableOpacity 
                                style={[
                                    styles.paymentButton,
                                ]} 
                                onPress={()=>{
                                    onSelectApplePay()
                                    // onSelectPaymentType({method_id:'apple', method:'applepay'})
                                }}
                                disabled={paymentType?.method === 'applepay'}
                            >
                                <View style={styles.paymentButtonLeft}>
                                    <SvgUri 
                                        source={Images.applePayIconSvg}
                                        style={{opacity: paymentType?.method == 'applepay' ? 0.6 : 1}
                                    }
                                    />
                                </View>
                                <View style={styles.paymentButtonRight}>
                                    <Text style={[styles.paymentButtonRightText,{opacity: paymentType?.method == 'applepay' ? 0.6 : 1}]}>
                                        {"Apple Pay"}
                                    </Text>
                                </View>
                                {
                                    paymentType?.method === 'applepay' &&
                                    <View style={styles.paymentButtonTick}>
                                        <SvgUri 
                                            source={Images.paymentTickSvg}
                                            style={{opacity: paymentType?.method == 'applepay' ? 0.6 : 1}}
                                        />
                                    </View>
                                }
                            </TouchableOpacity>
                            :
                            <TouchableOpacity 
                                style={styles.paymentButton} 
                                onPress={()=>{
                                    onSelectGooglePay()
                                    // onSelectPaymentType({method_id:'google', method:'gpay'})
                                }}
                                disabled={paymentType?.method === 'google'}
                            >
                                <View style={styles.paymentButtonLeft}>
                                    <SvgUri 
                                        source={Images.googlePayIconSvg}
                                        style={{opacity: paymentType?.method == 'gpay' ? 0.6 : 1}}
                                    />
                                </View>
                                <View style={styles.paymentButtonRight}>
                                    <Text style={[styles.paymentButtonRightText, {opacity: paymentType?.method == 'gpay' ? 0.6 : 1}]}>
                                        {"Google Pay"}
                                    </Text>
                                </View>
                                {
                                    paymentType?.method === 'gpay' &&
                                    <View style={styles.paymentButtonTick}>
                                        <SvgUri 
                                            source={Images.paymentTickSvg}
                                            style={{opacity: paymentType?.method == 'gpay' ? 0.6 : 1}}
                                        />
                                    </View>
                                }
                            </TouchableOpacity>
                        }
                        <TouchableOpacity style={styles.paymentButton} onPress={()=>{setCardModal(true)}}>
                            <View style={styles.paymentButtonLeft}>
                                <SvgUri 
                                    source={Images.addPaymentIconSvg}
                                />
                            </View>
                            <View style={styles.paymentButtonRight}>
                                <Text style={styles.paymentButtonRightText}>
                                    {"Add Card"}
                                </Text>
                            </View>
                            {/* {
                                paymentType?.method_id === 'add' &&
                                <View style={styles.paymentButtonTick}>
                                    <SvgUri 
                                        source={Images.paymentTickSvg}
                                    />
                                </View>
                            } */}
                        </TouchableOpacity>
                    </ScrollView>
                </View>
                <View style={styles.modalFooter}>
                    {
                        paymentType ? 
                        <TouchableOpacity 
                            style={styles.footerButtonActive} 
                            onPress={() => { onSubmitPaymentType() }}
                            disabled={loader}
                        >
                            <Text style={styles.footerButtonTextActive}>
                                {"Submit"}
                            </Text>
                        </TouchableOpacity> :
                        <TouchableOpacity style={styles.footerButton} disabled={true}>
                            <Text style={styles.footerButtonText}>
                                {"Submit"}
                            </Text>
                        </TouchableOpacity> 
                    }
                </View>
            </View>
        </View>
        {
            cardModal &&
            <Payment 
                onClose={()=>setCardModal(false)} 
                loader={loader} 
                onSubmitCard={onSubmitCard}
                amountModal={amountModal}
                setAmountModal={setAmountModal}
                createPayment={createPayment}
                paymentSecretResponse={paymentSecretResponse}
            />
        }
        
    </Modal>
    )
}

const styles = StyleSheet.create({
    modalContainer:{
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    modalContent:{
        height: getHeight(100),
        width: getWidth(100),
        paddingVertical: 40,
        backgroundColor: '#17202D',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    modalHeader: {
        flexDirection: 'row',
        height: getHeight(8),
        width: getWidth(100),
        paddingHorizontal: 20,
        alignItems: 'center'
    },
    backButton: {
        width: 50,
        height: 50,
        borderRadius: 12,
        backgroundColor: "#253040",
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        fontFamily: MEDIUM,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#FDFDFD"
    },
    content: {
        height: getHeight(72),
    },
    paymentButton: {
        flexDirection: 'row',
        width: getWidth(90),
        height: getHeight(8),
        paddingVertical: 10,
        paddingHorizontal: 15, 
        backgroundColor: "#253040",
        marginVertical: 10,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    paymentButtonLeft:{
        flex: .15
    },
    paymentButtonRight:{
        flex: .85
    },
    paymentButtonRightText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#F4F4F4'
    },
    paymentButtonTick: {
        position: 'absolute',
        right: "10%",
        top: "55%"
    },
    modalFooter: {
        height: getHeight(6),
        width: getWidth(100),
        paddingHorizontal: 20,
        justifyContent: 'center'
    },
    footerButton: {
        borderWidth: 2,
        borderColor: '#253040',
        borderRadius: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        width: '100%',
        backgroundColor: '#253040'
    },
    footerButtonText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#5E718A'
    },
    footerButtonActive: {
        borderRadius: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        width: '100%',
        backgroundColor: "#A98C6F"
    },
    footerButtonTextActive: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#FFFFFF'
    }
})

export default SelectPaymentModal;