const Images = {
    arrow_back: require('./arrowBack.png'),
    arrow_down: require('./arrowDown.png'),
    pet_picture_placeholder: require('./petPicturePlaceholder.png'),
    camera_button: require('./cameraButton.png'),
    pet_profile_placeholder: require('./petProfile.png'),
    calender:require('./calender.png'),
    home:require('./homeIcon.png'),
    office:require('./officeIcon.png'),
    search:require('./searchIcon.png'),
    menu:require('./menuIcon.png'),
    currentLocation:require('./currentlocation.png'),
    clock:require('./clock.png'),
    marker:require('./marker.png'),
    cab: require('./cab.jpeg'),
    arrow_right: require('./arrowRight.png'),
    clock_gold: require('./clockGold.png'),
    marker_blak: require('./markerBlack.png'),
    arrow_back_black: require('./arrowBackBlack.png'),
    profile_image: require('./profile.png'),
    driverLocation:require('./driverLocation.png'),
    car:require('./car.png'),
    car_icon:require('./carBlack.png'),
    star:require('./star.png'),
    myLocation:require('./locationButton.png'),
    locationBlackIcon: require('./locationBlackIcon.png'),
    profileImage:require('./profileImage.png'),
    recieptIcon:require('./recieptIcon.png'),
    helpIcon:require('./helpIcon.png'),
    supportIcon:require('./supportIcon.png'),
    carWhiteIcon:require('./carWhite.png'),
    downloadIcon:require('./download.png'),
    messageIcon:require('./message.png'),
    edit:require('./edit.png'),
    human:require('./human.png'),
    human_pets:require('./humanAndPets.png'),
    pets:require('./pets.png'),
    plus:require('./plus.png'),
    minus:require('./minus.png'),
    star_dark:require('./star_dark.png'),
    set_location:require('./set_location.png'),
    select_location:require('./select_location.png'),
    find_lost_item:require('./lostItem.png'),
    safety: require('./safety.png'),
    share_ride: require('./sharedRide.png'),
    call: require('./call.png'),

    supportIconSvg:require('./supportIcon.svg'),
    recieptIconSvg:require('./recieptIcon.svg'),
    helpIconSvg:require('./helpIcon.svg'),
    // menu icons
    accountIconSvg:require('./accountIcon.svg'),
    rideHistoryIconSvg:require('./rideHistory.svg'),
    supportIconSvg1:require('./supportIcon1.svg'),
    termAndConditionIconSvg:require('./termsAndCondition.svg'), 
    privacyPolicyIconSvg:require('./privacyPolicy.svg'),
    faqIconSvg:require('./faq.svg'),
    logoutIconSvg:require('./logout.svg'),
    findLostItemSvg:require('./findLostItem.svg'),
    cardIconSvg: require("./cardIcon.svg"),
    googlePayIconSvg: require("./googlePayIcon.svg"),
    applePayIconSvg: require("./applePayIcon.svg"),
    addPaymentIconSvg: require("./addPaymentMethodIcon.svg"),
    paymentTickSvg: require("./paymentTick.svg"),
    clockIconSvg: require("./clockIcon.svg"),
    downloadIconSvg: require("./downloadIcon.svg"),
    mailIconSvg: require("./mail.svg"),
    startLocationSvg: require("./startLocationIcon.svg")

};
export default Images;
  