import React, { useEffect, useRef, useState } from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../../assets/Images/Images";
import CountryPhoneInput from "./component/CountryPhoneInput";
import CustomButton from "../../../../component/CustomButton";
import { backNavigation } from "../../../../root/navigation/rootNavigation";
import { fSize } from "../../../../utils/Responsive";
import { SEMI_BOLD } from "../../../../utils/typography";
import { resetState, setEditCountry, setEditPhoneError, setEditPhoneNumber, setError, updatePhoneNumber } from "../../../../redux/actions/phoneNumberAction";

import { useIsFocused } from "@react-navigation/native";


const EditPhone = (props) => {
    const dispatch = useDispatch()
    const { editPhoneNumber, editCountry, isEditPhoneError, isLoading } = useSelector(state => state.phoneNumber)

    const phoneInput = useRef(null);

    // const [phoneNumber, setPhoneNumber] = useState(phoneState.phoneNumber)
    // const [country, setCountry] = useState(phoneState.country)

    const isVisible = useIsFocused();

    useEffect(() => {
        if (isVisible) {
            phoneInput.current.onChangeText('')
            dispatch(resetState())
        }
    }, [isVisible])



    const checkIfPhoneNumberValid = async () => {      
        if (phoneInput.current?.isValidNumber(editPhoneNumber)) {          
            let inputVariable = {
                "phone": editPhoneNumber,
                "cc": phoneInput.current.getCallingCode()
            }        
            dispatch(updatePhoneNumber(inputVariable));
        } else {
            dispatch(setEditPhoneError(true))
        }
    }

    const onChangeCountry = (country) => {
        dispatch(setEditPhoneError(false))
        dispatch(setEditCountry({
            code: country?.cca2,
            value: country?.callingCode[0]
        }))
    }

    const onChangeNumber = (number) => {
        dispatch(setEditPhoneError(false))
        dispatch(setEditPhoneNumber(number))
    }



    return (
        <View style={styles.container}>
            <View style={styles.topView}>
                <TouchableOpacity style={styles.topViewButton} onPress={() => { backNavigation() }}>
                    <Image source={Images.arrow_back} style={styles.topViewButtonIcon} />
                </TouchableOpacity>
            </View>
            <View style={styles.middleView}>
                <View style={styles.middleViewSection1}>
                    {
                        isEditPhoneError
                            ?
                            <Text style={styles.middleViewSection1Text1}>
                                {`You entered a wrong number,\nPlease enter the correct one`}
                            </Text>
                            :
                            <Text style={styles.middleViewSection1Text}>
                                {"Update phone number"}
                            </Text>
                    }

                </View>
                <View style={styles.middleViewSection2}>
                    <CountryPhoneInput
                        phoneInputRef={phoneInput}
                        phoneNumber={editPhoneNumber}
                        country={editCountry}
                        onChangeNumber={(number) => onChangeNumber(number)}
                        onChangeCountry={(code) => onChangeCountry(code)}
                    />
                </View>
            </View>
            <View style={styles.bottomView}>
                <CustomButton
                    title={"Next"}
                    onPress={() => checkIfPhoneNumberValid()}
                    disable={false}
                    styleContainer={styles.bottomViewButton}
                    textStyle={styles.bottomViewButtonText}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#17202D',
        padding: 20
    },
    topView: {
        flex: 1
    },
    topViewButton: {
        padding: 10,
        width: 44,
        height: 44,
        backgroundColor: '#253040',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewButtonIcon: {
        width: 8,
        height: 16,
    },
    middleView: {
        flex: 3
    },
    middleViewSection1: {
        flex: 5,
        justifyContent: 'flex-end'
    },
    middleViewSection1Text: {
        color: '#E1CDA9',
        fontSize: fSize(20),
        lineHeight: 24,
        fontFamily: SEMI_BOLD
    },

    middleViewSection1Text1: {
        paddingBottom: 10,
        color: '#FF7373',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20)
    },
    middleViewSection2: {
        flex: 5,
        justifyContent: 'flex-end'
    },
    bottomView: {
        flex: 6,
        justifyContent: 'flex-end'
    },
    bottomViewButton: {
        backgroundColor: '#2EC1AC',
        height: 54,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bottomViewButtonText: {
        color: '#FFFCF5',
        fontSize: fSize(14),
        fontFamily: SEMI_BOLD
    },
})

export default EditPhone;