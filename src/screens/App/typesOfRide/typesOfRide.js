
import { View, StyleSheet, Image, Text, FlatList, ScrollView, Dimensions, TouchableOpacity, Platform } from 'react-native'
import React, { useRef, useEffect, useState, createRef, useMemo, memo } from 'react';

import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Geolocation from 'react-native-geolocation-service';
import DatePicker from 'react-native-date-picker';
import * as Progress from 'react-native-progress';
import { useIsFocused } from '@react-navigation/core';

import Images from '../../../assets/Images/Images';
import { useSelector, useDispatch } from 'react-redux';
import { GoogleCloudKey } from '../../../config';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';
import { fSize } from '../../../utils/Responsive';
import PrimaryButton from '../../../component/PrimaryButton';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScreenNames } from '../../../utils/ScreenNames';
import { getScheduledAvailableCab, getSocketData, rideInProgress, setIsCalendar, setSelectedCab } from '../../../redux/actions/ridingAction';
import DarkButton from '../../../component/DarkButton';
import googleMapStyle from '../../../utils/googleMapStyle';
import { find_dimesions } from '../../../utils/helper';
import MapViewDirections from 'react-native-maps-directions';
import { getCurrentLocation } from '../../../redux/actions/homeAction';
import MyLocationButton from '../component/MyLocationButton';
import NotSelectPaymentModal from './component/notSelectPaymentModal';
import SelectPaymentModal from './component/selectPaymentModal';
import SvgUri from 'react-native-svg-uri';
import { getPaymentSecret, getStripeInformation } from '../../../redux/actions/paymentAction';
import { initStripe } from '@stripe/stripe-react-native';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const TypesOfRide = (props) => {
    const mapRef = useRef(null);
    const isVisible = useIsFocused();

    const {
        currentPlace,
        destinationPlace,
    } = useSelector(state => state.map)

    const {
        availableCabsResponse,
        selectedCab,
        isCalendar,
        availableCabsLoading,
        scheduleDate,isRideInProgress
    } = useSelector(state => state.riding)

    const {
        currentLocation,
    } = useSelector(state => state.home)

    const {
        stripeResponse,
        paymentSecretResponse
    } = useSelector(state => state.payment)

    const {
        userResponse
    } = useSelector(state => state.auth)


    const dispatch = useDispatch()

    const [dynamicHeight, setDynamicHeight] = useState(height)
    const [origin, setOrigin] = useState()
    const [destination, setDestination] = useState()
    const { pets, humanCount } = useSelector(state => state.riding)
    const [region, setRegion] = useState({
        latitude: 18.5011567,
        longitude: 73.9242136,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    })
    const [selectedIndex, setSelectedIndex] = useState()
    const [date, setDate] = useState()
    const [paymentType, setPaymentType] = useState()
    const [notPaymentModal, setNotPaymentModal] = useState(false)
    const [selectPaymentModal, setSelectPaymentModal] = useState(false)
    const [message, setMessage] = useState('')

    const onSelectCab = (cab) => {
        if(!paymentType){
            setMessage("please select payment method")
            setNotPaymentModal(true)
            return 
        }
        if (selectedCab !== null && selectedCab?.id !== cab.id) {
            dispatch(setSelectedCab(cab))
        } else if (selectedCab === null) {
            dispatch(setSelectedCab(cab))
        } else {
            dispatch(setSelectedCab(null))
        }
    }
    
    const onSetPickupTimedate = () => {
        let date1 = date ? date : new Date()
        setDate(date1)
        dispatch(getScheduledAvailableCab(date1))
    }
    
    const retrieveCabs = () => {
        if(!origin || ! destination) return;

        var petIds = []
        if (pets.length > 0) {           
            for (let i=0; i < pets.length; i++) {               
                if (pets[i].isSelected) {                      
                    petIds.push(pets[i].pid)
                }
            }
        }
        let requestData = {
            message: 601,
            location: {
                from: [origin?.latitude, origin?.longitude],
                to: [destination?.latitude, destination?.longitude]
            },
            passengers: {
                humans: humanCount,
                pets: petIds
            }
        }

        if (availableCabsResponse == null ||availableCabsResponse?.data?.length === 0) {
            dispatch(getSocketData(requestData))
        }
    }

    const onNextClick = () => {
        if(selectedCab === null){
            setMessage("please select cab")
            setNotPaymentModal(true)
            return
        }
        props.navigation.navigate(ScreenNames.PICKUP_CONFIRMATION)
    }

    useEffect(() => {
        if (currentPlace && currentPlace !== null) {
            setOrigin({
                latitude: currentPlace?.lat,
                longitude: currentPlace?.long
            })
        }
        if (destinationPlace && destinationPlace !== null) {
            setDestination({
                latitude: destinationPlace?.lat,
                longitude: destinationPlace?.long
            })
        }
    }, [currentPlace, destinationPlace])

    useEffect(() => {
        if (isVisible) {
            dispatch(getCurrentLocation())
            dispatch(setSelectedCab(null))
        }
    }, [isVisible])

    useEffect(() => {
        if (origin && destination && isVisible && !isRideInProgress) {
            dispatch(rideInProgress(true))
            mapRef.current.fitToCoordinates([
                origin,
                destination
            ], {
                edgePadding: styles.markerEdge,
                animated: true
            })          

            retrieveCabs()
        }
    }, [origin, destination])

    useEffect(() => {
        if (currentLocation && currentLocation !== null) {
            mapRef.current.animateToRegion({
                ...currentLocation,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
            }, 1000)
        }
    }, [currentLocation])

    useEffect(() => {
        if (!selectedCab || selectedCab !== null) {
            setSelectedIndex(selectedCab)
        }
    }, [selectedCab])

    useEffect(()=>{
        if(selectPaymentModal){
            dispatch(getStripeInformation())
        }
    },[selectPaymentModal])

    useEffect(() => {
        if (stripeResponse !== null && selectPaymentModal) {
            initStripe({
                publishableKey: stripeResponse?.key,
                merchantIdentifier: 'merchant.com.furrybaxi',
            });
            dispatch(getPaymentSecret())
        }
    }, [stripeResponse])
    
    return (
        <View style={styles.container}>
            <MapView.Animated
                provider={PROVIDER_GOOGLE}
                style={{ ...StyleSheet.absoluteFillObject, height: dynamicHeight }}
                ref={mapRef}
                initialRegion={region}            
                onRegionChange={(e) => {
                    setRegion(e)
                }}
                showsCompass={false}
                customMapStyle={googleMapStyle}
                showsMyLocationButton={false}
            >
                {
                    currentLocation
                    &&
                    <Marker
                        coordinate={currentLocation}
                    >
                        <Image source={Images.driverLocation} style={styles.driverLocation} />
                    </Marker>
                }
                {
                    origin &&
                    <Marker coordinate={origin}>
                        <View style={styles.markerContainer}>
                            <View style={styles.markerDot} />
                        </View>
                    </Marker>
                }
                {
                    destination &&
                    <Marker coordinate={destination}  >
                        <Image source={Images.marker} style={{ width: 26, height: 36 }} />
                    </Marker>
                }
                <MapViewDirections
                    origin={origin}
                    destination={destination}
                    apikey={GoogleCloudKey}
                    strokeWidth={4}
                    strokeColor="#6E7887"
                    onReady={(result) => {
                        mapRef.current.fitToCoordinates(result.coordinates, {
                            edgePadding: styles.markerEdge,
                            animated: true
                        })
                    }}
                />
            </MapView.Animated>
            <View style={styles.menuOverLay}>
                <TouchableOpacity onPress={() => { dispatch(rideInProgress(false)), props.navigation.goBack() }}>
                    <View style={styles.backImageContainer}>
                        <Image
                            source={Images.arrow_back}
                            style={styles.iconImages}
                        />
                    </View>
                </TouchableOpacity>
            </View>

            {
                isCalendar
                    ?
                    <View style={styles.actionContainer2} onLayout={(event) => setDynamicHeight(find_dimesions(height, event.nativeEvent.layout))}>
                        <MyLocationButton marginTop={-60} />
                        <View style={styles.actionContainer2Section1}>
                            <Text style={styles.actionContainer2Section1Text1}>
                                {"Schedule a Ride"}
                            </Text>
                            <Text style={styles.actionContainer2Section1Text2}>
                                {"Driver may arrive between 11:45 - 12:00PM"}
                            </Text>
                        </View>
                        <View style={styles.actionContainer2Section2}>
                            <DatePicker
                                date={date ? date : new Date()}
                                onDateChange={(dt) => {
                                    if (dt) {
                                        setDate(dt)
                                    } else {
                                        setDate()
                                    }
                                }}
                                mode={'datetime'}
                                textColor={"#F9D591"}
                                androidVariant={"nativeAndroid"}
                                customStyles={{ dateInput: { borderWidth: 0 } }}
                                style={{ backgroundColor: '#253040', alignSelf: 'center', }}
                            />
                        </View>
                        <View style={styles.actionContainer2Section3}>
                            <View style={styles.actionContainer2Section3Left}>
                                <TouchableOpacity
                                    style={styles.actionContainer2Section3LeftButton}
                                    onPress={() => {
                                        dispatch(setIsCalendar(false))
                                    }}
                                >
                                    <Text style={styles.actionContainer2Section3LeftButtonText}>
                                        {"Cancel"}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.actionContainer2Section3Right}>
                                <PrimaryButton
                                    disable={false}
                                    onPress={() => {
                                        onSetPickupTimedate()
                                    }}
                                    title={"Set Pick up Time"}
                                />
                            </View>
                        </View>
                    </View>
                    :
                    <View
                        style={styles.actionContainer1}
                        onLayout={(event) => setDynamicHeight(find_dimesions(height, event.nativeEvent.layout))}
                    >
                        <MyLocationButton marginTop={-60} />
                        <View style={styles.actionContainer1Section1}>
                            <SafeAreaView style={{ paddingTop: Platform.OS === 'ios' ? 0 : 20 }}>
                                {
                                    !availableCabsLoading
                                        ?
                                        <ScrollView>
                                            {
                                                (availableCabsResponse?.data && availableCabsResponse?.data?.length > 0)
                                                    ?
                                                    availableCabsResponse.data?.map((item, index) => (
                                                        <TouchableOpacity onPress={() => { onSelectCab(item) }} key={index}>
                                                            <View style={[styles.listItem, {
                                                                backgroundColor: (selectedIndex?.id === item.id) ? "#FFFCF5" : "#FDFFFF",
                                                            }]}
                                                            >
                                                                <View style={styles.listItemLeft}>
                                                                    <Image
                                                                        source={Images.cab}
                                                                        style={styles.listItemImage}
                                                                    />
                                                                </View>
                                                                <View style={styles.listItemMiddle}>
                                                                    <Text style={styles.listItemMiddleText1}>
                                                                        {item.type}
                                                                    </Text>
                                                                    <Text style={styles.listItemMiddleText2}>
                                                                        {item.type}
                                                                    </Text>
                                                                </View>
                                                                <View style={styles.listItemRight}>
                                                                    <Text style={styles.listItemRightText1}>
                                                                        {item.Price}
                                                                    </Text>
                                                                </View>
                                                            </View>
                                                        </TouchableOpacity>
                                                    ))
                                                    :
                                                    <View style={styles.dataNotFoundContainer}>
                                                        <Text style={styles.dataNotFoundTextHeading}>
                                                            {"No Cabs"}
                                                        </Text>
                                                        <Text style={styles.dataNotFoundText}>
                                                            {"All drivers are busy at the moment, retry after some time"}
                                                        </Text>
                                                    </View>
                                            }
                                        </ScrollView>
                                        :
                                        <View style={styles.progressBarContainer}>
                                            <View style={styles.progressBarContainerSection1}>
                                                <Text style={styles.progressBarContainerSection1Text}>
                                                    {"Looking for Cabs"}
                                                </Text>
                                                <Text style={styles.dataNotFoundText}>
                                                    {"Waiting cab to your location"}
                                                </Text>
                                            </View>
                                            {/* <View style={{height: hp(4)}}/> */}
                                            <View style={styles.progressBarContainerSection2}>
                                                <Progress.Bar
                                                    height={5}
                                                    color={"#F9D591"}
                                                    unfilledColor={"#E2EAED"}
                                                    borderColor={"transparent"}
                                                    style={{ flex: 1 }}
                                                    indeterminate={true}
                                                />
                                            </View>
                                        </View>
                                }
                            </SafeAreaView>
                        </View>
                        <View style={styles.actionContainer1Section2}>
                            {
                                availableCabsResponse?.data?.length > 0 &&
                                <TouchableOpacity style={styles.actionContainer1Section2Row1} onPress={()=>setSelectPaymentModal(true)}>
                                    <View style={styles.actionContainer1Section2Row1Left}>
                                        <Text style={styles.actionContainer1Section2Row1LeftText1}>
                                            Payment Type
                                        </Text>
                                    </View>
                                    <View style={styles.actionContainer1Section2Row1Right}>
                                        <Image source={Images.arrow_right} style={styles.actionContainer1Section2Row1RightImage} />
                                    </View>
                                </TouchableOpacity>
                            }
                            <View style={styles.actionContainer1Section2Row2}>
                                <View style={styles.actionContainer1Section2Row2Left}>
                                    {
                                        !date
                                        &&
                                        <TouchableOpacity
                                            onPress={() => {
                                                dispatch(setIsCalendar(true))
                                            }}
                                            style={styles.actionContainer1Section2Row2LeftImageContainer}
                                        >
                                            <SvgUri 
                                                source={Images.clockIconSvg}
                                            />
                                        </TouchableOpacity>
                                    }
                                </View>
                                <View style={styles.actionContainer1Section2Row2Right}>
                                    {
                                        (!availableCabsResponse?.data?.length) ?
                                        <>
                                        {
                                            availableCabsLoading ?
                                            <DarkButton
                                                disable={true}
                                                onPress={() => {
                                                    // props.navigation.navigate(ScreenNames.PICKUP_CONFIRMATION)
                                                }}
                                                title={"Retry"}
                                            />
                                            :
                                            <PrimaryButton
                                                disable={false}
                                                onPress={() => {
                                                    retrieveCabs()
                                                }}
                                                title={"Retry"}
                                            />
                                        }
                                        </>
                                        :
                                        <>
                                            {
                                                !selectedIndex
                                                    ?
                                                    <DarkButton
                                                        disable={true}
                                                        onPress={() => {
                                                            // props.navigation.navigate(ScreenNames.PICKUP_CONFIRMATION)
                                                        }}
                                                        title={"Next"}
                                                    />
                                                    :
                                                    <PrimaryButton
                                                        disable={false}
                                                        onPress={() => {
                                                            onNextClick()
                                                        }}
                                                        title={"Next"}
                                                    />
                                            }
                                        </>
                                    }
                                </View>
                            </View>
                        </View>
                    </View>
            }
            {
                notPaymentModal &&
                <NotSelectPaymentModal 
                    message={message} 
                    onClose={()=>{
                        setMessage('')
                        setNotPaymentModal(false)
                    }} 

                />
            }
            {
                selectPaymentModal &&
                <SelectPaymentModal 
                    setPaymentType={setPaymentType} 
                    paymentType={paymentType} 
                    onClose={()=>setSelectPaymentModal(false)}
                    stripeResponse={stripeResponse}
                    paymentSecretResponse={paymentSecretResponse}
                    userResponse={userResponse}
                    dispatch={dispatch}
                />
            }
        </View>

    )
}


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
    },
    driverLocation: {
        height: 50,
        width: 50
    },
    menuOverLay: {
        position: 'absolute',
        top: 20,
        minHeight: hp(10),
        right: 0,
        left: 20,
    },
    iconImages: {
        // height: 14,
        // width: 14,
    },
    backImageContainer: {
        height: 44,
        width: 44,
        backgroundColor: '#253040',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    actionContainer1: {
        backgroundColor: "#FDFFFF",
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
    },
    actionContainer1Section1: {
        backgroundColor: "#FDFFFF",
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    actionContainer1Section2: {
        minHeight: hp(22),
        backgroundColor: "#17202D",
        paddingHorizontal: 20,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        justifyContent: 'center'
    },
    markerContainer: {
        backgroundColor: "#F9D591",
        borderRadius: 12.5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    markerDot: {
        backgroundColor: "#253040",
        width: 14,
        height: 14,
        borderRadius: 7,
    },
    listItem: {
        flexDirection: 'row',
        paddingHorizontal: 20,
        paddingBottom: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    listItemLeft: {
        flex: 0.2
    },
    listItemImage: {
        width: 50,
        height: 45
    },
    listItemMiddle: {
        flex: 0.6
    },
    listItemMiddleText1: {
        fontFamily: MEDIUM,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#6D7580"
    },
    listItemMiddleText2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#A3A8AF"
    },
    listItemRight: {
        flex: 0.2,
        alignItems: 'flex-end'
    },
    listItemRightText1: {
        fontFamily: MEDIUM,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#6D7580"
    },
    actionContainer1Section2Row1: {
        flexDirection: 'row'
    },
    actionContainer1Section2Row1Left: {
        flex: 0.9,
        justifyContent: 'center',
        height: hp(8)
    },
    actionContainer1Section2Row1LeftText1: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9",
    },
    actionContainer1Section2Row1Right: {
        flex: 0.1,
        justifyContent: 'center',
        alignItems: 'flex-end'
    },
    actionContainer1Section2Row1RightImage: {
        width: 8,
        height: 16
    },
    actionContainer1Section2Row2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
        //height: hp(8)
    },
    actionContainer1Section2Row2Left: {
        height: 54,
        flex: 0.1
    },
    actionContainer1Section2Row2LeftImageContainer: {
        borderColor: '#253040',
        borderWidth: 1,
        borderRadius: 15,
        width: 75,
        height: 54,
        justifyContent: 'center',
        alignItems: 'center'
    },
    actionContainer1Section2Row2LeftImage: {
        width: 25,
        height: 25
    },
    actionContainer1Section2Row2Right: {
        //width:'100%'
        paddingLeft: 20,
        flex: 0.8,
        alignSelf: 'flex-end',
    },
    dataNotFoundContainer: {
        paddingHorizontal: 20,
        paddingBottom: 10,
        justifyContent: 'center',
        // height: '100%'
    },
    dataNotFoundTextHeading: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#253040",
        paddingBottom: 10
        // textAlign: 'flex-start'
    },
    dataNotFoundText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: "#6D7580",
        // textAlign: 'flex-start'
    },
    actionContainer2: {
        backgroundColor: "#17202D",
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        minHeight: hp(50),
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
        // padding: 20
    },
    actionContainer2Section1: {
        height: hp(13),
        justifyContent: 'space-around',
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    actionContainer2Section1Text1: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 18,
        color: "#E1CDA9",
        textAlign: 'center'
    },
    actionContainer2Section1Text2: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        lineHeight: 14,
        color: "#B59B7C",
        textAlign: 'center',
    },
    actionContainer2Section2: {
        // height: hp(25),
        paddingHorizontal: 5,
        backgroundColor: "#253040",
        marginHorizontal: 20,
        borderRadius: 20,
        justifyContent: 'center'
    },
    actionContainer2Section3: {
        height: hp(12),
        flexDirection: 'row',
        alignItems: 'center',
        marginHorizontal: 20,
        justifyContent: 'space-between'
    },
    actionContainer2Section3Left: {
        flex: .35
    },
    actionContainer2Section3LeftButton: {
        backgroundColor: "#17202D",
        borderWidth: 1.5,
        borderColor: "#253040",
        height: 54,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15
    },
    actionContainer2Section3LeftButtonText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#FFFCF5"
    },
    actionContainer2Section3Right: {
        flex: .6
    },
    progressBarContainer: {
        flex: 1,
        paddingHorizontal: 20,
        justifyContent: 'space-between',
    },
    progressBarContainerSection1: {
        flex: 1,
    },
    progressBarContainerSection1Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#253040"
    },
    progressBarContainerSection2: {
        //flex:1, 
        flexDirection: 'row',
        paddingVertical: 20
    },
    markerEdge: {
        top: 100,
        bottom: 100,
        left: 30,
        right: 30
    }
});

export default memo(TypesOfRide);
