import React from 'react';
import { TouchableOpacity, Text, } from 'react-native';

const PaymentButton = ({
    title,
    onPress,
    disable,
    buttonStyle,
    buttonTextStyle
}) => {
    return (
        <TouchableOpacity style={buttonStyle} onPress={onPress} disabled={disable}>
            <Text style={buttonTextStyle}>
                {title}
            </Text>
        </TouchableOpacity>
    );
};


export default PaymentButton;