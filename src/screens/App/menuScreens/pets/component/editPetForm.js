import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import DatePicker from 'react-native-date-picker'
import DropDownPicker from 'react-native-dropdown-picker';
import { View, Text, TextInput, Image, TouchableOpacity, StyleSheet,  } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'

import { MEDIUM } from '../../../../../utils/typography';
import { fSize } from '../../../../../utils/Responsive';
import Images from '../../../../../assets/Images/Images';

const validate = values => {
    const errors = {
    }
   
    if (!values.name) {
        errors.name = 'name is empty '
    }

    if (!values.category) {
        errors.category={pcid : 'please select category'}
    }

    if (!values.breed) {
        errors.breed={
            name:'Breed is empty'
        } 
    }

    if (!values.size) {
        errors.size ={ psid:'please select the size'} 
    }

    if (!values.dob) {
        errors.dob = 'please select the size'
    }

    if (!values.about) {
        errors.about = 'about is empty'
    }
    return errors
}

const renderDropdownSize = ({ placeholder, input, item, change, isSizeDropdownOpen, 
    openSizeDropdown, hideAllDropdown, onSizeFieldChange, 
    meta: { touched, error, warning } }) => {
     
    return (
        <View style={{ flex: 1 }} onTouchStart={()=>{hideAllDropdown('category')}}>
            <DropDownPicker
                {...input}
                items={item}
                defaultValue={input.value ? input.value : null}
                isVisible={isSizeDropdownOpen}
                placeholder={placeholder}
                placeholderStyle={styles.drowpdownPlaceholder}
                style={[input.value ? styles.drowpdownDarkContainer : styles.drowpdownContainer, touched && error && styles.errorBorder]}
                itemStyle={{ justifyContent: 'flex-start' }}
                selectedLabelStyle={styles.dropdownLable}
                customArrowDown={ArrowDown}
                customArrowUp={ArrowDown}
                dropDownStyle={styles.dropdownPickerContainer}
                labelStyle={styles.drowpdownPickerContainer}
                dropDownContainerStyle={{ color: 'red' }}
                onOpen={() => openSizeDropdown(true)}
                onClose={() => openSizeDropdown(false)}
                // onChangeItem={item => {
                //     onSizeFieldChange({
                //         name: item.label,
                //         psid: item.value
                //     })
                // }}
                onChangeItem={item => change(input.name,item.value) }
            >
            </DropDownPicker>
        </View>
    )
}

const renderDropdownCategory = ({ placeholder, input, item, change, 
    openCategoryDropdown, isCategoryDropdownOpen, hideAllDropdown, onCategoryFieldChange, meta: { touched, error, warning } }) => {
    return (
        <View style={{ flex: 1 }} onTouchStart={()=>{hideAllDropdown('size')}}>
            <DropDownPicker
                {...input}
                items={item}
                defaultValue={input.value ? input.value : null}
                isVisible={isCategoryDropdownOpen}
                placeholder={placeholder}
                placeholderStyle={styles.drowpdownPlaceholder}
                style={[input.value ? styles.drowpdownDarkContainer : styles.drowpdownContainer, touched && error && styles.errorBorder]}
                itemStyle={{ justifyContent: 'flex-start' }}
                selectedLabelStyle={styles.dropdownLable}
                customArrowDown={ArrowDown}
                customArrowUp={ArrowDown}
                dropDownStyle={styles.dropdownPickerContainer}
                labelStyle={styles.drowpdownPickerContainer}
                dropDownContainerStyle={{ color: 'red' }}
                onOpen={() => openCategoryDropdown(true)}
                onClose={() => openCategoryDropdown(false)}
                onChangeItem={item => change(input.name,item.value) }
            >
            </DropDownPicker>
        </View>
    )
}

const renderDatePicker = ({ placeholder, input, label, type, 
    isDatePickerOpen, openDatePicker, change, hideAllDropdown, meta: { touched, error, warning } }) => {
    return (
        <View onTouchStart={()=>{hideAllDropdown()}}>
            <TouchableOpacity
                onPress={() => { openDatePicker(true) }}
                style={[
                    input.value ?
                        styles.inputBoxBlack : styles.inputBoxWhite,
                    touched && error && styles.errorBorder,
                    { flexDirection: 'row', justifyContent: 'space-between' }]}>
                <Text style={[input.value ? styles.dropdownLable : styles.drowpdownPlaceholder, { paddingVertical: hp(1) }]}>
                    {input.value ? input.value : placeholder}
                </Text>
                <Image source={Images.calender} style={{ height: 16, width: 16, margin: hp(1) }} />
            </TouchableOpacity>
            <DatePicker
                modal
                mode={'date'}
                open={isDatePickerOpen}
                date={new Date()}
                title={'Select '+ placeholder}
                confirmText={'Done'}
                textColor={'#E2EAED'}
                onConfirm={(date) => {
                    var fomatedDate = (new Date(Number(date)).getFullYear()) + "-"
                        + ("0" + (new Date(Number(date)).getMonth() + 1)).slice(-2) + "-"
                        + ("0" + new Date(Number(date)).getDate()).slice(-2)
                    openDatePicker(false)
                    change(input.name, fomatedDate)
                }}
                onCancel={() => {
                    openDatePicker(false)
                }}
            />
        </View>
    )
}

const ArrowDown = () => {
    return <Image source={Images.arrow_down} style={styles.arrowDown} />
}

const renderInput = ({ placeholder, input, multiline, hideAllDropdown, meta: { touched, error } }) => {
    return (
        <View onTouchStart={()=>{hideAllDropdown()}}>
            <TextInput
                style={[
                    input.value ?
                        styles.inputBoxBlack :
                        styles.inputBoxWhite,
                    touched && error && styles.errorBorder,
                    multiline && styles.textArea
                ]}
                returnKeyType="done"
                value={input.value}
                placeholder={placeholder}
                multiline={multiline}
                numberOfLines={multiline ? 4 : 1}
                placeholderTextColor={'#A58B6C'}
                {...input}
            />
        </View>
    )
}


let EditPetForm = props => {
    const {
        change, openDatePicker, openSizeDropdown,openCategoryDropdown,
        category, size, isDatePickerOpen, isCategoryDropdownOpen,isSizeDropdownOpen,
        onSizeFieldChange, onCategoryFieldChange,
    } = props;

    const [sizeKey , setSizeKey] = useState(0)
    const [categoryKey , setCategoryKey] = useState(0)
    
    useEffect(() => {
        setSizeKey(sizeKey + 1)
    }, [size])

    useEffect(() => {
        setCategoryKey(categoryKey + 1)
    }, [category])

    
    const hideAllDropdown = (key) => {
        if(key === 'size'){
            openSizeDropdown(false)
            setSizeKey(sizeKey+1)
        }else if(key === 'category'){
            openCategoryDropdown(false)
            setCategoryKey(sizeKey+1)
        }else{
            openCategoryDropdown(false)
            openSizeDropdown(false)
            setSizeKey(sizeKey+1)
            setCategoryKey(sizeKey+1)
        }
    }

    return (
        <View>
            <View style={styles.content}>
                <Field
                    placeholder="Enter Name"
                    name="name"
                    component={renderInput}
                    hideAllDropdown={hideAllDropdown}
                />
            </View>
            <View style={[styles.content, {zIndex: 2}]}>
                <Field
                    placeholder="Select the Category"
                    name="category.pcid"
                    change={change}
                    item={category}
                    isCategoryDropdownOpen={isCategoryDropdownOpen}
                    openCategoryDropdown={openCategoryDropdown}
                    component={renderDropdownCategory}
                    key={categoryKey}
                    change={change}
                    hideAllDropdown={hideAllDropdown}
                    onCategoryFieldChange={onCategoryFieldChange}
                />
            </View>
            <View style={[styles.content, {zIndex: 1}]}>
                <Field
                    placeholder="Breed"
                    name="breed.name"
                    component={renderInput}                 
                    hideAllDropdown={hideAllDropdown}
                />
            </View>
            <View style={[styles.container, styles.content, {zIndex: 2}]}>
                <View style={{ width: '48%' }} >
                    <Field
                        placeholder="Select Size"
                        name="size.psid"
                        item={size}
                        change={change}
                        isSizeDropdownOpen={isSizeDropdownOpen}
                        openSizeDropdown={openSizeDropdown}
                        component={renderDropdownSize}
                        key={sizeKey}
                        change={change}
                        hideAllDropdown={hideAllDropdown}
                        onSizeFieldChange={onSizeFieldChange}
                    />
                </View>
                <View style={{ width: '48%' }}>
                    <Field
                        placeholder="Date of Birth"
                        name="dob"
                        isDatePickerOpen={isDatePickerOpen}
                        openDatePicker={openDatePicker}
                        change={change}
                        component={renderDatePicker}
                        hideAllDropdown={hideAllDropdown}
                    />
                </View>
            </View>

            <View style={[styles.content, {zIndex: 1}]}>
                <Field
                    placeholder="Type something for anything specific"
                    name="about"
                    multiline={true}
                    component={renderInput}
                    hideAllDropdown={hideAllDropdown}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    content: {
        marginVertical: 5
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    inputBoxWhite: {
        backgroundColor: '#E2EAED',
        color: '#F9D591',
        fontFamily: MEDIUM,
        borderRadius: 15,
        fontSize: fSize(12),
        padding: 10,
        height: hp(7),
    },
    inputBoxBlack: {
        backgroundColor: '#253040',
        color: '#F9D591',
        fontFamily: MEDIUM,
        borderRadius: 15,
        fontSize: fSize(12),
        height: hp(7),
        padding: 10,
    },
    textArea: {
        textAlignVertical: "top",
        height: hp(16)
    },
    errorBorder: {
        borderWidth: 1.5,
        borderColor: '#FF7373'
    },
    drowpdownContainer: {
        backgroundColor: '#E2EAED',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        paddingLeft:2,
        paddingRight:10,
        height: hp(7)
    },
    drowpdownDarkContainer: {
        backgroundColor: '#253040',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
        borderBottomLeftRadius: 15,
        borderBottomRightRadius: 15,
        borderColor: '#253040',
        paddingLeft:2,
        paddingRight:10,
        height: hp(7),
    },
    drowpdownPlaceholder: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        color: '#A58B6C',
    },
    dropdownLable: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        color: '#F9D591',
        zIndex: 2
    },
    dropdownPickerContainer: {
        backgroundColor: '#EDF2F3',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
    },
    drowpdownPickerContainer: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        color: '#253040'
    },
    arrowDown: {
        height: 9,
        width: 16,
        tintColor: '#A58B6C'
    }
})

EditPetForm = reduxForm({
    form: 'editPetForm',
    validate,
    enableReinitialize: true,
    shouldValidate: () => true,

})(EditPetForm)

EditPetForm = connect(
    state => ({
        initialValues: { ...state.petEdit.petProfile }
    })
)(EditPetForm)


export default EditPetForm;