import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, TouchableOpacity, View, Text, TextInput, ScrollView, Modal, FlatList, RefreshControl } from 'react-native';
import BackButton from '../../../../component/BackButton';
import Header from '../../../../component/Header';
// import Images from '../../../assets/Images/Images';
// import { backNavigation } from '../../../root/navigation/rootNavigation';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../../utils/typography';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import { fSize } from '../../../../utils/Responsive';
import CustomGradientButton from '../../../../component/CustomGradientButton';
import CustomButton from '../../../../component/CustomButton';
import commanStyle from '../../../../utils/commanStyle';
import Images from '../../../../assets/Images/Images';
import { ScreenNames } from '../../../../utils/ScreenNames';
import { useDispatch, useSelector } from 'react-redux';
import { getPets, setEditResponse, setFromScreen } from '../../../../redux/actions/petEditAction';





const PetList = (props) => {
    const dispatch = useDispatch()
    const { pets } = useSelector(state => state.petEdit)

    const [isRefreshing, setIsRefreshing] = useState(false);

    useEffect(() => {
        dispatch(getPets())
    }, [])

    const _onRefresh = () => {
        setIsRefreshing(true);
        dispatch(getPets()).then(() => {
            setIsRefreshing(false)
        });
    };

    const headerLeft = () => {
        return <BackButton onPress={() => { props.navigation.goBack() }} />
    }
    const openPet = (item) => {
        dispatch(setEditResponse(item))
        props.navigation.navigate(ScreenNames.PET_DETAILS)

    }
    const createPet = () => {
        dispatch(setEditResponse(null))
        dispatch(setFromScreen(ScreenNames.PET_LIST))
        props.navigation.navigate(ScreenNames.EDIT_PETS)
    }

    const renderItem = ({ item }) => {
        return (
            <TouchableOpacity onPress={() => openPet(item)} style={styles.petCard}>
                <View style={styles.petCardContainer}>
                    <View>
                        {
                            item?.pic ?
                                <Image source={{ uri: item?.pic }} style={styles.profileImage} />
                                :
                                <Image source={Images.pet_profile_placeholder} style={styles.profileImage} />
                        }

                    </View>
                    <View style={styles.petInfo}>
                        <Text style={styles.petInfoText1}>{item?.name}</Text>
                        <Text style={styles.petInfoText2}>{`(${item?.category?.name})`}</Text>
                        <Text style={styles.petInfoText3}>{item?.breed?.name}</Text>
                    </View>
                </View>


            </TouchableOpacity>
        );
    };

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <Header
                    leftView={headerLeft}
                />
            </View>
            <View style={styles.content}>
                <FlatList
                    scrollEnabled
                    data={pets}
                    renderItem={renderItem}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    // contentContainerStyle={{ alignItems: 'center' }}
                    refreshControl={
                        <RefreshControl
                            refreshing={isRefreshing}
                            onRefresh={() => _onRefresh()}
                        />
                    }
                />
            </View>

            <View style={styles.footer}>
                <CustomGradientButton
                    title={'Add Pets'}
                    onPress={() => createPet()}
                    disable={false}
                    gradientColor={['#DAC5A2', '#A98C6F']}
                    styleContainer={commanStyle.buttonContainer}
                    textStyle={commanStyle.primaryButtonText}
                />
            </View>

        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    contentSection1Row2Image: {
        width: 100,
        height: 100,
        borderRadius: 20,
        aspectRatio: 1,
    },
    container: {
        flex: 1,
        backgroundColor: '#17202D',
        padding: 20
    },

    header: {
        height: hp(10),
    },
    content: {
        flex: 1,
        //padding: 20,
    },
    petCard: {
        backgroundColor: '#253040',
        borderRadius: 20,
        marginVertical: 10,


    },
    petCardContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',

        margin: 15
    },
    profileImage: {
        height: 80,
        width: 80,
        borderRadius: 20
    },
    petInfo: {
        //alignContent:'flex-end',
        //textAlign:'right',
        justifyContent: 'space-evenly'
    },
    petInfoText1: {
        textAlign: 'right',
        fontSize: fSize(14),
        fontFamily: SEMI_BOLD,
        color: '#F9D591'
    },
    petInfoText2: {
        textAlign: 'right',
        fontSize: fSize(12),
        fontFamily: MEDIUM,
        color: '#E1CDA9'
    },
    petInfoText3: {
        textAlign: 'right',
        fontSize: fSize(14),
        fontFamily: REGULAR,
        color: '#F5EDDC'
    }
})

export default PetList;

