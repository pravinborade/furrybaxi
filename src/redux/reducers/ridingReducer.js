import Images from "../../assets/Images/Images";
import { store } from "../store/store";

const initialState = {
    isGoogleInputFoucused: false,
    userLocations: null,
    serviceLocationsResponse: null,
    availableCabsResponse: null,
    availableCabsLoading: false,
    selectedCab: null,
    selectedCabResponse: null,
    durationResponse: null,
    pickupOtpResponse: null,
    scheduleDate: null,
    isCalendar: false,
    scheduledCabResponse: null,
    selectedCabLoading: false,
    userTripResponse: null,
    userOnrideResponse: null,
    summaryResponse: null,
    ws: null,
    pets: [],
    humanCount: 0,
    isCapacityError: false,
    errorCounter: 0,
    petCount: 0,
    humanData: [{
        image: Images.human_pets,
        name: 'Human & pets',
        key: 1,
        isSelected: false
    },
    {
        image: Images.pets,
        name: 'pets',
        key: 3,
        isSelected: false
    },
    {
        image: Images.human,
        name: 'Human',
        key: 2,
        isSelected: false
    }],
    savedLocations: [],
    saveLocationDetail: null,
    saveLocationName: '',
    configurations: null,
    isRideInProgress: false,
    shouldStartRideFromMap: false,
    isConfirmed: false,
    activeKey:0
};

const ridingReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_GOOGLE_INPUT_FOCUSED':
            return {
                ...state,
                isGoogleInputFoucused: action.isFocused
            }

        case 'SET_USER_LOCATIONS':
            return {
                ...state,
                saveLocationName: '',
                userLocations: action.locations
            }

        case 'SET_AVAILABLE_CABS_RESPONSE':
            return {
                ...state,
                availableCabsResponse: action.response
            }
        case 'SET_AVAILABLE_CABS_LOADING':
            return {
                ...state,
                availableCabsLoading: action.loading
            }

        case 'SET_SERVICE_LOCATION_RESPONSE':
            return {
                ...state,
                serviceLocationsResponse: action.response
            }

        case 'SET_SELECTED_CAB':
            return {
                ...state,
                selectedCab: action.cab
            }

        case 'SET_SELECTED_CAB_RESPONSE':
            return {
                ...state,
                selectedCabResponse: action?.response
            }

        case 'SET_DURATION_RESPONSE':
            return {
                ...state,
                durationResponse: action?.response
            }

        case 'SET_PICKUP_OTP_RESPONSE':
            return {
                ...state,
                pickupOtpResponse: action?.response
            }

        case 'SET_SCHEDULE_DATE':
            return {
                ...state,
                scheduleDate: action?.date
            }

        case 'SET_IS_CALENDAR':
            return {
                ...state,
                isCalendar: action?.flag
            }

        case "SET_SCHEDULED_CAB_RESPONSE":
            return {
                ...state,
                scheduledCabResponse: action.response
            }

        case "SET_SELECTED_CAB_LOADING":
            return {
                ...state,
                selectedCabLoading: action.loading
            }

        case "SET_USER_TRIP_RESPONSE":
            return {
                ...state,
                userTripResponse: action.response
            }

        case "SET_USER_ON_RIDE_RESPONSE":
            return {
                ...state,
                userOnrideResponse: action.response
            }

        case "SET_SUMMARY_RESPONSE":
            return {
                ...state,
                summaryResponse: action.response
            }

        case 'SET_WEB_SOCKET_CONNECT':
            return {
                ...state,
                ws: action.ws
            }


        case "RESET_STATE":
            return {
                ...initialState,
                isGoogleInputFoucused: false,
                isConfirmed: false,
                activeKey:0,
                humanData: [{
                    image: Images.human_pets,
                    name: 'Human & pets',
                    key: 1,
                    isSelected: false
                },
                {
                    image: Images.pets,
                    name: 'pets',
                    key: 3,
                    isSelected: false
                },
                {
                    image: Images.human,
                    name: 'Human',
                    key: 2,
                    isSelected: false
                }]
            }

        case "INCREMENT_HUMAN_COUNT":
            var eCounter = state?.errorCounter;
            var hcounter = state?.humanCount;
            var isError = false
            var passenger_count = state?.configurations?.passenger_count
            var maxCount = passenger_count ? passenger_count : 4
            if (eCounter < maxCount) {
                hcounter++;
                eCounter++;
            } else {
                isError = true
            }

            return {
                ...state,
                humanCount: hcounter,
                errorCounter: eCounter,
                isCapacityError: isError
            }

        case "DECREMENT_HUMAN_COUNT":
            return {
                ...state,
                humanCount: state?.humanCount > 0 ? state?.humanCount - 1 : 0,
                errorCounter: state?.errorCounter > 0 ? state?.humanCount > 0 ? state?.errorCounter - 1 : state?.errorCounter : 0,
                isCapacityError: false
            }

        case "SET_HUMAN_PETS":
            var data = [...action.pets]
            for (let i = 0; i < data.length; i++) {
                data[i].isSelected = false
            }
            return {
                ...state,
                pets: data,
                humanCount: 0,
                isCapacityError: false,
                errorCounter: 0,
                petCount: 0
            }

        case "SELECT_PET":
            var data = [...state?.pets]
            var counter = state?.errorCounter;
            var isError = false;
            var petCount = state?.petCount
            var passenger_count = state?.configurations?.passenger_count
            var maxCount = passenger_count ? passenger_count : 4
            for (let i = 0; i < data.length; i++) {
                if (data[i].pid == action.pet.pid) {
                    if (data[i].isSelected) {
                        data[i].isSelected = !(data[i].isSelected)
                        counter--;
                        petCount--;
                    } else {
                        if (counter < maxCount) {
                            data[i].isSelected = !(data[i].isSelected)
                            counter++;
                            petCount++;

                        } else {
                            isError = true
                        }
                    }
                }
            }
            return {
                ...state,
                pets: data,
                errorCounter: counter,
                isCapacityError: isError,
                petCount: petCount
            }

        case "SET_HUMAN_DATA":
            return {
                ...state,
                humanData: action.humanData
            }

        case "SET_CAPACITY_ERROR":
            return {
                ...state,
                isCapacityError: action.isCapacityError
            }

        case "SET_SAVE_LOCATION_DETAIL":
            return {
                ...state,
                saveLocationDetail: action.saveLocationDetail
            }

        case "SET_SAVED_LOCATION_NAME":
            return {
                ...state,
                saveLocationName: action.saveLocationName
            }

        case 'SET_CONFIGURATIONS':
            return {
                ...state,
                configurations: action.payload
            }

        case 'RIDE_IN_PROGRESS':
            return {
                ...state,
                isRideInProgress: action.isRideInProgress
            }

        case 'SHOULD_START_RIDE_FROM_MAP':
            return {
                ...state,
                shouldStartRideFromMap: action.shouldStartRideFromMap
            }

        case 'SET_IS_CONFIRM':
            return {
                ...state,
                isConfirmed: action.isConfirmed
            }

        case 'SET_ACTIVE_KEY':
            return {
                ...state,
                activeKey: action.activeKey
            }

        default:
            return state;
    }
};

export default ridingReducer;
