const initialState = {
    sizeEdit: [],
    categoryEdit: [],
    petProfile: {},
    isEditError: false,
    isPicEditError: false,
    isSizeEditError: false,
    isCategoryEditError: false,
    isDateEditPickerOpen: false,
    isSizeEditDropdownOpen: false,
    isCategoryEditDropdownOpen: false,
    pets: [],
    fromScreen: null
};

const petEditReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_PET_EDIT_PROFILE_RESPONSE':
            return {
                ...state,
                petProfile: { ...action.editResponse },
                isEditError: false
            }


        case 'SET_PET_EDIT_PROFILE_ERROR':
            return {
                ...state,
                isEditError: action.isEditError
            }

        case 'SET_CATEGORY_EDIT_RESPONSE':
            return {
                ...state,
                categoryEdit: [...action.categoryEdit],
                isCategoryEditError: false
            }

        case 'SET_CATEGORY_EDIT_ERROR':
            return {
                ...state,
                isCategoryEditError: action.isCategoryEditError
            }

        case 'SET_SIZE_EDIT_RESPONSE':
            return {
                ...state,
                sizeEdit: [...action.sizeEdit],
                isSizeEditError: false
            };

        case 'SET_SIZE_EDIT_ERROR':
            return {
                ...state,
                isSizeEditError: action.isSizeEditError
            }

        case 'OPEN_DATE_EDIT_PICKER':
            return {
                ...state,
                isDateEditPickerOpen: action.isDateEditPickerOpen

            };

        case 'OPEN_CATEGORY_EDIT_DROPDOWN':
            return {
                ...state,
                isCategoryEditDropdownOpen: action.isCategoryEditDropdownOpen
            }
        case 'OPEN_SIZE_EDIT_DROPDOWN':
            return {
                ...state,
                isSizeEditDropdownOpen: action.isSizeEditDropdownOpen
            }

        case 'SET_PET_EDIT_PICTURE':
            return {
                ...state,
                petProfile: {
                    ...state.petProfile,
                    pic: action.pic
                },
                isPicEditError: false
            }

        case 'SET_EDIT_PET_PICTURE_ERROR':
            return {
                ...state,
                isPicEditError: action.isPicEditError
            }

        case 'ON_CHANGE_EDIT_CATEGORY':
            return {
                ...state,
                petProfile: {
                    ...state.petProfile,
                    category: action.categoryEdit
                }
            }

        case 'ON_CHANGE_EDIT_SIZE':
            return {
                ...state,
                petProfile: {
                    ...state.petProfile,
                    size: action.sizeEdit
                }
            }

        case 'SET_PETS':
            return {
                ...state,
                pets: action.pets
            }
        case 'SET_FROM_SCREEN':
            return {
                ...state,
                fromScreen: action.fromScreen
            }



        default:
            return state;
    }
};

export default petEditReducer;
