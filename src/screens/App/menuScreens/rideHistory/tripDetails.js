import moment from "moment";
import React, { useRef, useState, useEffect } from "react";
import { Dimensions, Image, ScrollView, StyleSheet, Text, Touchable, TouchableOpacity, View } from "react-native";
import { Rating } from "react-native-ratings";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
import SvgUri from 'react-native-svg-uri';

import Images from "../../../../assets/Images/Images";
import { getRideDetails } from "../../../../redux/actions/rideAction";
import { backNavigation, navigate } from "../../../../root/navigation/rootNavigation";
import { DATE_FORMATS, PAYMENT_METHODS, RIDE_STATUS } from "../../../../utils/constants";
import { fSize } from "../../../../utils/Responsive";
import { ScreenNames } from "../../../../utils/ScreenNames";
import { MEDIUM, REGULAR } from "../../../../utils/typography";
import DriverDetails from "./component/DriverDetails";
import PaymentDetails from "./component/PaymentDetails";
import ReceiptModal from "./component/ReceiptModal";



const TripDetails = (props) => {
    const dispatch = useDispatch()
    const {rideDetailsResponse} = useSelector(state=>state.ride)
  
    const [tripID, setTripID] = useState(props?.route?.params?.id)
    const [modalVisible, setModalVisible] = useState(false);


    const onClickReceipt = () => {
        if(rideDetailsResponse?.status === RIDE_STATUS.CANCELLED){
            setModalVisible(true)
            return 
        }
        navigate(ScreenNames.TRIP_RECEIPT, {id: rideDetailsResponse?.payment?.id})
    }

    const onClickHelp = () => {
        navigate(ScreenNames.TRIP_HELP, {id: tripID})
    }

    const onClickFindLostItem = () => {
        navigate(ScreenNames.FIND_LOST_ITEM, {id: tripID})
    }

    const onclickReport = () => {
        navigate(ScreenNames.TRIP_REPORT,{id: tripID})
    }

    useEffect(()=>{
        if(tripID){
            dispatch(getRideDetails(tripID))
        }
    },[tripID])

    return (
        <View style={styles.container}>
            <View style={styles.topView}>
                <TouchableOpacity style={styles.topViewButton} onPress={() => { backNavigation() }}>
                    <Image source={Images.arrow_back} style={styles.topViewButtonIcon} />
                </TouchableOpacity>
            </View>
            <View style={styles.middleView}>
                <View style={styles.middleViewSection}>
                    <Text style={styles.middleViewSectionText}>
                        {"Trip Details"}
                    </Text>
                </View>
            </View>
            <ScrollView style={{flex:1}}>
                <View style={styles.container1}>
                    <View style={styles.container1Section1}>
                        <Image source={{uri: rideDetailsResponse?.ride?.map_url}} style={styles.mapImage} />
                    </View>
                    <View style={styles.container1Section2}>
                        <View style={styles.container1Section2Row1}>
                            <View style={styles.container1Section2Row1Left}>
                                <Text style={styles.container1Section2Row1LeftText}>
                                    {moment(rideDetailsResponse?.timestamp).format(DATE_FORMATS.DO_MMM_HH_MM_A)}
                                </Text>
                            </View>
                            <View style={styles.container1Section2Row1Right}>
                                <Text style={styles.container1Section2Row1RightText}>
                                    {rideDetailsResponse?.currency}{rideDetailsResponse?.price}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.container1Section2Row2}>
                            <Text style={styles.container1Section2Row2Text}>
                                {"Ride "}{rideDetailsResponse?.status}
                            </Text>
                        </View>
                        <View style={{ paddingVertical: 10 }}></View>
                        <View style={styles.container1Section2Row3}>
                            <View style={styles.container1Section2Row3Left}>
                                <SvgUri 
                                    height={24}
                                    width={24}
                                    source={Images.startLocationSvg}
                                />
                                {/* <Image source={Images.locationBlackIcon} style={styles.container1Section2Row3LeftImage} /> */}
                                <View style={styles.container1Section2Row3LeftDivider} />
                                <View style={styles.container1Section2Row3LeftDotConatiner}>
                                    <View style={styles.container1Section2Row3LeftDot} />
                                </View>
                            </View>
                            <View style={styles.container1Section2Row3Right}>
                                <View>
                                    <Text style={styles.container1Section2Row3RightText1}>
                                        {rideDetailsResponse?.source_name}
                                    </Text>
                                    <Text style={styles.container1Section2Row3RightText2} numberOfLines={2}>
                                        {rideDetailsResponse?.source_address}
                                    </Text>
                                </View>
                                <View style={{paddingTop: 40}}>
                                    <Text style={styles.container1Section2Row3RightText1}>
                                        {rideDetailsResponse?.destination_name}
                                    </Text>
                                    <Text style={styles.container1Section2Row3RightText2} numberOfLines={2}>
                                        {rideDetailsResponse?.destination_address}
                                    </Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.container2}>
                    <View style={styles.container2Section1}>
                        <Text style={styles.container2Section1Text}>
                            {"Driver Details"}        
                        </Text>
                    </View>
                    <View style={styles.container2Section2}>
                       <DriverDetails data={rideDetailsResponse}/>
                    </View>  
                    <View style={styles.container2Section3}>
                    {
                        rideDetailsResponse?.status === RIDE_STATUS.COMPLETED
                            &&
                        rideDetailsResponse?.payment?.method === PAYMENT_METHODS.CARD
                            &&     
                        <>
                        <View style={styles.container2Section3Row1}>
                                <Text style={styles.container2Section3Row1Text}>
                                    {"Payment Details"}        
                                </Text>
                            </View>
                            <View style={styles.container2Section3Row2}>
                                <PaymentDetails data={rideDetailsResponse?.payment} />
                            </View>
                        </>
                    }
                        <View style={styles.container2Section3ListContainer}>
                            <TouchableOpacity style={[styles.container2Section3ListItem, styles.borderTopBottom]} onPress={()=>{onClickReceipt()}}>
                                <View style={styles.container2Section3ListItemLeft}>
                                    <SvgUri
                                        source={Images.recieptIconSvg}
                                    />
                                </View>
                                <View style={styles.container2Section3ListItemRight}>
                                    <Text style={styles.container2Section3ListItemRightText}>
                                        {"Receipt"}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.container2Section3ListItem, styles.borderBottom]} onPress={()=>{onClickHelp()}}>
                                <View style={styles.container2Section3ListItemLeft}>
                                    <SvgUri
                                        source={Images.helpIconSvg}
                                    />
                                </View>
                                <View style={styles.container2Section3ListItemRight}>
                                    <Text style={styles.container2Section3ListItemRightText}>
                                        {"Help"}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.container2Section3ListItem, styles.borderBottom]} onPress={()=>{onClickHelp()}}>
                                <View style={[styles.container2Section3ListItemLeft, {marginLeft: 5}]}>
                                    <SvgUri
                                        source={Images.findLostItemSvg}
                                    />
                                </View>
                                <View style={styles.container2Section3ListItemRight}>
                                    <Text style={[styles.container2Section3ListItemRightText,{marginLeft: -5}]}>
                                        {"Find Lost Item"}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.container2Section3ListItem, styles.borderBottom]} onPress={()=>{onclickReport()}}>
                                <View style={styles.container2Section3ListItemLeft}>
                                    <SvgUri
                                        source={Images.supportIconSvg}
                                    />
                                </View>
                                <View style={styles.container2Section3ListItemRight}>
                                    <Text style={styles.container2Section3ListItemRightText}>
                                        {"Report"}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>          
                </View>
            </ScrollView>
            {
                modalVisible &&
                <ReceiptModal 
                    title={"Sorry"}
                    message1={"Your trip was cancelled"}
                    message2={"Hence, the receipt is unavailable"}
                    onOk={()=>{
                        setModalVisible(false)
                    }}
                />
            }
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#253040",
        paddingTop: 20
    },
    topView: {
        flex: 0.1,
        paddingHorizontal: 20
    },
    topViewButton: {
        padding: 10,
        width: 44,
        height: 44,
        backgroundColor: '#323E51',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewButtonIcon: {
        width: 8,
        height: 16,
    },
    middleView: {
        flex: 0.1,
        paddingHorizontal: 20
    },
    middleViewSection: {
        flex: 1,
        justifyContent: 'center'
    },
    middleViewSectionText: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F5EDDC"
    },
    container1: {
        flex: 8,
        backgroundColor: "#253040",
        paddingHorizontal: 20
    },
    container1Section1: {
        height: hp(24),
        borderRadius: 15,
        backgroundColor: "#FFFFFF",
        zIndex: 2,
        overflow: 'hidden' 
    },
    mapImage: {
        width: "100%",
        height: undefined,
        aspectRatio: 2/1.2
    },
    container1Section2: {
        backgroundColor: "#323E51",
        borderRadius: 15,
        borderTopRightRadius: 0,
        borderTopLeftRadius: 0,
        marginTop: -15,
        padding: 15,
        paddingTop: 30
    },
    container1Section2Row1: {
        flex: 0.2,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5
    },
    container1Section2Row1Left: {
        flex: 0.8,
    },
    container1Section2Row1LeftText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 20,
        color: "#F5EDDC"
    },
    container1Section2Row1Right: {
        flex: 0.2,
        alignItems: 'flex-end'
    },
    container1Section2Row1RightText: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#E1CDA9"
    },
    container1Section2Row2: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5
    },
    container1Section2Row2Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 20,
        color: "#F5EDDC"
    },
    container1Section2Row3: {
        flex: 0.2,
        flexDirection: 'row',
    },
    container1Section2Row3Left: {
        flex: 0.15,
        justifyContent: 'center'
    },
    container1Section2Row3LeftImage: {
        width: 20,
        height: 20,
    },
    container1Section2Row3LeftDivider: {
        marginVertical: 5,
        borderLeftWidth: 2,
        marginLeft: 10,
        height: 60,
        borderColor: '#9D7E62'
    },
    container1Section2Row3LeftDotConatiner: {
        height: 20,
        width: 20,
        borderRadius: 10,
        backgroundColor: "#E1CDA9",
        justifyContent: 'center',
        alignItems: 'center'
    },
    container1Section2Row3LeftDot: {
        height: 12,
        width: 12,
        borderRadius: 6,
        backgroundColor: '#B59B7C'
    },
    container1Section2Row3Right: {
        flex: 0.85,
        // justifyContent: 'space-between'
    },
    container1Section2Row3RightText1: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9"
    },
    container1Section2Row3RightText2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#A58B6C",
        paddingTop: 5
    },  
    container2: {
        flex: 1,
        backgroundColor: "#17202D",
        borderRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
        padding: 20,
        marginTop: 20,
    },
    container2Section1: {
        flex: 1,
        paddingTop: 10,
        paddingBottom: 15
    },
    container2Section1Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F5EDDC"
    },
    container2Section2: {
        flex: 4,
    },
    container2Section3: {
        flex: 5,
        paddingTop: 20
    },
    container2Section3Row1: {
        flex: 1
    },
    container2Section3Row1Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F5EDDC"
    },
    container2Section3Row2: {
        paddingTop: 15,
    },
    container2Section3ListContainer: {
        paddingVertical: 20
    },
    borderTopBottom: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#564F47",
    },
    borderBottom: {
        borderBottomWidth: 1,
        borderColor: "#564F47",
    },
    container2Section3ListItem: {
        flexDirection: 'row',
        paddingVertical: 15
    },
    container2Section3ListItemLeft: {
        flex: 0.20,
        alignItems: 'flex-start'
    },
    container2Section3ListItemLeftImage: {
        width: 20,
        height: 25,
    },
    container2Section3ListItemRight: {
        flex: 0.80,
        alignItems: 'flex-start',
        justifyContent: 'center',
    },
    container2Section3ListItemRightText: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F5EDDC"
    }
})

export default TripDetails;