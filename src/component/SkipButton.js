import React from "react";
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

import { fSize } from "../utils/Responsive";
import { REGULAR } from "../utils/typography";

const SkipButton = ({onPress,title}) => (
    <TouchableOpacity  onPress={onPress} style={styles.skipButton}>
        <Text style={styles.skipButtonText}>
            {title}
        </Text>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    skipButton:{
        backgroundColor:'transparent',
        height:44,
        alignItems:'center',
        justifyContent:'center'
    },
    skipButtonText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        textAlign: 'center',
        color: '#F5EDDC'
    }
})

export default SkipButton;

