import React, { useState } from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text, Dimensions, TextInput, ScrollView, SafeAreaView } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../../../assets/Images/Images";
import { backNavigation, navigate } from "../../../../../root/navigation/rootNavigation";
import { fSize } from "../../../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../../utils/typography";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import ReportHeader from "./components/reportHeader";
import ReportFooter from "./components/reportFooter";
import { vehicleDifferent } from "../../../../../redux/actions/rideAction";
import { ScreenNames } from "../../../../../utils/ScreenNames";


const screenHeight = Dimensions.get('screen').height
const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}

const ReportVehicleDifferent = (props) => {
    const dispatch = useDispatch()
    const [tripID, setTripID] = useState(props?.route?.params?.id)
    const [colorModelMake, setColorModelMake] = useState('')
    const [licensePlate, setLicensePlate] = useState('')
    const [differentDriver, setDifferentDriver] = useState('')
    const [otherPassenger, setOtherPassenger] = useState('')

    const [isError, setIsError] = useState({})
    
    const onSubmit = async () => {
        try {
            let formData = {
                trip_id: tripID,
                color_model_make: colorModelMake,
                license_plate: licensePlate,
                different_drive: differentDriver,
                outher_passangers: otherPassenger,
            }
            let resp = await vehicleDifferent(formData)
            navigate(ScreenNames.TRIP_DETAILS,{id:tripID})
        } catch (error) {
            console.log(error)
        }
    }

    const checkIfAllFieldFillup = () => {
        if(colorModelMake && licensePlate && differentDriver && otherPassenger){
            return true
        }else{
            return false
        }
    }
    
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <ReportHeader />
            </View>
            <KeyboardAwareScrollView 
                contentContainerStyle={styles.content} 
                enableOnAndroid={true} 
                scrollEnabled={true}
            >
            <View style={styles.content}>
                <ScrollView contentInsetAdjustmentBehavior="always">
                    <View style={styles.contentBody}>
                        <View style={styles.contentBodySectionTitle}>
                            <Text style={styles.contentBodySectionTitleText}>
                                {"My driver’s vehicle was different"}
                            </Text>
                        </View>
                        <View style={styles.contentBodyDescription}>
                            <Text style={styles.contentBodyDescriptionText} >
                                {
                                    `If your driver arrived in a vehicle different that what the app 
                                    confirmed ( vehicle make, model or license plate number), 
                                    please provide the information here.`
                                }
                            </Text>
                        </View>
                        <View style={styles.contentBodyFormItem}>
                            <Text style={styles.contentBodyFormItemLabel}>
                                {"Vehicle’s color, make, and model"}
                            </Text>
                            <TextInput
                                placeholderTextColor={"#ADADAE"}
                                style={[
                                    styles.contentBodyFormItemInput,
                                    isError["colorModelMake"] && styles.error,
                                ]}
                                value={colorModelMake}
                                onChangeText={(text)=>{
                                    setColorModelMake(text);
                                    if(isError["colorModelMake"]){
                                        setIsError["colorModelMake"] = false
                                    }
                                }}
                            />
                        </View>
                        <View style={styles.contentBodyFormItem}>
                            <Text style={styles.contentBodyFormItemLabel}>
                                {"License plate"}
                            </Text>
                            <TextInput
                                placeholderTextColor={"#ADADAE"}
                                placeholder={"Can you recall any numbers or letters?"}
                                style={[
                                    styles.contentBodyFormItemInput,
                                    isError["licensePlate"] && styles.error,
                                ]}
                                value={licensePlate}
                                onChangeText={(text)=>{
                                    setLicensePlate(text);
                                    if(isError["licensePlate"]){
                                        setIsError["licensePlate"] = false
                                    }
                                }}
                            />
                        </View>
                        <View style={styles.contentBodyFormItem}>
                            <Text style={styles.contentBodyFormItemLabel}>
                                {"Was your driver a different person?"}
                            </Text>
                            <TextInput
                                placeholderTextColor={"#ADADAE"}
                                style={[
                                    styles.contentBodyFormItemInput,
                                    isError["differentDriver"] && styles.error,
                                ]}
                                value={differentDriver}
                                onChangeText={(text)=>{
                                    setDifferentDriver(text);
                                    if(isError["differentDriver"]){
                                        setIsError["differentDriver"] = false
                                    }
                                }}
                            />
                        </View>
                        <View style={styles.contentBodyFormItem}>
                            <Text style={styles.contentBodyFormItemLabel}>
                                {"Were other passengers in the vehicle?"}
                            </Text>
                            <TextInput
                                placeholderTextColor={"#ADADAE"}
                                style={[
                                    styles.contentBodyFormItemInput,
                                    isError["otherPassenger"] && styles.error,
                                ]}
                                value={otherPassenger}
                                onChangeText={(text)=>{
                                    setOtherPassenger(text);
                                    if(isError["otherPassenger"]){
                                        setIsError["otherPassenger"] = false
                                    }
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
            </KeyboardAwareScrollView>
            <View style={styles.footer}>
               <ReportFooter 
                    comment={checkIfAllFieldFillup()} 
                    onSubmit={onSubmit} 
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: getHeight(100),
        backgroundColor: '#17202D',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        height: getHeight(Platform.OS === 'ios' ? 5 : 15 ),
        paddingHorizontal: 20,
        justifyContent: 'center'
    },
    content: {
        height: getHeight(Platform.OS === 'ios' ? 80 : 70 ),
    },
    contentBody: {
        paddingTop: 10,
        paddingHorizontal: 20,
        justifyContent: 'flex-end'
    },
    contentBodySection1Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#F9D591"
    },
    contentBodySectionTitle: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    contentBodySectionTitleText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#E1CDA9",
    },
    contentBodyDescription: {
        paddingVertical: 10
    },
    contentBodyDescriptionText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 22,
        color: "#FFFFFF",
        // textAlign: 'center'
    },
    contentBodyFormItem: {
        marginVertical: 10
    },
    contentBodyFormItemLabel: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9",
    },
    contentBodyFormItemInput: {
        marginVertical: 5,
        height:50,
        borderRadius: 15,
        backgroundColor: "#E2EAED",
        paddingHorizontal: 10,
        color:'#ADADAE'
    },
    error: {
        borderWidth: 2,
        borderColor: 'red'
    },
    footer: {
        height: getHeight(Platform.OS === 'ios' ? 5 : 5 ),
        paddingHorizontal: 20,
        justifyContent:'center'
    }
})

export default ReportVehicleDifferent;