
import appService from '../../service/appService'
import { ScreenNames } from '../../utils/ScreenNames'
import { navigate } from "../../root/navigation/rootNavigation";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { store } from '../store/store';

export function openDateEditPicker(isDateEditPickerOpen) {
    return {
        type: 'OPEN_DATE_EDIT_PICKER',
        isDateEditPickerOpen
    }
}

export function openCategoryEditDropdown(isCategoryEditDropdownOpen) {
    return {
        type: 'OPEN_CATEGORY_EDIT_DROPDOWN',
        isCategoryEditDropdownOpen
    }
}

export function openSizeEditDropdown(isSizeEditDropdownOpen) {
    return {
        type: 'OPEN_SIZE_EDIT_DROPDOWN',
        isSizeEditDropdownOpen
    }
}


export function setPicEditPicture(pic) {
    return {
        type: 'SET_PET_EDIT_PICTURE',
        pic
    }
}


export function setPicEditError(isPicEditError) {
    return {
        type: 'SET_PET_EDIT_PICTURE_ERROR',
        isPicEditError
    };
}

export function setEditResponse(editResponse) {
    return {
        type: 'SET_PET_EDIT_PROFILE_RESPONSE',
        editResponse
    };
}

export function setEditError(isEditError) {
    return {
        type: 'SET_PET_EDIT_PROFILE_ERROR',
        isEditError
    };
}

export function setCategoryEdit(categoryEdit) {
    return {
        type: 'SET_CATEGORY_EDIT_RESPONSE',
        categoryEdit
    };
}

export function setCategoryEditError(isCategoryEditError) {
    return {
        type: 'SET_CATEGORY_EDIT_ERROR',
        isCategoryEditError
    };
}

export function setSizeEdit(sizeEdit) {
    return {
        type: 'SET_SIZE_EDIT_RESPONSE',
        sizeEdit
    };
}

export function setSizeEditError(isSizeEditError) {
    return {
        type: 'SET_SIZE_EDIT_ERROR',
        isSizeEditError
    };
}

export function onChangeEditCategory(categoryEdit) {
    return {
        type: 'ON_CHANGE_EDIT_CATEGORY',
        categoryEdit
    };
}

export function onChangeEditSize(sizeEdit) {
    return {
        type: 'ON_CHANGE_EDIT_SIZE',
        sizeEdit
    };
}

export function setPets(pets) {
    return {
        type: 'SET_PETS',
        pets
    };
}
export function setFromScreen(fromScreen) {
    return {
        type: 'SET_FROM_SCREEN',
        fromScreen
    };
}

export function getEditCategory() {
    return async (dispatch) => {
        return appService.get('fb/v1/pet/category')
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                var temp = response.map((item) => {
                    return { label: item.name, value: item.pcid }
                })
                dispatch(setCategoryEdit(temp))

            })
            .catch((error) => {

                console.log(error)
                dispatch(setCategoryEditError(true))
            });
    };
}

export function getEditSize() {
    return async (dispatch) => {
        return appService.get('fb/v1/pet/size')
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {

                var temp = response.map((item) => {
                    return { label: item.name, value: item.psid }
                })
                dispatch(setSizeEdit(temp))
            })
            .catch((error) => {

                console.log(error)
                dispatch(setSizeEditError(true))
            });
    };
}

export function createPetEditProfile(data) {
    return async (dispatch) => {
        return appService.post('fb/v1/pet', data)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setEditResponse(response))
                navigate(ScreenNames.EDIT_PET_PICTURE)
            })
            .catch((error) => {
                console.log(error)
                dispatch(setEditError(true))
            });
    };
}

export function updatePetEditProfile(data) {
    return async (dispatch) => {
        return appService.patch(`fb/v1/pet/${data.pid}`, data)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setEditResponse(response))
                dispatch(getPets())
                navigate(ScreenNames.EDIT_PET_PICTURE)
            })
            .catch((error) => {
          
                console.log(error)
                dispatch(setEditError(true))
            });
    };
}

export function uploadPetEditPic(data, token) {
    return async (dispatch) => {
        return appService.post(`fb/v1/pet/upload/${token}`, data)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setEditResponse(response))
                dispatch(getPets())
                if (store.getState().petEdit.fromScreen) {
                    navigate(store.getState().petEdit.fromScreen)
                }
            })
            .catch((error) => {
                console.log(JSON.stringify(error))
                dispatch(setPicEditError(true))
            });
    };
}


export function getPets() {
    return async (dispatch) => {
        return appService.get(`fb/v1/pet`)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(setPets(response))
            })
            .catch((error) => {
                console.log(JSON.stringify(error))
                //dispatch(setError(true))
            });
    };
}

export function deletePet(id) {
    return async (dispatch) => {
        return appService.delete(`fb/v1/pet/${id}`)
            .then((response) => {
                if (response.status !== 200) {
                    throw Error(response);
                }
                return response.data;
            })
            .then((response) => {
                dispatch(getPets())
                navigate(ScreenNames.PET_LIST)
            })
            .catch((error) => {
                console.log(JSON.stringify(error))
                //dispatch(setError(true))
            });
    };
}