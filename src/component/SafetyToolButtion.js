import React, { memo, useMemo } from 'react'
import { Share, View, Text, Image, StyleSheet, TouchableOpacity, Modal, Linking } from 'react-native'
import Images from '../assets/Images/Images';
import { setSafetyModal, shareTrip } from '../redux/actions/rideAction';

import { store } from '../redux/store/store';
import { fSize } from '../utils/Responsive';
import { MEDIUM, REGULAR } from '../utils/typography';
const SafetyToolButton = ({
    bottomMargin
}) => {
console.log('#######################################')
    const ride = store.getState().ride  
    return (
        <TouchableOpacity onPress={() => store.dispatch(setSafetyModal(true))} style={[styles.container, { bottom: bottomMargin - 60 }]}>
            <Image source={Images.safety} style={styles.safety} />
            <Modal
                visible={ride?.isSafetyModalVisible}
                transparent={true}
                onRequestClose={() => {
                    store.dispatch(setSafetyModal(false))
                }}
            >
                <View onTouchStart={() => store.dispatch(setSafetyModal(false))} style={styles.modalContainer}>
                    <View onTouchStart={(e) => e.stopPropagation()} style={styles.modalSubContainer}>
                        <View style={styles.infoContainer}>
                            <Text style={styles.modalHeading}>{'Safety Tools'}</Text>
                        </View>
                        <TouchableOpacity onPress={() => store.dispatch(shareTrip())} style={styles.infoContainer}>
                            <Image source={Images.share_ride} style={styles.shareRide} />
                            <Text style={styles.shareRideText}>{'Share My Ride'}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                Linking.openURL(`tel:${119}`)
                                store.dispatch(setSafetyModal(false))
                            }} style={styles.infoContainer}>
                            <Image source={Images.call} style={styles.call} />
                            <Text style={styles.policeAssistanceText}>{'Police Assistance'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </Modal>
        </TouchableOpacity>
    )
}
const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        left: 0,
        zIndex: 0,
        padding: 15,
        backgroundColor: '#253040',
        borderRadius: 40,
        margin: 15,
    },
    safety: {
        // height: 24,
        // width: 20
    },
    modalContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.3)',
        justifyContent: 'flex-end',


    },
    modalSubContainer: {
        backgroundColor: '#17202D',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
        padding: 20
    },
    modalHeading: {
        color: '#F6F6F6',
        fontSize: fSize(20),
        fontFamily: MEDIUM
    },
    shareRideText: {
        color: '#F3F4F4',
        fontSize: fSize(16),
        fontFamily: REGULAR
    },

    infoContainer: {
        paddingVertical: 10,
        flexDirection: 'row'
    },

    policeAssistanceText: {
        color: '#F3F4F4',
        fontSize: fSize(16),
        fontFamily: REGULAR
    },
    shareRide: {
        margin: 5,
        marginRight: 15,
        height: 25,
        width: 19
    },
    call: {
        margin: 5,
        marginRight: 15,
        height: 22,
        width: 22
    }


})
export default  memo(SafetyToolButton);