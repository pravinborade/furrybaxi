import React, { memo } from "react";
import { View, TouchableOpacity, StyleSheet, Text, Image } from "react-native";
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Rating, AirbnbRating } from 'react-native-ratings';

import Images from "../../../assets/Images/Images";
import { fSize } from "../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../utils/typography";
import { ScreenNames } from "../../../utils/ScreenNames";
import { navigate } from "../../../root/navigation/rootNavigation";

const Container2 = (props) => {
    const {  currentPlace, destinationPlace,
        selectedCabResponse, userResponse, 
        durationResponse, onClickContact
    } = props;    
  
    return (
        <View style={styles.actionContainer1}  >
            <View style={styles.actionContainer1Top}>
                <View style={styles.actionContainer1TopButton}/>
                <Text style={styles.actionContainer1TopText}>
                    {`Your Ride is arriving in ${durationResponse?.data?.duration ? durationResponse?.data?.duration : 0} min`}
                </Text>
                <View style={styles.actionContainer1TopBorder} />
            </View>
            <View style={styles.actionContainer1MiddleView}>
                <View style={styles.actionContainer1Middle}>
                    <View style={styles.actionContainer1MiddleContainer}>
                        <View style={styles.actionContainer1MiddleContainerLeft}>
                            {
                                selectedCabResponse?.data?.driver ?
                                <Image source={{uri:selectedCabResponse?.data?.driver?.profile}}
                                    style={styles.actionContainer1MiddleContainerLeftImage}
                                />
                             :  <Image source={Images.profile_image}
                                    style={styles.actionContainer1MiddleContainerLeftImage}
                                />
                            }
                           
                            <Rating
                                type={'custom'}
                                style={{ paddingVertical: 5,width:5 }}
                                imageSize={10}
                                tintColor={"#404D62"}
                                ratingColor={"#F9D591"}
                                ratingBackgroundColor='#fff'
                                ratingCount={5}
                                fractions={5}
                                startingValue={selectedCabResponse?.data?.rating}
                            />
                        </View>
                        <View style={{ flex: .5 }}>

                        </View>
                        <View style={styles.actionContainer1MiddleContainerRight}>
                            <Text style={styles.actionContainer1MiddleContainerRightText1}>
                               {selectedCabResponse?.data?.driver?.first_name +" "+selectedCabResponse?.data?.driver?.last_name}
                            </Text>
                            <Text style={styles.actionContainer1MiddleContainerRightText2}>
                                {selectedCabResponse?.data?.cab?.vehicle_no}
                                <Text style={styles.actionContainer1MiddleContainerRightText2Text}>
                                    {" / "+selectedCabResponse?.data?.cab?.model}
                                </Text>
                            </Text>
                            <View style={styles.actionContainer1MiddleContainerRightView}>
                                <TouchableOpacity
                                    style={styles.actionContainer1MiddleContainerRightViewButton1}
                                >
                                    <Text style={styles.actionContainer1MiddleContainerRightViewButton1Text}>
                                        {"Message"}
                                    </Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.actionContainer1MiddleContainerRightViewButton2} onPress={onClickContact}>
                                    <Text style={styles.actionContainer1MiddleContainerRightViewButton2Text}>
                                        {"Contact"}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.actionContainer1MiddleViewBottom}>
                    <View style={styles.actionContainer1MiddleViewBottomTop}>
                        <Text style={styles.actionContainer1MiddleViewBottomTopText1}>
                            {"Your Current Ride"}
                        </Text>
                        <Text style={styles.actionContainer1MiddleViewBottomTopText2}>
                            {"Share"}
                        </Text>
                    </View>
                    <View style={styles.actionContainer1MiddleViewBottomMiddle}>
                        <View style={styles.actionContainer1MiddleViewBottomMediumVerticalLine}/>
                        <View style={styles.actionContainer1MiddleViewBottomMiddleLeft}>
                            <Image source={Images.currentLocation}/>
                        </View>
                        <View style={styles.actionContainer1MiddleViewBottomMiddleCenter}>
                            <Text numberOfLines={1} ellipsizeMode='tail' style={styles.actionContainer1MiddleViewBottomMiddleCenterText1}>
                                {currentPlace?.name}
                            </Text>
                            <View style={{height:hp(.1)}}></View>
                            <Text numberOfLines={2} ellipsizeMode='tail' style={styles.actionContainer1MiddleViewBottomMiddleCenterText2}>
                                {currentPlace?.address}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.actionContainer1MiddleViewBottomEnd}>
                        <View style={styles.actionContainer1MiddleViewBottomEndLeft}>
                            <View style={styles.actionContainer1MiddleViewBottomEndLeftContainer}>
                                <View style={styles.actionContainer1MiddleViewBottomEndLeftContainerDot}/>
                            </View>
                        </View>
                        <View style={styles.actionContainer1MiddleViewBottomEndMiddle}>
                            <Text numberOfLines={1} ellipsizeMode='tail' style={styles.actionContainer1MiddleViewBottomEndMiddleText1}>
                                {destinationPlace?.name}
                            </Text>
                            <View style={{height:hp(.1)}}></View>
                            <Text numberOfLines={2} ellipsizeMode='tail' style={styles.actionContainer1MiddleViewBottomEndMiddleText2}>
                                {destinationPlace?.address}
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.actionContainer1BottomTop}>
                <View style={styles.actionContainer1BottomTopView1}>
                    <View style={styles.actionContainer1BottomTopView1Left}>
                        <Text style={styles.actionContainer1BottomTopView1LeftText}>
                            {"Payment"}
                        </Text>
                    </View>
                    <View style={styles.actionContainer1BottomTopView1Right}>
                        <Text style={styles.actionContainer1BottomTopView1RightText}>
                            {"Change >"}
                        </Text>
                    </View>
                </View>
                {
                    userResponse?.payments && userResponse?.payments?.method === "card"
                        &&
                    <View style={styles.actionContainer1BottomTopView2}>
                        <View style={styles.actionContainer1BottomTopView2Row1}>
                            <Text style={styles.actionContainer1BottomTopView2Row1Text1}>
                                {userResponse?.payments?.card_type +" "+ userResponse?.payments?.method}
                            </Text>
                            <Text style={styles.actionContainer1BottomTopView2Row1Text2}>
                                {selectedCabResponse?.data?.price}
                            </Text>
                        </View>  
                        <View style={styles.actionContainer1BottomTopView2Row2}>
                            <Text style={styles.actionContainer1BottomTopView2Row2Text}>
                                {"xxxx xxxx xxxx "+ userResponse.payments.last_four}
                            </Text>
                        </View>      
                    </View>
                }
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    actionContainer1Top: {
        height: hp(8),
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    actionContainer1TopButton: {
        height: hp(1),
        width: hp(8),
        backgroundColor: '#A3A8B1',
        borderRadius: 25
    },
    actionContainer1TopText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#F9D591"
    },
    actionContainer1TopBorder: {
        height: hp(.2),
        backgroundColor: "#253040",
        width: '80%',
    },
    actionContainer1MiddleView: {
        height: hp(45),
        backgroundColor: '#253040',
        borderRadius: 16,
        justifyContent:'flex-start'
    },
    actionContainer1Middle: {
        height: hp(20),
        justifyContent: 'flex-start'
    },
    actionContainer1MiddleContainer: {
        backgroundColor: "#E2EAED",
        borderRadius: 20,
        height: hp(18),
        flexDirection: 'row',
        padding: 10,
    },
    actionContainer1MiddleContainerLeft: {
        flex: 2.5,
        backgroundColor: '#404D62',
        borderRadius: 15,
        alignItems: 'center'
    },
    actionContainer1MiddleContainerLeftImage: {
        width: '100%',
        height: '80%',
        resizeMode: 'cover',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    actionContainer1MiddleContainerRight: {
        flex: 7.5,
        justifyContent: 'space-around',
    },
    actionContainer1MiddleContainerRightText1: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#364458"
    },
    actionContainer1MiddleContainerRightText2: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#364458"
    },
    actionContainer1MiddleContainerRightText2Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        //lineHeight: 15,
        color: "#364458"
    },
    actionContainer1MiddleContainerRightView: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    actionContainer1MiddleContainerRightViewButton1: {
        backgroundColor: "#F1F7F9",
        borderRadius: 10,
        paddingVertical: 8,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    actionContainer1MiddleContainerRightViewButton1Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#6D7580"
    },
    actionContainer1MiddleContainerRightViewButton2: {
        backgroundColor: "#F1F7F9",
        borderRadius: 10,
        paddingVertical: 8,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    actionContainer1MiddleContainerRightViewButton2Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#6D7580"
    },
    actionContainer1MiddleViewBottom: {
        height: hp(25),
        padding: 10,
        justifyContent:'space-between'
    },
    actionContainer1MiddleViewBottomTop: {
        height: hp(4),
        flexDirection: 'row',  
    },
    actionContainer1MiddleViewBottomTopText1: {
        flex: .8,
        textAlign: 'left',
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#F5EDDC"
    },
    actionContainer1MiddleViewBottomTopText2: {
        flex: .2,
        textAlign: 'right',
        fontFamily: REGULAR,
        fontSize: fSize(13),
        lineHeight: 16,
        color: "#E2EAED"
    },
    actionContainer1MiddleViewBottomMiddle: {
        height: hp(8),
        flexDirection: 'row',  
    },
    actionContainer1MiddleViewBottomMediumVerticalLine: {
        width: hp(.2),
        height: hp(5),
        backgroundColor: "#9D7E62",
        position: 'absolute',
        top: 20,
        left: 7
    },
    actionContainer1MiddleViewBottomMiddleLeft: {
        flex:1,
    },
    actionContainer1MiddleViewBottomMiddleCenter: {
        flex:9,
    },
    actionContainer1MiddleViewBottomMiddleCenterText1: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9"
    },
    actionContainer1MiddleViewBottomMiddleCenterText2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#A58B6C"
    },
    actionContainer1MiddleViewBottomEnd: {
        height: hp(8),
        flexDirection: 'row',  
    },
    actionContainer1MiddleViewBottomEndMiddle: {
        flex:9,
    },
    actionContainer1MiddleViewBottomEndMiddleText1: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9"
    },
    actionContainer1MiddleViewBottomEndMiddleText2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#A58B6C"
    },
    actionContainer1MiddleViewBottomEndLeftContainer: {
        padding: 8,
        width: 8,
        height: 8,
        backgroundColor: "#E1CDA9",
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    actionContainer1MiddleViewBottomEndLeftContainerDot: {
        width: 8,
        height: 8,
        backgroundColor: "#B59B7C",
        borderRadius: 8
    },
    actionContainer1MiddleViewBottomEndLeft: {
        flex:1
    },
    actionContainer1BottomTop: {
        height: hp(25),
        justifyContent: 'flex-start'
    },
    actionContainer1BottomTopView1: {
        height: hp(8),
        flexDirection: 'row',
        alignItems: 'center'
    },
    actionContainer1BottomTopView1Left: {
        flex: 7
    },
    actionContainer1BottomTopView1LeftText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#F5EDDC"
    },
    actionContainer1BottomTopView1Right: {
        flex: 3,
        alignItems: 'flex-end'
    },
    actionContainer1BottomTopView1RightText: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#F5EDDC"
    },
    actionContainer1BottomTopView2: {
        height: hp(15),
        backgroundColor: '#222831',
        borderRadius: 16,
        padding: 20
    },
    actionContainer1BottomTopView2Row1: {
        flexDirection: 'row',
        height: hp(5)
    },
    actionContainer1BottomTopView2Row1Text1: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9",
        flex: 7,
        textAlign: 'left'
    },
    actionContainer1BottomTopView2Row1Text2: {
        fontFamily: MEDIUM,
        fontSize: fSize(16),
        lineHeight: 19,
        color: "#E1CDA9",
        flex: 3,
        textAlign: 'right'
    },
    actionContainer1BottomTopView2Row2: {
        flexDirection: 'row',
        height: hp(5)
    },
    actionContainer1BottomTopView2Row2Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        lineHeight: 14,
        color: "#A58B6C",
        textAlign: 'left'
    },
})

export default memo(Container2);