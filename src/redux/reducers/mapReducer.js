import { createRef } from "react";
import Images from "../../assets/Images/Images";

const initialState = {
    currentPlace: null,
    destinationPlace: null,
    searchResult: [],
};
  
  const mapReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_CURRENT_PLACE':
            return {
                ...state,
                currentPlace: {...action.currentPlace}
            }    
        
        case 'SET_DESTINATION_PLACE':
            return {
                ...state,
                destinationPlace: {...action.destinationPlace}
            }

        case 'SET_SEARCH_RESULT':
            return {
                ...state,
                searchResult: [...action.searchResult]
            }


        default:
            return state;
    }
  };
  
  export default mapReducer;
  