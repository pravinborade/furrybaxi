import React, { useEffect, useState } from 'react';
import {Provider, useDispatch} from 'react-redux';
import { StatusBar, LogBox  } from 'react-native';

import { store } from '../redux/store/store';
import Navigator from './navigation/Navigator';
import { setIsRegistered, setToken } from '../redux/actions/authAction';
import AsyncStorage from '@react-native-async-storage/async-storage';

const dispatch = store.dispatch;

const App = ()  => {

    useEffect(()=>{
        LogBox.ignoreAllLogs();
        getToken()
    },[])

    const getToken =  async () => {
        let tk = await AsyncStorage.getItem('token')
        let isBool = await AsyncStorage.getItem('isRegistered')
        dispatch(setToken(tk))
        if(isBool !== null){
            dispatch(setIsRegistered(JSON.parse(isBool)))
        }else{
            dispatch(setIsRegistered(null))
        }
    }

    return (
        <Provider store={store}>
            <StatusBar  backgroundColor={'#17202D'} hidden={true} />
            <Navigator />
        </Provider>
    );
};
export default App;
