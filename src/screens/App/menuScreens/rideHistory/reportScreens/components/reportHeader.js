import React from 'react';
import { View, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import Images from '../../../../../../assets/Images/Images';
import { backNavigation } from '../../../../../../root/navigation/rootNavigation';
import { fSize } from '../../../../../../utils/Responsive';
import { REGULAR } from '../../../../../../utils/typography';

const ReportHeader = () => {
    return(
        <>
            <View style={styles.headerLeft}>
                <TouchableOpacity style={styles.headerLeftButton} onPress={() => { backNavigation() }}>
                    <Image source={Images.arrow_back} style={styles.headerButtonIcon} />
                </TouchableOpacity>
            </View>
            <View style={styles.headerRight}>
                <Text style={styles.headerRightText}>
                    {"Help"}
                </Text>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    headerLeft: {
        flex: .4,
    },
    headerRight: {
        flex: .6,
    },
    headerRightText: {
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#F4F4F4"
    },
    headerLeftButton: {
        padding: 10,
        width: 44,
        height: 44,
        backgroundColor: '#253040',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerButtonIcon: {
        width: 8,
        height: 16,
    },
})
export default ReportHeader;