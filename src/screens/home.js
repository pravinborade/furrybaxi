
import * as React from 'react'
import { View ,Text, StyleSheet, Platform, Alert, Linking, BackHandler } from 'react-native'
import { SafeAreaView } from 'react-native-safe-area-context';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import {  fSize } from '../utils/Responsive';
import { ScreenNames } from '../utils/ScreenNames';
import OutlineButton from '../component/OutlineButton';
import PrimaryButton from '../component/PrimaryButton';
import { BOLD, REGULAR, SEMI_BOLD } from '../utils/typography';
import {check,request, openSettings, PERMISSIONS, RESULTS} from 'react-native-permissions';
import { useDispatch, useSelector } from 'react-redux';
import { checkLocationPermission } from '../redux/actions/homeAction';

 const Home = ({navigation}) => {
    const {isLocationAccess} = useSelector(state=> state.home)
    const dispatch = useDispatch()

    React.useEffect(()=>{
        dispatch(checkLocationPermission())
    },[])

    
    const checkPermission = () => {
        if(isLocationAccess){
            navigation.navigate(ScreenNames.PHONE_NUMBER)
        }else{
            dispatch(checkLocationPermission())
        }
    }
    return(
        <SafeAreaView style={styles.container}>
            <View style={styles.header}/>
            <View style={styles.content}>
                <View style={styles.contentSection1}>
                    <Text style={styles.contentSection1Text1}>{"Hello"}</Text>
                </View>
                <View style={styles.contentSection2}>
                    <Text style={styles.contentSection2Text1}>{"Are you new to "}</Text>
                    <Text style={styles.contentSection2Text2}>{"Furry Baxy App"}</Text>
                </View>    
                <View style={styles.contentSection3}>
                    <Text style={styles.contentSection3Text1}>
                        {`World's Only On Demand Pet-Friendly Ride Haling Service`}
                    </Text>
                </View>
                <View style={styles.contentSection4}>
                    <Text style={styles.contentSection4Text1}>
                        {"By clicking Yes, you are accepting our privacy policy,\nT&C"}
                    </Text>
                </View>    
            </View>    
            <View style={styles.footer}>
                <PrimaryButton 
                    title={"Yes, I'm new"}
                    onPress={(e)=>{checkPermission()}}
                />
                <View style={{height:15}}/>
                <OutlineButton 
                    title={"I have an account"}
                    textColor={"#E1CDA9"}
                    borderColor={"#E1CDA9"}
                    onPress={(e)=>{checkPermission()}}
                />
            </View>    
        </SafeAreaView> 
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#17202D',
        flex: 1,
        padding:20
    },
    header: {
        flex: 1,
    },
    content: {
        flex: 4,
    },
    footer: {
        flex: 2,
        justifyContent:'flex-end'
    },
    contentSection1:{
       marginVertical: 10
    },
    contentSection1Text1: {
        fontSize: fSize(62),
        fontFamily: BOLD,
        lineHeight: 76,
        color: '#F5EDDC',
    },
    contentSection2:{
        marginBottom: 10
    },
    contentSection2Text1: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: '#E1CDA9',
    },
    contentSection2Text2: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(32),
        lineHeight: 40,
        color: '#E1CDA9',
        paddingVertical:5
    },
    contentSection3:{
        marginBottom: 10
    },
    contentSection3Text1: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 20,
        color: '#B59B7C',
        paddingVertical:5
    },
    contentSection4:{
        marginVertical: 10
    },
    contentSection4Text1: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 20,
        color: '#FFFFFF',
        paddingVertical:5
    },
})


export default Home;
