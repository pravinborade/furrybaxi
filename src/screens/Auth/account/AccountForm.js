import React from 'react';
import { Field, reduxForm} from 'redux-form';
import { View,TextInput,StyleSheet,Text} from 'react-native';

import { MEDIUM } from '../../../utils/typography';
import { fSize } from '../../../utils/Responsive';

const validate = values => {    
    const errors = {
    }

    if (!values.f_name) {
        errors.f_name = 'first name must not be empty '
    }

    if (!values.l_name) {
        errors.l_name = 'last name  not be empty '
    }

    if (!values.email) {
        errors.email = 'email must not be empty'
    }

    if (values.email) {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(String(values.email).toLowerCase())){
           errors.email = 'invalid email'
        }
    }
    return errors
}



const renderInput = ({ placeholder, input, label, type, meta: { touched, error, warning ,} }) => {
    return (
        <View style={{}}>
            <TextInput
                style={[input.value ? styles.inputBoxBlack:styles.inputBoxWhite,touched && error && styles.errorBorder ]}
                value={input.value}
                placeholder={placeholder}
                placeholderTextColor={'#A58B6C'}
                {...input}
            />
            {   
             touched && error  && error !== 'email must not be empty' && error !== 'invalid email'  && input?.name =='email' && <Text style={{color:'#FF7373',paddingHorizontal:5}}>{error}</Text>
            }
        </View>
    )
}

let AccountForm = props => {
    const { handleSubmit, onSubmit} = props;
    return (
        <View style={{}}>
            <View style={styles.container}>
                <View style={{width:'48%'}} >
                    <Field
                        placeholder="First Name"
                        name="f_name"
                        component={renderInput}
                    />
                </View>
                <View style={{width:'48%'}}>
                    <Field
                        placeholder="Last Name"
                        name="l_name"
                        component={renderInput}
                    />
                </View>
            </View>
            <View style={{marginVertical:10}}>
                <Field
                    placeholder="Email Address"
                    name="email"
                    component={renderInput}
                />
            </View>
        </View>
    )
}

AccountForm = reduxForm({
    form: 'account-form',
    validate,
    
    shouldValidate: () => true,
})(AccountForm)

const styles = StyleSheet.create({
    container:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    inputBoxWhite:{
        backgroundColor:'#E2EAED',
        color:'#F9D591',
        fontFamily:MEDIUM,
        borderRadius:15,
        fontSize:fSize(12),
        padding:10,
        height:54,
    },
    inputBoxBlack:{
        backgroundColor:'#253040',
        color:'#F9D591',
        fontFamily:MEDIUM,
        borderRadius:15,
        fontSize:fSize(12),
        height: 54,
        padding:10,
       
    },
    errorBorder:{
        borderWidth:1.5,
        borderColor:'#FF7373'
    }
})

export default AccountForm;