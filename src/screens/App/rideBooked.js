import React, { useRef, useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Text, Dimensions, TouchableOpacity } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import Images from '../../assets/Images/Images';
import { useSelector } from 'react-redux';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../utils/typography';
import { fSize } from '../../utils/Responsive';
import { ScreenNames } from '../../utils/ScreenNames';
import googleMapStyle from '../../utils/googleMapStyle';
import { GoogleCloudKey } from '../../config';
import MapViewDirections from 'react-native-maps-directions';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const RideBooked = (props) => {
    let mapRef = useRef(null);
    const {
        currentPlace,
        destinationPlace,
    } = useSelector(state => state.map)

    const [origin, setOrigin] = useState()
    const [destination, setDestination] = useState()
    const [region, setRegion] = useState({
        latitude: 18.5011567,
        longitude: 73.9242136,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    })

    useEffect(() => {
        if (currentPlace && currentPlace !== null) {
            setOrigin({
                latitude: currentPlace?.lat,
                longitude: currentPlace?.long
            })
        } else {
            setOrigin({ latitude: 18.5011567, longitude: 73.9242136 })
        }
    }, [currentPlace])

    useEffect(() => {
        if (destinationPlace && destinationPlace !== null) {
            setDestination({
                latitude: destinationPlace?.lat,
                longitude: destinationPlace?.long
            })
        } else {
            setDestination({ latitude: 18.6924, longitude: 74.1323 })
        }

    }, [destinationPlace])

    const getRegionForCoordinates = (points) => {
        // points should be an array of { latitude: X, longitude: Y }
        let minX, maxX, minY, maxY;

        // init first point
        ((point) => {
            minX = point.latitude;
            maxX = point.latitude;
            minY = point.longitude;
            maxY = point.longitude;
        })(points[0]);

        // calculate rect
        points.map((point) => {
            minX = Math.min(minX, point.latitude);
            maxX = Math.max(maxX, point.latitude);
            minY = Math.min(minY, point.longitude);
            maxY = Math.max(maxY, point.longitude);
        });

        const midX = (minX + maxX) / 2;
        const midY = (minY + maxY) / 2;
        const deltaX = (maxX - minX);
        const deltaY = (maxY - minY);
        let tempCoords = {
            latitude: Number(midX),
            longitude: Number(midY)
        }

        mapRef.current.animateToCoordinate(tempCoords, 1);

        setRegion({
            latitude: midX,
            longitude: midY,
            latitudeDelta: deltaX * 1.5,
            longitudeDelta: deltaY * 1.5
        });
    }

    useEffect(() => {
        if (origin && destination) {
            getRegionForCoordinates([origin, destination])
        }
        setTimeout(() => {
            props.navigation.navigate(ScreenNames.START_RIDE)
        }, 3000)
    }, [origin, destination])

    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE}
                style={{ ...StyleSheet.absoluteFillObject, height: hp(80) }}
                ref={mapRef}
                initialRegion={region}
                // onRegionChange={(e)=>{
                //     setRegion(e)
                // }}
                showsUserLocation={false}
                showsCompass={false}
                customMapStyle={googleMapStyle}
            >
                <Marker
                    coordinate={region}
                >
                    <Image source={Images.driverLocation} style={styles.driverLocation} />
                </Marker>
                {
                    origin &&
                    <Marker coordinate={origin}>
                        <View style={styles.markerContainer}>
                            <View style={styles.markerDot} />
                        </View>
                    </Marker>
                }
                {
                    destination &&
                    <Marker coordinate={destination} >
                        <Image source={Images.marker} style={{ width: 26, height: 36 }} />
                    </Marker>

                }
                {/* {
                    origin && destination &&
                    <MapView.Polyline
                        coordinates={[origin, destination]}
                        strokeWidth={3}
                        strokeColor="#6E7887"
                    />
                } */}

                <MapViewDirections
                    origin={origin}
                    destination={destination}
                    apikey={GoogleCloudKey}
                    strokeWidth={4}
                    strokeColor="#6E7887"
                    onReady={(result) => {
                        mapRef.current.fitToCoordinates(result.coordinates, {
                            edgePadding: styles.markerEdge,
                            animated: true
                        })
                    }}
                />
            </MapView>
            <View style={styles.menuOverLay}>
                <TouchableOpacity onPress={() => {
                    props.navigation.navigate(ScreenNames.START_RIDE)
                }}
                >
                    <View style={styles.backImageContainer}>
                        <Image
                            source={Images.arrow_back_black}
                            style={styles.iconImages}
                        />
                    </View>
                </TouchableOpacity>
            </View>

            <View style={styles.actionContainer1}>
                <View style={styles.actionContainer1Section1}>
                    <View style={styles.actionContainer1Section1Row1}>
                        <Text style={styles.actionContainer1Section1Row1Text1}>
                            {"Your Ride Booked"}
                        </Text>
                    </View>
                    <View style={styles.actionContainer1Section1Row2}>
                        <Text style={styles.actionContainer1Section1Row2Text2}>
                            {"You’ll be picked\nup from current location at 11:15am"}
                        </Text>
                    </View>
                    <View style={styles.actionContainer1Section1Row3}>
                        <Text style={styles.actionContainer1Section1Row3Text}>
                            {"You’ll be Notified"}
                        </Text>
                    </View>
                </View>
            </View>
        </View>

    )
}


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
    },
    driverLocation: {
        height: 50,
        width: 50
    },
    menuOverLay: {
        position: 'absolute',
        top: 20,
        minHeight: hp(10),
        right: 0,
        left: 20,
    },
    iconImages: {
        height: 30,
        width: 15,
    },
    backImageContainer: {
        height: 44,
        width: 44,
        borderRadius: 12,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    markerContainer: {
        backgroundColor: "#F9D591",
        borderRadius: 12.5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    markerDot: {
        backgroundColor: "#253040",
        width: 14,
        height: 14,
        borderRadius: 7,
    },
    actionContainer1: {
        backgroundColor: "#FDFFFF",
        position: 'absolute',
        top: hp(70),
        bottom: 0,
        left: 0,
        right: 0,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
    },
    actionContainer1Section1: {
        height: hp(30),
        backgroundColor: "#17202D",
        padding: 20,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    actionContainer1Section1Row1: {
        height: hp(8),
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    actionContainer1Section1Row1Text1: {
        fontFamily: SEMI_BOLD,
        fontSize: 18,
        lineHeight: 20,
        color: "#F9D591"
    },
    actionContainer1Section1Row1Text2: {
        fontFamily: REGULAR,
        fontSize: 14,
        lineHeight: 17,
        color: "#B59B7C"
    },
    actionContainer1Section1Row2: {
        height: hp(8),
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    actionContainer1Section1Row2Text2: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: "#E1CDA9",
        textAlign: 'center'
    },
    actionContainer1Section1Row3: {
        height: hp(8),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#253040",
        borderRadius: 15,
        backgroundColor: '#222831'
    },
    actionContainer1Section1Row3Text: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 20,
        color: "#E2EAED",
    },

    markerEdge: {
        top: 100,
        bottom: 100,
        left: 30,
        right: 30
    }

});

export default memo(RideBooked);
