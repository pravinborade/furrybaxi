
const initialState = {
    isLocationAccess: false,
    currentLocation: null,
   
};
  
const homeReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_LOCATION_ACCESS':
            return {
                ...state, 
                isLocationAccess:action.payload,
            };

        case 'SET_CURRENT_LOCATION':
            return {
                ...state, 
                currentLocation: action.payload,
            };
       
        default:
            return state;
    }
};
  
  export default homeReducer;
  
