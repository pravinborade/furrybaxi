import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, View, Text, Platform, TextInput, SafeAreaView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// 
import BackButton from '../../../component/BackButton';
import DarkButton from '../../../component/DarkButton';
import Header from '../../../component/Header';
import PrimaryButton from '../../../component/PrimaryButton';
import { fSize } from '../../../utils/Responsive';
import { ScreenNames } from '../../../utils/ScreenNames';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';
import PaymentButton from '../component/PaymentButton';
import appService from "../../../service/appService";

import { initStripe, useStripe } from '@stripe/stripe-react-native';
import CustomGradientButton from '../../../component/CustomGradientButton';
import commanStyle from '../../../utils/commanStyle';
import CustomButton from '../../../component/CustomButton';


const Payment = (props) => {
    const { navigation } = props;
    const [paymentMethod, setPaymentMethod] = useState('')
    const headerLeft = () => {
        return <BackButton onPress={() => {
            navigation.goBack()
        }} />
    }

    const onSelectPaymentMethod = (key) => {
        setPaymentMethod(key)
    }

    const onSubmitMethod = () => {
        props.navigation.navigate(ScreenNames.CARD_PAYMENT)
    }

    return (
        <SafeAreaView style={styles.container}>
            <Header
                leftView={headerLeft}
            />
            <View style={styles.content}>
                <Text style={styles.contentText1}>
                    {"How would you like to Pay?"}
                </Text>
                <Text style={styles.contentText2}>
                    {"You’ll be charged only after the ride"}
                </Text>
                <CustomButton
                    title={"Credit / Debit card"}
                    onPress={() => { onSelectPaymentMethod('card') }}
                    disable={false}
                    styleContainer={styles.buttonStyle}
                    textStyle={styles.buttonTextStyle}
                />
                {/* <CustomButton
                    title={Platform.OS === 'ios' ? "Apple Pay" : "Google Pay"}
                    onPress={() => Platform.OS === 'ios' ? onSelectPaymentMethod('apple') : onSelectPaymentMethod('google')}
                    disable={false}
                    styleContainer={styles.buttonStyle}
                    textStyle={styles.buttonTextStyle}
                /> */}
            </View>
            <View style={styles.footer}>
                <CustomGradientButton
                    title={'Next'}
                    onPress={() => {onSubmitMethod()}}
                    disable={false}
                    gradientColor={paymentMethod !== '' ? ['#DAC5A2', '#A98C6F'] : ['#253040', '#17202D']}
                    styleContainer={commanStyle.buttonContainer}
                    textStyle={paymentMethod !== '' ? commanStyle.primaryButtonText : commanStyle.darkButtonText}
                />
            </View>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#17202D',
        flex: 1,
        padding: 20
    },

    content: {
        height: '60%',
    },

    footer: {
        height: '20%',
        justifyContent: 'flex-end'
    },

    contentText1: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 24,
        color: "#E1CDA9",
        marginVertical: 5,
    },

    contentText2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 17,
        color: "#A58B6C",
        marginVertical: 5
    },

    buttonStyle: {
        backgroundColor: '#E2EAED',
        borderRadius: 15,
        height: 54,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingHorizontal: 15,
        paddingVertical: 20,
        marginVertical: 5
    },
    buttonTextStyle: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#253040'
    },
})


export default Payment;