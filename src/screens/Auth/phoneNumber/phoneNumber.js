
import React, { useRef, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { View, Text, StyleSheet, ScrollView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { setCountry, setError, setPhoneNumber, submitPhoneNumber, resetState } from '../../../redux/actions/phoneNumberAction';

import Header from '../../../component/Header';
import { fSize } from '../../../utils/Responsive';
import DarkButton from '../../../component/DarkButton';
import BackButton from '../../../component/BackButton';
import PrimaryButton from '../../../component/PrimaryButton';
import { MEDIUM, SEMI_BOLD } from '../../../utils/typography';
import CountryPhoneInput from '../../../component/CountryPhoneInput';
import { backListener, removeBackListener } from '../../../utils/backHandler';
import CustomGradientButton from '../../../component/CustomGradientButton';
import commanStyle from '../../../utils/commanStyle'

const PhoneNumber = (props) => {
    const dispatch = useDispatch();
    const { phoneNumber, country, isError, isLoading } = useSelector(state => state.phoneNumber)

    const phoneInput = useRef(null);
    const isVisible = useIsFocused();

    useEffect(() => {
        if (isVisible) {
            phoneInput.current.onChangeText('')
            dispatch(resetState())
        }
        backListener()
        return () => {
            removeBackListener()
        };
    }, [isVisible])

    const checkIfPhoneNumberValid = async () => {
        if (phoneInput.current?.isValidNumber(phoneNumber)) {
            let inputVariable = {
                "phone": phoneNumber,
                "country_code": phoneInput.current.getCallingCode(),
                "role":"passenger"
            }
            dispatch(submitPhoneNumber(inputVariable));
        } else {
            dispatch(setError(true))
        }
    }

    const onChangeNumber = (number) => {
        dispatch(setError(false))
        dispatch(setPhoneNumber(number))
    }

    const onChangeCountry = (country) => {
        dispatch(setError(false))
        dispatch(setCountry({
            code: country?.cca2,
            value: country?.callingCode[0]
        }))
    }

    const renderHeadingContainer = () => (
        <>
            {
                phoneNumber
                    ?
                    <Text style={styles.contentSection1Text1}>{"What’s your Phone Number?"}</Text>
                    :
                    <Text style={styles.contentSection1Text1}>{"Let’s start with your phone"}</Text>
            }
        </>
    )

    const renderErrorConatiner = () => (
        <Text style={styles.contentSection1Text2}>
            {`You entered a wrong number,\nPlease enter the correct one`}
        </Text>
    )

    const headerLeft = () => {
        return <BackButton onPress={() => { props.navigation.goBack() }} />
    }


    return (
        <SafeAreaView style={styles.container}>
            <ScrollView
                contentContainerStyle={{ flex: 1 }}
                keyboardShouldPersistTaps='handled'
            >
                <Header
                    leftView={headerLeft}
                />
                <View style={styles.content}>
                    <View style={styles.contentSection1}>
                        {
                            isError
                                ?
                                renderErrorConatiner()
                                :
                                renderHeadingContainer()
                        }
                        <Text style={styles.contentSection1Text3}>{"An OTP will be sent to your number"}</Text>
                    </View>
                    <View style={styles.contentSection2}>
                        <CountryPhoneInput
                            phoneInputRef={phoneInput}
                            phoneNumber={phoneNumber}
                            country={country}
                            onChangeNumber={(number) => onChangeNumber(number)}
                            onChangeCountry={(code) => onChangeCountry(code)}
                        />
                    </View>
                </View>
                <View style={styles.footer}>
                    <CustomGradientButton
                        title={'Next'}
                        onPress={() => phoneNumber && !isError && checkIfPhoneNumberValid()}
                        disable={false}
                        gradientColor={phoneNumber && !isError ? ['#DAC5A2', '#A98C6F'] : ['#253040', '#17202D']}
                        styleContainer={commanStyle.buttonContainer}
                        textStyle={phoneNumber && !isError ? commanStyle.primaryButtonText : commanStyle.darkButtonText}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#17202D',
        padding: 20
    },
    content: {
        height: '60%'
    },
    contentSection1: {
        marginVertical: 10
    },
    contentSection1Text1: {
        color: '#E1CDA9',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        marginVertical: 5
    },
    contentSection1Text2: {
        marginVertical: 5,
        color: '#FF7373',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20)
    },
    contentSection1Text3: {
        paddingBottom: 10,
        color: '#A58B6C',
        fontFamily: MEDIUM,
        fontSize: fSize(14)
    },
    contentSection2: {
        marginVertical: 5
    },
    footer: {
        height: "20%",
        justifyContent: 'flex-end'
    }
});

export default PhoneNumber;
