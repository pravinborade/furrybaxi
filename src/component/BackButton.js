import React from "react";
import { TouchableOpacity, Image, StyleSheet } from 'react-native';
import Images from "../assets/Images/Images";

const BackButton = ({onPress}) => (
    <TouchableOpacity  onPress={onPress} style={styles.backButton}>
        <Image source={Images.arrow_back} style={styles.backArrow}/>
    </TouchableOpacity>
)

const styles = StyleSheet.create({
    backButton:{
        backgroundColor:'#253040',
        borderRadius:12,
        height:44,
        width:44,
        alignItems:'center',
        justifyContent:'center'
    },
    backArrow:{
        height:16,
        width:8,
        margin:10
    },
})

export default BackButton;

