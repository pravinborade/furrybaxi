import React from 'react';
import{TouchableOpacity,Text} from 'react-native';

const Button = ({
    title,
    onPress,
    style,
    textStyle,
    disable
  }) => {
    return (
        <TouchableOpacity style={style} onPress={onPress} disabled={disable}>
            <Text style={textStyle}>
                {title}
            </Text>
        </TouchableOpacity>
    );
  };

 

export default Button;