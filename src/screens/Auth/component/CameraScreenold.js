'use strict';
import React, { PureComponent } from 'react';
import { RNCamera } from 'react-native-camera';
import { Image, StyleSheet, Text, TouchableOpacity, View,BackHandler } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import Images from '../../../assets/Images/Images';
import BackButton from '../../../component/BackButton';
import Header from '../../../component/Header';

const PendingView = () => (
    <View
      style={{
        width: 280,
        height: 280,
        backgroundColor: "#F5F7F8",
        borderRadius: 35
      }}
    >
      <Text>Waiting</Text>
    </View>
);

class CustomCamera extends PureComponent {
    constructor(props){
        super(props)  
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);  
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress',this.handleBackButtonClick);
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }


    handleBackButtonClick() {
        this.props.setIsCapture()
        return true;
    }
    

    takePicture = async () => {
        if (this.camera) {
            const options = { 
                quality: 0.5, 
                base64: true,
                width: 200,
            };
            const data = await this.camera.takePictureAsync(options);
            this.props.onCaptureImage(data.uri)
        }
    };

    headerLeft = () => {
        return <BackButton onPress={()=>{
            this.props.setIsCapture()
        }}/>
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Header 
                        leftView={this.headerLeft}
                        bgColor='#000000'
                    />
                </View>
                <View style={styles.content}>
                    <View style={styles.contentSection1}>
                        <RNCamera
                            ref={ref => {
                                this.camera = ref;
                            }}
                            style={styles.preview}
                            type={'back'}
                            flashMode={'off'}
                            ratio={'1:1'}
                            captureAudio={false}
                            androidCameraPermissionOptions={{
                                title: 'Permission to use camera',
                                message: 'We need your permission to use your camera',
                                buttonPositive: 'Ok',
                                buttonNegative: 'Cancel',
                            }}
                            androidRecordAudioPermissionOptions={{
                                title: 'Permission to use audio recording',
                                message: 'We need your permission to use your audio',
                                buttonPositive: 'Ok',
                                buttonNegative: 'Cancel',
                            }}
                        />
                    </View>
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.footerButton}>
                        <Image 
                            source={Images.camera_button}
                            style={styles.footerButtonImage}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000',
    },
    header: {
        height: hp(20),
        padding: 20
    },
    content: {
        height: hp(60),
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    footer: {
        height: hp(20),
        padding: 40,
        justifyContent:'flex-end',
        alignItems: 'center'
    },
    contentSection1: {
        width: 220,
        height: 220,
        borderRadius: 35,
        borderWidth: 1,
        overflow: 'hidden'
    },
    preview: {
        width: 220,
        height: 220,
        aspectRatio: 1
    },
    footerButton: {
        justifyContent:'center',
        alignItems: 'center',
        backgroundColor: '#F6F6F6',
        borderWidth:1,
        borderColor: 'transparent',
        borderRadius: 30,
        width:60,
        height:60,
        padding: 10
    },
    footerButtonImage: {
 
    }
});

export default CustomCamera;