
import appService from '../../service/appService'
import {ScreenNames} from '../../utils/ScreenNames'
import { navigate } from "../../root/navigation/rootNavigation";
import AsyncStorage from '@react-native-async-storage/async-storage';

export function openDatePicker(isDatePickerOpen){
    return{
        type: 'OPEN_DATE_PICKER',
        isDatePickerOpen
    }
}

export function openCategoryDropdown(isCategoryDropdownOpen){
    return{
        type: 'OPEN_CATEGORY_DROPDOWN',
        isCategoryDropdownOpen
    }
}

export function openSizeDropdown(isSizeDropdownOpen){
    return{
        type: 'OPEN_SIZE_DROPDOWN',
        isSizeDropdownOpen
    }
}


export function setPicPicture(pic){
    return{
        type: 'SET_PET_PICTURE',
        pic
    }
}


export function setPicError(isPicError) {
    return {
        type: 'SET_PET_PICTURE_ERROR',
        isPicError
    };
}

export function setResponse(response) {
    return {
        type: 'SET_PET_PROFILE_RESPONSE',
        response
    };
}

export function setError(isError) {
    return {
        type: 'SET_PET_PROFILE_ERROR',
        isError
    };
}

export function setCategory(category) {
    return {
        type: 'SET_CATEGORY_RESPONSE',
        category
    };
}

export function setCategoryError(isCategoryError) {
    return {
        type: 'SET_CATEGORY_ERROR',
        isCategoryError
    };
}

export function setSize(size) {
    return {
        type: 'SET_SIZE_RESPONSE',
        size
    };
}

export function setSizeError(isSizeError) {
    return {
        type: 'SET_SIZE_ERROR',
        isSizeError
    };
}

export function onChangeCategory(category) {
    return {
        type: 'ON_CHANGE_CATEGORY',
        category
    };
}

export function onChangeSize(size) {
    return {
        type: 'ON_CHANGE_SIZE',
        size
    };
}



export  function  getCategory() {
    return async (dispatch) => {
        return appService.get('fb/v1/pet/category')
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{
            
            var temp = response.map((item)=> {
                return{label:item.name , value: item.pcid}
            })
            dispatch(setCategory(temp))
           
        })
        .catch((error) =>{  
            
            console.log(error)
            dispatch(setCategoryError(true))
        });
    };
}

export  function  getSize() {
    return async (dispatch) => {
        return appService.get('fb/v1/pet/size')
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{
            
            var temp = response.map((item)=> {
                return{label:item.name , value: item.psid}
            })
            dispatch(setSize(temp))
        })
        .catch((error) =>{  
            
            console.log(error)
            dispatch(setSizeError(true))
        });
    };
}

export  function  createPetProfile (data) {
    return async (dispatch) => {
        return appService.post('fb/v1/pet',data)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{
            dispatch(setResponse(response))
            navigate(ScreenNames.PET_PICTURE)
        })
        .catch((error) =>{  
            console.log(error)
            dispatch(setError(true))
        });
    };
}

export  function  updatePetProfile (data) {
    return async (dispatch) => {
        return appService.patch(`fb/v1/pet/${data.pid}`,data)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{
            dispatch(setResponse(response))
            navigate(ScreenNames.PET_PICTURE)
        })
        .catch((error) =>{  
            console.log(error)
            dispatch(setError(true))
        });
    };
}

export  function  uploadPetPic(data,token) {
    return async (dispatch) => {
        return appService.post(`fb/v1/pet/upload/${token}`,data)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{
            dispatch(setResponse(response))
            navigate(ScreenNames.PET_PROFILE)
        })
        .catch((error) =>{  
            console.log(JSON.stringify(error))
            dispatch(setPicError(true))
        });
    };
}


