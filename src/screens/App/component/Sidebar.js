import { useNavigation } from "@react-navigation/native";
import React,{useEffect, useState} from "react";
import { Dimensions, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Rating } from "react-native-ratings";
import SvgUri from "react-native-svg-uri";
import { useSelector, } from "react-redux";
import Images from "../../../assets/Images/Images";
import { clearToAsyncStorage } from "../../../redux/actions/authAction";
import { backNavigation, navigate } from "../../../root/navigation/rootNavigation";
import { MENU_ITEMS } from "../../../utils/menuItems";
import { fSize } from "../../../utils/Responsive";
import { ScreenNames } from "../../../utils/ScreenNames";
import { REGULAR, SEMI_BOLD } from "../../../utils/typography";
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const Sidebar = ({toggleMenu}) => {
    const { 
        userResponse
    } = useSelector(state=> state.auth)
     
    const onNavigate = ({key, name}) => {
        if(name === 'Logout'){
            clearToAsyncStorage()
        }else{
            if(ScreenNames[key]){
                navigate(key)
            }
        }
        setTimeout(()=>{
            toggleMenu()   
        },1000)
    }
    return(
        <View style={styles.container}>
            <View style={styles.menuTop}>
                <TouchableOpacity style={styles.menuTopButton} onPress={()=>{toggleMenu()}}>
                    <Image source={Images.arrow_back} style={styles.menuTopButtonIcon}/>
                </TouchableOpacity>
            </View>
            <View style={styles.menuMiddle}>
                <View style={styles.menuMiddleTop}>
                    <Text style={styles.menuMiddleTopText}>
                        {userResponse?.data?.first_name +' '+ userResponse?.data?.last_name}
                    </Text>
                </View>
                <View style={styles.menuMiddleBottom}>
                    <View style={styles.menuMiddleBottomView}>
                        <Image source={Images.star} style={styles.menuMiddleBottomViewImage}/>
                        <Text style={styles.menuMiddleBottomViewText}>
                            {userResponse?.data?.rating}
                        </Text>
                    </View>
                </View>
            </View>
            <View style={styles.menuBottom}>
                <View style={styles.menuBottomList}>
                        {
                            MENU_ITEMS.map(item=>(
                                <View style={styles.menuBottomListItem} key={item.id}>
                                    <View style={styles.menuBottomListItemLeft}>
                                        <SvgUri
                                            source={item.icon}
                                        />
                                       {/* <View style={styles.menuBottomListItemLeftIcon}>
                                       </View>      */}
                                    </View>
                                    <TouchableOpacity style={styles.menuBottomListItemRight} 
                                        onPress={()=>{onNavigate(item)}}
                                    >
                                        <Text style={styles.menuBottomListItemRightText}>
                                            {item.name}
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            ))
                        }
                </View>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        top: 0,
        left:0,
        bottom:0,
        backgroundColor: "#17202D",
        minWidth: width/1.2 ,
        zIndex: 1,
        padding: 20,
        flex: 1 
    },
    menuTop:{
        flex: 1
    },
    menuTopButton: {
        padding: 10,
        width: 44,
        height: 44,
        backgroundColor: '#253040',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    menuTopButtonIcon: {
        width: 8,
        height: 16,
    },
    menuMiddle:{
        flex: 2.5,
    },
    menuMiddleTop: {
        flex: 5,
        justifyContent: 'center'    
    },
    menuMiddleTopText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(22),
        lineHeight: 27,
        color: "#ffffff"
    },
    menuMiddleBottom: {
        flex: 5,
    },
    menuMiddleBottomView: {
        backgroundColor: '#253040',
        borderRadius: 10,
        width: 70,
        height: 40,
        alignItems: 'center',
        justifyContent: 'space-around' ,
        flexDirection: 'row',
        padding: 8  ,

    },
    menuMiddleBottomViewImage: {
        height: 14,
        width: 14
    },  
    menuMiddleBottomViewText: {
        fontFamily: REGULAR ,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#E2EAED',
    },
    menuBottom:{
        flex: 7.5,
    },
    menuBottomList:{
        flex: 1,
        flexDirection: 'column',
    },
    menuBottomListItem:{
        flex: 1,
        flexDirection: 'row',
        maxHeight: 50,
    },
    menuBottomListItemLeft:{
        flex: 2,
        justifyContent: 'center'
    },
    menuBottomListItemLeftIcon:{
        width: 18,
        height: 18,
        borderRadius: 50,
        backgroundColor: '#A35858'
    },
    menuBottomListItemRight:{
        flex: 8,
        justifyContent:'center',
    },
    menuBottomListItemRightText: {
        fontFamily: REGULAR,
        fontSize: fSize(16),
        lineHeight: 20,
        color: '#F3F4F4',
    },
})


export default Sidebar;