import React, { useRef, useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Dimensions, TouchableOpacity, Text, Modal ,Linking} from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Modalize } from 'react-native-modalize';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';

import Images from '../../assets/Images/Images';
import { useDispatch, useSelector } from 'react-redux';
import Container2 from './onRideComponent/Container2';
import { cancelRide } from '../../redux/actions/ridingAction';
import googleMapStyle from '../../utils/googleMapStyle';
import { MEDIUM, REGULAR } from '../../utils/typography';
import { fSize } from '../../utils/Responsive';
import { find_bottom_dimesions, find_dimesions } from '../../utils/helper';
import SafetyToolButtion from '../../component/SafetyToolButtion';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const OnRide = (props) => {
    const dispatch = useDispatch()
    const modalizeRef = useRef(null);
    const mapRef = useRef(null);
    const {
        currentPlace,
        destinationPlace,
    } = useSelector(state => state.map)

    const { userTripResponse,
        userOnrideResponse
    } = useSelector(state => state.riding)
    // const { isSafetyModalVisible } = useSelector(state => state.ride)

    const {
        userResponse
    } = useSelector(state => state.auth)

    const [origin, setOrigin] = useState()
    const [isSafetyModalVisible, setSafetyModalVisible] = useState(false)
    const [destination, setDestination] = useState()
    const [region, setRegion] = useState({
        latitude: 18.5011567,
        longitude: 73.9242136,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    })
    const [wayPoints, setWayPoints] = useState([])

    const [dynamicHeight, setDynamicHeight] = useState(height)
    const [showDetails, setShowDetails] = useState(false);

    useEffect(() => {
        if (userTripResponse && userTripResponse !== null) {
            debugger
            setOrigin({
                latitude: userTripResponse.data.trip.source[0],
                longitude: userTripResponse.data.trip.source[1],
            })
            setDestination({
                latitude: userTripResponse.data.trip.destination[0],
                longitude: userTripResponse.data.trip.destination[1],
            })
        }
    }, [userTripResponse])


    useEffect(() => {
        if (userOnrideResponse && userOnrideResponse !== null) {
            let tmp = []
            setOrigin({
                latitude: userOnrideResponse?.data?.location?.[0],
                longitude: userOnrideResponse?.data?.location?.[1],
            })
            userOnrideResponse?.data?.waypoints?.map((way, ind) => {
                tmp.push({
                    latitude: way[0],
                    longitude: way[1]
                })
            })
            mapRef.current.fitToCoordinates(tmp, {
                edgePadding: styles.markerEdge,
                animated: true
            })
            setWayPoints(tmp)

        }
    }, [userOnrideResponse])


    const onRideCancel = () => {
        let data = {
            id: userOnrideResponse?.data?.id
        }
        dispatch(cancelRide(data))
    }

    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE}
                ref={mapRef}
                style={{ ...StyleSheet.absoluteFillObject, height: dynamicHeight }}
                initialRegion={region}
                onRegionChange={(e) => {
                    setRegion(e)
                }}
                showsUserLocation={false}
                showsCompass={false}
                customMapStyle={googleMapStyle}

            >

                {
                    origin &&
                    <Marker coordinate={origin}>
                        <Image
                            source={Images.car_icon}
                            style={{ width: 28, height: 28 }}
                        />

                    </Marker>
                }
                {
                    destination &&
                    <Marker coordinate={destination}>
                        <Image
                            source={Images.marker}
                            style={{ width: 26, height: 36 }}

                        />
                    </Marker>
                }
                {
                    origin && destination &&
                    <MapView.Polyline
                        coordinates={[...wayPoints]}
                        strokeWidth={3}
                        strokeColor="#6E7887"
                    />
                }
            </MapView>

            <View style={styles.menuOverLay}>
                <TouchableOpacity style={styles.backImageContainer}
                    onPress={() => { props.navigation.goBack() }}
                >
                    <Image
                        source={Images.arrow_back_black}
                        style={styles.iconImages}
                    />
                </TouchableOpacity>
            </View>

            {/* {
                <SafetyToolButtion bottomMargin={height*50/100} />
            }   */}

            {
                !isSafetyModalVisible && <TouchableOpacity onPress={() => setSafetyModalVisible(true)} style={styles.seftyContainer}>
                    <Image source={Images.safety} />
                </TouchableOpacity>
            }



            <Modal
                visible={isSafetyModalVisible}
                transparent={true}
                onRequestClose={() => {
                    setSafetyModalVisible(true)
                }}
            >
                <View onTouchStart={() => setSafetyModalVisible(false)} style={styles.modalContainer}>
                    <View onTouchStart={(e) => e.stopPropagation()} style={styles.modalSubContainer}>
                        <View style={styles.infoContainer}>
                            <Text style={styles.modalHeading}>{'Safety Tools'}</Text>
                        </View>
                        <TouchableOpacity onPress={() => store.dispatch(shareTrip())} style={styles.infoContainer}>
                            <Image source={Images.share_ride} style={styles.shareRide} />
                            <Text style={styles.shareRideText}>{'Share My Ride'}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                Linking.openURL(`tel:${119}`)
                                store.dispatch(setSafetyModal(false))
                            }} style={styles.infoContainer}>
                            <Image source={Images.call} style={styles.call} />
                            <Text style={styles.policeAssistanceText}>{'Police Assistance'}</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </Modal>

            {
                !isSafetyModalVisible &&
                <Modalize
                    // ref={modalizeRef}
                    alwaysOpen={hp(42)}
                    modalStyle={{
                        backgroundColor: "#17202D",
                        padding: 10,
                    }}
                    handleStyle={{
                        display: 'none'
                    }}
                    rootStyle={{
                        zIndex: 1
                    }}
                    modalHeight={hp(90)}
                >



                    <Container2
                        currentPlace={currentPlace}
                        destinationPlace={destinationPlace}
                        userTripResponse={userTripResponse}
                        userOnrideResponse={userOnrideResponse}
                        userResponse={userResponse}
                        onLayout={(event) => setDynamicHeight(find_dimesions(height, event.nativeEvent.layout))}
                    />
                </Modalize>

            }

            {
                !isSafetyModalVisible &&
                <View style={styles.floatButton}>
                    <TouchableOpacity style={styles.floatButtonBottom}
                        onPress={() => {
                            onRideCancel()
                        }}
                    >
                        <Text style={styles.floatButtonText}>
                            {"Cancel Ride"}
                        </Text>
                    </TouchableOpacity>
                </View>
            }


        </View>

    )
}


const styles = StyleSheet.create({
    container: {
        // ...StyleSheet.absoluteFillObject,
        flex: 1,
    },
    driverLocation: {
        height: 50,
        width: 50
    },
    menuOverLay: {
        position: 'absolute',
        top: 20,
        minHeight: hp(10),
        right: 0,
        left: 20,
    },
    iconImages: {
        height: 20,
        width: 10,
    },
    backImageContainer: {
        height: 30,
        width: 15,
    },
    markerContainer: {
        backgroundColor: "#F9D591",
        borderRadius: 12.5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    markerDot: {
        backgroundColor: "#253040",
        width: 14,
        height: 14,
        borderRadius: 7,
    },
    markerEdge: {
        top: 100,
        bottom: 100,
        left: 30,
        right: 30
    },
    floatButton: {
        position: 'absolute',
        height: 80,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 2,
        backgroundColor: "#17202D",
    },
    floatButtonBottom: {
        backgroundColor: "rgba(255, 115, 115, 0.3)",
        borderRadius: 15,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 3,
        marginHorizontal: 10
    },
    floatButtonText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#FB6B6B",
        opacity: 1
    },
    modalContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.3)',
        justifyContent: 'flex-end',


    },
    modalSubContainer: {
        backgroundColor: '#17202D',
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
        padding: 20
    },
    modalHeading: {
        color: '#F6F6F6',
        fontSize: fSize(20),
        fontFamily: MEDIUM
    },
    shareRideText: {
        color: '#F3F4F4',
        fontSize: fSize(16),
        fontFamily: REGULAR
    },

    infoContainer: {
        paddingVertical: 10,
        flexDirection: 'row'
    },

    policeAssistanceText: {
        color: '#F3F4F4',
        fontSize: fSize(16),
        fontFamily: REGULAR
    },
    shareRide: {
        margin: 5,
        marginRight: 15,
        height: 25,
        width: 19
    },
    call: {
        margin: 5,
        marginRight: 15,
        height: 22,
        width: 22
    },
    seftyContainer: {
        position: 'absolute',
        left: 0,
        zIndex: 2,
        padding: 15,
        backgroundColor: '#253040',
        borderRadius: 40,
        margin: 15,
        bottom: height * 42 / 100

    },
});

export default  gestureHandlerRootHOC(memo(OnRide));
