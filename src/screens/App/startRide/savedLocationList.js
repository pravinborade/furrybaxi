import React, { useRef, useEffect, useState, memo } from 'react';
import { View, StyleSheet, SafeAreaView, Text, FlatList, RefreshControl, TouchableOpacity, Image } from "react-native"
import { useDispatch, useSelector } from 'react-redux';
import Images from '../../../assets/Images/Images';
import BackButton from '../../../component/BackButton';
import Header from '../../../component/Header';
import { setDestinationPlace } from '../../../redux/actions/mapAction';
import { rideInProgress } from '../../../redux/actions/ridingAction';
import { navigate } from '../../../root/navigation/rootNavigation';
import { fSize } from '../../../utils/Responsive';
import { ScreenNames } from '../../../utils/ScreenNames';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';

const SavedLocationList = (props) => {
     const dispatch = useDispatch()
     const { savedLocations, userLocations } = useSelector(state => state.riding)
     const [isRefreshing, setIsRefreshing] = useState(false);


     const _onRefresh = () => {
          //setIsRefreshing(true);
          // dispatch(getPets()).then(() => {
          //      setIsRefreshing(false)
          // });
     };
     const headerLeft = () => {
          return <BackButton onPress={() => { props.navigation.goBack() }} />
     }

     const headerMiddle = () => {
          return <Text style={styles.headerTitle}>{'Choose a Place'}</Text>
     }

     const selectRideLocation = (item) => {  
          dispatch(rideInProgress(false)) 
          dispatch(setDestinationPlace(item))
          navigate(ScreenNames.TYPE_OF_RIDES)
     }

     const renderItem = ({ item }) => {
          return (
               <TouchableOpacity onPress={() => selectRideLocation(item)} style={styles.placeCard}>
                    <View style={styles.placeCardContainer}>
                         <View style={{ justifyContent: 'center' }}>
                              <Image source={Images.star_dark} style={styles.placeIcon} />
                         </View>
                         <View style={styles.placeInfo}>
                              <Text style={styles.placeInfoText1}>{item?.name}</Text>
                              <Text style={styles.placeInfoText2}>{`${item?.address}`}</Text>

                         </View>
                    </View>
               </TouchableOpacity>
          );
     };

     return (
          <SafeAreaView style={styles.container}>
               <Header cmHeight={'10%'} leftView={headerLeft} middleView={headerMiddle} />
               <View style={{ flex: 1 }}>
                    <FlatList
                         scrollEnabled
                         data={userLocations?.user_home}
                         renderItem={renderItem}
                         showsVerticalScrollIndicator={false}
                         keyExtractor={(item, index) => index.toString()}
                         refreshControl={
                              <RefreshControl
                                   refreshing={isRefreshing}
                                   onRefresh={() => _onRefresh()}
                              />
                         }
                    />
                    <TouchableOpacity style={styles.bottomButtion} onPress={() => navigate(ScreenNames.ADD_NEW_LOCATION)}>
                         <Text style={styles.bottomButtionTitle}>{'Add New Location'}</Text>
                    </TouchableOpacity>
               </View>

          </SafeAreaView>
     )
}
const styles = StyleSheet.create({
     container: {
          flex: 1,
          backgroundColor: '#17202D',
          padding: 20
     },
     headerTitle: {
          fontSize: fSize(18),
          color: '#FDFDFD',
          fontFamily: MEDIUM,
          marginLeft: -20
     },
     placeCard: {

     },
     placeCardContainer: {
          flexDirection: 'row'

     },
     placeIcon: {
          height: 20,
          width: 20,
          marginHorizontal: 15

     },
     placeInfo: {
          padding: 10,
          borderBottomWidth: 1,
          width: '90%',
          borderBottomColor: '#919498'

     },
     placeInfoText1: {
          color: '#E1CDA9',
          fontFamily: MEDIUM,
          fontSize: fSize(14)

     },
     placeInfoText2: {
          color: '#A58B6C',
          fontFamily: REGULAR,
          fontSize: fSize(12)
     },
     bottomButtion: {
          padding: 10
     },

     bottomButtionTitle: {
          color: '#F9D591',
          fontSize: fSize(14),
          fontFamily: SEMI_BOLD,
          alignSelf: 'center'
     }
})
export default memo(SavedLocationList);