
export const RIDE_STATUS = {
    COMPLETED: 'completed',
    UPCOMING: 'scheduled',
    CANCELLED: 'cancelled'
};

export const PAYMENT_METHODS = {
    CARD: 'card',
    APPLE: 'applepay',
    GOOGLE: 'gpay'
};

export const DATE_FORMATS = {
    DO_MMM_HH_MM_A: 'Do MMM  HH:MM A'
}