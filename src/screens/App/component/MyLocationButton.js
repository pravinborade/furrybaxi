
import React from 'react';
import { StyleSheet,  TouchableOpacity, Image } from 'react-native';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import { useDispatch } from 'react-redux';
import Images from '../../../assets/Images/Images';
import { getCurrentLocation } from '../../../redux/actions/homeAction';

const MyLocationButton = (props) => {
    const { marginTop } = props;
    const dispatch = useDispatch();

    return(
        <TouchableOpacity style={[styles.floatButton, {marginTop: marginTop}]} 
            onPress={()=>{dispatch(getCurrentLocation())}}
        >
            <Image source={Images.myLocation} style={styles.imageIcon} />  
        </TouchableOpacity>    
    )
}




const styles = StyleSheet.create({
    floatButton:{
        position:'absolute',
        backgroundColor: '#FDFFFF',
        height: 50,
        width: 50,
        borderRadius: 25 ,
        padding: 5,
        zIndex: 6,
        right: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageIcon: {
        width: 30,
        height: 30
    }
})



export default MyLocationButton