import { currentLocation, locationDetails, searchPlaces, selectPlace } from "../../service/mapService";
import { ScreenNames } from "../../utils/ScreenNames";
import { checkCurrentLocationValid, getSocketData, rideInProgress, setSavedLocationDetail, setSaveLocationDetail } from "./ridingAction";
import { navigate } from "../../root/navigation/rootNavigation";

export function setCurrentPlace(currentPlace) {
    return {
        type: 'SET_CURRENT_PLACE',
        currentPlace
    };
}

export function setDestinationPlace(destinationPlace) {
    return {
        type: 'SET_DESTINATION_PLACE',
        destinationPlace
    };
}

export function setSearchResult(searchResult) {
    return {
        type: 'SET_SEARCH_RESULT',
        searchResult
    };
}

export  function  getLocationDetails(latlng) {
    return async (dispatch) => {
        return locationDetails(latlng)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response?.data
        })
        .then((response) =>{
            let data = null;
            if(response?.results?.length > 0){
                data = {
                    address: response?.results[0]?.formatted_address.replace(/[()]/g,''),
                    ulid: null,
                    lat: response?.results[0]?.geometry?.location?.lat,
                    long: response?.results[0]?.geometry?.location?.lng,
                    location_type: 'user_location',
                    name: response?.results[0]?.formatted_address.replace(/[()]/g,''),
                    plid: response?.results[0]?.place_id,
                }
            }
           
            dispatch(setCurrentPlace(data))
        })
        .catch((error) =>{  
            console.log(error)
        });
    };
}

export  function  getSavedLocationDetails(latlng) {
    return async (dispatch) => {
        return locationDetails(latlng)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response?.data
        })
        .then((response) =>{
            let data = null;        
            if(response?.results?.length > 0){             
                var sname=response?.results[0]?.formatted_address.split(',')[0] +""+response?.results[0]?.formatted_address.split(',')[1]
                data = {
                    address: response?.results[0]?.formatted_address.replace(/[()]/g,''),
                    ulid: null,
                    lat: response?.results[0]?.geometry?.location?.lat,
                    long: response?.results[0]?.geometry?.location?.lng,
                    location_type: 'user_location',
                    name: response?.results[0]?.formatted_address.replace(/[()]/g,''),
                    plid: response?.results[0]?.place_id,
                    short_name:sname
                }
            }         
            dispatch(setSaveLocationDetail(data))           
        })
        .catch((error) =>{  
            console.log(error)
        });
    };
}

export  function  getSearchPlaces(text, searchableLocation) {
    return async (dispatch) => {
        return searchPlaces(text, searchableLocation)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response?.data;
        })
        .then((response) =>{
            let data = []         
            if(response.predictions?.length > 0) {
                response.predictions?.map(item=>{
                    data.push({
                        address: item.description,
                        ulid: null,
                        location_type: 'user_search',
                        name: item.structured_formatting?.main_text,
                        plid: item.place_id,
                        searched: true
                    })
                })
            }
            dispatch(setSearchResult(data))
        })
        .catch((error) =>{  
            console.log(error)
        });
    };
}


export  function  getSavedSearchedLocationDetail(item) {  
  
    return async (dispatch) => {
        return selectPlace(item.plid)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response?.data;
        })
        .then((response) =>{
            let data = null    
         
            if(response?.result){
                var sname=response?.result?.formatted_address.split(',')[0] +""+response?.result?.formatted_address.split(',')[1]
                data = {
                    address: response?.result?.formatted_address.replace(/[()]/g,''),
                    ulid: item.ulid,
                    lat: response?.result?.geometry?.location?.lat,
                    long: response?.result?.geometry?.location?.lng,
                    location_type: 'user_home',
                    name: item.name.replace(/[()]/g,''),
                    short_name:sname
                }
         
                dispatch(setSaveLocationDetail(data))               
                //navigate(ScreenNames.SAVE_LOCATION)
            }
        })
        .catch((error) =>{  
            console.log(error)
        });
    };
}

export  function  getDestinationSearchedPlace(source, item) {
    return async (dispatch) => {
        return selectPlace(item.plid)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response?.data;
        })
        .then((response) =>{
            let data = null
            if(response?.result){
                data = {
                    address: response?.result?.formatted_address,
                    ulid: item.ulid,
                    lat: response?.result?.geometry?.location?.lat,
                    long: response?.result?.geometry?.location?.lng,
                    location_type: 'user_destination',
                    name: item.name
                }
              
                dispatch(rideInProgress(false))
                dispatch(setDestinationPlace(data))
                navigate(ScreenNames.TYPE_OF_RIDES)
            }
        })
        .catch((error) =>{  
            console.log(error)
        });
    };
}

export  function  getDestinationPlace(source, item) {
    
    return async (dispatch) => {
        let latLng = item?.lat +','+ item.long
        return locationDetails(latLng)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response?.data
        })
        .then((response) =>{
            let data = null;
            if(response?.results?.length > 0){
                data = {
                    address: response?.results[0]?.formatted_address,
                    ulid: null,
                    lat: response?.results[0]?.geometry?.location?.lat,
                    long: response?.results[0]?.geometry?.location?.lng,
                    location_type: 'user_location',
                    name: response?.results[0]?.formatted_address,
                    plid: response?.results[0]?.place_id,
                }
                dispatch(setDestinationPlace(data))
                navigate(ScreenNames.TYPE_OF_RIDES)
            }
        })
        .catch((error) =>{  
            console.log(error)
        });
    };
}
