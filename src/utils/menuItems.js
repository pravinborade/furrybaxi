import Images from "../assets/Images/Images";

export const MENU_ITEMS = [{
    id:1,
    name: 'Account',
    key: 'ACCOUNT_PROFILE',
    icon: Images.accountIconSvg
},{
    id:2,
    name: 'Ride History',
    key: 'TRIP_LIST',
    icon: Images.rideHistoryIconSvg
},
// {
//     id:3,
//     name: 'Support',
//     key: 'SUPPORT',
//     icon: Images.supportIconSvg
// },
{
    id:4,
    name: 'Terms And Conditions',
    key: 'TERMS_AND_CONDITIONS',
    icon: Images.termAndConditionIconSvg
},
{
    id:5,
    name: 'Privacy Policy',
    key: 'PRIVACY_POLICY',
    icon: Images.privacyPolicyIconSvg
},
// {
//     id:6,
//     name: 'FAQ',
//     key: 'FAQ',
//     icon: Images.faqIconSvg
// },
{
    id:7,
    name: 'Logout',
    key: 'LOGOUT',
    icon: Images.logoutIconSvg
}]
