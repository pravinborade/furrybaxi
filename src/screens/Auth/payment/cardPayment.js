import React, { useEffect, useState } from "react";
import { SafeAreaView, StyleSheet, Text, View , Dimensions, Alert} from 'react-native';
import { useDispatch, useSelector } from "react-redux";

import BackButton from "../../../component/BackButton";
import Header from "../../../component/Header";
import { fSize } from "../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../utils/typography";
import { ScreenNames } from '../../../utils/ScreenNames';
import appService from "../../../service/appService";
import { initStripe, CardField, useConfirmSetupIntent } from '@stripe/stripe-react-native';
import commanStyle from "../../../utils/commanStyle";
import CustomGradientButton from "../../../component/CustomGradientButton";
import AmountModal from "./amountModal";
import { getPaymentSecret, getStripeInformation, savePaymentDetails } from "../../../redux/actions/paymentAction";

const screenHeight = Dimensions.get('screen').height

const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}

const CardPayment = (props) => {
    const { navigation } = props;
    const [cardDetails, setCardDetails] = useState()
    const [loader, setLoader] = useState(false)
    const [amountModal, setAmountModal] = useState(false)
    const [clientSecretKey, setClientSecretKey] = useState()

    const { confirmSetupIntent } = useConfirmSetupIntent();
    const dispatch = useDispatch()
    
    const {
        stripeResponse,
        paymentSecretResponse
    } = useSelector(state => state.payment)

    const {
        userResponse,
    } = useSelector(state => state.auth)

    const headerLeft = () => {
        return <BackButton onPress={() => {
            navigation.goBack()
        }} />
    }

    

    const createPayment = async() =>{
        setLoader(true)
        const billingDetails = {
            email: userResponse?.data?.email,
        };
        try {
            const { setupIntent, error } = await confirmSetupIntent(clientSecretKey, {
                type: 'Card',
                billingDetails,
            });
            
            if (error) {
                Alert.alert(error.message)
            }else{
                saveCard(setupIntent.paymentMethodId)
            }
        } catch (error) {
            console.log(error)
        } finally{
            setLoader(false)
        }
    }

    const onSubmitCard = async () => {
        setAmountModal(true)
    }

    const saveCard = async(methodID) => {
        setLoader(true)
        let data = {
            method_id: methodID,
            method: "card"
        }
        try {
            let resp = await savePaymentDetails(data)
            if(resp){
                setAmountModal(false)
                setLoader(false)
                props.navigation.navigate(ScreenNames.CREATE_PET_PROFILE)
            }
        } catch (error) {
            setAmountModal(false)
            setLoader(false)
            console.log(error)
        }
    }

    useEffect(() => {
        if (stripeResponse !== null) {
            initStripe({
                publishableKey: stripeResponse?.key,
                merchantIdentifier: 'merchant.com.furrybaxi',
            });
            dispatch(getPaymentSecret())
        }
    }, [stripeResponse])

    useEffect(()=>{
        dispatch(getStripeInformation())
    },[])

    useEffect(()=>{
        if(paymentSecretResponse !== null){
            setClientSecretKey(paymentSecretResponse?.clientSecretKey)
        }
    },[paymentSecretResponse])

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                {headerLeft()}
            </View>
            <View style={styles.content}>
                <View style={{ padding: 20 }}>
                    <Text style={styles.contentSection1Text1}>
                        {"Enter your Card Details"}
                    </Text>
                    {/* <Text style={styles.contentSection1Text2}>
                        {"is simply dummy text of the printing"}
                    </Text> */}
                </View>
                <View style={styles.contentSection2}>
                    <CardField
                        postalCodeEnabled={false}
                        placeholder={{
                            number: 'Enter Card Number',
                            expiration: "Expiry Date",
                            cvc: "Enter CVV"
                        }}
                        cardStyle={{
                            backgroundColor: '#253040',
                            textColor: '#F6F3ED',
                            placeholderColor: "#D1DBEA",
                            borderColor: '#596475',
                            borderWidth: 1,
                            textErrorColor: 'red'
                        }}
                        style={{
                            width: '100%',
                            height: 50,
                            justifyContent: 'space-between'
                        }}
                        onCardChange={(cardDetails) => {
                            setCardDetails(cardDetails)
                        }}
                    />
                </View>
                <View style={styles.footer}>
                    <CustomGradientButton
                        title={"Save Card"}
                        onPress={() => (!cardDetails?.complete || loader) ? {} : onSubmitCard()}
                        disable={false}
                        gradientColor={(!cardDetails?.complete || loader) ? ['#253040', '#17202D'] : ['#DAC5A2', '#A98C6F']}
                        styleContainer={commanStyle.buttonContainer}
                        textStyle={(!cardDetails?.complete || loader) ? commanStyle.darkButtonText : commanStyle.primaryButtonText}
                    />
                </View>
            </View>
            {
                amountModal &&
                <AmountModal disabled={loader}  onOk={createPayment}/>
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#17202D',
        // flex: 1,
        height: getHeight(100)
    },

    header: {
        padding: 20,
        height: getHeight(10)
    },

    content: {
        height: getHeight(40)
    },

    footer: {
        padding: 20,
        justifyContent: 'flex-end',
        backgroundColor: '#253040',
    },

    contentSection1Text1: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: '#E1CDA9',
        paddingVertical: 2
    },

    contentSection1Text2: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#A58B6C',
        paddingVertical: 2
    },
    contentSection2: {
        flex: 1,
        padding: 20,
        backgroundColor: '#253040',
        bordeTopWidth: 5,
        borderColor: 'transparent',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    },
    contentSection2Text1: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#E1CDA9',
        marginBottom: 5
    },

    drowpdownContainer: {
        backgroundColor: '#253040',
        borderBottomWidth: 1,
        borderLeftColor: '#253040',
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderTopWidth: 0,
        borderBottomColor: '#596475',
        paddingLeft: 2,
        paddingRight: 10,
    },
    drowpdownPlaceholder: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        color: '#D1DBEA',
        marginLeft: 0
    },
    dropdownLable: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        color: '#F6F3ED',
        zIndex: 2
    },
    dropdownPickerContainer: {
        backgroundColor: '#EDF2F3',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
    },
    drowpdownPickerContainer: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        color: '#253040'
    },
    arrowDown: {
        height: 9,
        width: 16,
        tintColor: '#D1DBEA'
    },
})

export default CardPayment;