import React from "react";
import { useDispatch, useSelector } from 'react-redux';
import { Image, StyleSheet, View, Text, Platform } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { launchImageLibrary } from 'react-native-image-picker';

import Header from "../../../../component/Header";
import Images from "../../../../assets/Images/Images";
import { fSize } from "../../../../utils/Responsive";
import BackButton from "../../../../component/BackButton";
import SkipButton from "../../../../component/SkipButton";
import CustomCamera from "../../../../component/CameraScreen";
import PrimaryButton from "../../../../component/PrimaryButton";
import OutlineButton from "../../../../component/OutlineButton";
import { MEDIUM, SEMI_BOLD } from "../../../../utils/typography";
import { setPicEditPicture, uploadPetEditPic, } from "../../../../redux/actions/petEditAction";
import { backListener, removeBackListener } from "../../../../utils/backHandler";



const EditPetPicture = (props) => {
    const dispatch = useDispatch();

    React.useEffect(() => {
        backListener()
        return () => {
            removeBackListener()
        };
    }, []);

    const [isCapture, setIsCapture] = React.useState(false);

    const { petProfile } = useSelector(state => state.petEdit)


    const headerLeft = () => {
        return <BackButton onPress={() => { props.navigation.goBack() }} />
    }

    const headerRight = () => {
        return <SkipButton title={'Skip'} onPress={() => { }} />
    }

    const onCaptureImage = async (uri) => {
        dispatch(setPicEditPicture(uri))
        uploadPic(uri)
        setIsCapture(false)
    }

    const extractImage = async (responseData) => {
        // if(responseData && responseData.assets?.[0]?.type == "image/jpeg"){
        dispatch(setPicEditPicture(responseData?.assets?.[0]?.uri))
        uploadPic(responseData?.assets?.[0]?.uri)
        // }
    }

    const openGallery = React.useCallback(() => {
        let options = {
            saveToPhotos: true,
            mediaType: 'photo',
            includeBase64: false,
            maxWidth: 300,
            maxHeight: 300,
            quality: 1
        }
        launchImageLibrary(options, extractImage);
    }, []);

    const uploadPic = (uri) => {
        const data = new FormData();
        data.append('pic', {
            name: petProfile.name,
            type: "image/jpeg",
            uri: Platform.OS === 'ios' ? uri.replace('file://', '') : uri,
        });
        dispatch(uploadPetEditPic(data, petProfile.pid))
    }


    if (!isCapture) {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <Header
                        leftView={headerLeft}
                       // rightView={headerRight}

                    />
                </View>
                <View style={styles.content}>
                    <View style={styles.contentSection1}>
                        <View style={styles.contentSection1PictureContainer}>
                            {
                                !petProfile.pic ?
                                    <Image
                                        source={Images.pet_picture_placeholder}
                                        style={styles.contentSection1PictureContainerPlaceHolderImage}
                                    />
                                    :
                                    <Image
                                        resizeMethod={'scale'}
                                        resizeMode={'cover'}
                                        style={styles.contentSection1PictureContainerImage}
                                        source={{ uri: petProfile.pic }}
                                    />
                            }
                        </View>
                    </View>
                    <View style={styles.contentSection2}>
                        <Text style={styles.contentSection2Text1}>
                            {`Upload ${petProfile.name}’s Picture`}
                        </Text>
                    </View>
                    <View style={styles.contentSection3}>
                        <Text style={styles.contentSection3Text1}>
                            {"Click or Upload from the gallery"}
                        </Text>
                    </View>
                </View>
                <View style={styles.footer}>
                    <PrimaryButton
                        title={"Open Camera"}
                        onPress={(e) => { setIsCapture(true) }}
                    />
                    <View style={{ height: 15 }} />
                    <OutlineButton
                        title={"Upload from Gallery"}
                        textColor={"#E1CDA9"}
                        borderColor={"#253040"}
                        onPress={(e) => {
                            openGallery()
                        }}
                    />
                </View>
            </SafeAreaView>
        )
    } else {
        return (
            <CustomCamera onCaptureImage={onCaptureImage} setIsCapture={() => { setIsCapture(false) }} />
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#17202D',
        flex: 1,
    },
    header: {
        height: hp(20),
        padding: 20,
    },
    content: {
        height: hp(55),
        padding: 20,
    },
    footer: {
        height: hp(25),
        justifyContent: 'flex-end',
        padding: 20
    },
    contentSection1: {
        height: hp(35),
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentSection2: {
        height: hp(5),
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentSection3: {
        height: hp(5),
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    contentSection1PictureContainer: {
        width: 220,
        height: 220,
        backgroundColor: '#253040',
        borderWidth: 1,
        borderColor: 'transparent',
        borderRadius: 35,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden'
    },
    contentSection1PictureContainerPlaceHolderImage: {
        width: 100,
        aspectRatio: 1.1,
    },
    contentSection1PictureContainerImage: {
        width: 220,
        height: 220,
        borderWidth: 1,
        aspectRatio: 1
    },
    contentSection2Text1: {
        color: '#E1CDA9',
        fontSize: fSize(20),
        fontFamily: SEMI_BOLD,
        lineHeight: 24
    },
    contentSection3Text1: {
        color: '#A58B6C',
        fontSize: fSize(14),
        fontFamily: MEDIUM,
        lineHeight: 17
    },
})

export default EditPetPicture;