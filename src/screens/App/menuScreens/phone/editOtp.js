import React, { useEffect, useRef, useState } from "react"
import { Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import Images from "../../../../assets/Images/Images";
import CustomButton from "../../../../component/CustomButton";

import { fSize } from "../../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../utils/typography";
import { useDispatch, useSelector } from "react-redux";
import { editPhoneOtp, resetState, setEditOtp, setEditOtpError, setError, setOtp, updatePhoneOtp } from "../../../../redux/actions/otpAction";

import { useIsFocused } from "@react-navigation/native";
import Header from "../../../../component/Header";
import BackButton from "../../../../component/BackButton";
import OtpInput from "../../../../component/OtpInput";

const EditOtp = (props) => {
    //const [isError, setIsError] = useState(false)
    const dispatch = useDispatch();


    const phoneNumberState = useSelector(state => state.phoneNumber)

    const { editOtp, isEditOtpError } = useSelector(state => state.otp)
    const otpRef = useRef(null)
    const isVisible = useIsFocused();

    useEffect(() => {
        if (isVisible) {
            resetInput()
            dispatch(resetState())
        }
    }, [isVisible])

    const submit = () => {
        let inputVariable = {
            "otp_id": phoneNumberState?.editResponse?.otp_id,
            "otp": editOtp
        }
        JSON.stringify(inputVariable)
        dispatch(editPhoneOtp(inputVariable));
    }

    const onChangeOtp = (e) => {
        dispatch(setEditOtpError(false))
        dispatch(setEditOtp(e))
    }

    const resendOtp = () => {
        resetInput()
        dispatch(setEditOtp(''))
        let inputVar = {
            phone: phoneNumberState?.editPhoneNumber,
            cc: phoneNumberState?.editCountry?.value
        }
        dispatch(editPhoneOtp(inputVar))
    }

    const resetInput = () => {

        otpRef.current.reset();
    }

    const headerLeft = () => {
        return <BackButton onPress={() => { props.navigation.goBack() }} />
    }


    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.topView}>
                <Header
                    leftView={headerLeft}
                />
            </View>
            <View style={styles.middleView}>
                <View style={styles.middleViewSection1}>
                    {
                        isEditOtpError ?
                            <Text style={styles.middleViewSection1ErrorText1}>
                                {"You entered a wrong code,\n"}
                                <Text style={styles.middleViewSection1ErrorText2}>
                                    {"Please enter the correct one"}
                                </Text>
                            </Text> :
                            <Text style={[styles.middleViewSection1Text]}>
                                {"Please enter the Code"}
                            </Text>
                    }
                </View>
                <View style={styles.middleViewSection2}>
                    <Text style={[styles.middleViewSection2Text, , isEditOtpError && { color: '#F9D591' }]}>
                        {`A 4 digit code is sent to +${phoneNumberState?.editCountry?.value} ${phoneNumberState?.editPhoneNumber}`}
                    </Text>
                </View>
                <View style={styles.middleViewSection3}>
                    <OtpInput
                        otpRef={otpRef}
                        otp={editOtp}
                        onChangeOtp={(e) => { onChangeOtp(e) }}
                        isError={isEditOtpError}
                    />
                </View>
                <View style={styles.middleViewSection4}>
                    <TouchableOpacity onPress={() => { resendOtp() }}>
                        <Text style={styles.middleViewSection4Text}>
                            {"I haven’t received the code, Resend it Again >"}
                        </Text>
                        <View style={styles.middleViewSection4Border}></View>
                    </TouchableOpacity>

                </View>
            </View>
            <View style={styles.bottomView}>
                <CustomButton
                    title={"Next"}
                    onPress={() => submit()}
                    disable={false}
                    styleContainer={styles.bottomViewButton}
                    textStyle={styles.bottomViewButtonText}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#17202D',
        padding: 20
    },
    topView: {
        height: '20%',
        // padding: 20,
    },
    topViewButton: {
        padding: 10,
        width: 44,
        height: 44,
        backgroundColor: '#253040',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    topViewButtonIcon: {
        width: 8,
        height: 16,
    },
    middleView: {
        flex: 1,
    },
    middleViewSection1: {
        //flex: 2,
        justifyContent: 'flex-end',
        paddingVertical: 10
    },
    middleViewSection1Text: {
        color: '#E1CDA9',
        fontSize: fSize(20),
        lineHeight: 24,
        fontFamily: SEMI_BOLD,
        textAlign: 'left'
    },
    middleViewSection1ErrorText1: {
        color: '#FF7373',
        fontSize: fSize(18),
        lineHeight: 22,
        fontFamily: MEDIUM,
        textAlign: 'left'
    },
    middleViewSection1ErrorText2: {
        color: '#FF7373',
        fontSize: fSize(18),
        lineHeight: 22,
        fontFamily: SEMI_BOLD,
        textAlign: 'left'
    },
    middleViewSection2: {
        //flex: .8
    },
    middleViewSection2Text: {
        color: '#A58B6C',
        fontSize: fSize(12),
        lineHeight: 15,
        fontFamily: REGULAR
    },
    middleViewSection3: {
        // flex: 2.2,
        justifyContent: 'center'
    },
    middleViewSection4: {
        // flex: 2,
        justifyContent: 'center'
    },
    middleViewSection4Text: {
        color: '#D1DBEA',
        fontSize: fSize(12),
        lineHeight: 15,
        fontFamily: REGULAR
    },
    middleViewSection4Border: {
        width: '90%',
        borderBottomWidth: 0.8,
        borderBottomColor: "#596475",
        paddingTop: 10
    },
    bottomView: {
        //flex: 5,
        justifyContent: 'flex-start'
    },
    bottomViewButton: {
        backgroundColor: '#2EC1AC',
        height: 54,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
    },
    bottomViewButtonText: {
        color: '#FFFCF5',
        fontSize: fSize(14),
        fontFamily: SEMI_BOLD
    }
})

export default EditOtp;