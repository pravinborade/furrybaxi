import React from 'react';
import{StyleSheet} from 'react-native';
import OtpInputs from 'react-native-otp-inputs';
import { fSize } from '../../../../../utils/Responsive';
import { MEDIUM } from '../../../../../utils/typography';

const OtpInput = React.forwardRef(({otp, onChangeOtp, isError}, otpRef) =>  (
        <OtpInputs
            ref={otpRef}
            defaultValue={otp}
            autofillFromClipboard={false}
            numberOfInputs={4}
            style={styles.inputContainer}
            inputStyles={[styles.codeInputFieldStyle, isError && styles.errorFieldStyle]}
            handleChange={(e)=>{onChangeOtp(e)}}
            selectionColor={"#253040"}
        />
    ));


  const styles = StyleSheet.create({
    inputContainer:{
        width: '90%',
        height: 120,
        flexDirection: 'row',
        paddingTop:20,
        justifyContent: 'space-between',
        alignItems: 'flex-start',
    }, 
    codeInputFieldStyle: {
        width: 54,
        height: 78,
        borderWidth: 1,
        borderRadius: 10,
        borderColor :'#E1CDA9',
        textAlign: 'center',
        fontFamily: MEDIUM,
        fontSize: fSize(24),
        lineHeight: 29,
        color: '#F9D591',
    },
    errorFieldStyle:{
        // borderColor :'#FF7373',
        borderWidth: 0,
        color: '#FF7373',
        backgroundColor: 'rgba(255, 115, 115, 0.3)'
    }
})

export default React.memo(OtpInput);