import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, FlatList, Dimensions, Platform } from 'react-native'
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { fSize } from '../../../../utils/Responsive';
import { BOLD, MEDIUM, REGULAR, SEMI_BOLD } from '../../../../utils/typography';
import Images from '../../../../assets/Images/Images';
import MyLocationButton from '../../component/MyLocationButton';
import { useDispatch, useSelector } from 'react-redux';
import {  setHumanData } from '../../../../redux/actions/ridingAction';
const { width, height } = Dimensions.get('window');

const ActionContainer1 = (props) => {

    const { setHumanPetsList, activeKey, setActiveKey, isConfirmed, setConfirmed, isUpcomingRideView, onSelectLocation, setGoogleInputFoucused, userLocations, inputRef } = props;
    const { pets, humanCount, humanData, petCount } = useSelector(state => state.riding)
    const dispatch = useDispatch()

    const onPress = (item) => {
        var data = [...humanData]
        for (let i = 0; i < data.length; i++) {
            if (data[i].key == item.key) {
                data[i].isSelected = true
            } else {               
                data[i].isSelected = false
            }
        }
        if(activeKey!=item.key){
            setHumanPetsList()
        }
        setHumanData(data)
        dispatch(setActiveKey(item.key))
        dispatch(setConfirmed(false))
    }

    return (
        <View>
            <MyLocationButton marginTop={-60} />
            {isUpcomingRideView &&
                <View style={styles.actionUpcomingContainer1Section0}>
                    <View style={{ flexDirection: 'row' }}>
                        <Image source={Images.car} style={{ height: 18, width: 20 }} />
                        <Text style={styles.actionUpcomingContainer1Section0Text0}>{'Upcoming Ride'}</Text>
                    </View>
                    <TouchableOpacity>
                        <Text style={styles.actionUpcomingContainer1Section0Text}>
                            {'View'}
                        </Text>
                    </TouchableOpacity>
                </View>
            }

            <View style={styles.actionContainer}>
                <View style={styles.actionContainer1Section1}>
                    <Text style={styles.actionContainer1Section1Text1}>{"Good Morning"}</Text>
                    <View style={{ height: hp(1) }}></View>
                    <Text style={styles.actionContainer1Section1Text2}>{"Where do you want to go?"}</Text>
                </View>
                <View style={{ height: hp(2) }}></View>
                <View style={styles.actionContainer1Section2}>
                    <TextInput
                        ref={inputRef}
                        placeholder="Enter your Destination"
                        placeholderTextColor="#000"
                        style={styles.searchInput}
                        onChangeText={(text) => { }}
                        placeholderTextColor={"#ADADAE"}
                        onFocus={() => {
                            setGoogleInputFoucused(true)
                        }}
                    />
                    <Image source={Images.search} style={styles.searchIcon} />
                </View>
               
                <View style={{paddingVertical:30}}>
                    {
                        humanCount == 0 && petCount == 0 ? <Text style={styles.passangerTitle}>{'Maximum four passengers allowed at a time'}</Text> :
                            <Text style={styles.passangerTitle}> {`${humanCount} Passengers + ${petCount} pets`}</Text>
                    }
                </View>               
   
                <View style={styles.actionContainer1Section4}>               
                    <View style={styles.actionContainer1Section4ItemContainer}>
                        {
                            humanData.map((item, index) => (
                                <View
                                    style={styles.actionContainer1Section4ItemContainerBox}
                                    key={index}
                                >
                                    {
                                        item.isSelected ?
                                            <TouchableOpacity onPress={() => onPress(item)} style={[styles.actionContainer1Section4ItemContainerBoxIconOuter, { flexDirection: 'row', width: width * 45 / 100 }]}>
                                                <View style={styles.actionContainer1Section4ItemContainerBoxIcon}>
                                                    <Image source={item.image} style={styles.human} />
                                                </View>
                                                <Text style={styles.actionContainer1Section4ItemContainerBoxIconText}>{item?.name}</Text>
                                            </TouchableOpacity>
                                            :
                                            <TouchableOpacity onPress={() => onPress(item)} style={styles.actionContainer1Section4ItemContainerBoxIconOuter}>
                                                <View style={styles.actionContainer1Section4ItemContainerBoxIcon}>
                                                    <Image source={item.image} style={styles.human} />
                                                </View>
                                            </TouchableOpacity>
                                    }
                                </View>
                            ))}
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    iconImages: {
        height: 25,
        width: 25
    },
    actionContainer: {
        backgroundColor: '#253040',
        padding: 20,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        // zIndex: 15,
        elevation: (Platform.OS === 'android') ? 50 : 0

    },

    actionUpcomingContainer1Section0: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 15,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },

    actionUpcomingContainer1Section0Text0: {
        color: '#9D7E62',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        paddingHorizontal: 5

    },
    actionUpcomingContainer1Section0Text: {
        color: '#9D7E62',
        fontFamily: MEDIUM,
        fontSize: fSize(12)

    },
    actionContainer1Section1: {
        maxHeight: hp(10),
    },
    actionContainer1Section1Text1: {
        fontFamily: BOLD,
        fontSize: fSize(22),
        lineHeight: 27,
        color: "#FDFDFD"
    },
    actionContainer1Section1Text2: {
        fontFamily: REGULAR,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#F9D591"
    },
    actionContainer1Section2: {
        maxHeight: hp(10),
    },
    // actionContainer1Section3: {
    //     maxHeight: hp(20),
    //     justifyContent: 'flex-start',
    // },

    // actionContainer1Section3Item: {
    //     flexDirection: 'row',
    //     justifyContent: 'flex-start',
    //     alignItems: 'center',
    //     backgroundColor: '#253040',
    //     marginBottom: hp(4),
    // },
    // actionContainer1Section3ItemLeft: {
    //     flex: .12,
    //     alignItems: 'flex-start',
    // },
    // actionContainer1Section3ItemRight: {
    //     flex: .88,
    //     borderBottomWidth: 1,
    //     paddingBottom: 10,
    //     borderColor: '#564f47',
    // },
    // actionContainer1Section3ItemRightText1: {
    //     fontFamily: MEDIUM,
    //     fontSize: fSize(14),
    //     lineHeight: 17,
    //     color: '#E1CDA9',
    //     marginBottom: 5
    // },
    // actionContainer1Section3ItemRightText2: {
    //     fontFamily: REGULAR,
    //     fontSize: fSize(12),
    //     lineHeight: 15,
    //     color: "#A58B6C"
    // },

    actionContainer1Section4: {
        // position: 'absolute',
        // bottom: '5%',
        // left: 20, right: 20,
        backgroundColor: "#17202D",
        borderRadius: 20,
        //zIndex: 3,

    },

    actionContainer1Section4ItemContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    actionContainer1Section4ItemContainerBox: {

    },
    actionContainer1Section4ItemContainerBoxIcon: {
        width: 50,
        height: 50,
        borderRadius: 15,
        backgroundColor: '#B59B7C',
        justifyContent: 'center'
    },
    actionContainer1Section4ItemContainerBoxIconOuter: {
        backgroundColor: '#2D3C51',
        alignItems: 'center',
        justifyContent: 'flex-start',
        borderRadius: 15,
        padding: 6
    },
    actionContainer1Section4ItemContainerBoxIconText: {
        color: '#F9D591',
        fontSize: fSize(12),
        fontFamily: MEDIUM,
        padding: 5,
    },
    searchInput: {
        backgroundColor: '#FFFFFF',
        height: 50,
        borderRadius: 15,
        paddingVertical: 5,
        paddingHorizontal: 15,
        fontSize: fSize(14),
        fontFamily: MEDIUM,
    },
    searchIcon: {
        position: 'absolute',
        right: 15,
        top: 15
    },

    // Human  & pets
    human: {
        alignSelf: 'center',
        tintColor: '#9D7E62'
    },
    humanContainer: {
        backgroundColor: '#2D3C51',
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 10,
        padding: 10,
        borderRadius: 15,
    },
    humanTitle: {
        color: '#E1CDA9',
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        alignSelf: 'center',
        paddingVertical: 8
    },
    HumanButton: {
        backgroundColor: '#B59B7C',
        width: 50,
        height: 50,
        borderRadius: 12,
        justifyContent: 'center',
        padding: 10
    },
    humanCount: {
        color: '#E1CDA9',
        fontSize: fSize(18),
        fontFamily: SEMI_BOLD,
        alignSelf: 'center',
        paddingHorizontal: 20
    },
    petsContainer: {

    },
    petCard: {
        backgroundColor: '#253040',
        padding: 10,
        margin: 10,
        borderRadius: 12,
        flexDirection: 'row',
    },


    petImage: {
        height: 45,
        width: 45,
        borderRadius: 10,
        margin: 5
    },
    selectedPetBox: {

    },
    petBoxContainer: {
        backgroundColor: '#17202D',
        width: 50,
        height: 50,
        borderRadius: 12,
        justifyContent: 'center',
        padding: 10
    },

    petBox: {
        borderColor: '#B59B7C',
        width: 40,
        height: 40,
        borderWidth: 1,
        borderRadius: 12,
        alignSelf: 'center',
    },
    selectedPetBox: {
        backgroundColor: '#B59B7C',
        width: 40,
        height: 40,

        borderRadius: 12,
        alignSelf: 'center',
    },
    petTitle: {
        padding: 2,
        color: '#E1CDA9',
        fontSize: fSize(14),
        fontFamily: MEDIUM
    },
    petCategoryTitle: {
        padding: 2,
        color: '#F5EDDC',
        fontSize: fSize(14),
        fontFamily: REGULAR
    },

    passangerTitle: {
        color: '#E1CDA9',
        fontSize: fSize(14),
        fontFamily: MEDIUM
    }
});

export default ActionContainer1;