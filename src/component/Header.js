import React from 'react';
import { StyleSheet, View } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const Header = ({
    leftView,
    middleView,
    rightView,
    bgColor,
    cmHeight
}) => {
    const LeftView = leftView
    const MiddleView = middleView
    const RightView = rightView
    return (
        <View style={[styles.headerContainer, bgColor && { backgroundColor: bgColor }, cmHeight && { height: hp(cmHeight) }]}>
            <View style={styles.leftView}>
                {leftView ? leftView() : <></>}
            </View>
            <View style={styles.middleView}>
                {middleView ? middleView() : <></>}
            </View>
            <View style={styles.rightView}>
                {rightView ? rightView() : <></>}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    headerContainer: {
        backgroundColor: '#17202D',
        paddingVertical: 10,
        height: '20%',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    leftView: {

    },
    middleView: {
        //marginLeft:-30
    },

    rightView: {

    }

})

export default Header;