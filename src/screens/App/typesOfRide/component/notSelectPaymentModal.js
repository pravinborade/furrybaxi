import React from 'react';
import {StyleSheet, View, Modal, Dimensions, Text, TouchableOpacity } from 'react-native';
import { fSize } from '../../../../utils/Responsive';
import { REGULAR } from '../../../../utils/typography';

const screenHeight = Dimensions.get('screen').height
const screenWidth = Dimensions.get('screen').width

const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}
const getWidth = (wd) => {
    return screenWidth * (wd / 100)
}

const NotSelectPaymentModal = (props) => {
    return(
        <Modal
            transparent={true}
            visible={true}
        >
        <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
                <Text style={styles.contentTitle}>{props.message}</Text>
                <TouchableOpacity style={styles.submitButton} onPress={props?.onClose}>
                    <Text style={styles.submitButtonText}>
                        {"Okay"}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    </Modal>
    )
}

const styles = StyleSheet.create({
    modalContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    modalContent:{
        height: getHeight(25),
        width: getWidth(80),
        paddingVertical: 40,
        backgroundColor: '#253040',
        borderRadius: 20,
        alignItems: 'center',
    },
    contentTitle:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 22,
        color: "#F5EDDC"
    },
    submitButton:{
        marginTop: 40,
        width: getWidth(30),
        padding: 10,
        borderRadius: 20,
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: "#9D7E62"
    },
    submitButtonText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 22,
        color: "#F5EDDC"
    }
})

export default NotSelectPaymentModal;