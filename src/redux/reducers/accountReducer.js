const initialState = {
    pic: null,
    isPicError: false,
    isVerifyEmailModal: false
};

const accountReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_ACCOUNT_PICTURE':
            return {
                ...state,
                pic: action.pic,
                isPicError: false
            }

        case 'SET_PET_PICTURE_ERROR':
            return {
                ...state,
                isPicError: action.isPicError
            }

        case 'SET_VERIFY_MAIL_MODAL':
            return {
                ...state,
                isVerifyEmailModal: action.isVerifyEmailModal,
            };


        default:
            return state;
    }
};

export default accountReducer;
