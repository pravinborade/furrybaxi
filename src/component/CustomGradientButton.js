import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import{StyleSheet,TouchableOpacity,Text} from 'react-native';

import { fSize, sHeight } from '../utils/Responsive';
import { MEDIUM } from '../utils/typography';

const CustomGradientButton = ({
    title,
    onPress,
    disable,
    gradientColor,
    styleContainer,
    textStyle
  }) => {
    return (
        <TouchableOpacity style={{width:'100%'}} onPress={onPress}>
            <LinearGradient
                useAngle={true}
                angle={180} 
                colors={gradientColor} 
                start={{x: 0.0 , y: 0.0}}
                end={{x: 0.0, y: 1.0}}
                // locations={[-0.3, 1.5]}
                style={styleContainer}
            >
                <Text style={textStyle}>
                    {title}
                </Text>
            </LinearGradient>  
        </TouchableOpacity>
    );
  };

  const styles = StyleSheet.create({
    bottomContainer:{
        justifyContent: 'center',
        alignItems: 'center',
        height: 54,
        borderRadius: 15
    },
    buttonText:{
        color:'#E2EAED',
        fontFamily:MEDIUM,
        fontSize: fSize(14)
    }
  })

  export default CustomGradientButton;