import {applyMiddleware, compose, createStore} from 'redux';

import thunk from 'redux-thunk';
import rootReducer from '../reducers/rootReducer';


let middleware = [];
middleware = [...middleware, thunk];

const store = createStore(rootReducer, compose(applyMiddleware(...middleware)));
const getStore = () => {
  return store;
};

export {store, getStore};
