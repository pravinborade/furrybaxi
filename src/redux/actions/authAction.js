import AsyncStorage from "@react-native-async-storage/async-storage";
import { Alert } from "react-native";
import { backNavigation, navigate } from "../../root/navigation/rootNavigation";
import appService from "../../service/appService";
import { ScreenNames } from "../../utils/ScreenNames";

export function setUserResponse(response) {
    return {
        type: 'SET_USER_RESPONSE',
        response
    };
}    

export function resetUserResponse(response) {
    return {
        type: 'RESET_USER_RESPONSE',
        response
    };
}

export function setToken(token) {
    return {
        type: 'SET_TOKEN',
        token
    };
}

export function setIsRegistered(flag) {
    return {
        type: 'SET_IS_REGISTERED',
        flag
    };
}

export function getUserDetails() {
    return async (dispatch) => {
        return appService.get('fb/v1/accounts/user')
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{       
            dispatch(setUserResponse(response))
        })
        .catch((error) =>{ 
            console.log(error)
        });
    };
}

export const setToAsyncStorage = async (data) => {
    try {
        await AsyncStorage.setItem('token', data.access_token);
        await AsyncStorage.setItem('refreshToken', data.refresh_token);
        await AsyncStorage.setItem('isRegistered', JSON.stringify(data.profile_completed));
    } catch (e) {
        console.log(e);
    }
};

export const setIsRegisteredToAsyncStorage = async (data) => {
    try {
        await AsyncStorage.setItem('isRegistered', JSON.stringify(data.profile_completed));
    } catch (e) {
        console.log(e);
    }
};

export const clearToAsyncStorage = async () => {
    try {
        await AsyncStorage.clear()
        navigate(ScreenNames.HOME)
    } catch (e) {
        console.log(e);
    }
};

