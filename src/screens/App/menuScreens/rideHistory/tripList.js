import React, { useEffect, useState } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text, ScrollView } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../../assets/Images/Images";
import { getRideList } from "../../../../redux/actions/rideAction";
import { backNavigation, navigate } from "../../../../root/navigation/rootNavigation";
import { CONSTANTS, RIDE_STATUS } from "../../../../utils/constants";
import { fSize } from "../../../../utils/Responsive";
import { ScreenNames } from "../../../../utils/ScreenNames";
import { REGULAR, SEMI_BOLD } from "../../../../utils/typography";
import CompletedList from "./component/CompletedList";
import UpcomingList from "./component/UpcomingList";

const Buttons = [{
    id:1,
    text: 'Upcoming'
},
{
    id:2,
    text: 'Completed'
}]


const TripList = () => {
    const dispatch = useDispatch()
    const {rideListResponse} = useSelector(state=>state.ride)

    const [activeButton, setActiveButton] = useState(1)
    const [upcomingList, setUpcomingList] = useState([])
    const [completedList, setCompletedList] = useState([])

    useEffect(()=>{
        dispatch(getRideList()) 
    },[])

    useEffect(()=>{
        if(rideListResponse && rideListResponse !== null){
            let tempUpcomingList = []
            let tempCompletedList = []
            rideListResponse?.data?.map(item=>{
                if(item.status === RIDE_STATUS.UPCOMING) {
                    tempUpcomingList.push(item)
                }else{
                    tempCompletedList.push(item)
                }
            })
            setUpcomingList(tempUpcomingList)
            setCompletedList(tempCompletedList)
        }
    },[rideListResponse])

    const onPressListItem = (item) => {
        navigate(ScreenNames.TRIP_DETAILS, {id: item.id})
    }

    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerLeft}>
                    <TouchableOpacity style={styles.headerLeftButton} onPress={()=>{backNavigation()}}>
                        <Image source={Images.arrow_back} style={styles.headerButtonIcon}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.headerRight}>
                    <Text style={styles.headerRightText}>
                        {"Ride History"}
                    </Text>
                </View>
            </View>
            <View style={styles.content}>
                <View style={styles.contentButtonContainer}>
                    {
                        Buttons?.map(item=>(
                            <TouchableOpacity 
                                style={
                                    activeButton === item.id ? 
                                    styles.contentButtonContainerActiveButton
                                    :
                                    [
                                        styles.contentButtonContainerButton,
                                        item.id === 1 ? styles.letfBorder : styles.rightRadius
                                    ]
                                }
                                onPress={()=>{setActiveButton(item.id)}}
                                key={item.id}
                            >
                                <Text 
                                     style={
                                        activeButton === item.id ? 
                                        styles.contentButtonContainerActiveButtonText:
                                        styles.contentButtonContainerButtonText
                                     }   
                                >
                                    {item.text}
                                </Text>
                            </TouchableOpacity>
                                
                        ))
                    }
                </View>
            </View>
            <View style={styles.bottomView}>
                <ScrollView>
                    {
                        activeButton === 1 ? 
                        <>
                        {
                            upcomingList?.map((item,index)=>(
                                <TouchableOpacity disabled={true} key={index}>
                                    <UpcomingList data={item} />
                                </TouchableOpacity>
                            ))
                        }
                        </>
                         :
                        <>
                        {
                            completedList?.map((item,index)=>(
                                <TouchableOpacity onPress={()=>{onPressListItem(item)}} key={index}>
                                    <CompletedList data={item}/>
                                </TouchableOpacity>
                            ))
                        }
                        </> 
                    }
                    
                </ScrollView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#17202D',
        padding: 20
    },
    header: {
        flex:1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerLeft: {
        flex: .3
    },
    headerRight: {
        flex: .7
    },
    headerRightText: {
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#F4F4F4"
    },
    headerLeftButton: {
        padding: 10,
        width: 44,
        height: 44,
        backgroundColor: '#253040',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerButtonIcon: {
        width: 8,
        height: 16,
    },
    content: {
        flex: 1
    },
    contentButtonContainer: {
        flex: 1,
        flexDirection: 'row',
    }, 
    contentButtonContainerButton: {
        flex: 5,
        borderRadius: 16,
        backgroundColor: '#253040',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentButtonContainerButtonText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#EEEEEE"
    },
    contentButtonContainerActiveButton: {
        flex: 5,
        borderRadius: 16,
        backgroundColor: '#E2EAED',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    contentButtonContainerActiveButtonText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#17202D"
    },
    letfBorder:{
        borderTopRightRadius:0,
        borderBottomRightRadius:0,
        marginRight: -30
    }, 
    rightRadius: {
        borderTopLeftRadius:0,
        borderBottomLeftRadius:0,
        marginLeft: -30,
        zIndex: -1

    },   
    bottomView: {
        flex: 7
    },
})

export default TripList;