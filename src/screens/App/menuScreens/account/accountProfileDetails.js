import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, TouchableOpacity, View, Text, TextInput, ScrollView, Modal } from 'react-native';
import BackButton from '../../../../component/BackButton';
import Header from '../../../../component/Header';
// import Images from '../../../assets/Images/Images';
// import { backNavigation } from '../../../root/navigation/rootNavigation';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../../utils/typography';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import { fSize } from '../../../../utils/Responsive';
import CustomGradientButton from '../../../../component/CustomGradientButton';
import CustomButton from '../../../../component/CustomButton';
import { submit } from 'redux-form';
import { useDispatch, useSelector } from 'react-redux';
import { setVerifyEmailModal, updateAccount } from '../../../../redux/actions/accountAction';



const contents = [
    { id: 0, title: 'Enter new email Address', buttonTitle: 'Update', placeholder: 'enter mail' },
    { id: 1, title: 'Confirm email Address', buttonTitle: 'Update', placeholder: 'enter confirm mail' },
    { id: 2, title: 'First Name', buttonTitle: 'Save', placeholder: 'enter first name' },
    { id: 3, title: 'Last Name', buttonTitle: 'Save', placeholder: 'enter last name' },
    // { id: 4, title: 'Mobile Number', buttonTitle: 'Save', placeholder: 'enter mobile number' },
]



const AccountProfileDetails = (props) => {
    //const selectedContent = contents[0]
    const param = props.route.params

    const dispatch = useDispatch()
    const { userResponse } = useSelector(state => state.auth)

    const [email, setEmail] = useState(null)
    const [confirmEmail, setConfirmEmail] = useState(null)
    const [firstName, setFirstName] = useState(userResponse?.data?.first_name)
    const [lastName, setLastName] = useState(userResponse?.data?.last_name)

    const [error, setError] = useState(null)
    const [isSubmit, setSubmit] = useState(false)
    const [key, setKey] = useState(param.key)
    const [selectedContent, setSelectedContent] = useState(contents[param.key])

    const { isVerifyEmailModal } = useSelector((state) => state.account)
    // const [isVerifyEmailModal, setVerifyEmailModal] = useState(false)

    const headerLeft = () => {
        return <BackButton onPress={() => {
            if (key == 1) {
                setSelectedContent(contents[0])
                setKey(0)
            } else {
                props.navigation.goBack()
            }
        }
        } />
    }

    const emailVaildation = async () => {
        var error = false
        if (email == null || email == '') {
            setError("please enter email")
            error = true
        } else {
            const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(String(email).toLowerCase())) {
                setError("Invaild email")
                error = true
            } else {
                setError(null)
                error = false
            }
        }
        return error
    }

    const confirmEmailVaildation = () => {
        var error = false
        if (confirmEmail == null || confirmEmail == '') {
            setError('please enter Confirm email')
            error = true

        } else {
            if (confirmEmail != email) {
                setError('email not match')
                error = true
            } else {
                setError(null)
                error = false
            }

        }
        return error
    }

    const firstNameVaildation = () => {
        var error = false
        if (firstName == null || firstName == '') {
            setError('please enter first name')
            error = true
        } else {
            setError(null)
            error = false
        }
        return error
    }

    const lastNameVaildation = () => {
        var error = false
        if (lastName == null || lastName == '') {
            setError('please enter last name')
            error = true
        } else {
            setError(null)
            error = false
        }
        return error
    }

    const validation = async () => {
        var error = false
        if (selectedContent.id == 0) {
            error = await emailVaildation()
        }
        if (selectedContent.id == 1) {
            error = await confirmEmailVaildation()
        }
        if (selectedContent.id == 2) {
            error = await firstNameVaildation()
        }
        if (selectedContent.id == 3) {
            error = await lastNameVaildation()
        }
        return error
    }

    const submit = async () => {
        setSubmit(true)
        var val = await validation()
        if (!val) {
            setError(null)
            setSubmit(false)
            if (key == 2) {
                var data = {
                    f_name: firstName
                }
                dispatch(updateAccount(data))
            }
            if (key == 3) {
                var data = {
                    l_name: lastName
                }
                dispatch(updateAccount(data))
            }

            if (key == 0) {
                setKey(1)
                setSelectedContent(contents[1])
            }
            if (key == 1) {
                var data = {
                    email: email
                }
                dispatch(updateAccount(data))
            }

        }
    }


    const onChangeText = async (text) => {
        if (selectedContent.id == 0) {
            await setEmail(text)
        }
        if (selectedContent.id == 1) {
            await setConfirmEmail(text)
        }
        if (selectedContent.id == 2) {
            await setFirstName(text)
        }
        if (selectedContent.id == 3) {
            await setLastName(text)
        }
    }

    useEffect(() => {
        if (isSubmit) { emailVaildation() }
    }, [email])

    useEffect(() => {
        if (isSubmit) { confirmEmailVaildation() }
    }, [confirmEmail])

    useEffect(() => {
        if (isSubmit) { firstNameVaildation() }
    }, [firstName])

    useEffect(() => {
        if (isSubmit) { lastNameVaildation() }
    }, [lastName])

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <Header
                    leftView={headerLeft}
                />
            </View>
            <View style={styles.content}>
                <Text style={styles.contentTitle}>{selectedContent.title}</Text>
                <TextInput
                    style={[styles.contentInputBoxWhite, isSubmit && error && styles.errorBorder]}
                    placeholder={selectedContent.placeholder}
                    placeholderTextColor={'#A58B6C'}
                    value={key == 2 ? firstName : key == 3 ? lastName : key == 0 ? email : key == 1 ? confirmEmail : ''}
                    onChangeText={(text) => onChangeText(text)}
                />
                {
                    isSubmit && error && <Text style={{ color: '#FF7373', paddingHorizontal: 5 }}>{error}</Text>
                }
            </View>
            <View style={styles.footer}>
                <CustomButton
                    title={selectedContent.buttonTitle}
                    onPress={() => submit()}
                    disable={false}
                    styleContainer={styles.bottomButton}
                    textStyle={styles.bottomButtonText}
                />
            </View>
            <Modal
                transparent={true}
                visible={isVerifyEmailModal}
                onRequestClose={() => {
                    dispatch(setVerifyEmailModal(false))
                }}
            >
                <View style={styles.madalContiner}>
                    <View style={styles.modalContent}>
                        <Text style={styles.contentTitle}>{'Verify email address'}</Text>
                        <Text style={styles.contentDescription}>{'An email has been sent to your registered email address'}</Text>
                        <Text style={styles.contentDescription}>{'Please check your email inbox'}</Text>
                    </View>
                </View>
            </Modal>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#17202D',
    },

    header: {
        height: hp(20),
        padding: 20
    },

    content: {
        //minHeight: hp(60),
        flex: 1,
        padding: 20,
        //justifyContent:'space-between'
        //backgroundColor: 'red'
    },
    contentTitle: {
        paddingBottom: 10,
        color: '#E1CDA9',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20)
    },
    contentInputBoxWhite: {
        backgroundColor: '#E2EAED',
        color: '#A58B6C',
        fontFamily: MEDIUM,
        borderRadius: 15,
        fontSize: fSize(12),
        padding: 10,
        height: 54,
    },

    footer: {
        height: hp(20),
        justifyContent: 'flex-end',
        padding: 20,

    },

    bottomButton: {
        backgroundColor: '#2EC1AC',
        height: 54,
        borderRadius: 15,
        justifyContent: 'center',
        marginVertical: 10,
        alignItems: 'center',

    },
    bottomtButtonText: {
        textAlign: 'center',
        color: '#FFFCF5',
        fontSize: fSize(14),
        fontFamily: SEMI_BOLD
    },

    errorBorder: {
        borderWidth: 1,
        borderColor: '#FF7373'
    },
    madalContiner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.8)'
    },
    modalContent: {
        height: hp(30),
        width: wp(80),
        backgroundColor: '#253040',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },

    contentDescription: {
        fontSize: fSize(12),
        fontStyle: REGULAR,
        color: '#F5EDDC',
        paddingHorizontal: 20,
        paddingVertical: 10,
        textAlign: 'center',
        lineHeight: 20
    }
})

export default AccountProfileDetails;

