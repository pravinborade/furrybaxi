
import React from 'react';
import { change, submit } from "redux-form";
import { useDispatch, useSelector } from 'react-redux';
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, Dimensions, Alert, SafeAreaView, KeyboardAvoidingView } from 'react-native'
import { LoginManager, AccessToken, GraphRequestManager, GraphRequest } from 'react-native-fbsdk-next'

import Header from '../../../component/Header';
import { fSize } from '../../../utils/Responsive';
import AccountForm from './AccountForm';
import BackButton from '../../../component/BackButton';
import { MEDIUM, SEMI_BOLD } from '../../../utils/typography';
import { createAccount } from '../../../redux/actions/accountAction';
import { backListener, removeBackListener } from '../../../utils/backHandler';
import CustomGradientButton from '../../../component/CustomGradientButton';
import commanStyle from '../../../utils/commanStyle';
import CustomButton from '../../../component/CustomButton';

const screenHeight = Dimensions.get('screen').height
const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}

const Account = (props) => {
    const dispatch = useDispatch();

    React.useEffect(() => {
        backListener()
        return () => {
            removeBackListener()
        };
    }, [])

    const submitAccountForm = async (data) => {
        data.isFormLogin = true
        await dispatch(createAccount(data))
    }

    const headerLeft = () => {
        return <BackButton onPress={() => { props.navigation.goBack() }} />
    }

    const handleFacebookLogin = async () => {
        await LoginManager.logOut()
        LoginManager.logInWithPermissions(['public_profile']).then(
            login => {
                if (login.isCancelled) {
                    console.log('Login Canceled');
                } else {
                    AccessToken.getCurrentAccessToken().then(
                        (data) => {
                            const accessToken = data.accessToken.toString()
                            getInfoFromToken(accessToken)
                        })
                }
            },
            error => {

                console.log('Error no login ', error)
            }
        )
    }

    const getInfoFromToken = (token) => {
        const PROFILE_REQUEST_PARAMS = {
            fields: {
                string: 'id, name, first_name, last_name, birthday, email'
            },
        }
        const profileRequest = new GraphRequest('/me', { token, parameters: PROFILE_REQUEST_PARAMS },
            (error, result) => {
                if (error) {
                    console.log('Login Info has an error:', error)
                }
                else {
                    if (result.isCancelled) {
                        console.log("Login cancelled");
                    }
                    if (result.email === undefined) {
                        dispatch(change('account-form', 'f_name', result.first_name))
                        dispatch(change('account-form', 'l_name', result.last_name))
                        dispatch(change('account-form', 'email', ''))
                    }
                    else {
                        let data = {
                            email: result.email,
                            f_name: result.first_name,
                            l_name: result.last_name,
                            role: "passenger"
                        }
                        dispatch(createAccount(data))
                    }
                }
            },
        )
        new GraphRequestManager().addRequest(profileRequest).start()
    }

    const handleGoogleLogin = async () => {
        try {
            await GoogleSignin.signOut();
            await GoogleSignin.hasPlayServices();
            const { user } = await GoogleSignin.signIn();
            let data = {
                email: user.email,
                f_name: user.givenName,
                l_name: user.familyName
            }
            dispatch(createAccount(data))

        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            } else if (error.code === statusCodes.IN_PROGRESS) {
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            } else {
            }
        }
    };

    return (
        <SafeAreaView style={styles.mainContainer}>
            <View style={styles.container}>
                <View style={{height: getHeight(10)}}>
                    {headerLeft()}
                </View>
                <ScrollView keyboardShouldPersistTaps='handled' contentContainerStyle={{ flexGrow: 1 }}>
                <View style={styles.content}>
                    <View style={{ height: '60%' }}>
                        <Text style={styles.contentText1}>{'Let’s create your Account'}</Text>
                        <Text style={styles.contentText2}>
                            {'Please fill in the following details to setup your Furry Baxi account'}
                        </Text>
                        <AccountForm onSubmit={submitAccountForm} />
                        <CustomGradientButton
                            title={'Create Account'}
                            onPress={() => { dispatch(submit('account-form')) }}
                            disable={false}
                            gradientColor={['#DAC5A2', '#A98C6F']}
                            styleContainer={commanStyle.buttonContainer}
                            textStyle={commanStyle.primaryButtonText}
                        />

                        <View style={{ paddingVertical: 20 }}>
                            <TouchableOpacity>
                                <Text style={styles.contentText3}>
                                    {'Already have an Account?'}
                                </Text>
                            </TouchableOpacity>
                            <View style={styles.contentLine} />
                        </View>
                    </View>
                    <View style={styles.footer}>
                        <CustomButton
                            onPress={() => { handleGoogleLogin() }}
                            styleContainer={styles.googleLoginButton}
                            textStyle={{ color: '#6D7580' }}
                            title={'SignUp using Google'}
                        />
                        <CustomButton
                            onPress={() => { handleFacebookLogin() }}
                            styleContainer={styles.facebookLoginButton}
                            textStyle={{ color: '#F5F7F8' }}
                            title={'SignUp using Facebook'}
                        />
                    </View>
                </View>
                </ScrollView>
            </View>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: '#17202D',
        height:getHeight(100)
    },
    container: {
        paddingHorizontal: 20,
        minHeight:getHeight(100),
    },

    content: {
        height: getHeight(80),
        justifyContent: 'space-between',
    },
    footer: {
        justifyContent: 'flex-start',
    },

    contentText1: {
        paddingBottom: 10,
        color: '#E1CDA9',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20)
    },

    contentText2: {
        paddingBottom: 10,
        color: '#A58B6C',
        fontFamily: MEDIUM,
        fontSize: fSize(14)
    },

    contentSection5: {
        paddingVertical: 20
    },
    contentText3: {
        color: '#B59B7C',
        fontSize: fSize(12),
        fontFamily: MEDIUM,
        textAlign: 'center',
    },
    contentLine: {
        borderBottomColor: '#564F47',
        borderBottomWidth: 2,
        marginVertical: 30,
        width: '50%',
        alignSelf: 'center'
    },
    googleLoginButton: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 54,
        borderRadius: 15,
        backgroundColor: '#F5F7F8',
        marginBottom: 5,
    },
    facebookLoginButton: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 54,
        borderRadius: 15,
        backgroundColor: '#1577BE',
        marginTop: 5,
    },

});

export default Account;
