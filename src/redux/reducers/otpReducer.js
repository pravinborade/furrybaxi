const initialState = {
    otp: '',
    response: {},
    isError: false,
    editOtp: '',
    editOtpResponse: {},
    isEditOtpError: false
};

const otpReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_OTP':
            return {
                ...state,
                otp: action.otp,
                isError: false
            };

        case 'SET_EDIT_OTP':
            return {
                ...state,
                editOtp: action.editOtp,
                isEditOtpError: false
            };

        case 'SET_OTP_RESPONSE':
            return {
                ...state,
                response: { ...action.response },
                isError: false
            };

        case 'SET_EDIT_OTP_RESPONSE':
            return {
                ...state,
                editOtpResponse: { ...action.editOtpResponse },
                isEditOtpError: false
            };

        case 'SET_OTP_ERROR':
            return {
                ...state,
                isError: action.isError
            }

        case 'SET_EDIT_OTP_ERROR':
            return {
                ...state,
                isEditOtpError: action.isEditOtpError
            }

        case 'RESET_OTP_STATE':
            return {
                ...initialState
            }

        default:
            return state;
    }
};

export default otpReducer;
