import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import accountReducer from './accountReducer';
import authReducer from './authReducer';
import homeReducer from './homeReducer';
import mapReducer from './mapReducer';
import otpReducer from './otpReducer';
import petProfileReducer from './petProfileReducer';
import petEditReducer from './petEditReducer';
import phoneNumberReducer from './phoneNumberReducer';
import rideReducer from './rideReducer';
import ridingReducer from './ridingReducer';
import paymentReducer from './paymentReducer';

const rootReducer = combineReducers({
    form: formReducer,
    phoneNumber: phoneNumberReducer,
    otp: otpReducer,
    petProfile: petProfileReducer,
    petEdit:petEditReducer,
    riding: ridingReducer,
    map: mapReducer,
    auth: authReducer,
    home: homeReducer,
    ride: rideReducer,
    account: accountReducer,
    payment: paymentReducer
});


export default rootReducer;
