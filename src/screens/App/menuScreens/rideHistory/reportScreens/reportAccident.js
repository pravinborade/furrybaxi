import React, { useState } from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text, Dimensions, TextInput, ScrollView, SafeAreaView, Platform, Modal, PermissionsAndroid } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../../../assets/Images/Images";
import { backNavigation, navigate } from "../../../../../root/navigation/rootNavigation";
import { fSize } from "../../../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../../utils/typography";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { Switch } from 'react-native-switch';
import CustomSwitch from "./components/customSwitch";
import DatePicker from 'react-native-date-picker'
import moment from "moment";
import ReportHeader from "./components/reportHeader";
import ReportFooter from "./components/reportFooter";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import { accidentDetails } from "../../../../../redux/actions/rideAction";
import { ScreenNames } from "../../../../../utils/ScreenNames";


const screenHeight = Dimensions.get('screen').height
const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}

const ReportAccident = (props) => {
    const dispatch = useDispatch()
    const [tripID, setTripID] = useState(props?.route?.params?.id)
    
    const [isHospitalised, setIsHospitalised] = useState(false)
    const [isConsulted, setIsConsulted] = useState(false)
    const [accidentDate, setAccidentDate] = useState(new Date())
    const [accidentTime, setAccidentTime] = useState(new Date())
    const [locationOfAccident, setLocationOfAccident] = useState('')
    const [wereInVehicle, setWereInVehicle] = useState('')
    const [anyoneInjured, setAnyoneInjured] = useState('')
    const [howAccidentOccur, setHowAccidentOccur] = useState('')
    const [accidentPhotos, setAccidentPhotos] = useState([])
    const [additionalDetails, setAdditionalDetails] = useState('')

    const [openDateModal, setOpenDateModal] = useState(false)
    const [openTimeModal, setOpenTimeModal] = useState(false)

    const [openCameraModal, setOpenCameraModal] = useState(false)

    const onSubmit = async () => {
        try {
            
            const formData = new FormData();
            formData.append("trip_id", tripID)
            formData.append("accident_date", getDate(accidentDate))
            formData.append("accident_time", getTime(accidentTime))
            formData.append("accident_location", locationOfAccident)
            formData.append("were_u_in", wereInVehicle)
            formData.append("was_anyone_injured", anyoneInjured)
            formData.append("was_anyone_hospital", isHospitalised ? "Yes" : "No")
            formData.append("paid_medical_expansives", isConsulted ? "Yes" : "No")
            formData.append("how_did_accident", howAccidentOccur)
            formData.append("additional_details", additionalDetails)
            accidentPhotos?.map((item,index)=>{
                formData.append('accident_images', {
                    name: item?.name,
                    type: item?.type,
                    uri: Platform.OS === 'ios' ? item?.uri.replace('file://', '') : item?.uri
                })
            })
            let resp = await accidentDetails(formData)
            navigate(ScreenNames.TRIP_DETAILS,{id:tripID})
        } catch (error) {
            console.log(error)
        }
    }

    const getDate = (date) => {
        let dd = ("0" + date?.getDate()).slice(-2)
        let mm = ("0" + (date?.getMonth() + 1)).slice(-2)
        let yyyy = date?.getFullYear()

        return dd + "-" + mm + "-" + yyyy;
    }

    const getTime = (date) => {
        let hh = ("0" + date?.getHours()).slice(-2)
        let mm = ("0" + (date?.getMinutes() + 1)).slice(-2)
        let ss = ("0" + (date?.getSeconds() + 1)).slice(-2)

        return hh + ":" + mm + ":" + ss;
    }

    const onOpenCamera = async() => {
        let options = {
            saveToPhotos: true,
            durationLimit: 1,
            cameraType: 'back',
            selectionLimit: 0
        }
        setOpenCameraModal(false)
        const result = await launchCamera(options, extractImage);
    }

    const requestCameraPermission = async () => {
        if(Platform.OS === 'ios'){
            onOpenCamera()
        }else{
            try {
              const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                  title: "App Camera Permission",
                  message:"App needs access to your camera ",
                  buttonNeutral: "Ask Me Later",
                  buttonNegative: "Cancel",
                  buttonPositive: "OK"
                }
              );
              if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                // console.log("Camera permission given");
                onOpenCamera()
              } else {
                console.log("Camera permission denied");
              }
            } catch (err) {
              console.warn(err);
            }
        }
    };

    const extractImage = async (responseData) => {     
         
        setAccidentPhotos([
            ...accidentPhotos,
            {
                name: responseData?.assets?.[0]?.fileName,
                type: responseData?.assets?.[0]?.type,
                uri: responseData?.assets?.[0]?.uri,
            }
        ])      
    }

   

    const onOpenGallery = async() => {
        setOpenCameraModal(false)
        let options = {
            saveToPhotos: true,
            mediaType: 'photo',
            includeBase64: false,
            maxWidth: 300,
            maxHeight: 300,
            quality: 1,
            selectionLimit:0
        }
        const result = await launchImageLibrary(options, extractImage);
    }

    const checkIfAllFieldFillup = () => {
        if(locationOfAccident && accidentDate && accidentTime){
            return true
        }else{
            return false
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
              <ReportHeader /> 
            </View>
            <KeyboardAwareScrollView
                contentContainerStyle={styles.content}
                enableOnAndroid={true}
                scrollEnabled={true}
            >
                <View style={styles.content}>
                    <ScrollView contentInsetAdjustmentBehavior="always">
                        <View style={styles.contentBody}>
                            <View style={styles.contentBodySectionTitle}>
                                <Text style={styles.contentBodySectionTitleText}>
                                    {"I was involved in an accident"}
                                </Text>
                            </View>
                            <View style={styles.contentBodyDescription}>
                                <Text style={styles.contentBodyDescriptionText} >
                                    {   
                                        `Furry Baxi us committed to the safey of everyone using its ride hailing services.
                                        If you have been involved in an accident, please make sure that you are safe. 
                                        Please notify the police and medical personal if neccesary.
                                        Also please let us know the accident details by sharing the detials with us below. 
                                        We will get in touch with you.`
                                    }
                                </Text>
                            </View>
                            <View style={styles.contentBodyFormItem}>
                                <Text style={styles.contentBodyFormItemLabel}>
                                    {"Date of accident"}
                                </Text>
                                <TouchableOpacity style={styles.contentBodyFormItemInput} onPress={() => setOpenDateModal(true)}>
                                    <Text style={styles.contentBodyFormItemInputText}>
                                        {accidentDate ? getDate(accidentDate) : "dd/mm/yyyy"}
                                    </Text>
                                    <Image
                                        style={styles.calenderIcon}
                                        source={Images.calender}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.contentBodyFormItem}>
                                <Text style={styles.contentBodyFormItemLabel}>
                                    {"Time of accident"}
                                </Text>
                                <TouchableOpacity style={styles.contentBodyFormItemInput} onPress={() => setOpenTimeModal(true)}>
                                    <Text style={styles.contentBodyFormItemInputText}>
                                        {accidentTime ? getTime(accidentTime) : "hh:mm:ss"}
                                    </Text>
                                    <Image
                                        style={styles.calenderIcon}
                                        source={Images.clock_gold}
                                    />
                                </TouchableOpacity>
                                {/* <TextInput
                                    style={styles.contentBodyFormItemInput}
                                    placeholderTextColor={"#ADADAE"}
                                /> */}
                            </View>
                            <View style={styles.contentBodyFormItem}>
                                <Text style={styles.contentBodyFormItemLabel}>
                                    {"Location of accident"}
                                </Text>
                                <TextInput
                                    style={[styles.contentBodyFormItemInput, { height: 120 }]}
                                    placeholderTextColor={"#ADADAE"}
                                    placeholder={"Include intersection if applicable"}
                                    multiline
                                    numberOfLines={20}
                                    value={locationOfAccident}
                                    onChangeText={(text)=>{
                                        setLocationOfAccident(text);
                                    }}
                                />
                            </View>
                            <View style={styles.contentBodyFormItem}>
                                <Text style={styles.contentBodyFormItemLabel}>
                                    {"Were you in the vehicle?"}
                                </Text>
                                <TextInput
                                    style={styles.contentBodyFormItemInput}
                                    placeholderTextColor={"#ADADAE"}
                                    placeholder={"Yes or No"}
                                    value={wereInVehicle}
                                    onChangeText={(text)=>{
                                        setWereInVehicle(text);
                                    }}
                                />
                            </View>
                            <View style={styles.contentBodyFormItem}>
                                <Text style={styles.contentBodyFormItemLabel}>
                                    {"Was anyone injured"}
                                </Text>
                                <TextInput
                                    style={[styles.contentBodyFormItemInput, { height: 120 }]}
                                    placeholderTextColor={"#ADADAE"}
                                    placeholder={"If yes, please describe the injuries"}
                                    multiline
                                    numberOfLines={20}
                                    value={anyoneInjured}
                                    onChangeText={(text)=>{
                                        setAnyoneInjured(text);
                                    }}
                                />
                            </View>
                            <View style={styles.contentBodyRow}>
                                <View style={styles.contentBodyRowLeft}>
                                    <Text style={styles.contentBodyRowLeftText}>
                                        {"Was anyone hospitalised?"}
                                    </Text>
                                </View>
                                <View style={styles.contentBodyRowRight}>
                                    <CustomSwitch
                                        value={isHospitalised}
                                        onChange={(value) => {
                                            setIsHospitalised(value)
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={styles.contentBodyRow}>
                                <View style={styles.contentBodyRowLeft}>
                                    <Text style={styles.contentBodyRowLeftText}>
                                        {`Have you consulted a doctor and paid any medical or hospital expenses?`}
                                    </Text>
                                </View>
                                <View style={styles.contentBodyRowRight}>
                                    <CustomSwitch
                                        value={isConsulted}
                                        onChange={(value) => {
                                            setIsConsulted(value)
                                        }}
                                    />
                                </View>
                            </View>
                            <View style={styles.contentBodyFormItem}>
                                <Text style={styles.contentBodyFormItemLabel}>
                                    {"How did the accident occur?"}
                                </Text>
                                <TextInput
                                    style={[styles.contentBodyFormItemInput, { height: 120 }]}
                                    placeholderTextColor={"#ADADAE"}
                                    multiline
                                    numberOfLines={20}
                                    value={howAccidentOccur}
                                    onChangeText={(text)=>{
                                        setHowAccidentOccur(text);
                                    }}
                                />
                            </View>
                            <View style={styles.contentBodyFormItem}>
                                <Text style={styles.contentBodyFormItemLabel}>
                                    {"Accident Photos"}
                                </Text>
                                <TouchableOpacity 
                                    style={[styles.contentBodyFormItemInput,
                                        {   height: 120, 
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                    }]} 
                                    onPress={() => setOpenCameraModal(true)}
                                >
                                    <Image
                                        style={{width: 32}}
                                        source={Images.camera_button}
                                    />
                                </TouchableOpacity>
                            </View>
                            <View style={styles.contentBodyFormItem}>
                                <Text style={styles.contentBodyFormItemLabel}>
                                    {"Share additional details"}
                                </Text>
                                <TextInput
                                    style={[styles.contentBodyFormItemInput, { height: 120 }]}
                                    placeholderTextColor={"#ADADAE"}
                                    multiline
                                    numberOfLines={20}
                                    value={additionalDetails}
                                    onChangeText={(text)=>{
                                        setAdditionalDetails(text);
                                    }}
                                />
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </KeyboardAwareScrollView>
            <View style={styles.footer}>
                <ReportFooter comment={checkIfAllFieldFillup} onSubmit={onSubmit} />
            </View>
            <DatePicker
                modal
                date={accidentDate}
                open={openDateModal}
                onConfirm={(date) => {
                    setOpenDateModal(false)
                    setAccidentDate(date)
                }}
                onCancel={() => {
                    setOpenDateModal(false)
                }}
                mode={"date"}
            />
            <DatePicker
                modal
                date={accidentTime}
                open={openTimeModal}
                onConfirm={(date) => {
                    setOpenTimeModal(false)
                    setAccidentTime(date)
                }}
                onCancel={() => {
                    setOpenTimeModal(false)
                }}
                mode={"time"}
            />
            <Modal
                animationType="slide"
                transparent={true}
                visible={openCameraModal}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setOpenCameraModal(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <View style={styles.actionRow}>
                            <TouchableOpacity style={styles.actionButton} onPress={()=>{ setOpenCameraModal(false)}}>
                                <Text style={styles.buttonText}>
                                    {"Cancel"}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.imageButton} onPress={()=>{requestCameraPermission()}}>
                                <Text style={styles.buttonText}>
                                    {"Open Camera"}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity 
                                style={[
                                    styles.imageButton,
                                    {backgroundColor: "#E1CDA9"}
                                ]} 
                                onPress={()=>{onOpenGallery()}}

                            >
                                <Text style={styles.buttonText}>
                                    {"Open Gallery"}
                                </Text>
                            </TouchableOpacity>  
                        </View>
                    </View>
                </View>
            </Modal>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: getHeight(100),
        backgroundColor: '#17202D',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        height: getHeight(Platform.OS === 'ios' ? 5 : 15 ),
        paddingHorizontal: 20,
        justifyContent: 'center'
    },
    content: {
        height: getHeight(Platform.OS === 'ios' ? 80 : 70 ),
    },
    contentBody: {
        paddingTop: 10,
        paddingHorizontal: 20,
        justifyContent: 'flex-end'
    },
    contentBodySection1Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#F9D591"
    },
    contentBodySectionTitle: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    contentBodySectionTitleText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#E1CDA9",
    },
    contentBodyDescription: {
        paddingVertical: 10
    },
    contentBodyDescriptionText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 22,
        color: "#FFFFFF",
        // textAlign: 'center'
    },
    contentBodyFormItem: {
        marginVertical: 10
    },
    contentBodyFormItemLabel: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9",
    },
    contentBodyFormItemInput: {
        marginVertical: 5,
        height: 50,
        borderRadius: 15,
        backgroundColor: "#E2EAED",
        paddingHorizontal: 10,
        justifyContent: 'center',
        color:'#ADADAE'
    },
    contentBodyFormItemInputText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#ADADAE",
    },
    calenderIcon: {
        position: 'absolute',
        right: 20,
        width: 20,
        height: 20
    },
    contentBodyRow: {
        flexDirection: 'row',
        marginVertical: 15
    },
    contentBodyRowLeft: {
        flex: 0.9,
    },
    contentBodyRowLeftText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9",
    },
    contentBodyRowRight: {
        flex: 0.3,
        alignItems: 'flex-end',
    },
    error: {
        borderWidth: 2,
        borderColor: 'red'
    },
    footer: {
        height: getHeight(Platform.OS === 'ios' ? 5 : 5 ),
        paddingHorizontal: 20,
        justifyContent: 'center',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
        width: "100%",
    },
    actionRow: {
        height: getHeight(6),
        flexDirection: 'row',
        justifyContent: 'flex-end',
        width: "100%"
    },  
    actionButton: {
        paddingVertical: 15,
        paddingHorizontal: 10,
    },
    buttonContainer: {
        padding: 35,
        width: "100%",
        alignItems: 'center'
    },
    imageButton: {
        paddingVertical: 15,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical: 8,
        borderWidth: 2,
        borderRadius: 25,
        width: "100%"
    },
    buttonText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color:'#ADADAE'
    }
})

export default ReportAccident;