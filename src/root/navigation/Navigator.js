import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Routes } from './Routes';
import { navigationRef } from './rootNavigation';

import { GoogleSignin} from '@react-native-google-signin/google-signin';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Navigator = () => {
    React.useEffect(() => {
        GoogleSignin.configure({
            webClientId:'204407207898-knmnd5fu51v5epugcqsp32lf5741cbjs.apps.googleusercontent.com',
            offlineAccess: true,
        });
       
    }, []);

    return (
        <NavigationContainer ref={navigationRef}>
            <Routes/>
        </NavigationContainer>
      );
}
export default Navigator