import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, TouchableOpacity, View, Text, TextInput, ScrollView, Modal } from 'react-native';
import BackButton from '../../../../component/BackButton';
import Header from '../../../../component/Header';
// import Images from '../../../assets/Images/Images';
// import { backNavigation } from '../../../root/navigation/rootNavigation';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../../utils/typography';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { SafeAreaView } from 'react-native-safe-area-context';
import { fSize } from '../../../../utils/Responsive';
import CustomGradientButton from '../../../../component/CustomGradientButton';
import CustomButton from '../../../../component/CustomButton';
import { submit } from 'redux-form';
import Images from '../../../../assets/Images/Images';
import { ScreenNames } from '../../../../utils/ScreenNames';
import { useDispatch, useSelector } from 'react-redux';
import { getUserDetails } from '../../../../redux/actions/authAction';
import { setAccountPicture, setAccountPictureError, setVerifyEmailModal } from '../../../../redux/actions/accountAction';



const AccountProfile = (props) => {
    const dispatch = useDispatch()
    const { userResponse } = useSelector(state => state.auth)  

    useEffect(() => {
        dispatch(getUserDetails())
    }, [])

    const openDetail = (key) => {
        props.navigation.navigate(ScreenNames.ACCOUNT_PROFILE_DETAIL, { key: key })
    }

    const viewPets = () => {
    }

    const uploadPic = () => {
        dispatch(setAccountPicture(null))
        props.navigation.navigate(ScreenNames.ACCOUNT_PICTURE)
    }


    const headerLeft = () => {
        return (
            <TouchableOpacity onPress={() => props.navigation.goBack()} style={styles.backButton}>
                <Image source={Images.arrow_back} style={styles.backArrow} />
            </TouchableOpacity>
        )
    }

    const headerMiddle = () => {
        return (
            <View style={{ paddingVertical: 5, marginLeft: -40 }}>
                <Text style={styles.headerTitle}>{'Edit Profile'}</Text>
            </View>
        )
    }


    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <Header leftView={headerLeft} middleView={headerMiddle} bgColor={'#253040'} cmHeight={15} />
            </View>
            <View style={styles.content}>
                <View style={styles.contentSection1}>
                    <View style={styles.contentSection1Row1} />
                    <View style={styles.contentSection1Row2}>
                        <View style={styles.contentSection1Row2ImageContainer}>
                            {
                                userResponse?.data?.pic ?
                                    <Image source={{ uri: userResponse?.data?.pic }} style={styles.contentSection1Row2Image} />
                                    :
                                    <Image source={Images.profileImage} style={styles.contentSection1Row2Image} />
                            }
                            <TouchableOpacity onPress={() => uploadPic()} style={styles.editIcon}>
                                <Image source={Images.edit} style={styles.edit} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.contentSection1Row3}>
                        <Text style={styles.contentSection1Row3Text1}>{`${userResponse?.data?.first_name}`}</Text>
                        <View style={styles.contentSection1Row3TextContainer}>
                            <Image source={Images.star} style={styles.ratings} />
                            <Text style={styles.contentSection1Row3TextContainerText}>
                                {userResponse?.data?.rating}
                            </Text>
                        </View>
                    </View>
                </View>
                <View style={styles.contentSection2}>
                    <TouchableOpacity onPress={() => openDetail(2)} style={styles.contentSection2Row}>
                        <Text style={styles.contentSection2Text1}>{'First Name'}</Text>
                        <Text style={styles.contentSection2Text2}>{userResponse?.data?.first_name}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => openDetail(3)} style={styles.contentSection2Row}>
                        <Text style={styles.contentSection2Text1}>{'Last Name'}</Text>
                        <Text style={styles.contentSection2Text2}>{userResponse?.data?.last_name}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => openDetail(0)} style={styles.contentSection2Row}>
                        <Text style={styles.contentSection2Text1}>{'Email Address'}</Text>
                        <Text style={styles.contentSection2Text2}>{userResponse?.data?.email}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { props.navigation.navigate(ScreenNames.EDIT_PHONE) }} style={styles.contentSection2Row}>
                        <Text style={styles.contentSection2Text1}>{'Mobile Number'}</Text>
                        <Text style={styles.contentSection2Text2}>{userResponse?.data?.phone}</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.footer}>
                <CustomButton
                    onPress={() => props.navigation.navigate(ScreenNames.PET_LIST)}
                    styleContainer={styles.buttonContainer}
                    textStyle={styles.buttonText}
                    title={'View pets'}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#253040',
    },
    header: {
        flex: 1,
        padding: 20
    },
    headerTitle: {
        color: '#F4F4F4',
        fontFamily: MEDIUM,
        fontSize: fSize(20)
    },
    backButton: {
        backgroundColor: '#17202D',
        borderRadius: 12,
        height: 44,
        width: 44,
        alignItems: 'center',
        justifyContent: 'center'
    },
    backArrow: {
        height: 16,
        width: 8,
        margin: 10
    },
    content: {
        flex: 8,
    },
    contentSection1: {
        flex: 4,
        backgroundColor: '#17202D',
    },
    contentSection1Row1: {
        flex: 2,
        backgroundColor: '#253040',
    },
    contentSection1Row2: {
        flex: 2,
        alignItems: 'center',
        backgroundColor: '#17202D',
        paddingTop: 10
    },
    contentSection1Row2ImageContainer: {
        width: 100,
        height: 100,
        backgroundColor: '#fff',
        position: 'absolute',
        marginTop: -50,
        borderRadius: 20,
        zIndex: 2
    },
    contentSection1Row2Image: {
        width: 100,
        height: 100,
        borderRadius: 20,
        aspectRatio: 1,
    },
    contentSection1Row3: {
        flex: 3,
        backgroundColor: '#17202D',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    contentSection1Row3Text1: {
        color: '#FFFFFF',
        fontSize: fSize(22),
        lineHeight: 25,
        fontFamily: SEMI_BOLD,
    },
    contentSection1Row3TextContainer: {
        flexDirection: 'row',
        marginTop: 5,
        width: '100%',
        justifyContent: 'center'
    },
    contentSection1Row3TextContainerText: {
        color: '#E2EAED',
        fontSize: fSize(14),
        fontFamily: REGULAR,
        marginLeft: 5,
        paddingVertical:-10
    },
    contentSection2: {
        flex: 9,
        backgroundColor: '#17202D',
        padding: 20,
        paddingBottom: 0,
        paddingTop: 0,
        justifyContent: 'space-between'
    },
    contentSection2Row: {
        backgroundColor: '#253040',
        paddingVertical: 12,
        paddingHorizontal: 16,
        borderRadius: 16,
    },
    contentSection2Text1: {
        color: '#F9D591',
        fontSize: fSize(14),
        fontFamily: REGULAR
    },
    contentSection2Text2: {
        marginTop: 5,
        color: '#FFFFFF',
        fontSize: fSize(16),
        fontFamily: MEDIUM,
        borderBottomColor: '#5E718A',
        borderBottomWidth: 1
    },
    buttonContainer: {
        height: 54,
        borderColor: '#253040',
        borderWidth: 2,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        marginVertical: 20
    },
    buttonText: {
        color: '#E1CDA9',
        fontSize: fSize(14),
        fontFamily: MEDIUM
    },
    profileImage: {
        borderRadius: 20,
        width: wp(35),
        aspectRatio: 1,
        //tintColor: '#fff'
    },
    edit: {
        height: 20,
        aspectRatio: 1,
    },
    editIcon: {
        backgroundColor: '#253040',
        position: 'absolute',
        bottom: 0,
        right: 0,
        zIndex: 1,
        padding: 5,
        borderRadius: 8,
        borderBottomRightRadius: 19
    },
    ratings: {
        height: 16,
        width: 16,
        marginHorizontal: 5,
        marginVertical:2
    },
    footer: {
        backgroundColor: '#17202D',
        flex: .8,
        padding: 20,
    },
    buttonContainer: {
        height: 54,
        borderColor: '#253040',
        borderWidth: 2,
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: '#E1CDA9',
        fontSize: fSize(14),
        fontFamily: MEDIUM
    },

})

export default AccountProfile;

