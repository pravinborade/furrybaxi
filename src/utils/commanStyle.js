import { StyleSheet } from "react-native";
import { fSize } from "./Responsive";
import { MEDIUM } from "./typography";

export default commanStyle = StyleSheet.create({
    buttonContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 54,
        borderRadius: 15,
        //margin:10,
    },

    primaryButtonText: {
        color: '#E2EAED',
        fontFamily: MEDIUM,
        fontSize: fSize(14)
    },



    darkButtonText: {
        color: '#5E718A',
        fontFamily: MEDIUM,
        fontSize: fSize(14)
    }




});

