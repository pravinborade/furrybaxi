import moment from "moment";
import React, { useEffect, useState } from "react";
import { StyleSheet, View, TouchableOpacity, Image, Text } from "react-native";
import SvgUri from "react-native-svg-uri";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../../assets/Images/Images";
import {  checkPermission, getRidePaymentDetails, resendReceiptEmail } from "../../../../redux/actions/rideAction";
import { backNavigation } from "../../../../root/navigation/rootNavigation";
import { DATE_FORMATS } from "../../../../utils/constants";
import { fSize } from "../../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../utils/typography";
import ReceiptModal from "./component/ReceiptModal";

const TripReceipt = (props) => {
    const dispatch = useDispatch()
    const {ridePaymentResponse} = useSelector(state=>state.ride)
    const [tripID, setTripID] = useState(props?.route?.params?.id)
    const [modalVisible, setModalVisible] = useState(false);

    useEffect(()=>{
        if(tripID){
            dispatch(getRidePaymentDetails(tripID))
        }
    },[tripID])

    const resendEmail = async() => {
        try {
            let resp = await resendReceiptEmail(tripID)
            if(resp){
                setModalVisible(true)
            }
        } catch (error) {
            
        }
    }
    
    return(
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerLeft}>
                    <TouchableOpacity style={styles.headerLeftButton} onPress={()=>{backNavigation()}}>
                        <Image source={Images.arrow_back} style={styles.headerLeftButtonIcon}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.headerRight}>
                    <Text style={styles.headerRightText}>{"Receipt"}</Text>
                </View>
            </View>
            <View style={styles.content}>
                <View style={styles.contentSection1}>
                    <View style={styles.contentSection1Left}>
                        <Text style={styles.contentSection1LeftText1}>
                            {`Here’s your receipt\nfor your ride, ${ridePaymentResponse?.user_name}`}
                        </Text>
                        <Text style={styles.contentSection1LeftText2}>
                            {moment(ridePaymentResponse?.timestamp).format(DATE_FORMATS.DO_MMM_HH_MM_A)}
                        </Text>
                    </View>
                    <View style={styles.contentSection1Right}>
                        <Image 
                            source={Images.carWhiteIcon}
                            style={styles.contentSection1RightImage}
                        />
                    </View>
                </View>
                <View style={styles.contentSection2}>
                    <View style={styles.contentSection2Row1}>
                        <View style={styles.contentSection2Row1Left}>
                            <Text style={styles.contentSection2Row1LeftText}>
                                {"Total"}
                            </Text>
                        </View>
                        <View style={styles.contentSection2Row1Right}>
                            <Text style={styles.contentSection2Row1RightText}>
                                {ridePaymentResponse?.total}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.contentSection2Row2}>
                        <View style={styles.contentSection2Row2Left}>
                            <Text style={styles.contentSection2Row2LeftText}>
                                {"Trip Charge"}
                            </Text>
                        </View>
                        <View style={styles.contentSection2Row2Right}>
                            <Text style={styles.contentSection2Row2RightText}>
                                {ridePaymentResponse?.trip_charge}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.contentSection2Row3}>
                        <View style={styles.contentSection2Row3Left}>
                            <Text style={styles.contentSection2Row3LeftText}>
                                {"Subtotal"}
                            </Text>
                        </View>
                        <View style={styles.contentSection2Row3Right}>
                            <Text style={styles.contentSection2Row3RightText}>
                                {ridePaymentResponse?.subtotal}
                            </Text>
                        </View>
                    </View>
                    <View style={styles.contentSection2Row4}>
                        <View style={{flexDirection: 'row'}}>
                            <View style={styles.contentSection2Row4Left}>
                                <Text style={styles.contentSection2Row4LeftText}>
                                    {"Before Taxes"}
                                </Text>
                            </View>
                            <View style={styles.contentSection2Row4Right}>
                                <Text style={styles.contentSection2Row4RightText}>
                                    {ridePaymentResponse?.without_tax}
                                </Text>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row', paddingTop: 10}}>
                            <View style={styles.contentSection2Row4Left}>
                                <Text style={styles.contentSection2Row4LeftText}>
                                    {"Taxes"}
                                </Text>
                            </View>
                            <View style={styles.contentSection2Row4Right}>
                                <Text style={styles.contentSection2Row4RightText}>
                                    {ridePaymentResponse?.tax}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.contentSection2Row5}>
                        <View style={{flexDirection: 'row'}}>
                            <View style={styles.contentSection2Row5Section1}>
                                <Text style={styles.contentSection2Row5Section1Text}>
                                    {"Amount Charged"}
                                </Text>
                            </View>
                            <View style={styles.contentSection2Row4Right}/>
                        </View>
                        <View style={{flexDirection: 'row', paddingTop: 10}}>
                            <View style={styles.contentSection2Row5Left}>
                                <Text style={styles.contentSection2Row5LeftText}>
                                    {ridePaymentResponse?.method?.payment_method}
                                </Text>
                            </View>
                            <View style={styles.contentSection2Row5Right}>
                                <Text style={styles.contentSection2Row5RightText}>
                                    {ridePaymentResponse?.method?.charged}
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.footer}>
                <TouchableOpacity style={styles.footerListItem1} onPress={()=>{checkPermission(ridePaymentResponse?.receipt_url)}}>
                    <View style={[styles.footerListItem1Left, ]}>
                        <SvgUri 
                            source={Images.downloadIconSvg}
                        />
                        {/* <Image source={Images.downloadIcon} style={{height: 20, width: 17}} />        */}
                    </View>
                    <View style={styles.footerListItem1Right}>
                        <Text style={styles.footerListItem1RightText}>
                            {"Download PDF"}
                        </Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.footerListItem2} onPress={()=>{resendEmail()}}>
                    <View style={styles.footerListItem1Left}>
                        <SvgUri 
                            source={Images.mailIconSvg}
                        />
                       {/* <Image source={Images.messageIcon} style={{height: 18, width: 23}} />        */}
                    </View>
                    <View style={styles.footerListItem1Right}>
                        <Text style={styles.footerListItem1RightText}>
                            {"Resend email"}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
            {
                modalVisible &&
                <ReceiptModal 
                    title={"Sent"}
                    message1={"Your email has been sent."}
                    message2={""}
                    onOk={()=>{
                        setModalVisible(false)
                    }}
                />
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        backgroundColor: '#253040'
    },
    header: {
        flex:1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerLeft: {
        flex:0.35,
    },
    headerLeftButton: {
        padding: 10,
        width: 44,
        height: 44,
        backgroundColor: '#323E51',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerLeftButtonIcon: {
        width: 8,
        height: 16,
    },
    headerRight: {
        flex: 0.65,
        alignItems: 'flex-start',
    }, 
    headerRightText:{
        fontFamily: MEDIUM,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#F4F4F4"
    },
    content: {
        flex:5,
        borderRadius: 15,
        backgroundColor: '#323E51',
        marginTop: 10
    },
    contentSection1: {
        backgroundColor: "#E2EAED",
        borderRadius: 15,
        padding: 20,
        paddingBottom: 0,
        flexDirection: 'row'
    },
    contentSection1Left: {
        flex: 0.8,
        paddingBottom: 10
    },
    contentSection1LeftText1: {
        fontFamily: MEDIUM,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#000000"
    },
    contentSection1LeftText2: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 18,
        color: "#000000",
        paddingVertical: 20
    },
    contentSection1Right: {
        flex: 0.2,
        justifyContent: 'flex-end'
    },
    contentSection1RightImage: {
        position: 'absolute',
        right: 0,
        bottom: -10,
        height: 50,
        width: 105
    },
    contentSection2: {
        padding: 20
    },
    contentSection2Row1: {
        flexDirection: 'row',
        paddingVertical: 5,
        borderBottomWidth: 1,
        borderColor: '#B59B7C'
    },
    contentSection2Row1Left: {
        flex: 0.5,
        alignItems: 'flex-start'
    },
    contentSection2Row1LeftText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: '#FFFFFF'
    },
    contentSection2Row1Right: {
        flex: 0.5,
        alignItems: 'flex-end'
    },
    contentSection2Row1RightText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: '#FFFFFF'
    },
    contentSection2Row2: {
        flexDirection: 'row',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderColor: '#B59B7C'
    },
    contentSection2Row2Left: {
        flex: 0.5,
        alignItems: 'flex-start'
    },
    contentSection2Row2LeftText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    contentSection2Row2Right: {
        flex: 0.5,
        alignItems: 'flex-end'
    },
    contentSection2Row2RightText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    contentSection2Row3: {
        flexDirection: 'row',
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderColor: '#B59B7C'
    },
    contentSection2Row3Left: {
        flex: 0.5,
        alignItems: 'flex-start'
    },
    contentSection2Row3LeftText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    contentSection2Row3Right: {
        flex: 0.5,
        alignItems: 'flex-end'
    },
    contentSection2Row3RightText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    contentSection2Row4: {
        paddingVertical: 10,
        borderBottomWidth: 1,
        borderColor: '#B59B7C'
    },
    contentSection2Row4Left: {
        flex: 0.5,
        alignItems: 'flex-start'
    },
    contentSection2Row4LeftText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    contentSection2Row4Right: {
        flex: 0.5,
        alignItems: 'flex-end'
    },
    contentSection2Row4RightText:{
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    contentSection2Row5: {
        paddingVertical: 10,
    },
    contentSection2Row5Section1: {
        flex: 1,
        alignItems: 'flex-start'
    },
    contentSection2Row5Section1Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    contentSection2Row5Left: {
        flex: 0.5,
        alignItems: 'flex-start'
    },
    contentSection2Row5LeftText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    contentSection2Row5Right: {
        flex: 0.5,
        alignItems: 'flex-end'
    },
    contentSection2Row5RightText:{
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    footer: {
        flex:3,
        paddingTop: 40,
        justifyContent: 'flex-start'
    }, 
    footerListItem1: {
        flexDirection: 'row',
        paddingVertical: 15,
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderColor: "#564F47"
    },
    footerListItem1Left: {
        flex: 0.15,
        justifyContent:'flex-start',
        alignItems: 'flex-start',
        marginTop: -8
    },
    footerListItem1Right: {
        flex: 0.85
    },
    footerListItem1RightText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
    footerListItem2: {
        flexDirection: 'row',
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderColor: "#564F47"
    },
    footerListItem2Left: {
        flex: 0.15
    },
    footerListItem2Right: {
        flex: 0.85
    },
    footerListItem2RightText: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 20,
        color: '#FFFFFF'
    },
})

export default TripReceipt; 