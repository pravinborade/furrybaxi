import React, { useRef, useEffect, useState, memo } from 'react';
import { View, StyleSheet, Image, Text, Dimensions, TouchableOpacity } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Geolocation from 'react-native-geolocation-service';

import Images from '../../assets/Images/Images';
import { useSelector } from 'react-redux';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../utils/typography';
import { fSize } from '../../utils/Responsive';
import googleMapStyle from '../../utils/googleMapStyle';
import { find_dimesions } from '../../utils/helper';
import { GoogleCloudKey } from '../../config';
import MapViewDirections from 'react-native-maps-directions';
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const RideConfirming = (props) => {
    let mapRef = useRef();
    const [dynamicHeight, setDynamicHeight] = useState(height)
    const [origin, setOrigin] = useState()
    const [destination, setDestination] = useState()
    const [region, setRegion] = useState({
        latitude: 18.5011567,
        longitude: 73.9242136,
        latitudeDelta: LATITUDE_DELTA * 5,
        longitudeDelta: LONGITUDE_DELTA * 5,
    })
    const [wayPoints, setWayPoints] = useState([])

    const {
        currentPlace,
        destinationPlace,
    } = useSelector(state => state.map)

    const { pickupOtpResponse } = useSelector(state => state.riding)

    useEffect(() => {
        if (currentPlace && currentPlace !== null) {
            let data = {
                latitude: currentPlace?.lat,
                longitude: currentPlace?.long,
            }
            setOrigin(data)
        }

    }, [currentPlace])

    useEffect(() => {
        if (destinationPlace && destinationPlace !== null) {
            let data = {
                latitude: destinationPlace?.lat,
                longitude: destinationPlace?.long,
            }
            setDestination(data)
        }
    }, [destinationPlace])


    useEffect(() => {
        if (origin && destination) {
            setRegion({
                ...region,
                ...origin
            })
            setWayPoints([origin, destination])
            mapRef.current.fitToCoordinates([origin, destination], {
                edgePadding: styles.markerEdge,
                animated: true
            })
        }
    }, [origin, destination])


    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE}
                style={{ ...StyleSheet.absoluteFillObject, height: dynamicHeight }}
                ref={mapRef}
                initialRegion={region}
                onRegionChange={(e) => {
                    setRegion(e)
                }}               
                showsUserLocation={false}
                showsCompass={false}
                customMapStyle={googleMapStyle}
            >
                {
                    origin &&
                    <Marker coordinate={origin}>
                        <View style={styles.markerContainer}>
                            <View style={styles.markerDot} />
                        </View>
                    </Marker>
                }
                {
                    destination &&
                    <Marker coordinate={destination}>
                        <Image source={Images.marker} style={{ width: 26, height: 36 }} />
                    </Marker>
                }
                {/* {
                    origin && destination &&
                    <MapView.Polyline
                        coordinates={[...wayPoints]}
                        strokeWidth={3}
                        strokeColor="#6E7887"
                    />
                } */}
                <MapViewDirections
                    origin={origin}
                    destination={destination}
                    apikey={GoogleCloudKey}
                    strokeWidth={4}
                    strokeColor="#6E7887"
                    onReady={(result) => {
                        mapRef.current.fitToCoordinates(result.coordinates, {
                            edgePadding: styles.markerEdge,
                            animated: true
                        })
                    }}
                />
            </MapView>
            <View style={styles.menuOverLay}>
                <TouchableOpacity onPress={() => { props.navigation.goBack() }}>
                    <View style={styles.backImageContainer}>
                        <Image
                            source={Images.arrow_back_black}
                            style={styles.iconImages}
                        />
                    </View>
                </TouchableOpacity>
            </View>

            <View style={styles.actionContainer1} onLayout={(event) => setDynamicHeight(find_dimesions(height, event.nativeEvent.layout))}>
                <View style={styles.actionContainer1Section1}>
                    <View style={styles.actionContainer1Section1Row1}>
                        <Text style={styles.actionContainer1Section1Row1Text1}>
                            {"Ride Code"}
                        </Text>
                        <Text style={styles.actionContainer1Section1Row1Text2}>
                            {"Share with your Driver to Start your Ride"}
                        </Text>
                    </View>
                    <View style={styles.actionContainer1Section1Row2}>
                        <Text style={styles.actionContainer1Section1Row2Text}>
                            {pickupOtpResponse?.data?.otp?.toString()?.split('').join(' ')}
                        </Text>
                    </View>
                </View>
            </View>
        </View>

    )
}


const styles = StyleSheet.create({
    container: {
        //...StyleSheet.absoluteFillObject,
        flex: 1,
    },
    driverLocation: {
        height: 50,
        width: 50
    },
    menuOverLay: {
        position: 'absolute',
        top: 20,
        minHeight: hp(10),
        right: 0,
        left: 20,
    },
    iconImages: {
        height: 30,
        width: 15,
    },
    backImageContainer: {
        height: 44,
        width: 44,
        borderRadius: 12,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    markerContainer: {
        backgroundColor: "#F9D591",
        borderRadius: 12.5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    markerDot: {
        backgroundColor: "#253040",
        width: 14,
        height: 14,
        borderRadius: 7,
    },
    actionContainer1: {
        backgroundColor: "#FDFFFF",
        position: 'absolute',
        top: hp(70),
        bottom: 0,
        left: 0,
        right: 0,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
    },
    actionContainer1Section1: {
        height: hp(30),
        backgroundColor: "#17202D",
        paddingHorizontal: 20,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    actionContainer1Section1Row1: {
        height: hp(12),
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: 10,
    },
    actionContainer1Section1Row1Text1: {
        fontFamily: MEDIUM,
        fontSize: 18,
        lineHeight: 22,
        color: "#E1CDA9"
    },
    actionContainer1Section1Row1Text2: {
        fontFamily: REGULAR,
        fontSize: 14,
        lineHeight: 17,
        color: "#B59B7C"
    },
    buttonStyle: {
        backgroundColor: "#E2EAED",
        borderRadius: 15,
        height: 54,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 8
    },
    buttonTextStyle: {
        fontFamily: MEDIUM,
        fontSize: 12,
        lineHeight: 15,
        color: "#4E535A",
    },
    actionContainer1Section1Row2: {
        height: hp(15),
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#253040",
        borderRadius: 20
    },
    actionContainer1Section1Row2Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(34),
        lineHeight: 41,
        letterSpacing: 0.8,
        color: "#F9D591",
    },

    markerEdge: {
        top: 100,
        bottom: 100,
        left: 50,
        right: 50
    }

});

export default memo(RideConfirming);
