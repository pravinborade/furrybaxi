import { apiURL } from "../config";
import axios from "axios";

const header = {
    accept: 'application/json',
    'content-type': 'application/json',
  };

const authService = {
    get(url) {
        return axios.get(apiURL + url, {
          headers: header,
        })
        .then((response) => this.successResponse(response))
        .catch((error) => this.errorResponse(error));
      },

      post(url,payload) {
        return axios.post(apiURL + url, payload, {
          headers: header,
        })
        .then((response) => this.successResponse(response))
        .catch((error) => this.errorResponse(error));
      },

      put(url,payload) {
        return axios.put(apiURL + url, payload, {
          headers: header,
        })
        .then((response) => this.successResponse(response))
        .catch((error) => this.errorResponse(error));
      },
      
      delete(url,payload) {
        return axios.delete(apiURL + url, payload, {
          headers: header,
        })
        .then((response) => this.successResponse(response))
        .catch((error) => this.errorResponse(error));
      },

      successResponse(response) {
        return Promise.resolve(response);
      },
    
      errorResponse(error) {
        return Promise.reject(error);
      }
    
}
export default authService;

