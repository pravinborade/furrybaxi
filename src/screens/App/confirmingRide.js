import React, { useRef, useEffect, useState, createRef, useMemo, memo } from 'react';
import { View, StyleSheet, Image, Text, FlatList, ScrollView, Dimensions, TouchableOpacity } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import * as Progress from 'react-native-progress';

import Images from '../../assets/Images/Images';
import { useDispatch, useSelector } from 'react-redux';
import { GoogleCloudKey } from '../../config';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../utils/typography';
import { fSize } from '../../utils/Responsive';
import PrimaryButton from '../../component/PrimaryButton';
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScreenNames } from '../../utils/ScreenNames';
import Geolocation from 'react-native-geolocation-service';
import { navigate } from "../../root/navigation/rootNavigation";
import { cancelRide, getSocketData } from '../../redux/actions/ridingAction';
import googleMapStyle from '../../utils/googleMapStyle';
import { find_dimesions } from '../../utils/helper';
import MapViewDirections from 'react-native-maps-directions';
import { getCurrentLocation } from '../../redux/actions/homeAction';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const ConfirmingRide = (props) => {
    let mapRef = useRef(null);
    const {
        currentPlace,
        destinationPlace,
    } = useSelector(state => state.map)

    const {
        availableCabsResponse,
        selectedCabResponse,
        selectedCab,
        selectedCabLoading
    } = useSelector(state => state.riding)

    const {
        currentLocation,
    } = useSelector(state => state.home)

    const dispatch = useDispatch()

    const [dynamicHeight, setDynamicHeight] = useState(height)
    const [origin, setOrigin] = useState()
    const [destination, setDestination] = useState()
    const [isAleadyRideRequest, setAleadyRideRequest] = useState(false)
    const [region, setRegion] = useState({
        latitude: 18.5011567,
        longitude: 73.9242136,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    })

    useEffect(() => {
        dispatch(getCurrentLocation())
        // mapRef.current.fitToCoordinates([
        //     {
        //         latitude: 18.5011567,
        //         longitude: 73.9242136,
        //     }
        // ], {
        //     edgePadding: styles.markerEdge,
        //     animated: true
        // })
    }, [])

    useEffect(() => {
        if(currentLocation){
            mapRef.current.fitToCoordinates([
                {
                    latitude: 18.5011567,
                    longitude: 73.9242136,
                }
            ], {
                edgePadding: styles.markerEdge,
                animated: true
            })
        }
    }, [currentLocation])

    useEffect(() => {
        if (currentPlace && currentPlace !== null) {
            setOrigin({
                latitude: currentPlace?.lat,
                longitude: currentPlace?.long
            })
        }
        if (destinationPlace && destinationPlace !== null) {
            setDestination({
                latitude: destinationPlace?.lat,
                longitude: destinationPlace?.long
            })
        }
    }, [currentPlace, destinationPlace])

     useEffect(() => {
        if (origin && destination) {

            mapRef.current.fitToCoordinates([
                origin,
                destination
            ], {
                edgePadding: styles.markerEdge,
                animated: true
            })

            let data = {
                message: 603,
                location: [currentPlace?.lat, currentPlace?.long],
                id: selectedCab?.id
            }
            if(!isAleadyRideRequest){
                setAleadyRideRequest(true)
                console.log("########getting socket data")
                dispatch(getSocketData(data))
            }
           
        }
    }, [origin, destination])

    const requestDriver = () => {
        let data = {
            message: 603,
            location: [currentPlace?.lat, currentPlace?.long],
            id: selectedCab?.id
        }
        dispatch(getSocketData(data))
    }

    const onRideCancel = () => {
        let data = {
            id: selectedCab?.id
        }     
        dispatch(cancelRide(data))
    }
    
    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE}
                style={{ ...StyleSheet.absoluteFillObject, height: dynamicHeight }}
                ref={mapRef}
                initialRegion={region}
                showsUserLocation={false}
                customMapStyle={googleMapStyle}
                onRegionChange={(e) => {
                    setRegion(e)
                }}

            >   
                {
                    currentLocation
                        &&
                    <Marker
                        coordinate={currentLocation}
                    >
                        <Image source={Images.driverLocation} style={styles.driverLocation} />
                    </Marker>
                }
                {
                    origin &&
                    <Marker coordinate={origin}>
                        <View style={styles.markerContainer}>
                            <View style={styles.markerDot} />
                        </View>
                    </Marker>
                }
                {
                    destination &&
                    <Marker coordinate={destination} >
                        <Image source={Images.marker} style={{ width: 26, height: 36 }} />
                    </Marker>
                }
                
                {/* {
                    origin && destination &&
                    <MapView.Polyline
                        coordinates={[origin, destination]}
                        strokeWidth={3}
                        strokeColor="#6E7887"
                    />
                } */}

                 <MapViewDirections
                    origin={origin}
                    destination={destination}
                    apikey={GoogleCloudKey}
                    strokeWidth={4}
                    strokeColor="#6E7887"
                    onReady={(result) => {
                        mapRef.current.fitToCoordinates(result.coordinates, {
                            edgePadding: styles.markerEdge,
                            animated: true
                        })
                    }}
                />
           </MapView>
            <View style={styles.menuOverLay}>
                <TouchableOpacity onPress={() => { props.navigation.goBack() }}>
                    <View style={styles.backImageContainer}>
                        <Image
                            source={Images.arrow_back_black}
                            style={styles.iconImages}
                        />
                    </View>
                </TouchableOpacity>
            </View>
            <View
                style={styles.actionContainer1}
                onLayout={(event) => setDynamicHeight(find_dimesions(height, event.nativeEvent.layout))}
            >
                <View style={styles.actionUpcomingContainer1Section0}>
                    {
                        selectedCabLoading &&
                        <>
                            <View style={styles.actionUpcomingContainer1Section0Top}>
                                <Text style={styles.actionUpcomingContainer1Section0TopText1}>
                                    {"Looking for Cabs"}
                                </Text>
                                <View style={{ height: 1 }} />
                                <Text style={styles.actionUpcomingContainer1Section0TopText2}>
                                    {"Waiting drivers to accept"}
                                </Text>
                            </View>
                            <View style={{ height: 10 }} />
                            <View style={styles.actionUpcomingContainer1Section0Bottom}>
                                <Progress.Bar
                                    height={5}
                                    color={"#F9D591"}
                                    unfilledColor={"#E2EAED"}
                                    borderColor={"transparent"}
                                    style={{ flex: 1 }}
                                    indeterminate={true}
                                />
                            </View>
                        </>
                    }
                    {
                        selectedCabResponse !== null && selectedCabResponse.status === 606 && !selectedCabLoading
                        &&
                        <Text style={styles.actionUpcomingContainer1Section0TopTextError}>
                            {"Sorry, Near by drivers not accepting trips now, try again."}
                        </Text>
                    }

                </View>
                <View style={styles.innerContainer}>
                    <View style={styles.actionContainer1Section1}>
                        <View style={styles.actionContainer1SectionView}>
                            <View style={styles.actionContainer1Section1ViewLeft}>
                                <View style={styles.actionContainer1Section1ViewLeftImageContainer}>
                                    <Image
                                        source={Images.cab}
                                        style={styles.actionContainer1Section1ViewLeftImage}
                                    />
                                </View>
                                <View style={styles.actionContainer1Section1ViewRight}>
                                    <View style={styles.actionContainer1Section1ViewRightTop}>
                                        <Text style={styles.actionContainer1Section1ViewRightTopText1}>
                                            {selectedCab?.type}
                                        </Text>
                                        <Text style={styles.actionContainer1Section1ViewRightTopText2}>
                                            {selectedCab?.desc}
                                        </Text>
                                    </View>
                                    <View style={styles.actionContainer1Section1ViewRightBottom}>
                                        <TouchableOpacity style={styles.actionContainer1Section1ViewRightBottomLeft}>
                                            <Text style={styles.actionContainer1Section1ViewRightBottomLeftText}>
                                                {selectedCab?.Price}
                                            </Text>
                                        </TouchableOpacity>
                                        {/* <Text style={styles.actionContainer1Section1ViewRightBottomRightText}>
                                            {selectedCab?.data?.cab?.ride_type}
                                        </Text> */}
                                    </View>
                                </View>
                            </View>
                            <View style={styles.actionContainer1SectionViewBottom}>
                                <Text style={styles.actionContainer1SectionViewBottomLeftText}>
                                    {availableCabsResponse?.passengers}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.actionContainer1Section2}>
                        {
                            selectedCabResponse !== null && selectedCabResponse?.status === 606 && !selectedCabLoading
                                ?
                                <PrimaryButton
                                    disable={false}
                                    onPress={() => {
                                        requestDriver()
                                    }}
                                    title={"Retry"}
                                />
                                :
                                <TouchableOpacity
                                    onPress={() => {
                                        onRideCancel()
                                    }}
                                    style={styles.actionContainer2BottomButton}
                                >
                                    <Text style={styles.actionContainer2BottomButtonText}>
                                        {"Cancel Ride"}
                                    </Text>
                                </TouchableOpacity>
                        }
                    </View>
                </View>
            </View>
        </View>

    )
}


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
    },
    driverLocation: {
        height: 50,
        width: 50
    },
    menuOverLay: {
        position: 'absolute',
        top: 20,
        minHeight: hp(10),
        right: 0,
        left: 20,
    },
    iconImages: {
        height: 30,
        width: 15,
    },
    markerContainer: {
        backgroundColor: "#F9D591",
        borderRadius: 12.5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    markerDot: {
        backgroundColor: "#253040",
        width: 14,
        height: 14,
        borderRadius: 7,
    },
    backImageContainer: {
        height: 44,
        width: 44,
        borderRadius: 12,
        justifyContent: 'flex-start',
        alignItems: 'flex-start'
    },
    actionContainer1: {
        position: 'absolute',
        // top: hp(44),
        bottom: 0,
        left: 0,
        right: 0,
    },
    innerContainer: {
        backgroundColor: "#17202D",
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        borderBottomRightRadius: 0,
        borderBottomLeftRadius: 0,
    },
    actionContainer1Section1: {
        height: hp(27),
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        padding: 20
    },
    actionContainer1SectionView: {
        backgroundColor: '#C2CFD3',
        borderRadius: 20,
    },
    actionContainer1Section1ViewLeft: {
        height: hp(17),
        backgroundColor: '#E2EAED',
        borderRadius: 20,
        flexDirection: 'row',
        padding: 10,
    },
    actionContainer1Section1ViewLeftImageContainer: {
        flex: 0.2,
        borderRadius: 15
    },
    actionContainer1Section1ViewLeftImage: {
        flex: 1,
        borderRadius: 15,
        width: "100%"
    },
    actionContainer1Section1ViewRight: {
        flex: 0.8,
        paddingHorizontal: 10,
        paddingVertical: 5,
        justifyContent: 'space-between'
    },
    actionContainer1Section1ViewRightTop: {
        flexDirection: 'column'
    },
    actionContainer1Section1ViewRightTopText1: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#364458"
    },
    actionContainer1Section1ViewRightTopText2: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#6D7580"
    },
    actionContainer1Section1ViewRightBottom: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    actionContainer1Section1ViewRightBottomLeft: {
        backgroundColor: '#F1F7F9',
        borderRadius: 10,
        paddingHorizontal: 8,
        paddingVertical: 6,
        alignItems: 'center',
        justifyContent: 'center'
    },
    actionContainer1Section1ViewRightBottomLeftText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#364458"
    },
    actionContainer1Section1ViewRightBottomRightText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(10),
        lineHeight: 12,
        color: "#6D7580"
    },
    actionContainer1SectionViewBottom: {
        flexDirection: 'row',
        padding: 10,
        height: hp(8),
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    actionContainer1SectionViewBottomLeftText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#364458",
    },
    actionContainer1Section2: {
        //height: hp(15),
        padding: 20,
        justifyContent: 'flex-end'
    },
    actionContainer2BottomButton: {
        backgroundColor: "rgba(255, 115, 115, 0.3)",
        borderRadius: 15,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
    },
    actionContainer2BottomButtonText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#FB6B6B",
        opacity: 1
    },
    actionUpcomingContainer1Section0: {
        //height: hp(15),
        backgroundColor: '#FDFFFF',
        padding: 20,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
    },
    actionUpcomingContainer1Section0Top: {
        // flex: 1,
        // width: '100%'
    },
    actionUpcomingContainer1Section0TopText1: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#253040",
    },
    actionUpcomingContainer1Section0TopText2: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#6D7580",
    },

    actionUpcomingContainer1Section0TopTextError: {
        fontFamily: REGULAR,
        fontSize: fSize(16),
        lineHeight: 21,
        color: "red",
    },

    actionUpcomingContainer1Section0Bottom: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    actionUpcomingContainer1Section0Text0: {
        color: '#9D7E62',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        paddingHorizontal: 5

    },
    actionUpcomingContainer1Section0Text: {
        color: '#9D7E62',
        fontFamily: MEDIUM,
        fontSize: fSize(12)

    },

    markerEdge: {
        top: 100,
        bottom: 100,
        left: 40,
        right: 40
    }
});

export default memo(ConfirmingRide);
