
import React from 'react';
import { submit } from "redux-form";
import { useDispatch, useSelector } from 'react-redux';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions } from 'react-native'


import Header from '../../../component/Header';
import { fSize } from '../../../utils/Responsive';
import BackButton from '../../../component/BackButton';
import SkipButton from '../../../component/SkipButton';
import { MEDIUM, SEMI_BOLD } from '../../../utils/typography';
import CreatePetProfileForm from '../component/CreatePetProfileForm';
import { getCategory, getSize, createPetProfile, openDatePicker, onChangeCategory, openCategoryDropdown, openSizeDropdown, onChangeSize, updatePetProfile } from '../../../redux/actions/petProfileAction';
import { backListener, removeBackListener } from '../../../utils/backHandler';
import { SafeAreaView } from 'react-native-safe-area-context';
import CustomGradientButton from '../../../component/CustomGradientButton';
import commanStyle from '../../../utils/commanStyle';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'

const screenHeight = Dimensions.get('screen').height
const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}

const CreatePetProfile = (props) => {
    const dispatch = useDispatch();
    const { category, size, isDatePickerOpen, isSizeDropdownOpen, isCategoryDropdownOpen } = useSelector(state => state.petProfile)

    React.useEffect(() => {
        dispatch(getCategory())
        dispatch(getSize())

        backListener()
        return () => {
            removeBackListener()
        };
    }, [])


    const headerLeft = () => {
        return <BackButton onPress={() => { props.navigation.goBack() }} />
    }

    const headerRight = () => {
        return <SkipButton title={'Skip'} onPress={() => { }} />
    }

    const openDatePicker1 = data => {
        dispatch(openDatePicker(data))
    }

    const openCategoryDropdown1 = data => {
        dispatch(openCategoryDropdown(data))
    }

    const openSizeDropdown1 = data => {
        dispatch(openSizeDropdown(data))
    }

    const onCategoryFieldChange = category => {
        dispatch(onChangeCategory(category))
    }

    const onSizeFieldChange = size => {
        dispatch(onChangeSize(size))
    }

    const submitCreatePetProfileForm = (data) => {
        var petProfile = {
            ...data,
            category: data.category.pcid,
            size: data.size.psid,
            breed: data.breed.name,
        }
        if (petProfile.pid) {
            dispatch(updatePetProfile(petProfile))

        } else {
            dispatch(createPetProfile(petProfile))
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }} >
                <TouchableOpacity style={{ height: getHeight(100) }} activeOpacity={1}
                    onPress={() => {
                        openCategoryDropdown1(false)
                        openSizeDropdown1(false)
                    }} >
                    <View style={{ height: getHeight(10) }}>
                        <Header
                            leftView={headerLeft}
                            // rightView={headerRight}
                        />
                    </View>

                    <View style={styles.content}>
                        <View>

                            <Text style={styles.contentText1}>{"Create your Pet Profile"}</Text>
                            <Text style={styles.contentText1}>{"Fill the following"}</Text>
                            <CreatePetProfileForm
                                onSubmit={submitCreatePetProfileForm}
                                category={category}
                                size={size}
                                isDatePickerOpen={isDatePickerOpen}
                                isSizeDropdownOpen={isSizeDropdownOpen}
                                isCategoryDropdownOpen={isCategoryDropdownOpen}
                                openDatePicker={openDatePicker1}
                                openSizeDropdown={openSizeDropdown1}
                                openCategoryDropdown={openCategoryDropdown1}
                                onSizeFieldChange={onSizeFieldChange}
                                onCategoryFieldChange={onCategoryFieldChange}
                            />
                        </View>

                        <View style={styles.footer}>
                            <CustomGradientButton
                                title={'Next'}
                                onPress={() => { dispatch(submit('petProfileForm')) }}
                                disable={false}
                                gradientColor={['#253040', '#17202D']}
                                styleContainer={commanStyle.buttonContainer}
                                textStyle={commanStyle.darkButtonText}
                            />
                        </View>

                    </View>




                </TouchableOpacity>

            </KeyboardAwareScrollView>

        </SafeAreaView >
    )
}


const styles = StyleSheet.create({
    container: {
        // flex: 1,
        backgroundColor: '#17202D',
        padding: 20,
        height: '100%'
    },
    content: {
        height: getHeight(75),
        // height: '100%'
        justifyContent: 'space-between',
    },
    contentText1: {
        paddingBottom: 10,
        color: '#E1CDA9',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20)
    },
    contentText1: {
        paddingBottom: 10,
        color: '#A58B6C',
        fontFamily: MEDIUM,
        fontSize: fSize(14)
    },

    footer: {
        //flex: 4
        //height: '0%',
        //padding: 20,
        //justifyContent: 'flex-end',
        // backgroundColor: 'red'
    },
});

export default CreatePetProfile;
