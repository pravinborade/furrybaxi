
import authService from "../../service/authService";
import appService from "../../service/appService";
import {ScreenNames} from '../../utils/ScreenNames'
import { navigate } from "../../root/navigation/rootNavigation";

export function setPhoneNumber(phoneNumber) {
    return {
        type: 'SET_PHONE_NUMBER',
        phoneNumber
    };
}

export function setCountry(country) {
    return {
        type: 'SET_COUNTRY',
        country
    };
}


export function setEditPhoneNumber(editPhoneNumber) {
    return {
        type: 'SET_EDIT_PHONE_NUMBER',
        editPhoneNumber
    };
}

export function setEditCountry(editCountry) {
    return {
        type: 'SET_EDIT_COUNTRY',
        editCountry
    };
}

export function setResponse(response) {
    return {
        type: 'SET_PHONE_NUMBER_RESPONSE',
        response
    };
}

export function setEditResponse(editResponse) {
    return {
        type: 'SET_EDIT_PHONE_NUMBER_RESPONSE',
        editResponse
    };
}

export function setError(isError) {
    return {
        type: 'SET_PHONE_NUMBER_ERROR',
        isError
    };
}
export function setEditPhoneError(isEditPhoneError) {
    return {
        type: 'SET_EDIT_PHONE_NUMBER_ERROR',
        isEditPhoneError
    };
}

export function resetState() {
    return {
        type: 'RESET_PHONE_NUMBER_STATE',
    };
}

export function setLoading(isLoading) {
    return {
        type: 'PHONE_NUMBER_LOADING',
        isLoading
    };
}

export  function  submitPhoneNumber (data) {
    return async (dispatch) => {
        dispatch(setLoading(true))
        return authService.post('fb/v1/auth/signup',data)
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{
            dispatch(setLoading(false))
            dispatch(setResponse(response))
            navigate(ScreenNames.OTP)
        })
        .catch((error) =>{
            console.log(error)
            dispatch(setLoading(false))  
            dispatch(setError(true))
        });
    };
}

export  function  updatePhoneNumber (data) {  
    return async (dispatch) => {
        dispatch(setLoading(true))
        return appService.put('fb/v1/accounts/user',data)
        .then((response) => {         
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{         
            dispatch(setLoading(false))
            dispatch(setEditResponse(response))
            navigate(ScreenNames.EDIT_OTP)
        })
        .catch((error) =>{          
            console.log(error)
            dispatch(setLoading(false))  
            dispatch(setEditPhoneError(true))
        });
    };
}