const initialState = {
    userResponse: null,
    token: null,
    isRegistered: false,
  
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'SET_USER_RESPONSE':
            return {
                ...state,
                userResponse: action.response,
            };

        case 'RESET_USER_RESPONSE':
            return {
                ...state,
                userResponse: null,
            };

        case 'SET_TOKEN':
            return {
                ...state,
                token: action.token,
            };

        case 'SET_IS_REGISTERED':
            return {
                ...state,
                isRegistered: action.flag,
            };

        default:
            return state;
    }
};

export default authReducer;
