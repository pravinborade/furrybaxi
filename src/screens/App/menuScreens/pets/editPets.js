
import React, { useEffect, useState } from 'react';
import { submit } from "redux-form";
import { connect, useDispatch, useSelector } from 'react-redux';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, SafeAreaView } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';


import Header from '../../../../component/Header';
import { fSize } from '../../../../utils/Responsive';
import BackButton from '../../../../component/BackButton';
import SkipButton from '../../../../component/SkipButton';
import DarkButton from '../../../../component/DarkButton';
import { MEDIUM, SEMI_BOLD } from '../../../../utils/typography';
import EditPetForm from './component/editPetForm';

import { backListener, removeBackListener } from '../../../../utils/backHandler';
import commanStyle from '../../../../utils/commanStyle';
import CustomGradientButton from '../../../../component/CustomGradientButton';
import { createPetEditProfile, getEditCategory, getEditSize, onChangeEditCategory, openCategoryEditDropdown, openDateEditPicker, openSizeEditDropdown, updatePetEditProfile } from '../../../../redux/actions/petEditAction';
import { ScreenNames } from '../../../../utils/ScreenNames';

let EditPets = (props) => {
    const {formData } = props;
    const dispatch = useDispatch();
    const [isBtnDisabled, setBtnDisabled] = useState(false)
    const { categoryEdit, sizeEdit, isDateEditPickerOpen, isSizeEditDropdownOpen, isCategoryEditDropdownOpen, petProfile } = useSelector(state => state.petEdit)

    useEffect(() => {
        dispatch(getEditCategory())
        dispatch(getEditSize())

        backListener()
        return () => {
            removeBackListener()
        };
    }, [])

    const enableBtn = (data) => {
        if (data && data.name && data.category && data.breed && data.size && data.dob &&data.about) {
            setBtnDisabled(true)
        } else {
            setBtnDisabled(false)
        }
    }
    useEffect(() => {
        enableBtn(formData['editPetForm']?.values)
    }, [formData])


    const headerLeft = () => {
        return <BackButton onPress={() => { props.navigation.goBack() }} />
    }

    const headerRight = () => {
        return <SkipButton title={'Skip'} onPress={() => {

            petProfile && petProfile.name ? props.navigation.navigate(ScreenNames.EDIT_PET_PICTURE) : props.navigation.goBack()

        }
        } />
    }

    const openDatePicker1 = data => {
        dispatch(openDateEditPicker(data))
    }

    const openCategoryDropdown1 = data => {
        dispatch(openCategoryEditDropdown(data))
    }

    const openSizeDropdown1 = data => {
        dispatch(openSizeEditDropdown(data))
    }

    const onCategoryFieldChange = category => {
        dispatch(onChangeEditCategory(category))
    }

    const onSizeFieldChange = size => {
        dispatch(onChangeEditCategory(size))
    }

    const submitCreatePetProfileForm = (data) => {
        var petProfile = {
            ...data,
            category: data.category.pcid,
            size: data.size.psid,
            breed: data.breed.name,
        }
        if (petProfile.pid) {
            dispatch(updatePetEditProfile(petProfile))
        } else {
            dispatch(createPetEditProfile(petProfile))
        }
    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#17202D' }}>
            <TouchableOpacity style={{ flex: 1 }} activeOpacity={1}
                onPress={() => {
                    openCategoryDropdown1(false)
                    openSizeDropdown1(false)
                }} >
                <ScrollView style={styles.container}>
                    <View style={styles.header}>
                        <Header
                            leftView={headerLeft}
                           // rightView={headerRight}
                        />
                    </View>
                    <View style={styles.content}>
                        <View style={styles.contentSection1}>
                            <Text style={styles.contentSection1Text1}>{
                              petProfile && petProfile.name ? "Edit Pet Profile" :"Create your Pet Profile"}</Text>
                        </View>
                        <View style={styles.contentSection2}>
                            <Text style={styles.contentSection2Text1}>{"Fill the following"}</Text>
                        </View>
                    </View>
                    <View style={styles.content2}>
                        <EditPetForm
                            onSubmit={submitCreatePetProfileForm}
                            category={categoryEdit}
                            size={sizeEdit}
                            isDatePickerOpen={isDateEditPickerOpen}
                            isSizeDropdownOpen={isSizeEditDropdownOpen}
                            isCategoryDropdownOpen={isCategoryEditDropdownOpen}
                            openDatePicker={openDatePicker1}
                            openSizeDropdown={openSizeDropdown1}
                            openCategoryDropdown={openCategoryDropdown1}
                            onSizeFieldChange={onSizeFieldChange}
                            onCategoryFieldChange={onCategoryFieldChange}
                        />
                    </View>
                </ScrollView>
                <View style={styles.footer}>

                
                    <CustomGradientButton
                        title={'Next'}
                        onPress={() => { dispatch(submit('editPetForm')) }}
                        disable={false}
                        gradientColor={ isBtnDisabled ?['#DAC5A2', '#A98C6F']:['#253040', '#17202D']}
                        styleContainer={commanStyle.buttonContainer}
                        textStyle={commanStyle.primaryButtonText}
                    />
                 
                </View>
            </TouchableOpacity>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        //flex: 1,

    },
    header: {
        height: hp(20),
        padding: 20,
    },
    content: {
        //flex:1,
        padding: 20,
        // height: hp(15)
    },
    contentSection1: {
        height: hp(6)
    },
    contentSection2: {
        height: hp(5)
    },
    contentSection1Text1: {
        paddingBottom: 10,
        color: '#E1CDA9',
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20)
    },
    contentSection2Text1: {
        paddingBottom: 10,
        color: '#A58B6C',
        fontFamily: MEDIUM,
        fontSize: fSize(14)
    },
    content2: {
        paddingHorizontal: 20
    },
    footer: {
        // height: hp(11),
        padding: 20,
        justifyContent: 'flex-start'
    },
});
EditPets = connect(
    state => ({
        formData: state.form
    })
)(EditPets)
export default EditPets;
