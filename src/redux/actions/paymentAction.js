import appService from "../../service/appService";

export function setStripeResponse(response) {
    return {
        type: 'SET_STRIPE_RESPONSE',
        response
    };
}

export function setPaymentSecretResponse(response) {
    return {
        type: 'SET_PAYMENT_SECRET_RESPONSE',
        response
    };
}


export  function  getStripeInformation () {
    return async (dispatch) => {
        return appService.get('fb/v1/keys/stripe')
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{
            dispatch(setStripeResponse(response))
        })
        .catch((error) =>{
            dispatch(setStripeResponse(null))
            console.log(error)
        });
    };
}

export  function  getPaymentSecret() {
    return async (dispatch) => {
        return appService.get('fb/v1/payment/future/secret')
        .then((response) => {
            if (response.status !== 200) {
                throw Error(response);
            }
            return response.data;
        })
        .then((response) =>{
            dispatch(setPaymentSecretResponse(response.data))
        })
        .catch((error) =>{
            dispatch(setPaymentSecretResponse(null))
            console.log(error)
        });
    };
}

export const savePaymentDetails = async(data) => {
    return appService.post('fb/v1/accounts/user/payment', data)
    .then((response) => {
        if (response.status !== 200) {
            throw Error(response);
        }
        return response.data
    })
    .then((response) => {
        return Promise.resolve(response)
    })
    .catch((error) => {
        return Promise.reject(error)
    });
}

export const savePaymentMethod = async(data) => {
    return appService.post('fb/v1/trips/payment_method', data)
    .then((response) => {
        if (response.status !== 200) {
            throw Error(response);
        }
        return response.data
    })
    .then((response) => {
        return Promise.resolve(response)
    })
    .catch((error) => {
        return Promise.reject(error)
    });
}
