import React, { useRef, useEffect, useState, memo } from 'react';
import { View, StyleSheet, SafeAreaView, Text, FlatList, RefreshControl, TouchableOpacity, Image, Dimensions, TextInput } from "react-native"
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { useDispatch, useSelector } from 'react-redux';
import Images from '../../../assets/Images/Images';
import BackButton from '../../../component/BackButton';
import CustomGradientButton from '../../../component/CustomGradientButton';
import Header from '../../../component/Header';
import { getCurrentLocation } from '../../../redux/actions/homeAction';
import { getLocationDetails, getSavedLocationDetails, getSavedSearchedLocationDetail, getSearchPlaces, setDestinationPlace, setSearchResult } from '../../../redux/actions/mapAction';
import { setGoogleInputFoucused, setSavedLocationName } from '../../../redux/actions/ridingAction';
import { navigate } from '../../../root/navigation/rootNavigation';
import commanStyle from '../../../utils/commanStyle';
import googleMapStyle from '../../../utils/googleMapStyle';
import { find_dimesions } from '../../../utils/helper';
import { fSize } from '../../../utils/Responsive';
import { ScreenNames } from '../../../utils/ScreenNames';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../utils/typography';
import CurrentLocation from '../component/CurrentLocationButtion';
import MyLocationButton from '../component/MyLocationButton';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


const MapSearch = (props) => {
     const dispatch = useDispatch()
     let mapRef = useRef(null);

     const { searchResult } = useSelector(state => state.map)
     const { saveLocationDetail, serviceLocationsResponse, shouldStartRideFromMap } = useSelector(state => state.riding)
     const { isLocationAccess, currentLocation } = useSelector(state => state.home)
     const [searchKeyword, setSearchKeyword] = useState('')
     const [isSearch, setSearch] = useState(false)
     const [shortAddress, setShortAddress] = useState(saveLocationDetail?.short_name)

     const [region, setRegion] = useState({
          latitude: 51.509865,
          longitude: -0.118092,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
     })

     useEffect(() => {
          dispatch(setSavedLocationName(''))
     }, [])
     useEffect(() => {
          if (saveLocationDetail) {
               setShortAddress(saveLocationDetail?.short_name)           
               if (isSearch) {                  
                    setSearch(false)
                    setRegion({
                         latitude: saveLocationDetail.lat,
                         longitude: saveLocationDetail.long,
                         latitudeDelta: LATITUDE_DELTA,
                         longitudeDelta: LONGITUDE_DELTA,
                    })
                    mapRef.current.animateToRegion({
                         latitude: saveLocationDetail.lat,
                         longitude: saveLocationDetail.long,
                         latitudeDelta: LATITUDE_DELTA,
                         longitudeDelta: LONGITUDE_DELTA
                    }, 1000);
               }
          }
     }, [saveLocationDetail])

     useEffect(() => {
          if (searchKeyword && searchKeyword != '') {
               onSearchLocation(searchKeyword)
               setShortAddress(searchKeyword)
          } else {
               dispatch(setSearchResult([]))
          }
     }, [searchKeyword])


     useEffect(() => {
          if (currentLocation && currentLocation !== null) {
               setRegion({
                    ...currentLocation,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA
               })
               mapRef.current.animateToRegion({
                    ...currentLocation,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA
               }, 1000);
          }
     }, [currentLocation])

     const onSearchLocation = async (text) => {
          var str = text;
          var prefix = '';
          if (serviceLocationsResponse?.postal_town) {
               prefix = `${serviceLocationsResponse?.postal_town}, `
          }

          if (str.indexOf(prefix) === 0) {
               setSearchKeyword('')
          } else {
               if (prefix.indexOf(str) >= 0) {
                    setSearchKeyword(str);
               } else {
                    setSearchKeyword(str);
               }
          }

          if (str) {
               await dispatch(getSearchPlaces((prefix + str), serviceLocationsResponse?.country_short))
          } else {
               await dispatch(setSearchResult([]))

          }
     };

     const headerLeft = () => {
          return <BackButton onPress={() => { props.navigation.goBack() }} />
     }

     const confirm = () => {
          if (shouldStartRideFromMap) {
               dispatch(setDestinationPlace(saveLocationDetail))
               navigate(ScreenNames.TYPE_OF_RIDES)
          } else {
               if (saveLocationDetail) {
                    navigate(ScreenNames.SAVE_LOCATION)
               } else {
                    var latLng = currentLocation.latitude + "," + currentLocation.longitude
                    dispatch(getSavedLocationDetails(latLng))
                    navigate(ScreenNames.SAVE_LOCATION)
               }
          }
     }

     const selectSearchLocation = (item) => {
          setSearch(true)
          setSearchKeyword('')
          dispatch(getSavedSearchedLocationDetail(item))
     }

     const renderItem = ({ item }) => {
          return (
               <TouchableOpacity onPress={() => selectSearchLocation(item)} style={styles.placeCard}>
                    <View style={styles.placeCardContainer}>
                         <View style={styles.placeInfo}>
                              <Text style={styles.placeInfoText1}>{item?.name}</Text>
                              <Text style={styles.placeInfoText2}>{`(${item?.address})`}</Text>

                         </View>
                    </View>
               </TouchableOpacity>
          );
     };

     return (
          <SafeAreaView style={styles.container}>
               <View style={{ paddingHorizontal: 20 }}>
                    <Header cmHeight={'10%'} leftView={headerLeft} />
               </View>

               <View style={{ flex: 1 }}>
                    <MapView
                         provider={PROVIDER_GOOGLE}
                         style={{ ...StyleSheet.absoluteFillObject }}
                         ref={mapRef}
                         initialRegion={region}                      
                         customMapStyle={googleMapStyle}
                         showsUserLocation={false}
                         showsCompass={false}
                         onRegionChange={(e) => {
                              setRegion(e)
                         }}
                         onRegionChangeComplete={(e) => {
                              let latLng = e?.latitude + ',' + e?.longitude;
                              dispatch(getSavedLocationDetails(latLng))
                         }}
                    >
                         {
                              region &&
                              <Marker
                                   coordinate={region}

                              >
                                   <Image source={Images.select_location} style={styles.selectLocation} />
                              </Marker>
                         }
                    </MapView>
                    <View style={[styles.topContainer,]}>
                         <View style={styles.topSubContainer} >   
                              <View style={{ paddingVertical: 10 }}>
                                   <TextInput
                                        placeholder="Enter name"                                       
                                        style={styles.searchInput}
                                        placeholderTextColor={"#ADADAE"}
                                        value={shortAddress}
                                        caretHidden={false}
                                        onChangeText={(text) => setSearchKeyword(text)}                                
                                   />

                              </View>
                         </View>

                         <View style={{ height: searchKeyword != '' ? height : 0 }}>

                              <FlatList
                                   scrollEnabled
                                   data={searchKeyword != '' ? searchResult : []}
                                   renderItem={renderItem}
                                   showsVerticalScrollIndicator={false}
                                   keyExtractor={(item, index) => index.toString()}

                              />
                         </View>
                    </View>

                    {searchKeyword == '' && <View style={styles.buttonContainer}>
                         <MyLocationButton marginTop={-60} />
                         <CustomGradientButton
                              title={'Confirm'}
                              onPress={() => confirm()}
                              disable={false}
                              gradientColor={['#253040', '#17202D']}
                              styleContainer={commanStyle.buttonContainer}
                              textStyle={commanStyle.primaryButtonText}
                         />
                    </View>
                    }



               </View>

          </SafeAreaView>
     )
}
const styles = StyleSheet.create({
     container: {
          flex: 1,
          backgroundColor: '#17202D',
          //padding: 20
     },

     placeCard: {

     },
     placeCardContainer: {
          flexDirection: 'row',
          padding: 10
     },
     placeIcon: {
          height: 20,
          width: 20,
          margin: 15

     },
     placeInfo: {       
          width: '100%',
          borderBottomColor: '#F2F2F2',
          borderBottomWidth: 1,
          paddingBottom: 10
     },
     placeInfoText1: {
          fontFamily: MEDIUM,
          fontSize: fSize(14),
          lineHeight: 17,
          color: "#6D7580"
     },
     placeInfoText2: {
          fontFamily: REGULAR,
          fontSize: fSize(12),
          lineHeight: 15,
          color: "#A3A8AF"
     },
     buttonContainer: {
          position: 'absolute',
          bottom: 20,
          left: 20,
          right: 20,
          opacity: 1
     },
     searchInput: {
          backgroundColor: '#FFFFFF',
          height: 50,
          borderRadius: 15,
          paddingVertical: 5,
          paddingHorizontal: 15,
          fontSize: fSize(14),
          fontFamily: MEDIUM,
          color: "#364458",
          flex: 1
          //textAlign:'right'
     },
     searchInputError: {
          backgroundColor: '#FFFFFF',
          height: 50,
          borderRadius: 15,
          borderColor: 'red',
          borderWidth: 1,
          paddingVertical: 5,
          paddingHorizontal: 15,
          fontSize: fSize(14),
          fontFamily: MEDIUM,
          color: "#364458"
     },

     topContainer: {       
          backgroundColor: '#fff',
          position: 'absolute',
          top: -15,
          left: 0,
          right: 0,


     },
     topSubContainer: {
          padding: 20,
          backgroundColor: '#17202D',
          borderBottomLeftRadius: 20,
          borderBottomRightRadius: 20
     },
     selectLocation: {
        
     }


})
export default memo(MapSearch);