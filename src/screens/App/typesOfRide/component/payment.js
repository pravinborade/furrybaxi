import React, { useState } from 'react';
import { StyleSheet, View, Text, Modal, Dimensions, TouchableOpacity, Image } from 'react-native';
import { CardField } from '@stripe/stripe-react-native';


import { fSize } from '../../../../utils/Responsive';
import { MEDIUM, REGULAR, SEMI_BOLD } from '../../../../utils/typography';

import Images from '../../../../assets/Images/Images';
import CustomGradientButton from "../../../../component/CustomGradientButton";
import AmountModal from './amountModal';
import { useSelector } from 'react-redux';

const screenHeight = Dimensions.get('screen').height
const screenWidth = Dimensions.get('screen').width

const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}
const getWidth = (wd) => {
    return screenWidth * (wd / 100)
}


const Payment = (props) => {
    const { loader, onSubmitCard, onClose, 
        amountModal, setAmountModal, createPayment
    } = props;
    const {       
        paymentSecretResponse
    } = useSelector(state => state.payment)
    const [cardDetails, setCardDetails] = useState()

    return (
        <Modal
            transparent={true}
            visible={true}
        >
            <View style={styles.modalContainer}>
                {
                    <View style={styles.modalContent}>
                        <View style={styles.modalHeader}>
                            <View style={{ flex: .20 }}>
                                <TouchableOpacity style={styles.backButton} onPress={() => onClose()}>
                                    <Image source={Images.arrow_back} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: .80 }}>
                                <Text style={styles.headerTitle}>
                                    {"Choose Payment Type"}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.content1}>
                            <Text style={styles.contentSection1Text1}>
                                {"Enter your Card Details"}
                            </Text>
                            <Text style={styles.contentSection1Text2}>
                                {"is simply dummy text of the printing"}
                            </Text>
                            <View style={styles.contentSection2}>
                                <CardField
                                    postalCodeEnabled={false}
                                    placeholder={{
                                        number: 'Enter Card Number',
                                        expiration: "Expiry Date",
                                        cvc: "Enter CVV"
                                    }}
                                    cardStyle={{
                                        backgroundColor: '#253040',
                                        textColor: '#F6F3ED',
                                        placeholderColor: "#D1DBEA",
                                        borderColor: '#596475',
                                        borderWidth: 1
                                    }}
                                    style={{
                                        width: '100%',
                                        height: 50,
                                        justifyContent: 'space-between'
                                    }}
                                    onCardChange={(cardDetails) => {
                                        setCardDetails(cardDetails)
                                    }}
                                />
                            </View>
                            <View style={styles.footer1}>
                                <CustomGradientButton
                                    title={"Save Card"}
                                    onPress={() => (!cardDetails?.complete || loader) ? {} : onSubmitCard(cardDetails)}
                                    disable={false}
                                    gradientColor={(!cardDetails?.complete || loader) ? ['#253040', '#17202D'] : ['#DAC5A2', '#A98C6F']}
                                    styleContainer={commanStyle.buttonContainer}
                                    textStyle={(!cardDetails?.complete || loader) ? commanStyle.darkButtonText : commanStyle.primaryButtonText}
                                />
                            </View>
                        </View>
                    </View>
                }
            </View>
            {
            amountModal && 
            <AmountModal 
                disabled={loader} 
                onClose={()=>{setAmountModal(false)}}  
                onOk={createPayment}
                paymentSecretResponse={paymentSecretResponse}
            />

        }
        </Modal>
    )
}


const styles = StyleSheet.create({
    modalContainer: {
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)',
        zIndex: 1
    },
    modalContent: {
        height: getHeight(100),
        width: getWidth(100),
        paddingVertical: 40,
        backgroundColor: '#17202D',
        borderRadius: 20,
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    modalHeader: {
        flexDirection: 'row',
        height: getHeight(8),
        width: getWidth(100),
        paddingHorizontal: 20,
        alignItems: 'center'
    },
    backButton: {
        width: 50,
        height: 50,
        borderRadius: 12,
        backgroundColor: "#253040",
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerTitle: {
        fontFamily: MEDIUM,
        fontSize: fSize(18),
        lineHeight: 22,
        color: "#FDFDFD"
    },
    content: {
        height: getHeight(78),
        paddingTop: getHeight(20),
        alignItems: 'center'
    },

    content1: {
        height: getHeight(78),
        width: getWidth(90),
        paddingTop: 20,
    },

    contentText1: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(18),
        lineHeight: 24,
        color: "#E1CDA9",
        marginVertical: 5,
    },

    contentText2: {
        fontFamily: REGULAR,
        fontSize: fSize(12),
        lineHeight: 17,
        color: "#A58B6C",
        marginVertical: 5
    },

    buttonStyle: {
        width: getWidth(85),
        backgroundColor: '#E2EAED',
        borderRadius: 15,
        height: 54,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 15,
        marginVertical: 5
    },
    buttonTextStyle: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#253040',
        textAlign: 'center'
    },
    modalFooter: {
        height: getHeight(8),
        width: getWidth(100),
        paddingHorizontal: 20
    },
    footerButton: {
        borderWidth: 2,
        borderColor: '#253040',
        borderRadius: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        width: '100%',
        backgroundColor: '#253040'
    },
    footerButtonText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#5E718A'
    },
    footerButtonActive: {
        borderRadius: 15,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: 48,
        width: '100%',
        backgroundColor: "#A98C6F"
    },
    footerButtonTextActive: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#FFFFFF'
    },
    contentSection1Text1: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: '#E1CDA9',
        paddingVertical: 2
    },

    contentSection1Text2: {
        fontFamily: REGULAR,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#A58B6C',
        paddingVertical: 2
    },
    contentSection2: {
        marginTop: 10,
        height: getHeight(60),
        padding: 20,
        backgroundColor: '#253040',
        bordeTopWidth: 5,
        borderColor: 'transparent',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20
    },
    contentSection2Text1: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: '#E1CDA9',
        marginBottom: 5
    },
    drowpdownContainer: {
        backgroundColor: '#253040',
        borderBottomWidth: 1,
        borderLeftColor: '#253040',
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderTopWidth: 0,
        borderBottomColor: '#596475',
        paddingLeft: 2,
        paddingRight: 10,
    },
    drowpdownPlaceholder: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        color: '#D1DBEA',
        marginLeft: 0
    },
    dropdownLable: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        color: '#F6F3ED',
        zIndex: 2
    },
    dropdownPickerContainer: {
        backgroundColor: '#EDF2F3',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20
    },
    drowpdownPickerContainer: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        color: '#253040'
    },
    arrowDown: {
        height: 9,
        width: 16,
        tintColor: '#D1DBEA'
    },
    footer1: {
        padding: 20,
        justifyContent: 'flex-end',
        backgroundColor: '#253040',
    }
})


export default Payment;