import React, { useState } from "react";
import { Image, StyleSheet, TouchableOpacity, View, Text, Dimensions, TextInput, ScrollView, SafeAreaView, Linking, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../../assets/Images/Images";
import { backNavigation, navigate } from "../../../../root/navigation/rootNavigation";
import { fSize } from "../../../../utils/Responsive";
import { MEDIUM, REGULAR, SEMI_BOLD } from "../../../../utils/typography";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";


const screenHeight = Dimensions.get('screen').height
const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}
export const REPORT_ITEMS = [{
    id:1,
    name: 'Call us',
    key: 'REPORT_CALL_US'
},{
    id:2,
    name: 'I was involved in an accident',
    key: 'REPORT_ACCIDENT'
},{
    id:3,
    name: 'My driver’s behavior made me feel unsafe',
    key: 'REPORT_DRIVER_BEHAVIOR'
},
{
    id:4,
    name: 'My driver drove dangerously',
    key: 'REPORT_DRIVE_DANGEROUS'
},
{
    id:5,
    name: 'My driver’s vehicle was different',
    key: 'REPORT_VEHICLE_DIFFERENT'
},
{
    id:6,
    name: 'My driver was not wearing a face mask',
    key: 'REPORT_FACE_MASK'
},
{
    id:7,
    name: 'My driver was demanding extra cash',
    key: 'REPORT_EXTRA_CASH'
},
// {
//     id:8,
//     name: 'Pet cage',
//     key: 'REPORT_PET_CAGE'
// },
{
    id:9,
    name: 'Payment related',
    key: 'REPORT_PAYMENT_RELATED'
}]

const TripReport = (props) => {
    const dispatch = useDispatch()
    const [tripID, setTripID] = useState(props?.route?.params?.id)
    const {configurations} = useSelector(state=>state.riding) 
    const onItemPress = (route) => {
        if(route !== 'REPORT_CALL_US'){
            navigate(route,{id:tripID})
        }else{
            if(configurations?.support_phone){
                Linking.openURL(`tel:${configurations?.support_phone}`)
            }else{
                Alert.alert("Not available!")
            }
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <View style={styles.headerLeft}>
                    <TouchableOpacity style={styles.headerLeftButton} onPress={()=>{backNavigation()}}>
                        <Image source={Images.arrow_back} style={styles.headerButtonIcon}/>
                    </TouchableOpacity>
                </View>
                <View style={styles.headerRight}>
                    <Text style={styles.headerRightText}>
                        {"Report Safety Issue"}
                    </Text>
                </View>
            </View>
            <KeyboardAwareScrollView 
                contentContainerStyle={styles.content} 
                enableOnAndroid={true} 
                scrollEnabled={true}
            >
            <View style={styles.content}>
                <ScrollView contentInsetAdjustmentBehavior="always">
                    <View style={styles.contentBody}>
                        <View style={styles.contentBodySection2}>
                            <Text style={styles.contentBodySection2Text}>
                                {"What do you need help with?"}
                            </Text>
                        </View>
                        <View style={styles.contentBodyList}>
                            {
                                REPORT_ITEMS?.map(item=>{
                                    return(
                                        <TouchableOpacity 
                                            key={item.key}
                                            style={styles.contentBodyListItem}
                                            onPress={()=>{
                                                onItemPress(item.key)
                                            }} 
                                        >
                                            <Text style={styles.contentBodyListItemText}>
                                                {item.name}
                                            </Text>
                                        </TouchableOpacity>
                                    )
                                })
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
            </KeyboardAwareScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: getHeight(100),
        backgroundColor: '#17202D',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        height: getHeight(10),
        paddingHorizontal: 20,
        justifyContent: 'center'
    },
    headerLeft: {
        flex: .2,
    },
    headerRight: {
        flex: .9,
    },
    headerRightText: {
        fontFamily: REGULAR,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#F4F4F4"
    },
    headerLeftButton: {
        padding: 10,
        width: 44,
        height: 44,
        backgroundColor: '#253040',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center'
    },
    headerButtonIcon: {
        width: 8,
        height: 16,
    },
    content: {
        height: getHeight(92),
    },
    contentBody: {
        paddingTop: 15,
        paddingHorizontal: 20,
        justifyContent: 'flex-end'
    },
    contentBodySection2: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    contentBodySection2Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#E1CDA9",
    },
    contentBodyList: {
        paddingVertical: 10,
    },
    contentBodyListItem: {
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#253040',
        borderRadius: 15,
        marginVertical: 8
    },
    contentBodyListItemText: {
        fontFamily: MEDIUM,
        fontSize: fSize(12),
        lineHeight: 15,
        color: "#F9D591",
    }
})

export default TripReport;