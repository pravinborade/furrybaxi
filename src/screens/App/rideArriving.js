import React, { useRef, useEffect, useState, createRef, memo } from 'react';
import { View, StyleSheet, Image, Text, Dimensions, TouchableOpacity, Linking, Alert } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Modalize } from 'react-native-modalize';
import { gestureHandlerRootHOC } from 'react-native-gesture-handler';

import Images from '../../assets/Images/Images';
import { useDispatch, useSelector } from 'react-redux';
import Container2 from './rideArrivingComponent/Container2';
import { cancelRide, onDriverContact } from '../../redux/actions/ridingAction';
import googleMapStyle from '../../utils/googleMapStyle';
import { getCurrentLocation } from '../../redux/actions/homeAction';
import { MEDIUM } from '../../utils/typography';
import { fSize } from '../../utils/Responsive';
import { find_dimesions } from '../../utils/helper';
import Container1 from './rideArrivingComponent/Container1';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const RideArriving = (props) => {
    const mapRef = useRef(null);
    const modalizeRef = useRef(null);
    const {
        currentPlace,
        destinationPlace,
    } = useSelector(state => state.map)

    const {
        selectedCab,
        selectedCabResponse,
        durationResponse
    } = useSelector(state => state.riding)

    const {
        userResponse
    } = useSelector(state => state.auth)

    const {
        currentLocation,
    } = useSelector(state => state.home)

    const dispatch = useDispatch()
    const [dynamicHeight, setDynamicHeight] = useState(height)
    const [origin, setOrigin] = useState()

    const [destination, setDestination] = useState()
    const [region, setRegion] = useState({
        latitude: 18.5011567,
        longitude: 73.9242136,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
    })

    const [showDetails, setShowDetails] = useState(false);
    const [wayPoints, setWayPoints] = useState([])

    useEffect(() => {
        dispatch(getCurrentLocation())
        return () => {

        };
    }, [])

    useEffect(() => {
        if (selectedCabResponse && selectedCabResponse != null) {
            debugger
            let tmp = []
            setOrigin({
                latitude: selectedCabResponse?.data?.trip?.source[0],
                longitude: selectedCabResponse?.data?.trip?.source[1]
            })
            setDestination({
                latitude: selectedCabResponse?.data?.trip?.destination[0],
                longitude: selectedCabResponse?.data?.trip?.destination[1]
            })
            selectedCabResponse?.data?.trip?.waypoints?.map((way, ind) => {
                tmp.push({
                    latitude: way[0],
                    longitude: way[1]
                })
            })
            setWayPoints(tmp)
        }
    }, [selectedCabResponse])

    useEffect(() => {
        if (durationResponse && durationResponse !== null) {
            debugger
            let tmp = []
            setOrigin({
                latitude: durationResponse?.data?.location?.[0],
                longitude: durationResponse?.data?.location?.[1]
            })
            durationResponse?.data?.waypoints?.map((way, ind) => {
                tmp.push({
                    latitude: way[0],
                    longitude: way[1]
                })
            })
            setWayPoints(tmp)
            mapRef.current.fitToCoordinates(tmp, {
                edgePadding: styles.markerEdge,
                animated: true
            })
        }

    }, [durationResponse])


    const onRideCancel = () => {
        let data = {
            id: selectedCabResponse?.data?.id
        }      
        dispatch(cancelRide(data))
    }

    const onClickContact = async () => {
        try {
            const resp = await onDriverContact()
            if(resp?.virtual_number){
                Linking.openURL(`tel:${resp?.virtual_number}`)
            }else{
                Alert.alert("Not available!")
            }
        } catch (error) {
            console.log(error)
            Alert.alert("Not available!")
        }
    }

    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE}
                style={{ ...StyleSheet.absoluteFillObject, height: dynamicHeight }}
                ref={mapRef}
                initialRegion={region}
                onRegionChange={(e) => {
                    setRegion(e)
                }}
                showsUserLocation={false}
                showsCompass={false}
                customMapStyle={googleMapStyle}
            >
                {
                    currentLocation &&
                    <Marker
                        coordinate={currentLocation}
                    >
                        <Image source={Images.driverLocation} style={styles.driverLocation} />
                    </Marker>
                }
                {
                    origin &&
                    <Marker coordinate={origin}>
                        <Image
                            source={Images.car_icon}
                            style={{ width: 26, height: 28 }}
                            resizeMode="contain"
                        />
                    </Marker>
                }
                {
                    destination &&
                    <Marker coordinate={destination} >
                        <Image source={Images.marker} style={{ width: 26, height: 36 }} />
                    </Marker>
                }
                {
                    origin && destination &&
                    <MapView.Polyline
                        coordinates={[...wayPoints]}
                        strokeWidth={3}
                        strokeColor="#6E7887"

                    />
                }
            </MapView>
            <View style={styles.menuOverLay}>
                <TouchableOpacity style={styles.backImageContainer} onPress={() => props.navigation.goBack()}>
                    <Image
                        source={Images.arrow_back_black}
                        style={styles.iconImages}
                    />
                </TouchableOpacity>
            </View>
            <Modalize
                alwaysOpen={hp(38)}
                modalStyle={{
                    backgroundColor: "#17202D",
                    padding: 10,
                }}

                handleStyle={{
                    display: 'none'
                }}
                rootStyle={{
                    zIndex: 1
                }}
          

            >
                <Container2
                    onClickContact={onClickContact}
                    currentPlace={currentPlace}
                    destinationPlace={destinationPlace}
                    selectedCabResponse={selectedCabResponse}
                    userResponse={userResponse}
                    durationResponse={durationResponse}
                    onLayout={(event) => setDynamicHeight(find_dimesions(height, event.nativeEvent.layout))}
                />
            </Modalize>
            <View style={styles.floatButton}>
                <TouchableOpacity style={styles.floatButtonBottom}
                    onPress={() => {
                        onRideCancel()
                    }}
                >
                    <Text style={styles.floatButtonText}>
                        {"Cancel Ride"}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
    },
    driverLocation: {
        height: 50,
        width: 50
    },
    menuOverLay: {
        position: 'absolute',
        top: 20,
        minHeight: hp(10),
        right: 0,
        left: 20,
    },
    iconImages: {
        height: 30,
        width: 15,
    },
    backImageContainer: {
        height: 30,
        width: 15,
    },
    markerContainer: {
        backgroundColor: "#F9D591",
        borderRadius: 12.5,
        height: 20,
        width: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    markerDot: {
        backgroundColor: "#253040",
        width: 14,
        height: 14,
        borderRadius: 7,
    },
    markerEdge: {
        top: 100,
        bottom: 100,
        left: 30,
        right: 30
    },
    floatButton: {
        position: 'absolute',
        height: 80,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 2,
        backgroundColor: "#17202D",
    },
    floatButtonBottom: {
        backgroundColor: "rgba(255, 115, 115, 0.3)",
        borderRadius: 15,
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 3,
        marginHorizontal: 10
    },
    floatButtonText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#FB6B6B",
        opacity: 1
    }


});

export default memo(RideArriving);
