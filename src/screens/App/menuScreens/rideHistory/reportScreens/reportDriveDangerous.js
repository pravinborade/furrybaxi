import React, { useState } from "react";
import { StyleSheet, View, Text, Dimensions, TextInput, ScrollView, SafeAreaView } from "react-native";
import { useDispatch } from "react-redux";
import { navigate } from "../../../../../root/navigation/rootNavigation";
import { fSize } from "../../../../../utils/Responsive";
import { MEDIUM, SEMI_BOLD } from "../../../../../utils/typography";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import CustomSwitch from "./components/customSwitch";
import ReportHeader from "./components/reportHeader";
import ReportFooter from "./components/reportFooter";
import { driveDangerous } from "../../../../../redux/actions/rideAction";
import { ScreenNames } from "../../../../../utils/ScreenNames";


const screenHeight = Dimensions.get('screen').height
const getHeight = (ht) => {
    return screenHeight * (ht / 100)
}

const ReportDriveDangerous = (props) => {
    const dispatch = useDispatch()
    const [tripID, setTripID] = useState(props?.route?.params?.id)
    const [comment, setComment] = useState('')
    const [isError, setIsError] = useState(false)
    const [isSleepy, setIsSleepy] = useState(false)
    const [isDistractedPhone, setIsDistractedPhone] = useState(false)
    const [isDistractedOther, setIsDistractedOther] = useState(false)
    const [isSpeedSlow, setIsSpeedSlow] = useState(false)
    const [isUnsafeDrive, setIsUnsafeDrive] = useState(false)

    const onSubmit = async () => {
        if (!comment) {
            setIsError(true)
            return
        }
        try {
            let formData = {
                trip_id: tripID,
                driver_look_sleepy: isSleepy,
                driver_distract_phone: isDistractedPhone,
                driver_distract_outher_then_phone: isDistractedOther,
                driver_too_speed_or_slow: isSpeedSlow,
                driver_ignore_traffic_rules: isUnsafeDrive,
                more_details: comment
            }
            let resp = await driveDangerous(formData)
            navigate(ScreenNames.TRIP_DETAILS,{id:tripID})
        } catch (error) {
            console.log(error)
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <ReportHeader />
            </View>
            <KeyboardAwareScrollView 
                contentContainerStyle={styles.content} 
                enableOnAndroid={true} 
                scrollEnabled={true}
            >
            <View style={styles.content}>
                <ScrollView contentInsetAdjustmentBehavior="always">
                    <View style={styles.contentBody}>
                        <View style={styles.contentBodySectionTitle}>
                            <Text style={styles.contentBodySectionTitleText}>
                                {"My driver drove dangerously"}
                            </Text>
                        </View>
                        <View style={styles.contentBodyDescription}>
                            <Text style={styles.contentBodyDescriptionText} >
                                { 
                                   `Furry Baxi us committed to the safey of everyone using its ride hailing services.
                                    If you feel your river was unsafe or disctracted at any time during this trip, please let us know.`
                                }
                            </Text>
                        </View>
                        <View style={styles.contentBodyRow}>
                            <View style={styles.contentBodyRowLeft}>
                                <Text style={styles.contentBodyRowLeftText}>
                                    {"Did your driver look sleepy?"}
                                </Text>
                            </View>
                            <View style={styles.contentBodyRowRight}>
                                <CustomSwitch
                                    value={isSleepy}
                                    onChange={value=>setIsSleepy(value)}
                                />
                            </View>        
                        </View>
                        <View style={styles.contentBodyRow}>
                            <View style={styles.contentBodyRowLeft}>
                                <Text style={styles.contentBodyRowLeftText}>
                                    {`Was your driver distracted by their phone?`}
                                </Text>
                            </View>
                            <View style={styles.contentBodyRowRight}>
                                <CustomSwitch
                                    value={isDistractedPhone}
                                    onChange={value=>setIsDistractedPhone(value)}
                                />
                            </View>        
                        </View>
                        <View style={styles.contentBodyRow}>
                            <View style={styles.contentBodyRowLeft}>
                                <Text style={styles.contentBodyRowLeftText}>
                                    {`Was your driver distracted by something other than their phone?`}
                                </Text>
                            </View>
                            <View style={styles.contentBodyRowRight}>
                                <CustomSwitch
                                    value={isDistractedOther}
                                    onChange={value=>setIsDistractedOther(value)}
                                />
                            </View>        
                        </View>
                        <View style={styles.contentBodyRow}>
                            <View style={styles.contentBodyRowLeft}>
                                <Text style={styles.contentBodyRowLeftText}>
                                    {`Was your driver speeding, or disrupting the flow of traffic by driving too slowly?`}
                                </Text>
                            </View>
                            <View style={styles.contentBodyRowRight}>
                                <CustomSwitch
                                    value={isSpeedSlow}
                                    onChange={value=>setIsSpeedSlow(value)}
                                />
                            </View>        
                        </View>
                        <View style={styles.contentBodyRow}>
                            <View style={styles.contentBodyRowLeft}>
                                <Text style={styles.contentBodyRowLeftText}>
                                    {`Did your driver perform unsafe trafficmanoeuvres, or ignore road signage/markings?`}
                                </Text>
                            </View>
                            <View style={styles.contentBodyRowRight}>
                                <CustomSwitch
                                    value={isUnsafeDrive}
                                    onChange={value=>setIsUnsafeDrive(value)}
                                />
                            </View>        
                        </View>
                        <View style={styles.contentBodyFormItem}>
                            <Text style={styles.contentBodyFormItemLabel}>
                                {"Please tell us what happened below in as much detail as possible:"}
                            </Text>
                            <TextInput
                                style={[
                                    styles.contentBodyFormItemInput,
                                    isError && styles.error,
                                    {height: 120}
                                ]}
                                placeholderTextColor={"#ADADAE"}
                                placeholder={"If yes, please describe the injuries"}
                                multiline
                                numberOfLines={20}
                                value={comment}
                                onChangeText={(text)=>{
                                    setComment(text);
                                    if(isError){
                                        setIsError(false)
                                    }
                                }}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
            </KeyboardAwareScrollView>
            <View style={styles.footer}>
                <ReportFooter comment={comment} onSubmit={onSubmit} />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: getHeight(100),
        backgroundColor: '#17202D',
    },
    header: {
        flexDirection: 'row',
        alignItems: 'center',
        height: getHeight(Platform.OS === 'ios' ? 5 : 15 ),
        paddingHorizontal: 20,
        justifyContent: 'center'
    },
    content: {
        height: getHeight(Platform.OS === 'ios' ? 80 : 70 ),
    },
    contentBody: {
        paddingTop: 10,
        paddingHorizontal: 20,
        justifyContent: 'flex-end'
    },
    contentBodySection1Text: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(20),
        lineHeight: 24,
        color: "#F9D591"
    },
    contentBodySectionTitle: {
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    contentBodySectionTitleText: {
        fontFamily: SEMI_BOLD,
        fontSize: fSize(16),
        lineHeight: 20,
        color: "#E1CDA9",
    },
    contentBodyDescription: {
        paddingVertical: 10
    },
    contentBodyDescriptionText:{
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 22,
        color: "#FFFFFF",
        // textAlign: 'center'
    },
    contentBodyFormItem: {
        marginTop: 10,
        marginBottom: 15
    },
    contentBodyFormItemLabel: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9",
    },
    contentBodyFormItemInput: {
        marginVertical: 5,
        height:50,
        borderRadius: 15,
        backgroundColor: "#E2EAED",
        paddingHorizontal: 10,
        color:"#ADADAE"
    },
    contentBodyRow: {
        flexDirection: 'row',
        marginVertical: 15
    },
    contentBodyRowLeft: {
        flex: 0.9,
    },
    contentBodyRowLeftText: {
        fontFamily: MEDIUM,
        fontSize: fSize(14),
        lineHeight: 17,
        color: "#E1CDA9",
    },
    contentBodyRowRight: {
        flex:0.3,
        alignItems: 'flex-end'
    },
    error: {
        borderWidth: 2,
        borderColor: 'red'
    },
    footer: {
        height: getHeight(Platform.OS === 'ios' ? 5 : 5 ),
        paddingHorizontal: 20,
        justifyContent: 'center'
    }
})

export default ReportDriveDangerous;